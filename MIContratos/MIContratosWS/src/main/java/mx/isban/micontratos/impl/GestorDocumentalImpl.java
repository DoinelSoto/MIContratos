package mx.isban.micontratos.impl;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanModificacionEstadosDocumentosRequest;
import mx.isban.micontratos.gestordocumental.ejb.BOGestorDocumental;
import mx.isban.micontratos.interfaces.GestorDocumentalService;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosFault;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * @author Daniel Hernández Soto
 * Clase con los servicios que harán el llamado
 * a los métodos que consumirán las transacciones: 
 * MI17, MI20, MI38
 */
@WebService(serviceName="GestorDocumentalService")
public class GestorDocumentalImpl extends SpringBeanAutowiringSupport implements GestorDocumentalService{

	/**
	 * Log
	 */
	private static final Logger LOG = Logger.getLogger(GestorDocumentalImpl.class.getName());
	
	/**
	 * BO
	 */
	@Autowired
	private BOGestorDocumental boGestorDocumental;
	
	/**
	 * Método encargado de la inicialización 
	 */
	@PostConstruct
	@WebMethod(exclude=true)
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	/**
	 * Método para settear boGestorDocumental
	 * @param boGestorDocumental
	 */
	@WebMethod(exclude=true)
	public void setBOGestorDocumental(BOGestorDocumental boGestorDocumental) {
		this.boGestorDocumental = boGestorDocumental;
	}
	
	/**
	 * Método que permitirá exponer como webService a la transacción MI17 : Consulta tabla auxiliar
	 * @param request
	 * @return BeanConsultaTablaAuxiliarGestorResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanConsultaTablaAuxiliarGestorResultado listarDocumentosContrato(@WebParam(name = "consultaTablaAuxiliarGestorBean", mode = WebParam.Mode.IN) BeanConsultaTablaAuxiliarGestorRequest request) throws MIContratosException {
		LOG.info("========= TRX MI17 =========");
		try {
			return boGestorDocumental.listarDocumentosContrato(request, null);
		}catch(BusinessException e){
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Service que consumirá a la transacción MI20 : Modificación de estados de documentos
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@Override
	public ResponseGenerico modificarEstatusDocumento(@WebParam(name = "modificacionEstadoDocumentosBean", mode = WebParam.Mode.IN) BeanModificacionEstadosDocumentosRequest request) throws MIContratosException{
		LOG.info("========= TRX MI20 =========");
		try {
			return boGestorDocumental.modificarEstatusDocumento(request, null);
		}catch(BusinessException e){
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Método que permitirá exponer como webService a la transacción MI38 : Consulta del indicador de obligatoriedad
	 * @param request
	 * @return BeanIndicadorObligatoriedadResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanIndicadorObligatoriedadResultado consultarVencimientoContrato(@WebParam(name = "indicadorObligatoriedadBean", mode = WebParam.Mode.IN) BeanIndicadorObligatoriedadRequest request) throws MIContratosException{
		//Instancia del bean de salida
		BeanIndicadorObligatoriedadResultado response = new BeanIndicadorObligatoriedadResultado();
		try {
			LOG.info("========= TRX MI38 =========");
			response = boGestorDocumental.consultarVencimientoContrato(request, null);
		}catch(BusinessException e){
			LOG.info("",e);
			throw new MIContratosException(e.getMessage(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
		return response;
	}
}