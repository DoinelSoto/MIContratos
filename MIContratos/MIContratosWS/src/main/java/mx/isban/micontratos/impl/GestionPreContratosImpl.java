package mx.isban.micontratos.impl;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanBajaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPreContratoRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPrecontratoResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanModificacionTablaAuxiliarRequest;
import mx.isban.micontratos.gestionprecontratos.ejb.BOGestionPreContratos;
import mx.isban.micontratos.interfaces.GestionPreContratosService;
import mx.isban.micontratos.validator.MIContratosException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * @author Daniel Hernández Soto
 * Clase con los servicios que harán el llamado a los
 * métodos que consumen las transacciones: MI10, MI11
 * MI12, MI13, MI14, M15
 */
@WebService(serviceName="GestionPreContratosService")
public class GestionPreContratosImpl extends SpringBeanAutowiringSupport implements GestionPreContratosService{
	
	/**
	 * Log
	 */
	private static final Logger LOG = Logger.getLogger(GestionPreContratosImpl.class.getName());
	
	/**
	 * BO
	 */
	@Autowired
	private BOGestionPreContratos boGestionPreContratos;
	
	/**
	 * Método encargado de la inicialización 
	 */
	@PostConstruct
	@WebMethod(exclude=true)
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	/**
	 * Método para settear el boGestionPreContratos
	 * @param boGestionPreContratos
	 */
	@WebMethod(exclude=true)
	public void setBOGestionContratos(BOGestionPreContratos boGestionPreContratos) {
		this.boGestionPreContratos = boGestionPreContratos;
	}
		
	/**
	 * WebService que ejecutará el consumo de la transacción MI10 : Alta en tabla auxiliar
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico altaDatosAuxiliar(@WebParam(name = "altaTablaAuxiliarBean", mode = WebParam.Mode.IN) BeanAltaTablaAuxiliarRequest request) throws MIContratosException {
		LOG.info("========= TRX MI10 :  =========");
		try {
			return boGestionPreContratos.altaDatosAuxiliar(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * WebService que ejecutará el consumo de la transacción MI11 : Alta de precontrato global
	 * @param request
	 * @return BeanAltaPreContratoGlobalResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanAltaPreContratoGlobalResultado altaFolioPreContrato(@WebParam(name = "altaPreContratoGlobalBean", mode = WebParam.Mode.IN) BeanAltaPreContratoGlobalRequest 	request) throws MIContratosException {
		LOG.info("========= TRX MI11 :  =========");
		try {
			return boGestionPreContratos.altaFolioPreContrato(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * WebService que ejecutará el consumo de la transacción MI12 : Modificación de tabla auxiliar
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico modificacionDatosAuxiliar(@WebParam(name = "modificacionTablaAuxiliarBean", mode = WebParam.Mode.IN) BeanModificacionTablaAuxiliarRequest request) throws MIContratosException {
		LOG.info("========= TRX MI12 :  =========");
		try {
			return boGestionPreContratos.modificacionDatosAuxiliar(request, null);			
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * WebService que ejecutará el consumo de la transacción MI13 : Baja de tabla auxiliar
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@Override
	public ResponseGenerico bajaDatosAuxiliar(@WebParam(name = "bajaTablaAuxiliarBean", mode = WebParam.Mode.IN) BeanBajaTablaAuxiliarRequest request) throws MIContratosException {
		try {
			LOG.info("========= TRX MI13 :  =========");
			return boGestionPreContratos.bajaDatosAuxiliar(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	
	/**
	 * WebService que ejecutará el consumo de la transacción MI14 : Consulta de lista de precontrato
	 * @param request
	 * @return BeanListaPrecontratoResultado
	 * @throws MIContratosException
	 */
	@Override
	 public BeanListaPrecontratoResultado consultaPreContratos(@WebParam(name = "listaPreContratoRequest", mode = WebParam.Mode.IN) BeanListaPreContratoRequest request) throws MIContratosException {		 
		 LOG.info("========= TRX MI14 :  =========");
		 try {
			 return boGestionPreContratos.consultaPreContratos(request, null);			
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	 }
	
	
	/**
	 * WebService que ejecutará el consumo de la transacción MI15 : Consulta tabla auxiliar
	 * @param request
	 * @return BeanConsultaTablaAuxiliarResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanConsultaTablaAuxiliarResultado detallePreContratos(@WebParam(name = "consultaTablaAuxiliarBean", mode = WebParam.Mode.IN) BeanConsultaTablaAuxiliarRequest request) throws MIContratosException {
		LOG.info("========= TRX MI15 :  =========");
		try {			
			return boGestionPreContratos.detallePreContratos(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
}