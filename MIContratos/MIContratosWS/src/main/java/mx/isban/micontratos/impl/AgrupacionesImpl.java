package mx.isban.micontratos.impl;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.agrupaciones.ejb.BOConsultaAgrupaciones;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanBajaContratosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorResultado;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosResultado;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.interfaces.AgrupacionesService;
import mx.isban.micontratos.validator.MIContratosException;

/**
 * @author Daniel Hernández Soto
 * Clase con los servicios que harán el llamado 
 * a los métodos que consumen las transacciones: MI32,
 * MI33, MI34, MI35, MI36
 */
@WebService(serviceName="AgrupacionesService")
public class AgrupacionesImpl extends SpringBeanAutowiringSupport implements AgrupacionesService{
	
	/**  LOG.infoeo de la clase. */
	private static final Logger LOG = Logger.getLogger(AgrupacionesImpl.class.getName());
		
	/**
	 * Instancia del BOConsultaAgrupaciones
	 */
	@Autowired
	private BOConsultaAgrupaciones boConsultaAgrupaciones;
	/**
	 * Método encargado de la inicialización 
	 */	
	@PostConstruct
	@WebMethod(exclude=true)
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	/**
	 * Método para settear el boConsultaAgrupaciones
	 * @param boConsultaAgrupaciones
	 */
	@WebMethod(exclude=true)
	public void setBOConsultaAgrupaciones(BOConsultaAgrupaciones boConsultaAgrupaciones) {
		this.boConsultaAgrupaciones = boConsultaAgrupaciones;
	}

	/**
	 * WebService que consumirá la transacción MI32 : Consulta de lista de contrato agrupador
	 * @param request
	 * @return BeanListaContratoAgrupadorResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanListaContratoAgrupadorResultado listaContratosAgrupadores(@WebParam(name = "listaContratosAgrupadorBean", mode = WebParam.Mode.IN) BeanListaContratoAgrupadorRequest request) throws MIContratosException{
		LOG.info("========= TRX MI32 =========");
		try {
			return boConsultaAgrupaciones.listaContratosAgrupadores(request, null);
		} catch(BusinessException e){
			LOG.info("", e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * WebService que consumirá la transacción MI33 : Alta de contrato agrupador
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico altaContratoAgrupador(@WebParam(name = "altaContratoAgrupadorBean", mode = WebParam.Mode.IN) BeanAltaContratoAgrupadorRequest request) throws MIContratosException {
		LOG.info("========= TRX MI33 =========");
		try {
			return boConsultaAgrupaciones.altaContratoAgrupador(request, null);
		} catch(BusinessException e){
			LOG.info("", e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * WebService que consumirá la transacción MI34 : consulta de la lista de contratos hijos
	 * @param request
	 * @return BeanListaContratosHijosResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanListaContratosHijosResultado listarContratosAgrupados(@WebParam(name = "listaContratosHijosBean", mode = WebParam.Mode.IN) BeanListaContratosHijosRequest request) throws MIContratosException {
		LOG.info("========= TRX MI34 =========");
		try {
			return boConsultaAgrupaciones.listarContratosAgrupados(request, null);
		} catch(BusinessException e){
			LOG.info("", e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * WebService que consumirá la transacción MI35 : Alta contratos hijos
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override		
	public ResponseGenerico altaContratoAgrupado(@WebParam(name = "altaContratosHijosBean", mode = WebParam.Mode.IN) BeanAltaContratosHijosRequest request) throws MIContratosException{
		LOG.info("========= TRX MI35 =========");
		try {
			return boConsultaAgrupaciones.altaContratoAgrupado(request, null);		
		}catch(BusinessException e){
			LOG.info("", e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * WebService que consumirá la transacción MI36 : Baja de contratos hijos agrupados
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico bajaContratoAgrupado(@WebParam(name = "bajaContratosBean", mode = WebParam.Mode.IN) BeanBajaContratosRequest request) throws MIContratosException{
		LOG.info("========= TRX MI36 =========");
		try {
			return boConsultaAgrupaciones.bajaContratoAgrupado(request, null);		
		} catch(BusinessException e){
			LOG.info("", e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
}
