package mx.isban.micontratos.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchResult;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIRequest;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIResultado;
import mx.isban.micontratos.validator.MIContratosException;
/**
 * @author Daniel Hernández Soto
 *
 */
@WebService(name="ConsultaTablaParametrosService")
public interface ConsultaTablaParametrosService {


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI04 : Consulta de catalogos de estatus operativo
	 * @param request 
	 * @return BeanCatalogoEstatusOperativoResultado
	 */
	@WebMethod
	BeanCatalogoEstatusOperativoResultado catalogoEstatusOperativos(@WebParam(name = "catalogoEstatusOperativoBean", mode = WebParam.Mode.IN) BeanCatalogoEstatusOperativoRequest request) throws MIContratosException;

	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI05 : Consulta de catalogos de SubEstatus
	 * @param request
	 * @return BeanCatalogoSubEstatusResultado
	 * @throws MIContratosException 
	 * @throws mx.isban.micontratos.validator.MIContratosException 
	 */
	@WebMethod
	BeanCatalogoSubEstatusResultado catalogoSubEstatus(@WebParam(name = "catalogoSubEstatusBean", mode = WebParam.Mode.IN) BeanCatalogoSubEstatusRequest request) throws MIContratosException;

	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI06 : Consulta de lista de tipos de servicio
	 * @param request
	 * @return BeanListaTiposServicioResultado
	 */
	@WebMethod
	BeanListaTiposServicioResultado tipoServicio(@WebParam(name = "listaTiposServicioBean", mode = WebParam.Mode.IN) BeanListaTiposServicioRequest request) throws MIContratosException;

	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI07 : Consulta de árbol de tipos de servicio
	 * @param request
	 * @return BeanArbolTiposServicioResultado
	 */
	@WebMethod
	BeanArbolTiposServicioResultado arbolTipoServicio(@WebParam(name = "arbolTiposServicioBean", mode = WebParam.Mode.IN) BeanArbolTiposServicioRequest request) throws MIContratosException;

	/**
	 * Web Service que permitirá exponer como webService a la transacción MI39 : Consulta de Branch
	 * @param request
	 * @return BeanConsultarBranchResponse
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanConsultarBranchResult consultaBranch(@WebParam(name = "consultaCatalogosBranchBean", mode = WebParam.Mode.IN)  BeanConsultarBranchRequest request) throws MIContratosException;	
	
	/**
	 * Web Service que permitirá exponer como webService el método tipoNecesidadAFI : Tipo de Necesidad AFI
	 * @param request
	 * @return BeanTipoNecesidadAFIResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanTipoNecesidadAFIResultado tipoNecesidadAFI(@WebParam(name = "tipoCatalogosNecesidadAFIBean", mode = WebParam.Mode.IN)  BeanTipoNecesidadAFIRequest request) throws MIContratosException;
	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI08 : Consulta de estados de documentos
	 * @param request
	 * @return BeanConsultaEstadoDocumentosResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanConsultaEstadoDocumentosResultado consultaEstadoDocumentos(@WebParam(name = "consultaEstadoDocumentosBean", mode = WebParam.Mode.IN) BeanConsultaEstadoDocumentosRequest request) throws MIContratosException;
	
	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI09 : Catálogos estructurales
	 * @param request
	 * @return BeanCatalogosEstructuralesResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanCatalogosEstructuralesResultado consultaCatalogosEstructurales(@WebParam(name = "catalogosEstructuralesBean", mode = WebParam.Mode.IN) BeanCatalogosEstructuralesRequest request) throws MIContratosException;
	
}
