package mx.isban.micontratos.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanModificacionEstadosDocumentosRequest;
import mx.isban.micontratos.validator.MIContratosException;
/**
 * @author Daniel Hernández Soto
 *
 */
@WebService(name="GestorDocumentalService")
public interface GestorDocumentalService {
	
	/**
	 * Método que permitirá exponer como webService a la transacción MI17 : Consulta tabla auxiliar
	 * @param request
	 * @return BeanConsultaTablaAuxiliarGestorResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanConsultaTablaAuxiliarGestorResultado listarDocumentosContrato(@WebParam(name = "consultaTablaAuxiliarGestorBean", mode = WebParam.Mode.IN) BeanConsultaTablaAuxiliarGestorRequest request) throws MIContratosException;


	/**
	 * Service que consumirá a la transacción 20 : Modificación de estados de documentos
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico modificarEstatusDocumento(@WebParam(name = "modificacionEstadoDocumentosBean", mode = WebParam.Mode.IN) BeanModificacionEstadosDocumentosRequest request) throws MIContratosException;

	
	/**
	 * Método que permitirá exponer como webService a la transacción MI38 : Consulta del indicador de obligatoriedad
	 * @param request
	 * @return BeanIndicadorObligatoriedadResultado
	 */
	@WebMethod
	BeanIndicadorObligatoriedadResultado consultarVencimientoContrato(@WebParam(name = "indicadorObligatoriedadBean", mode = WebParam.Mode.IN) BeanIndicadorObligatoriedadRequest request) throws MIContratosException;

}
