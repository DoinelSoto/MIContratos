package mx.isban.micontratos.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanBajaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPreContratoRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPrecontratoResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanModificacionTablaAuxiliarRequest;
import mx.isban.micontratos.validator.MIContratosException;
/**
 * @author Daniel Hernández Soto
 *
 */
@WebService(name="GestionPreContratosService")
public interface GestionPreContratosService {

	
	/**
	 * WebService que ejecutará el consumo de la transacción MI10 : Alta en tabla auxiliar
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@WebMethod
	ResponseGenerico altaDatosAuxiliar(@WebParam(name = "altaTablaAuxiliarBean", mode = WebParam.Mode.IN) BeanAltaTablaAuxiliarRequest     request) throws MIContratosException;


	/**
	 * WebService que ejecutará el consumo de la transacción MI11 : Alta de precontrato global
	 * @param request
	 * @return BeanAltaPreContratoGlobalResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanAltaPreContratoGlobalResultado altaFolioPreContrato(@WebParam(name = "altaPreContratoGlobalBean", mode = WebParam.Mode.IN) BeanAltaPreContratoGlobalRequest 	request) throws MIContratosException;


	/**
	 * WebService que ejecutará el consumo de la transacción MI12 : Modificación de tabla auxiliar
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico modificacionDatosAuxiliar(@WebParam(name = "modificacionTablaAuxiliarBean", mode = WebParam.Mode.IN) BeanModificacionTablaAuxiliarRequest request) throws MIContratosException;


	/**
	 * WebService que ejecutará el consumo de la transacción MI13 : Baja de tabla auxiliar
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico bajaDatosAuxiliar(@WebParam(name = "bajaTablaAuxiliarBean", mode = WebParam.Mode.IN) BeanBajaTablaAuxiliarRequest request) throws MIContratosException;

	
	/**
	 * WebService que ejecutará el consumo de la transacción MI14 : Lista de baja de contrato
	 * @param request
	 * @return BeanListaPrecontratoResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	 BeanListaPrecontratoResultado consultaPreContratos(@WebParam(name = "listaPreContratoRequest", mode = WebParam.Mode.IN) BeanListaPreContratoRequest request) throws MIContratosException;

	
	/**
	 * WebService que ejecutará el consumo de la transacción MI15 : Consulta tabla auxiliar
	 * @param request
	 * @return BeanConsultaTablaAuxiliarResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanConsultaTablaAuxiliarResultado detallePreContratos(@WebParam(name = "consultaTablaAuxiliarBean", mode = WebParam.Mode.IN) BeanConsultaTablaAuxiliarRequest request) throws MIContratosException;

	
}
