package mx.isban.micontratos.impl;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.reasignaciones.BeanAltaSubEstatusRequest;
import mx.isban.micontratos.interfaces.ReasignacionesService;
import mx.isban.micontratos.reasignaciones.ejb.BOReasignaciones;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosFault;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * @author Daniel Hernández Soto
 * Clase con los servicios que harán el llamado
 * a los métodos que consumirán las transacciones:
 * MI22
 */
@WebService(serviceName="ReasignacionesService")
public class ReasignacionesImpl extends SpringBeanAutowiringSupport implements ReasignacionesService{

	/**
	 * Log
	 */
	private static final Logger LOG = Logger.getLogger(ReasignacionesImpl.class.getName());
	
	/**
	 * BO
	 */
	@Autowired
	private BOReasignaciones boReasignaciones;

	/**
	 * Método encargado de la inicialización 
	 */	
	@PostConstruct
	@WebMethod(exclude=true)
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	/**
	 * Método para settear el boReasignaciones
	 * @param boConsultaCatalogos the boConsultaCatalogos to set
	 */
	@WebMethod(exclude=true)
	public void setBOReasignaciones(BOReasignaciones boReasignaciones) {
		this.boReasignaciones = boReasignaciones;
	}
	
	/**
	 * WebService que hará el llamado a el consumo de la transacción MI22 : alta de subestatus
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@Override
	public ResponseGenerico traspasoEjecutivo(@WebParam(name= "altaBean", mode = WebParam.Mode.IN ) BeanAltaSubEstatusRequest request) throws MIContratosException{
		//Instancia del response generico
		ResponseGenerico resultado = new ResponseGenerico();		
		try {
			LOG.info("========= TRX MI22 =========");
		resultado = boReasignaciones.traspasoEjecutivo(request, null);
		}catch(BusinessException e){
			LOG.info("",e);
			throw new MIContratosException(e.getMessage(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
		return resultado;
	}
}