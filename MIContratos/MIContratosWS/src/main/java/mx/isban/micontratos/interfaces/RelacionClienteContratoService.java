package mx.isban.micontratos.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanCambioTitularRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionResultado;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosResultado;
import mx.isban.micontratos.validator.MIContratosException;

/**
 * @author Daniel Hernández Soto
 *
 */
@WebService(name="RelacionClienteContratoService")
public interface RelacionClienteContratoService {
	
	
	/**
	 * Web Service que ejecutará el consumo de la transacción MI30 : Cambio de titular de contrato 
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico modificarTitularContrato(@WebParam(name = "cambioTitularBean", mode = WebParam.Mode.IN) BeanCambioTitularRequest request) throws MIContratosException;
	
	/**
	 * Web Service que ejecutará el consumo de la transacción OD39 y WS perfilamiento: Localizador de contratos por cliente 
	 * @param request
	 * @return BeanLocalizadorContratosResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanLocalizadorContratosResultado localizadorContratos(@WebParam(name = "localizadorContratosBean", mode = WebParam.Mode.IN) BeanLocalizadorContratosRequest request) throws MIContratosException;
	/**
	 * Web Service que ejecutará el consumo de consultaDatosContrato y servicios de Personas : Detalle Contrato Inversion
	 * @param request
	 * @return BeanDetalleContratoInversionResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanDetalleContratoInversionResultado detalleContratoInversion(@WebParam(name = "detalleContratoInversionBean", mode = WebParam.Mode.IN) BeanConsultaInversionRequest request) throws MIContratosException;
}
