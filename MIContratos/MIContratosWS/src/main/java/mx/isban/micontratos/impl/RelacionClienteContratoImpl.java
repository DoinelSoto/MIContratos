package mx.isban.micontratos.impl;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanCambioTitularRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionResultado;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosResultado;
import mx.isban.micontratos.interfaces.RelacionClienteContratoService;
import mx.isban.micontratos.relacionclientecontrato.ejb.BORelacionClienteContrato;
import mx.isban.micontratos.validator.MIContratosException;

/**
 * @author Daniel Hernández Soto
 * Clase con los servicios que harán el llamado
 * a los métodos que consumen las transacciones:
 * MI30
 * Y los servicios detalle de contrato de inversión y localizador de contratos
 */
@WebService(serviceName="RelacionClienteContratoService")
public class RelacionClienteContratoImpl extends SpringBeanAutowiringSupport implements RelacionClienteContratoService{
	
	/**
	 * Log
	 */
	private static final Logger LOG = Logger.getLogger(RelacionClienteContratoImpl.class.getName());
	
	/**
	 * BO
	 */
	@Autowired
	private BORelacionClienteContrato boRelacionClienteContrato;
	/**
	 * Método encargado de la inicialización 
	 */	
	@PostConstruct
	@WebMethod(exclude=true)
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	/**
	 * @param boConsultaCatalogos the boConsultaCatalogos to set
	 */
	@WebMethod(exclude=true)
	public void setBORelacionClienteContrato(BORelacionClienteContrato boRelacionClienteContrato) {
		this.boRelacionClienteContrato = boRelacionClienteContrato;
	}
		
	/**
	 * Web Service que ejecutará el consumo de la transacción MI30 : Cambio de titular de contrato 
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@Override
	public ResponseGenerico modificarTitularContrato(@WebParam(name = "cambioTitularBean", mode = WebParam.Mode.IN) BeanCambioTitularRequest request) throws MIContratosException{
		LOG.info("========= TRX MI30 =========");
		try {
			return boRelacionClienteContrato.modificarTitularContrato(request, null);
		}catch(BusinessException e){
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web Service que ejecutará el consumo de la transacción OD39 y WS perfilamiento: Localizador de contratos por cliente 
	 * @param request
	 * @return BeanLocalizadorContratosResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanLocalizadorContratosResultado localizadorContratos(@WebParam(name = "localizadorContratosBean", mode = WebParam.Mode.IN) BeanLocalizadorContratosRequest request) throws MIContratosException{
		LOG.info("========= localizadorContratos =========");
		try {
			return boRelacionClienteContrato.localizadorContratos(request, null);
		}catch(BusinessException e){
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web Service que ejecutará el consumo de consultaDatosContrato y servicios de Personas : Detalle Contrato Inversion
	 * @param request
	 * @return BeanDetalleContratoInversionResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanDetalleContratoInversionResultado detalleContratoInversion(@WebParam(name = "detalleContratoInversionBean", mode = WebParam.Mode.IN) BeanConsultaInversionRequest request) throws MIContratosException{
		LOG.info("========= detalleContratoInversion =========");
		try {
			return boRelacionClienteContrato.detalleContratoInversion(request, null);
		}catch(BusinessException e){
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
}