package mx.isban.micontratos.impl;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanCambioNumeroContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanMantenimientoDatosGeneralesRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionEstatusContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionIndicadorEstadoCuentaRequest;
import mx.isban.micontratos.gestioncontratos.ejb.BOGestionContratos;
import mx.isban.micontratos.interfaces.GestionContratosService;
import mx.isban.micontratos.validator.MIContratosException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * @author Daniel Hernández Soto
 * Clase con los servicios que harán el llamado
 * a los métodos que consumen las transacciones: 
 * MI16, MI18, MI19, M21, MI23, MI24, MI31, MI37, MI40
 * Y el servicio conversor de cuentas
 */
@WebService(serviceName="GestionContratosService")
public class GestionContratosImpl extends SpringBeanAutowiringSupport implements GestionContratosService{
	
	/**
	 * Log
	 */
	private static final Logger LOG = Logger.getLogger(GestionContratosImpl.class.getName());
	
	/**
	 * BO
	 */
	@Autowired
	private BOGestionContratos boGestionContratos;
	/**
	 * Método encargado de la inicialización 
	 */	
	@PostConstruct
	@WebMethod(exclude=true)
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}
	
	/**
	 * Método para settear boGestionContratos
	 * @param boConsultaCatalogos the boConsultaCatalogos to set
	 */
	@WebMethod(exclude=true)
	public void setBOGestionContratos(BOGestionContratos boGestionContratos) {
		this.boGestionContratos = boGestionContratos;
	}
	
	
	
	/**
	 * WebService que ejecutará el consumo de la transacción MI16 : Consulta de inversión
	 * @param request
	 * @return BeanConsultaInversionResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanConsultaInversionResultado consultaDatosContrato(@WebParam(name = "consultaInversionBean", mode = WebParam.Mode.IN) BeanConsultaInversionRequest request) throws MIContratosException {
		LOG.info("========= TRX MI16 =========");
		try {
			return boGestionContratos.consultaDatosContrato(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * WebService que ejecutará el consumo de la transacción MI18 : Alta de contratos de inversión
	 * @param request
	 * @return BeanAltaContratoInversionResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanAltaContratoInversionResultado altaContrato(@WebParam(name = "altaContratoInversionBean", mode = WebParam.Mode.IN) BeanAltaContratoInversionRequest request) throws MIContratosException {
		LOG.info("========= TRX MI18 =========");
		try {
			return boGestionContratos.altaContrato(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web service que ejecutará el consumo de la transacción MI19 : Modificación de estatus de contrato
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico cambioEstatusContrato(@WebParam(name = "modificacionEstatusContratoBean", mode = WebParam.Mode.IN) BeanModificacionEstatusContratoRequest request) throws MIContratosException{
		LOG.info("========= TRX MI19 =========");	
		try {
			return boGestionContratos.cambioEstatusContrato(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * Web service que ejecutará el consumo de la transacción MI21 : Modificación de número de contrato
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@Override
	public ResponseGenerico cambioAliasContrato(@WebParam(name = "cambioNumeroContratoBean", mode = WebParam.Mode.IN) BeanCambioNumeroContratoRequest request) throws MIContratosException{
		LOG.info("========= TRX MI21 : cambio de Alias de Contrato =========");
		try {
			return boGestionContratos.cambioAliasContrato(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * Web service que ejecutará el consumo de la transacción MI23 : Alta de contrato de inversión al amparo de una chequera
	 * @param request
	 * @return BeanAltaContratosAmparadosChequeraResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanAltaContratosAmparadosChequeraResultado altaCtoAmparadoChequera(@WebParam(name = "altaContratosAmparadosChequera", mode = WebParam.Mode.IN) BeanAltaContratosAmparadosChequeraRequest request) throws MIContratosException{
		try {
			return boGestionContratos.altaCtoAmparadoChequera(request, null);	
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web service que ejecutará el consumo de la transacción MI24 : Consulta de modificaciones del contrato
	 * @param request
	 * @return BeanConsultaModificacionesContratoResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanConsultaModificacionesContratoResultado historicoModifContrato(@WebParam(name = "consultaModificacionesContrato", mode = WebParam.Mode.IN) BeanConsultaModificacionesContratoRequest request) throws MIContratosException{
		try {
			return boGestionContratos.historicoModifContrato(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}	
	}
	
	
	/**
	 * Web service que ejecutará el consumo de la transacción MI31 : Mantenimiento de datos generales
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	*/
	@Override
	public ResponseGenerico modificacionDatosContrato(@WebParam(name = "mantenimientoDatosGeneralesBean", mode = WebParam.Mode.IN) BeanMantenimientoDatosGeneralesRequest request) throws MIContratosException{
		LOG.info("========= TRX MI31 =========");
		try {
			return boGestionContratos.modificacionDatosContrato(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

	/**
	 * Web service que ejecutará el consumo de la transacción MI37 : Baja del indicador de estado de cuenta
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@Override
	public ResponseGenerico modificaOpcionEdoCuenta(@WebParam(name = "modificacionIndicadorEstadoCuentaBean", mode = WebParam.Mode.IN) BeanModificacionIndicadorEstadoCuentaRequest request) throws MIContratosException{
		try {
			LOG.info("========= TRX MI37 =========");
			return  boGestionContratos.modificaOpcionEdoCuenta(request, null);		
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web service que ejecutará varias ocasiones la transacción MI16 ya envuelta en los servicios: Conversor de cuentas 
	 * @param request
	 * @return BeanConversorCuentasResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanConversorCuentasResultado conversorCuentas(@WebParam(name = "conversorCuentasBean", mode = WebParam.Mode.IN) BeanConversorCuentasRequest request) throws MIContratosException{
		try {
			LOG.info("========= conversorCuentas =========");
			return  boGestionContratos.conversorCuentas(request, null);		
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web service que ejecutará el consumo de la transacción MI40 : Lista de Contratos de Inversion
	 * @param request
	 * @return BeanListaContratosInversionResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanListaContratosInversionResultado listarContratosInversion(@WebParam(name = "listarContratosInversionBean", mode = WebParam.Mode.IN) BeanListaContratosInversionRequest request) throws MIContratosException{
		try {
			LOG.info("========= TRX MI40 =========");
			return  boGestionContratos.listarContratosInversion(request, null);		
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}

}