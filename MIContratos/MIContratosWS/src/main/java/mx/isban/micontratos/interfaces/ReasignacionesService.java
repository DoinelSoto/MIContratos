package mx.isban.micontratos.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.reasignaciones.BeanAltaSubEstatusRequest;
import mx.isban.micontratos.validator.MIContratosException;

/**
 * @author Daniel Hernández Soto
 *
 */
@WebService(name="Reasignaciones")
public interface ReasignacionesService {

	/**
	 * WebService que hará el llamado a el consumo de la transacción MI22 : alta de subestatus
	 * @param datosAlta	
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico traspasoEjecutivo(@WebParam(name= "altaBean", mode = WebParam.Mode.IN ) BeanAltaSubEstatusRequest datosAlta) throws MIContratosException;
	
}
