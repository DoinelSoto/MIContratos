package mx.isban.micontratos.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanCambioNumeroContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanMantenimientoDatosGeneralesRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionEstatusContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionIndicadorEstadoCuentaRequest;
import mx.isban.micontratos.validator.MIContratosException;
/**
 * @author Daniel Hernández Soto
 *
 */
@WebService(name="GestionContratosService")
public interface GestionContratosService {


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI16 : Consulta de inversión
	 * @param request
	 * @return BeanConsultaInversionResultado
	 */
	@WebMethod
	BeanConsultaInversionResultado consultaDatosContrato(@WebParam(name = "consultaInversionBean", mode = WebParam.Mode.IN) BeanConsultaInversionRequest request) throws MIContratosException;


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI18 : Alta de contratos de inversión
	 * @param request
	 * @return BeanAltaContratoInversionResultado
	 */
	@WebMethod
	BeanAltaContratoInversionResultado altaContrato(@WebParam(name = "altaContratoInversionBean", mode = WebParam.Mode.IN) BeanAltaContratoInversionRequest request) throws MIContratosException;


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI19 : Modificación de estatus de contrato
	 * @param request
	 * @return ResponseGenerico
	 */
	@WebMethod
	ResponseGenerico cambioEstatusContrato(@WebParam(name = "modificacionEstatusContratoBean", mode = WebParam.Mode.IN) BeanModificacionEstatusContratoRequest request) throws MIContratosException;


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI21 : Modificación de número de contrato
	 * @param request
	 * @return ResponseGenerico
	 */
	@WebMethod
	ResponseGenerico cambioAliasContrato(@WebParam(name = "cambioNumeroContratoBean", mode = WebParam.Mode.IN) BeanCambioNumeroContratoRequest request) throws MIContratosException;


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI23 : Alta de contrato de inversión al amparo de una chequera
	 * @param request
	 * @return BeanAltaContratosAmparadosChequeraResultado
	 */
	@WebMethod
	BeanAltaContratosAmparadosChequeraResultado altaCtoAmparadoChequera(@WebParam(name = "altaContratosAmparadosChequera", mode = WebParam.Mode.IN) BeanAltaContratosAmparadosChequeraRequest request) throws MIContratosException;

	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI24 : Consulta de modificaciones del contrato
	 * @param request
	 * @return BeanConsultaModificacionesContratoResultado
	 */
	@WebMethod
	BeanConsultaModificacionesContratoResultado historicoModifContrato(@WebParam(name = "consultaModificacionesContrato", mode = WebParam.Mode.IN) BeanConsultaModificacionesContratoRequest request) throws MIContratosException;

	
	
	/**
	 * Web Service que permitirá exponer como webService a transacción MI31 : Mantenimiento de datos generales
	 * @param request
	 * @return ResponseGenerico
	 */
	@WebMethod
	ResponseGenerico modificacionDatosContrato(@WebParam(name = "mantenimientoDatosGeneralesBean", mode = WebParam.Mode.IN) BeanMantenimientoDatosGeneralesRequest request) throws MIContratosException;


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI37 : Baja del indicador de estado de cuenta
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico modificaOpcionEdoCuenta(@WebParam(name = "modificacionIndicadorEstadoCuentaBean", mode = WebParam.Mode.IN) BeanModificacionIndicadorEstadoCuentaRequest request) throws MIContratosException;

	/**
	 * Web service que ejecutará varias ocasiones la transacción MI16 ya envuelta en los servicios: Conversor de cuentas
	 * @param request
	 * @return BeanConversorCuentasResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanConversorCuentasResultado conversorCuentas(@WebParam(name = "conversorCuentasBean", mode = WebParam.Mode.IN) BeanConversorCuentasRequest request) throws MIContratosException;

	/**
	**
	 * Web Service que permitirá exponer como webService a transacción MI40 : Lista de Contratos de Inversion
	 * @param request
	 * @return BeanListaContratosInversionResultado
	 */
	@WebMethod
	BeanListaContratosInversionResultado listarContratosInversion(@WebParam(name = "listarContratosInversionBean", mode = WebParam.Mode.IN) BeanListaContratosInversionRequest request) throws MIContratosException;

}
