package mx.isban.micontratos.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaEstatusContratosRequest;
import mx.isban.micontratos.beans.catalogos.BeanModificacionEstatusTablaCatalogoRequest;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.validator.MIContratosException;
/**
 * @author Daniel Hernández Soto
 *
 */
@WebService(name="CatalogosService")
public interface CatalogosService {


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI25 : Alta de catálogos de estatus
	 * @param request
	 * @return ResponseGenerico
	 */
	@WebMethod
	ResponseGenerico altaEstatusOperativo(@WebParam(name = "altaCatalogosEstatusBean", mode = WebParam.Mode.IN) BeanAltaCatalogosEstatusRequest request) throws MIContratosException;


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI26 : Baja de catálogos de estatus
	 * @param request
	 * @return ResponseGenerico
	 */
	@WebMethod
	ResponseGenerico bajaEstatusOperativo(@WebParam(name = "bajaEstatusContratosBean", mode = WebParam.Mode.IN) BeanBajaEstatusContratosRequest request) throws MIContratosException;


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI27 : Modificación de estatus de la tabla catálogo
	 * @param request
	 * @return ResponseGenerico
	 */
	@WebMethod
	ResponseGenerico modificarEstatusOperativo(@WebParam(name = "modificacionEstatusTablaCatalogoBean", mode = WebParam.Mode.IN) BeanModificacionEstatusTablaCatalogoRequest request) throws MIContratosException;
	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI28 : Alta de catálogos de SubEstatus
	 * @param request
	 * @return ResponseGenerico
	 */
	@WebMethod
	ResponseGenerico altaSubEstatusOperativo(@WebParam(name = "altaCatalogosSubEstatusBean", mode = WebParam.Mode.IN) BeanAltaCatalogosSubEstatusRequest request) throws MIContratosException;


	/**
	 * Web Service que permitirá exponer como webService a la transacción MI29 : Baja de catálogos de SubEstatus
	 * @param request
	 * @return ResponseGenerico
	 */
	@WebMethod
	ResponseGenerico bajaSubEstatusOperativo(@WebParam(name = "bajaCatalogosSubEstatus", mode = WebParam.Mode.IN) BeanBajaCatalogosSubEstatusRequest request) throws MIContratosException;
}