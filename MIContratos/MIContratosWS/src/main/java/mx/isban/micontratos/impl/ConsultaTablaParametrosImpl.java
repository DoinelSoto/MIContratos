package mx.isban.micontratos.impl;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchResult;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIRequest;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIResultado;
import mx.isban.micontratos.catalogos.ejb.BOConsultaTablaParametros;
import mx.isban.micontratos.interfaces.ConsultaTablaParametrosService;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosFault;

/**
 * @author Daniel Hernández Soto
 *  Clase con los servicios que harán el llamado a los métodos
 *  que consumiran las transacciones: MI04, MI05, MI06, MI07
 *  MI08, MI09, MI39
 *  Y el servicio consulta de tipo de servicio para un cliente 
 */
@WebService(serviceName="ConsultaTablaParametrosService")
public class ConsultaTablaParametrosImpl extends SpringBeanAutowiringSupport implements ConsultaTablaParametrosService{

	/**
	 * Log
	 */
	private static final Logger LOG = Logger.getLogger(ConsultaTablaParametrosImpl.class.getName());
	
	/**
	 * BO
	 */
	@Autowired
	private BOConsultaTablaParametros boConsultaTablaParametros;

	/**
	 * Método encargado de la inicialización 
	 */	
	@PostConstruct
	@WebMethod(exclude=true)
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	/**
	 * Método para settear el boConsultaTablaParametros
	 * @param boConsultaTablaParametros 
	 */
	@WebMethod(exclude=true)
	public void setBoConsultaCatalogos(BOConsultaTablaParametros boConsultaTablaParametros) {
		this.boConsultaTablaParametros = boConsultaTablaParametros;
	}
	
	
	/**
	 *  Web Service que permitirá exponer como webService a la transacción MI04 : Consulta de catalogos de estatus operativo
	 *  @param request
	 *  @return BeanCatalogoEstatusOperativoResultado
	 *  @throws MIContratosException
	 */
	@Override
	public BeanCatalogoEstatusOperativoResultado catalogoEstatusOperativos(@WebParam(name = "catalogoEstatusOperativoBean", mode = WebParam.Mode.IN) BeanCatalogoEstatusOperativoRequest request) throws MIContratosException{
		LOG.info("========= TRX MI04 =========");
		try {
			return boConsultaTablaParametros.catalogoEstatusOperativos(request, null);
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI05 : Consulta de catalogos de SubEstatus
	 * @param request
	 * @returnBeanCatalogoSubEstatusResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanCatalogoSubEstatusResultado catalogoSubEstatus(@WebParam(name = "catalogoSubEstatusBean", mode = WebParam.Mode.IN) BeanCatalogoSubEstatusRequest request) throws MIContratosException{
		LOG.info("========= TRX MI05 =========");
		try {
			return boConsultaTablaParametros.catalogoSubEstatus(request, null); 
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI06 : Consulta de lista de tipos de servicio
	 * @param request
	 * @return BeanListaTiposServicioResultad
	 * @throws MIContratosException}
	 */
	@Override
	public BeanListaTiposServicioResultado tipoServicio(@WebParam(name = "listaTiposServicioBean", mode = WebParam.Mode.IN) BeanListaTiposServicioRequest request) throws MIContratosException {
	LOG.info("========= TRX MI06 =========");	
		try {
			return boConsultaTablaParametros.tipoServicio(request, null);
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI07 : Consulta de árbol de tipos de servicio
	 * @param request
	 * @return BeanArbolTiposServicioResultado
	 * @throws MIContratosException
	 */
	@Override
	public BeanArbolTiposServicioResultado arbolTipoServicio(@WebParam(name = "arbolTiposServicioBean", mode = WebParam.Mode.IN) BeanArbolTiposServicioRequest request) throws MIContratosException {
		LOG.info("========= TRX MI07 =========");	
		try {
			return  boConsultaTablaParametros.arbolTipoServicio(request, null);
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),e.getMessage());
		}
	}
	/**
	 * Método que permitirá exponer como webService a la transacción M139: Consulta Branch
	 * @param request
	 * @return BeanConsultarBranchResult
	 * @throws MIContratosException 
	 */
	@Override
	public BeanConsultarBranchResult consultaBranch(@WebParam(name = "consultaCatalogosBranchBean", mode = WebParam.Mode.IN) BeanConsultarBranchRequest request) throws MIContratosException {
		LOG.info("========= TRX MI39 =========");
		try {
			return boConsultaTablaParametros.consultaBranch(request, null);	
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
	}
	
	/**
	* Método que permitirá exponer como webService a las transacciónes (OD29, PEL1 y PEL2) y MI07 : Tipo Necesidad AFI
	* @param request
	* @return BeanTipoNecesidadAFIResultado
	* @throws MIContratosException 
	*/
	@Override
	public BeanTipoNecesidadAFIResultado tipoNecesidadAFI(@WebParam(name = "tipoCatalogosNecesidadAFIBean", mode = WebParam.Mode.IN) BeanTipoNecesidadAFIRequest request) throws MIContratosException {
		LOG.info("========= tipoNecesidadAFI =========");
		try {
			return boConsultaTablaParametros.tipoNecesidadAFI(request, null);	
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getCode(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
	}
	
	/**
	 * Método que permitirá exponer como webService a la transacción MI08 : Consulta de estados de documentos
	 * @param request
	 * @return BeanConsultaEstadoDocumentosResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanConsultaEstadoDocumentosResultado consultaEstadoDocumentos(@WebParam(name = "consultaEstadoDocumentosBean", mode = WebParam.Mode.IN) BeanConsultaEstadoDocumentosRequest request) throws MIContratosException{
		LOG.info("========= TRX MI08 =========");
		try {
			return boConsultaTablaParametros.ejecutaConsultaEstadoDocumentos(request, null);
			} catch (BusinessException e) {
				LOG.info("",e);
				throw new MIContratosException(e.getCode(),new MIContratosFault(e.getCode(),e.getMessage()),e);
			}
	}
	
	/**
	 * Método que permitirá exponer como webService a la transacción MI09 : Catálogos estructurales
	 * @param request
	 * @return BeanCatalogosEstructuralesResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanCatalogosEstructuralesResultado consultaCatalogosEstructurales(@WebParam(name = "catalogosEstructuralesBean", mode = WebParam.Mode.IN) BeanCatalogosEstructuralesRequest request) throws MIContratosException {
		LOG.info("========= TRX MI09 =========");
		try {
			return boConsultaTablaParametros.ejecutaConsultaCatalogosEstructurales(request, null);
		} catch (BusinessException e) {
			LOG.info("",e);
			throw new MIContratosException(e.getCode(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
	}
}