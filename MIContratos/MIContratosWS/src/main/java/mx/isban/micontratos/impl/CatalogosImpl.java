package mx.isban.micontratos.impl;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaEstatusContratosRequest;
import mx.isban.micontratos.beans.catalogos.BeanModificacionEstatusTablaCatalogoRequest;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.catalogos.ejb.BOConsultaCatalogos;
import mx.isban.micontratos.interfaces.CatalogosService;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosFault;

/**
 * @author Daniel Hernández Soto
 * Clase con los servicios que harán el llamado a los
 * métodos que consumen las transacciones: MI25, MI26,
 * MI27, MI28, MI29
 */
@WebService(serviceName="CatalogosService")
public class CatalogosImpl extends SpringBeanAutowiringSupport implements CatalogosService{
	
	/**
	 * Log
	 */
	private static final Logger LOG = Logger.getLogger(CatalogosImpl.class.getName());
	
	/**
	 * Instancia del BOConsultaCatalogos
	 */
	@Autowired
	private BOConsultaCatalogos boConsultaCatalogos ;

	/**
	 * Método encargado de la inicialización 
	 */
	@PostConstruct
	@WebMethod(exclude=true)
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}
	
	/**
	 * Método para settear boConsultaCatalogos
	 * @param boConsultaCatalogos the boConsultaCatalogos to set
	 */
	@WebMethod(exclude=true)
	public void setBoConsultaCatalogos(BOConsultaCatalogos boConsultaCatalogos) {
		this.boConsultaCatalogos = boConsultaCatalogos;
	}

	
	/**
	 * Web Service que permitirá exponer como webService a la transacción MI25 : Alta de catálogos de estatus
	 * @param request 
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico altaEstatusOperativo(@WebParam(name = "altaCatalogosEstatusBean", mode = WebParam.Mode.IN) BeanAltaCatalogosEstatusRequest request) throws MIContratosException {
		LOG.info("========= TRX MI25 =========");
		//Instancia del response generico
		ResponseGenerico response = new ResponseGenerico();
		try {
			response = boConsultaCatalogos.altaEstatusOperativo(request, null);
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getMessage(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
		LOG.info("========================================================");
		return response;
	}

	/**
	 * Método que permitirexponer como webService a la transacción MI26 : Baja de catálogos de estatus 
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@Override
	public ResponseGenerico bajaEstatusOperativo(@WebParam(name = "bajaEstatusContratosBean", mode = WebParam.Mode.IN) BeanBajaEstatusContratosRequest request) throws MIContratosException {
		LOG.info("========= TRX MI26 =========");
		//Instancia del response generico
		ResponseGenerico response = new ResponseGenerico();
		try {			
			response = boConsultaCatalogos.bajaEstatusOperativo(request, null);
			LOG.info("====================================");
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getMessage(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
		return response;
	}

	/**
	 * Método que permitirá exponer como webService a la transacción MI27 : Modificación de estatus de la tabla catálogo
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico modificarEstatusOperativo(@WebParam(name = "modificacionEstatusTablaCatalogoBean", mode = WebParam.Mode.IN) BeanModificacionEstatusTablaCatalogoRequest request) throws MIContratosException {
		LOG.info("========= TRX MI27 =========");
		//Instancia del response generico
		ResponseGenerico response = new ResponseGenerico();
			try {
			response = boConsultaCatalogos.modificarEstatusOperativo(request, null);
			LOG.info("====================================");
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getMessage(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
		return response;
	}

	/**
	 * Método que permitirá exponer como WebService a la transacción MI28 : Alta de catálogos de SubEstatus
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico altaSubEstatusOperativo(@WebParam(name = "altaCatalogosSubEstatusBean", mode = WebParam.Mode.IN) BeanAltaCatalogosSubEstatusRequest request) throws MIContratosException {
		ResponseGenerico response = new ResponseGenerico();
		try {
			response = 	boConsultaCatalogos.altaSubEstatusOperativo(request, null);
			LOG.info("==================================================");
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getMessage(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
		return response;
	}

	/**
	 * Método que permitirá exponer como webService a la transacción MI29 : Baja de catálogos de SubEstatus
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	@Override
	public ResponseGenerico bajaSubEstatusOperativo(@WebParam(name = "bajaCatalogosSubEstatus", mode = WebParam.Mode.IN) BeanBajaCatalogosSubEstatusRequest request) throws MIContratosException {
		LOG.info("========= TRX MI29 =========");
		try {
			return boConsultaCatalogos.bajaSubEstatusOperativo(request, null);	
		} catch (BusinessException e) {
			LOG.info(""+ e);
			throw new MIContratosException(e.getMessage(),new MIContratosFault(e.getCode(),e.getMessage()),e);
		}
	}
}
