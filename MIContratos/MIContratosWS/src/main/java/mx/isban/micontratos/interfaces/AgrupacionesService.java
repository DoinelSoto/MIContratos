package mx.isban.micontratos.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanBajaContratosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorResultado;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosResultado;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.validator.MIContratosException;
/**
 * @author Daniel Hernández Soto
 *
 */
@WebService(name="AgrupacionesService")
public interface AgrupacionesService{

	
	/**
	 * WebService que consumirá la transacción MI32 : Consulta de lista de contrato agrupador
	 * @param request
	 * @return BeanListaContratoAgrupadorResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanListaContratoAgrupadorResultado listaContratosAgrupadores(@WebParam(name = "listaContratosAgrupadorBean", mode = WebParam.Mode.IN) BeanListaContratoAgrupadorRequest request) throws MIContratosException;

	
	/**
	 * WebService que consumirá la transacción MI33 : Alta de contrato agrupador
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico altaContratoAgrupador(@WebParam(name = "altaContratoAgrupadorBean", mode = WebParam.Mode.IN) BeanAltaContratoAgrupadorRequest request) throws MIContratosException;

	
	/**
	 *  WebService que ejecutará el consumo de la transacción MI34 : Consulta Lista Contratos Hijos
	 * @param request 
	 * @return BeanListaContratosHijosResultado
	 * @throws MIContratosException
	 */
	@WebMethod
	BeanListaContratosHijosResultado listarContratosAgrupados(@WebParam(name = "listaContratosHijosBean", mode = WebParam.Mode.IN) BeanListaContratosHijosRequest request) throws MIContratosException;

	
	/**
	 * WebService que ejecutará el consumo de la transacción MI35 : Alta contratos hijos
	 * @param request
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico altaContratoAgrupado(@WebParam(name = "altaContratosHijosBean", mode = WebParam.Mode.IN) BeanAltaContratosHijosRequest request) throws MIContratosException;


	/**
	 * WebService que hará la ejecución de la transacción MI36 : Baja de contratos hijos agruapados
	 * @param request	
	 * @return ResponseGenerico
	 * @throws MIContratosException
	 */
	@WebMethod
	ResponseGenerico bajaContratoAgrupado(@WebParam(name = "bajaContratosBean", mode = WebParam.Mode.IN) BeanBajaContratosRequest request) throws MIContratosException;

	
}
