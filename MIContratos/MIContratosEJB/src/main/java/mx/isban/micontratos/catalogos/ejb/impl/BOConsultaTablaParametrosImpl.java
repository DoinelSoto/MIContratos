package mx.isban.micontratos.catalogos.ejb.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioResponse;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchResult;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIRequest;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIResponse;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIResultado;
import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.catalogos.dao.DAOConsultaTablaParametros;
import mx.isban.micontratos.catalogos.ejb.BOConsultaTablaParametros;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosValidator;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasFisicasRequestBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasFisicasResponseBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasMoralesRequestBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasMoralesResponseBean;
import mx.isban.mipersonas.base.beans.MulticanalidadBean;
import mx.isban.mipersonas.localizadores.beans.ConsPersonasBucRequestBean;
import mx.isban.mipersonas.localizadores.beans.ConsPersonasBucResponseBean;


/**
 * @author Daniel Hernández Soto
 *	BO que hará el llamado a los métodos que consumen las transacciones: MI04, MI05, MI06, MI07, MI08, MI09, MI39
 * Y los servicios: Consulta de tipos de servicio para un cliente
 * 
 */
@Stateless
@Remote(BOConsultaTablaParametros.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOConsultaTablaParametrosImpl extends Architech implements BOConsultaTablaParametros{
	
	/**
	 * Serialización 
	 */
	private static final long serialVersionUID = -5320931224637863574L;
	
	/**
	 * Instancia DAOConsultaTablaParametros
	 */
	@EJB
	private DAOConsultaTablaParametros daoConsultaTablaParametros;
	
	/**
	 * Persona fisica
	 */
	private char personaFisica='F';
	/**
	 * Persona moral
	 */
	private char personaMoral='J';
	
	/**
	 * Método que ejecutará el consumo de la transaccion MI04 : Consulta de catálogos de estatus operativo
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return BeanCatalogoEstatusOperativoResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanCatalogoEstatusOperativoResultado catalogoEstatusOperativos(BeanCatalogoEstatusOperativoRequest beanAltaCatalogosEstatusRequest,ArchitechSessionBean sesion) throws BusinessException {
		//Campos no obligatorios
		String[] camposNoObligatorios = {
				beanAltaCatalogosEstatusRequest.getCodigoEstatus(),
				beanAltaCatalogosEstatusRequest.getRellamada().getMasDatos(),
				beanAltaCatalogosEstatusRequest.getRellamada().getMemento() };
		//Longitudes de campos no obligatorios
		int[] longitudesNoObligatorias={2,1,2};
		try {
			//Se validan campos
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanCatalogoEstatusOperativoResultado response = daoConsultaTablaParametros.catalogoEstatusOperativos(beanAltaCatalogosEstatusRequest, sesion);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			error("", e);
			throw new BusinessException(e.getCode(),e.getMessage());
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}	
	}

	/**
	 * Método que ejecutará el consumo de la transaccion MI05 : Consulta de catálogos de Subestaus operativo
	 * @param beanCatalogosSubEstatusRequest
	 * @param sesion
	 * @return BeanCatalogoSubEstatusResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanCatalogoSubEstatusResultado catalogoSubEstatus(BeanCatalogoSubEstatusRequest beanCatalogosSubEstatusRequest,ArchitechSessionBean sesion) throws BusinessException {
		//Campos no obligatorios
		String[] camposNoObligatorios = {
				beanCatalogosSubEstatusRequest.getRellamada().getMasDatos(),
				beanCatalogosSubEstatusRequest.getCodigoEstatus(),
				beanCatalogosSubEstatusRequest.getCodigoSubEstatus(),
				beanCatalogosSubEstatusRequest.getRellamada().getMemento() };
		//Longitudes de campos no obligatorios
		int[] longitudesNoObligatorias = {1,2,2,4};
		try {
			//Se validan campos
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);	
			BeanCatalogoSubEstatusResultado response = daoConsultaTablaParametros.catalogoSubEstatus(beanCatalogosSubEstatusRequest, sesion);
			//Se valida Código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			error("", e);
			throw new BusinessException(e.getCode(),e.getMessage());
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
		
	}
	
	/**
	 * Método que ejecutará el consumo de la transaccion MI06 : Consulta de la lista de tipo de servicios
	 * @param beanListaTiposServicioRequest
	 * @param sesion
	 * @return BeanListaTiposServicioResultado
	 * @throws MIContratosException 
	 */
	@Override
	public BeanListaTiposServicioResultado tipoServicio(BeanListaTiposServicioRequest beanListaTiposServicioRequest,ArchitechSessionBean sesion) throws BusinessException {
		//Campos no obligatorios
		String[] camposNoObligatorios = {
				beanListaTiposServicioRequest.getRellamada().getMasDatos(),
				beanListaTiposServicioRequest.getCodigoTipoServicio(),
				beanListaTiposServicioRequest.getRellamada().getMemento() };
		//Longitudes de campos no obligatorios
		int[] longitudesNoObligatorias = {1,2,2};
		try {
			//Se validan campos
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);	
			BeanListaTiposServicioResultado response = daoConsultaTablaParametros.tipoServicio(beanListaTiposServicioRequest, sesion);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			error("", e);
			throw new BusinessException(e.getCode(),e.getMessage());
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transaccion MI07 : Consulta de arbol de tipo de servicios
	 * @param beanArbolTiposServicioRequest
	 * @param sesion
	 * @return BeanArbolTiposServicioResultado
	 * @throws BusinessException
	 */
	@Override
	public BeanArbolTiposServicioResultado arbolTipoServicio(BeanArbolTiposServicioRequest beanArbolTiposServicioRequest,ArchitechSessionBean sesion) throws BusinessException {
			//Campos obligatorios
			String[] camposObligatorios = {beanArbolTiposServicioRequest.getCodigoTipoCliente(),beanArbolTiposServicioRequest.getCodigoPerfilInversion()};
			//Longitudes de campos obligatorios
			int[] longitudesObligatorias = {2,2};
			//Campos no obligatorios
			String[] camposNoObligatorios = {beanArbolTiposServicioRequest.getCodigoTipoServicio(),beanArbolTiposServicioRequest.getRellamada().getMasDatos(),beanArbolTiposServicioRequest.getRellamada().getMemento()};
			//Longitudes de campos no obligatorios
			int[] longitudesNoObligatorias = {2,1,6};
		try {
			//Se validan campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			//Se validan campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanArbolTiposServicioResultado response = daoConsultaTablaParametros.arbolTipoServicio(beanArbolTiposServicioRequest, sesion);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			error("", e);
			throw new BusinessException(e.getCode(),e.getMessage());
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
	
	/**
	 * Método que permitirá exponer como webService a la transacción :MI39 Consulta Branch
	 * @param beanConsultarBranchRequest
	 * @param session
	 * @return BeanConsultarBranchResult
	 * @throws BusinessException
	 */
	@Override
	public BeanConsultarBranchResult consultaBranch(BeanConsultarBranchRequest beanConsultarBranchRequest,
			ArchitechSessionBean session) throws BusinessException{
		//Campos no obligatorios
		String [] camposNoObligatorios={beanConsultarBranchRequest.getCodigoBranch(),beanConsultarBranchRequest.getCodigoEmpresa(),
				beanConsultarBranchRequest.getRellamada().getMemento(),
				beanConsultarBranchRequest.getRellamada().getMasDatos(),
				beanConsultarBranchRequest.getMulticanalidad().getCanalComercializacion(),
				beanConsultarBranchRequest.getMulticanalidad().getCanalOperacion(),
				beanConsultarBranchRequest.getMulticanalidad().getEmpresaConexion()};
		//Longitudes de campos no obligatorios
		int [] longitudesNoObligatorias={2,4,6,1,2,2,4};

		try {
			//Se validan campos
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanConsultarBranchResult response=daoConsultaTablaParametros.consultaBranch(beanConsultarBranchRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}		
	}
	
	/**
	 * Método que permitirá exponer como webService a las transacciónes (OD29, PEL1 y PEL2) y MI07 : Tipo Necesidad AFI
	 * @param beanTipoNecesidadAFIRequest
	 * @param session
	 * @return BeanTipoNecesidadAFIResultado
	 * @throws BusinessException
	 */
	@Override
	public BeanTipoNecesidadAFIResultado tipoNecesidadAFI(BeanTipoNecesidadAFIRequest beanTipoNecesidadAFIRequest,ArchitechSessionBean session) throws BusinessException {
		BeanTipoNecesidadAFIResultado resultado = new BeanTipoNecesidadAFIResultado();
		List<BeanTipoNecesidadAFIResponse> lstResponse = new ArrayList<>();
		resultado.setListaCatalogoTipoNecesidadAfi(lstResponse);
		//Campos obligatorios
		String [] camposObligatorios ={beanTipoNecesidadAFIRequest.getNumeroPersona()};
		//Longitudes de campos obligatorios
		int [] longitudes ={8};
		//Longitudes de campos no obligatorios
		String [] camposNoObligatorios ={beanTipoNecesidadAFIRequest.getMulticanalidad().getEmpresaConexion(),beanTipoNecesidadAFIRequest.getMulticanalidad().getCanalOperacion(),
				beanTipoNecesidadAFIRequest.getMulticanalidad().getCanalComercializacion()};
		//Longitudes de campos no obligatorios
		int [] longitudesNoObligatotios ={4,2,2};
		try {
			//Se validan campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			//Se validan campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatotios);
			ConsPersonasBucRequestBean bean = new ConsPersonasBucRequestBean();
			bean.setBuc(beanTipoNecesidadAFIRequest.getNumeroPersona());
			ConsPersonasBucResponseBean respLocaPorBuc = daoConsultaTablaParametros.localizadorPersonasBuc(beanTipoNecesidadAFIRequest,session);
			//Código de error
			resultado.setCodError(respLocaPorBuc.getCodError());
			//Mensaje de error
			resultado.setMsgError(respLocaPorBuc.getMsgError());
			
				String codPerfil = "";
				String codTipoCliente = "";

				MulticanalidadBean multi = new MulticanalidadBean();
				multi.setCanalComercial("");
				multi.setCanalOperacion("");
				multi.setEntidad("");
				//Se verifica el tipo de persona
				if(respLocaPorBuc.getTipoPersona()==personaFisica && respLocaPorBuc!=null){
					ConsultaDetPersonasFisicasRequestBean consultaPersona = new ConsultaDetPersonasFisicasRequestBean();
					consultaPersona.setNumeroPersona(beanTipoNecesidadAFIRequest.getNumeroPersona());
					consultaPersona.setMulticanalidad(multi);
					//Se hace la consulta como persona física
					ConsultaDetPersonasFisicasResponseBean persona = daoConsultaTablaParametros.consultaPersonaFisicas(consultaPersona);
					resultado.setCodError(persona.getCodError());
					resultado.setMsgError(persona.getMsgError());
					codPerfil = persona.getPerfil();
					codTipoCliente = persona.getTipoCliente();
				}else if (respLocaPorBuc.getTipoPersona()==personaMoral){
					ConsultaDetPersonasMoralesRequestBean consultaPersona = new ConsultaDetPersonasMoralesRequestBean();
					consultaPersona.setNumeroPersona(beanTipoNecesidadAFIRequest.getNumeroPersona());
					consultaPersona.setMulticanalidadBean(multi);
					//Se hace la consulta como persona moral
					ConsultaDetPersonasMoralesResponseBean persona = daoConsultaTablaParametros.consultaPersonaMoral(consultaPersona);
					resultado.setCodError(persona.getCodError());
					resultado.setMsgError(persona.getMsgError());
					codPerfil = persona.getPerfil();
					codTipoCliente = persona.getTipoCliente();
				}
				//Se verifíca que el código de perfil y el código de tipo cliente estén informados
				if(!codPerfil.isEmpty() && !codTipoCliente.isEmpty()){
					BeanArbolTiposServicioRequest beanArbol = new BeanArbolTiposServicioRequest();
					beanArbol.setCodigoPerfilInversion(codPerfil);
					beanArbol.setCodigoTipoCliente(codTipoCliente);
					beanArbol.setCodigoTipoServicio("");

					BeanPaginacion beanRella = new BeanPaginacion();
					beanRella.setMasDatos("");
					beanRella.setMemento("");
					beanArbol.setRellamada(beanRella);
					//Se obtiene el catalogo de servicios disponibles para el cliente
					lstResponse=getCatalogoServicios(beanArbol,session);
					//Se settea la lista
					resultado.setListaCatalogoTipoNecesidadAfi(lstResponse);
				}
			return resultado;
		} catch (BusinessException e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage());
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}	
	}
	
	/**
	 * Método que devolvera el catalogo de los tipos de servicio
	 * @param beanArbol
	 * @param session
	 * @return List
	 * @throws BusinessException 
	 */
	public List<BeanTipoNecesidadAFIResponse> getCatalogoServicios(BeanArbolTiposServicioRequest beanArbol, ArchitechSessionBean session) throws BusinessException{
		List<BeanTipoNecesidadAFIResponse> lstResponse= new ArrayList<>();
		do{
			BeanArbolTiposServicioResultado arbolTipoServicio = arbolTipoServicio(beanArbol, session);

			if(!arbolTipoServicio.getListaArbolTiposServicio().isEmpty()){
				for(int i=0;i<arbolTipoServicio.getListaArbolTiposServicio().size();i++){
					BeanArbolTiposServicioResponse arbol = arbolTipoServicio.getListaArbolTiposServicio().get(i);

					BeanTipoNecesidadAFIResponse resp = new BeanTipoNecesidadAFIResponse();
					resp.setCodigoTipoServicio(arbol.getCodigoTipoServicio());
					resp.setDescTipoServicio(arbol.getDescripcionTipoServicio());
					resp.setIndAsesoramiento(arbol.getIndicadorAsesoramiento());
					resp.setIndicadorNecesidadAfi(arbol.getIndicadorNecesidadAsesor());

					lstResponse.add(resp);
					
				}
				beanArbol.getRellamada().setMasDatos(arbolTipoServicio.getRellamada().getMasDatos());
				beanArbol.getRellamada().setMemento(arbolTipoServicio.getRellamada().getMemento());

			}
		}while(MIContratosConstantes.SI_HAY_DATOS.equals(beanArbol.getRellamada().getMasDatos()));
		
		return lstResponse;
		
	}
	/**
	 * Método que permitirá exponer como webService a la transacción MI08 : Consulta de estados de documentos
	 * @param beanConsultaEstadoDocumentosRequest
	 * @param session
	 * @return BeanConsultaEstadoDocumentosResultado
	 * @throws BusinessException
	 */
	@Override
	public BeanConsultaEstadoDocumentosResultado ejecutaConsultaEstadoDocumentos(BeanConsultaEstadoDocumentosRequest beanConsultaEstadoDocumentosRequest, ArchitechSessionBean session) throws BusinessException{
		//Campos no obligatorios
		String[] camposNoObligatorios = {
				beanConsultaEstadoDocumentosRequest.getCodigoEstadoDocumento(),
				beanConsultaEstadoDocumentosRequest.getRellamada().getMasDatos(),
				beanConsultaEstadoDocumentosRequest.getRellamada().getMemento()};
		//Longitudes de campos no obligatorios
		int[] longitudesNoObligatorias = {2,1,2};
		try {
			//Se validan campos
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);	
			BeanConsultaEstadoDocumentosResultado response = daoConsultaTablaParametros.consultaEstadoDocumentos(beanConsultaEstadoDocumentosRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			info("",e);
			throw new BusinessException(e.getCode(),e.getMessage());
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transaccion MI09 : Catálogos estructurales
	 * @param beanCatalogosEstructuralesRequest
	 * @param session
	 * @return BeanCatalogosEstructuralesResultado
	 * @throws BusinessException
	 */
	@Override
	public BeanCatalogosEstructuralesResultado ejecutaConsultaCatalogosEstructurales(BeanCatalogosEstructuralesRequest beanCatalogosEstructuralesRequest, ArchitechSessionBean session) throws BusinessException {
		//Campos obligatorios
		String[] camposObligatorios = {
				beanCatalogosEstructuralesRequest.getCodigoCatalogo()};
		//Longitudes de campos obligatorios
		int[] longitudesObligatorias = {2};
		//Campos no obligatorios
		String[] camposNoObligatorios = {
				beanCatalogosEstructuralesRequest.getCodigoEstructuralInversion(),
				beanCatalogosEstructuralesRequest.getRellamada().getMasDatos(),
				beanCatalogosEstructuralesRequest.getRellamada().getMemento()};
		//Longitudes de campos no obligatorios
		int[] longitudesNoObligatorias = {2,1,2};
		
		try {
			//Se validan campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			//Se validan campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);	
			BeanCatalogosEstructuralesResultado response = daoConsultaTablaParametros.consultaCatalogosEstructurales(beanCatalogosEstructuralesRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			info("",e);
			throw new BusinessException(e.getCode(),e.getMessage());
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
}