package mx.isban.micontratos.util;
/**************************************************************
 * Querétaro, Qro Noviembre 2016
 *
 * La redistribución y el uso en formas fuente y binario, 
 * son responsabilidad del propietario.
 * 
 * Este software fue elaborado en @Everis
 * por Jemima Del Ángel San Martín 
 * 
 * Para mas información, consulte <www.everis.com/mexico>
 ***************************************************************/

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.ConfigFactory;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.channels.cics.dto.RequestMessageCicsDTO;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.agave.dataaccess.factories.jms.ConfigFactoryJMS;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

import org.apache.commons.lang.StringUtils;

/**
 * Clase Singleton para el Isban Data Access
 * 
 * @author everis
 *
 */
public class SingletonIDA extends Architech{
	/**
	 * Versión de clase serializable
	 */
	private static final long serialVersionUID = 3761189760753101191L;
	/**
	 * Objeto singleton de la clase
	 */
	private static SingletonIDA ejecucionIDA;

	/**
	 * 
	 * @return SingletonIDA La instancia singleton
	 */
	public static SingletonIDA getSingletonInstance() {
		if (ejecucionIDA == null) {
			ejecucionIDA = new SingletonIDA();
		}
		return ejecucionIDA;
	}

	/**
	 * 
	 * @param mensajeTrama
	 *            Trama de entrada
	 * @param codeOperation
	 *            Código de operación
	 * @param trx
	 *            Transaccion
	 * @param bean
	 *            Logging bean
	 * @param mensajeLog
	 * 			mensaje
	 * @return ResponseMessageCicsDTO
	 * @throws ExceptionDataAccess
	 */
	public ResponseMessageCicsDTO ejecutarTransaccion(String mensajeTrama, String codeOperation, String trx,
			Object bean, String mensajeLog) throws ExceptionDataAccess {
		this.info(MIContratosConstantes.MENSAJE_ENTRADA + mensajeTrama);
		final RequestMessageCicsDTO requestDTO = new RequestMessageCicsDTO();
		requestDTO.setCodeOperation(codeOperation);
		requestDTO.setTypeOperation(ConfigFactoryJMS.OPERATION_TYPE_SEND_AND_RECEIVE_MESSAGE);
		requestDTO.setTransaction(trx);
		requestDTO.setMessage(mensajeTrama);

		final ResponseMessageCicsDTO responseDTO = (ResponseMessageCicsDTO) (DataAccess.getInstance(requestDTO, bean))
				.execute("ID_CANAL_CICS");

		this.info(MIContratosConstantes.MENSAJE_SALIDA + responseDTO.getResponseMessage());

		if (!ConfigFactory.CODE_SUCCESFULLY.equals(responseDTO.getCodeError())) {
			this.error(MIContratosConstantes.MENSAJE_ERROR + mensajeLog + responseDTO.getCodeError());
			this.error(responseDTO.getMessageError());
			throw new ExceptionDataAccess(MIContratosConstantes.MENSAJE_ERROR + mensajeLog,
					responseDTO.getCodeError(), responseDTO.getMessageError());
		}

		return responseDTO;
	}

	/**
	 * 
	 * @param mensajeTrama
	 *            Trama de entrada
	 * @param codeOperation
	 *            Código de operación
	 * @param trx
	 *            Transaccion
	 * @param bean
	 *            Logging bean
	 * @param mensajeLog
	 * 			mensaje
	 * @return ResponseMessageCicsDTO
	 * @throws ExceptionDataAccess
	 */
	public ResponseGenerico crearResponse(String mensajeTrama, String codeOperation, String trx, Object bean,
			String mensajeLog) throws ExceptionDataAccess {
		this.info(MIContratosConstantes.MENSAJE_ENTRADA + mensajeTrama);

		final ResponseMessageCicsDTO responseDTO = ejecutarTransaccion(mensajeTrama, codeOperation, trx, bean, mensajeLog);
		this.info(MIContratosConstantes.MENSAJE_RESPUESTA_OK + responseDTO.getCodeError());

		return crearResponse(responseDTO);

	}
	
	/**
	 * 
	 * @param responseDTO
	 * @return ResponseGenerico
	 */
	public ResponseGenerico crearResponse(ResponseMessageCicsDTO responseDTO) {
		ResponseGenerico response = new ResponseGenerico();
		response.setCodError(StringUtils.substring(responseDTO.getResponseMessage(), 8, 17).trim());
		response.setMsgError(StringUtils.substring(responseDTO.getResponseMessage(), 17, responseDTO.getResponseMessage()
				.indexOf(MIContratosConstantes.DELIMITADOR_OBJETO_TRAMA)).trim());
		return response;
	}

}
