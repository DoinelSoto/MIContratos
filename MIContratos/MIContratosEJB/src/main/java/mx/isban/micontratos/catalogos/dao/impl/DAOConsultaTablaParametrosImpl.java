package mx.isban.micontratos.catalogos.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioResponse;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoResponse;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusResponse;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesResponse;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosResponse;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchResponse;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchResult;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioResponse;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIRequest;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.catalogos.dao.DAOConsultaTablaParametros;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.util.SingletonIDA;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasFisicasRequestBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasFisicasResponseBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasMoralesRequestBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasMoralesResponseBean;
import mx.isban.mipersonas.datosgenerales.ejb.BOConsultaDatosPersona;
import mx.isban.mipersonas.localizadores.beans.ConsPersonasBucRequestBean;
import mx.isban.mipersonas.localizadores.beans.ConsPersonasBucResponseBean;
import mx.isban.mipersonas.localizadores.ejb.BOLocalizadorPersonas;

import org.apache.commons.lang.StringUtils;

/**
 * Dao para ejecutar transacciones:MI04, MI05, MI06, MI07,
 * MI39, MI08 Y MI09
 * @author Everis
 *
 */
@Stateless
@Local(DAOConsultaTablaParametros.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaTablaParametrosImpl extends Architech implements DAOConsultaTablaParametros{
	//Código de la transacción MI05
	private static final String MI05 = "MI05";
	//Código de operación de la transacción MI05
	private static final String MI05_CODE_OPERATION = "MI05_CONSULTA_DE_CATALOGO_SUB_ESTATUS";
	//Código de la transacción MI07
	private static final String MI07 = "MI07";
	//Código de operación de la transacción MI07
	private static final String MI07_CODE_OPERATION = "MI07_CONSULTA_LISTA_ARBOLES_TIPO_SERVICIO";
	//Código de la transacción MI06
	private static final String MI06 = "MI06";
	//Código de operación de la transacción MI07
	private static final String MI06_CODE_OPERATION = "MI06_CONSULTA_LISTA_TIPO_SERVICIO";
	//Código de la transacción MI04
	private static final String MI04 = "MI04";
	//Código de operación de la transacción MI04
	private static final String MI04_CODE_OPERATION = "MI04_CONSULTA_DE_CATALOGO_ESTATUS_OPERATIVO";
	//Código de la transacción MI39
	private static final String MI39 = "MI39";
	//Código de la transacción MI09
	private static final String MI09 = "MI09";
	//Código de operación de la transacción MI09
	private static final String MI09_CODE_OPERATION = "MI09_CONSULTA_CATALOGOS_ESTRUCTURALES";
	//Código de operación de la transacción MI08
	private static final String MI08_CODE_OPERATION = "MI08_CONSULTA_ESTADO_DOCUMENTOS";
	//Código de la transacción MI08
	private static final String MI08 = "MI08";
	//Código de operación de la transacción MI39
	private static final String MI39_CODE_OPERATION = "MI39_CODIGOS_BRANCH_ENTIDAD ";
	//JNDI para consumir el BOLocalizadorPersonas
	private static final String JNDI_PERSONAS_NIVELACCESO = "java:global/MIPersonasEAREJB/MIPersonasEJB/BOLocalizadorPersonasImpl!mx.isban.mipersonas.localizadores.ejb.BOLocalizadorPersonas";
	//JNDI para consumir el BOConsultaDatosPersona 
	private static final String JNDI_PERSONAS_FM = "java:global/MIPersonasEAREJB/MIPersonasEJB/BOConsultaDatosPersonaImpl!mx.isban.mipersonas.datosgenerales.ejb.BOConsultaDatosPersona";
	
	/**
	 * Instancia para acceder a los métodos de detalle de persona
	 */
	private BOConsultaDatosPersona boConsultaDatosPersona;
	
	/**
	 * Instancia para acceder al método de localización de personas por buc
	 */
	private BOLocalizadorPersonas boLocalizadorPersonas;
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7445172983800183118L;

	/**
	 * Método que construirá la trama de entrada para la transacción MI04 : Consulta de catalogo de estatus operativo
	 * @param beanCatalogoEstatusOperativoRequest
	 * @param sesion
	 * @return BeanCatalogoEstatusOperativoResultado
	 * @throws ExceptionDataAccess
	 */
	@Override
	public BeanCatalogoEstatusOperativoResultado catalogoEstatusOperativos(BeanCatalogoEstatusOperativoRequest beanCatalogoEstatusOperativoRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess {
		info("DAOConsultaTablaParametrosImpl::consultaCatalogoEstatusOperativo()");
		BeanCatalogoEstatusOperativoResultado resultado=new BeanCatalogoEstatusOperativoResultado();
		//StringBuilder para construír trama
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanCatalogoEstatusOperativoRequest.getCodigoEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCatalogoEstatusOperativoRequest.getRellamada().getMasDatos().toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanCatalogoEstatusOperativoRequest.getRellamada().getMemento(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		//Trama construída
		final String mensajeTrama = trama.toString(); 
		
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI04_CODE_OPERATION, MI04, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		//Código de error de la transacción
		resultado.setCodError(errores.getCodError());
		//Mensaje de error de la transacción
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanCatalogoEstatusOperativoResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
			//Desentramado
			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM+item.length());
			BeanCatalogoEstatusOperativoResponse bean = new BeanCatalogoEstatusOperativoResponse();
			bean.setCodigoEstatus(StringUtils.substring(item,	11	,13).trim());
			bean.setDescripcionEstatus(StringUtils.substring(item,13,	63).trim());
			bean.setIndicadorOperativo(StringUtils.substring(item,63,	64).trim());
			bean.setIndicadorBloqueo(StringUtils.substring(item,64,	65).trim());
			bean.setIndicadorCancelacion(StringUtils.substring(item,65,	66).trim());
			bean.setIndicadorBloqueoLegal(StringUtils.substring(item,66,	67).trim());
			bean.setIndicadorInversion(StringUtils.substring(item,67,	68).trim());			
			bean.setIndicadorDetalle(StringUtils.substring(item,68,	69).trim());		
			bean.setIndicadorBloqueable(StringUtils.substring(item,69,	70).trim());
			bean.setIndicadorCancelable(StringUtils.substring(item,70,	71).trim());
			bean.setIndicadorValidacion(StringUtils.substring(item,71,	72).trim());
			//Se agrega bean a la lista
			response.add(bean);
		}
		if(cadena.length>0){
			resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 72,	73));
			//Se verifica si hay mas datos
			if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
				//Se guarda el memento
				resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],73,	75 ));
			}
		}
		//Se settea la lista
		resultado.setListaCatalogoEstatusOperativo(response);
		}
		return resultado;
	}

	/**
	 * Método que ejecutará el consumo de la transaccion MI05 : Consulta de catálogos de Subestaus operativo
	 * @param beanCatalogosSubEstatusRequest
	 * @param sesion
	 * @return BeanCatalogoSubEstatusResultado
	 * @throws ExceptionDataAccess
	 */
	@Override
	public BeanCatalogoSubEstatusResultado catalogoSubEstatus(BeanCatalogoSubEstatusRequest beanCatalogosSubEstatusRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess {
		BeanCatalogoSubEstatusResultado resultado = new BeanCatalogoSubEstatusResultado();
		info("DAOConsultaTablaParametrosImpl::catalogoSubEstatus()");
		//StringBuilder para construír trama
		final StringBuilder trama = new StringBuilder();
		trama.append(StringUtils.leftPad(beanCatalogosSubEstatusRequest.getCodigoEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCatalogosSubEstatusRequest.getCodigoSubEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCatalogosSubEstatusRequest.getRellamada().getMasDatos().toUpperCase(), 1,MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanCatalogosSubEstatusRequest.getRellamada().getMemento(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		//Trama construída
		final String mensajeTrama = trama.toString(); 

		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI05_CODE_OPERATION, MI05, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		//Código de error de la transacción
		resultado.setCodError(errores.getCodError());
		//Mensaje de error de la transacción
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanCatalogoSubEstatusResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
			//Desentramado
			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM + item);
			BeanCatalogoSubEstatusResponse bean = new BeanCatalogoSubEstatusResponse();
			
			bean.setCodigoEstatus(StringUtils.substring(item, 11,	13).trim());
			bean.setCodigoSubEstatus(StringUtils.substring(item, 13,	15).trim());
			bean.setDescripcionSubEstatus(StringUtils.substring(item, 15,	65).trim());
			//Se añade bean a la lista
			response.add(bean);
		}
		if(cadena.length>0){
			resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 65,	66));
			//Se verifica si hay mas datos
			if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
				//Se gusrda el memento
			resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],66,	70 ));
			}
		}
		//Se settea la lista
		resultado.setListaCatalogoSubEstatus(response);
		}
		return resultado;
	}

	/**
	 * Método que ejecutará el consumo de la transaccion MI06 : Consulta de lista de tipo de servicio
	 * @param beanListaTiposServicioRequest
	 * @param sesion
	 * @return BeanListaTiposServicioResultado
	 * @throws ExceptionDataAccess 
	 */
	@Override
	public BeanListaTiposServicioResultado tipoServicio(BeanListaTiposServicioRequest beanListaTiposServicioRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess {
		BeanListaTiposServicioResultado resultado = new BeanListaTiposServicioResultado();
		info("DAOConsultaTablaParametrosImpl::consultaListaTipoServicio()");
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanListaTiposServicioRequest.getCodigoTipoServicio(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaTiposServicioRequest.getRellamada().getMasDatos().toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanListaTiposServicioRequest.getRellamada().getMemento(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama = trama.toString(); 
		
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI06_CODE_OPERATION, MI06, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanListaTiposServicioResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
	
			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM + item);
			BeanListaTiposServicioResponse bean = new BeanListaTiposServicioResponse();
			bean.setCodigoTipoServicio(StringUtils.substring(item, 11,	13).trim());
			bean.setDescripcionTipoServicio(StringUtils.substring(item, 13,	63).trim());
			bean.setIndicadorAsesoramiento(StringUtils.substring(item, 63,	64).trim());
			bean.setIndicadorNecesidadAsesor(StringUtils.substring(item, 64,	65).trim());
			response.add(bean);
		}
		if(cadena.length>0){
			resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 65,	66));
			if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
			resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],66,	68 ));
			}
		}
		resultado.setListaTiposServicio(response);
		}
		return resultado;
	}


	/**
	 * Método que mapeará el resultado de la ejecución de la consulta MI07 : Consulta de árbol de lista de tipo de servicio
	 * @param beanArbolTiposServicioRequest
	 * @param sesion
	 * @return BeanArbolTiposServicioResultado
	 * @throws ExceptionDataAccess
	 */
	@Override
	public BeanArbolTiposServicioResultado arbolTipoServicio(BeanArbolTiposServicioRequest beanArbolTiposServicioRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess {		
		BeanArbolTiposServicioResultado resultado=new BeanArbolTiposServicioResultado();
		info("DAOConsultaTablaParametrosImpl::arbolTipoServicio()");
		//StringBuilder para construír trama
		final StringBuilder trama = new StringBuilder();
		//Inicia entramado
		trama.append(StringUtils.leftPad(beanArbolTiposServicioRequest.getCodigoTipoCliente(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanArbolTiposServicioRequest.getCodigoPerfilInversion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanArbolTiposServicioRequest.getCodigoTipoServicio(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanArbolTiposServicioRequest.getRellamada().getMasDatos().toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanArbolTiposServicioRequest.getRellamada().getMemento(), 6, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		//Trama construída
		final String mensajeTrama = trama.toString(); 
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI07_CODE_OPERATION, MI07, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		//Código de error de la transacción
		resultado.setCodError(errores.getCodError());
		//Mensaje de error de la transacción
		resultado.setMsgError(errores.getMsgError());
		//Se valida si ocurrió un error
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanArbolTiposServicioResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
			//Desentramado
			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM+ item);
			BeanArbolTiposServicioResponse bean = new BeanArbolTiposServicioResponse();
			bean.setCodigoTipoCliente(StringUtils.substring(item, 11,	13).trim());
			bean.setCodigoPerfilInversion(StringUtils.substring(item, 13,	15).trim());
			bean.setCodigoTipoServicio(StringUtils.substring(item, 15,	17).trim());
			bean.setDescripcionTipoServicio(StringUtils.substring(item, 17,	67).trim());
			bean.setIndicadorAsesoramiento(StringUtils.substring(item, 67, 	68).trim());
			bean.setIndicadorNecesidadAsesor(StringUtils.substring(item, 68,	69).trim());
			//Se añade bean a la lista
			response.add(bean);
		}
		if(cadena.length>0){
			resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 69,	70));
			//Se verifíca si hay mas datos
			if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
				//Se settea el memento
				resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],70,	76 ));
			}
		}
		//Se settea la lista
		resultado.setListaArbolTiposServicio(response);
		}
		return resultado;
	}

	/**
	 * Metódo que ejecutará el consumo de la transacción MI39 : Modificación de catalogos de estatus
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return BeanConsultarBranchResult
	 * @throws ExceptionDataAccess
	 */
	@Override
	public BeanConsultarBranchResult consultaBranch(BeanConsultarBranchRequest beanConsultarBranchRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess {
		this.info("DAOConsultaCatalogosImpl::consultaBranch()");
		BeanConsultarBranchResult respuesta = new BeanConsultarBranchResult();
		//StringBuilder para construír trama
		final StringBuilder trama = new StringBuilder();
		//Inicia entramado
		trama.append(StringUtils.leftPad(beanConsultarBranchRequest.getMulticanalidad().getEmpresaConexion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultarBranchRequest.getMulticanalidad().getCanalOperacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultarBranchRequest.getMulticanalidad().getCanalComercializacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultarBranchRequest.getCodigoBranch(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultarBranchRequest.getCodigoEmpresa(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultarBranchRequest.getRellamada().getMasDatos(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanConsultarBranchRequest.getRellamada().getMemento(), 6, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama =  trama.toString();
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI39_CODE_OPERATION, MI39, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		//Código de error de la transacción
		respuesta.setCodError(errores.getCodError());
		//Mensaje de error de la transacción
		respuesta.setMsgError(errores.getMsgError());
		
		//Se valida si hubo algún error durante la transacción
		if(!respuesta.getCodError().startsWith(MIContratosConstantes.ER)){
			List<BeanConsultarBranchResponse> response = new ArrayList<>();
			//Se aplica un split a la trama de respuesta
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
						
			//Comienza el desentramado
			for (String item : cadena){
				info(MIContratosConstantes.INFORMACION_ITEM + item);
				BeanConsultarBranchResponse bean = new BeanConsultarBranchResponse();
				bean.setCodigoBranch(StringUtils.substring(item,11,	13).trim());
				bean.setCodigoEmpresa(StringUtils.substring(item,13,17).trim());
				bean.setDescripcionEmpresa(StringUtils.substring(item,17,67).trim());
				//Se añade bean a la lista
				response.add(bean);
			}
			if(cadena!=null && cadena.length>0){
				respuesta.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 67,	68));
				//Se verifíca si hay mas datos
				if(respuesta.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
					//Se settea el memento
					respuesta.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],68, 74));
				}
			}
			//Se settea la lista
			respuesta.setLstBranchEntidad(response);
		}
		return respuesta;
	}
	
	/**
	 * Método que 
	 * @param beanTipoNecesidadAFIRequest
	 * @param sesion
	 * @return ConsPersonasBucResponseBean
	 * @throws BusinessException
	 */
	@Override 
	public ConsPersonasBucResponseBean localizadorPersonasBuc(BeanTipoNecesidadAFIRequest beanTipoNecesidadAFIRequest,ArchitechSessionBean sesion) throws BusinessException {
		this.info("DAOConsultaCatalogosImpl::localizadorPersonasBuc");
		this.info("TIPO DE PERSONAS****************:1");
		this.info("TIPO DE PERSONAS****************:2");
		ConsPersonasBucResponseBean response = new ConsPersonasBucResponseBean();
		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			Object obj = ic.lookup(JNDI_PERSONAS_NIVELACCESO);
			obj = PortableRemoteObject.narrow(obj, BOLocalizadorPersonas.class);

			//Valida Instancia de BO
			if(obj instanceof BOLocalizadorPersonas){
				//Obtiente BO
				boLocalizadorPersonas = (BOLocalizadorPersonas) obj; 
			}
			
			ConsPersonasBucRequestBean bean = new ConsPersonasBucRequestBean();
			bean.setBuc(beanTipoNecesidadAFIRequest.getNumeroPersona());
			//Consulta BO
			response = boLocalizadorPersonas.localizacionPersonasPorBUC(null,bean);
		} catch (NamingException e) {
			//Excepcion de JNDI No se encontro el BO
			info(""+e);
			throw new BusinessException(e.getMessage());
		} 
		return response;
	}
	
	/**
	 * PEL1
	 * Transacción para consulta de persona moral.
	 * @param consultaDetPersonasMorales
	 * @return ConsultaDetPersonasMoralesResponseBean
	 * @throws BusinessException
	 */
	@Override
	public ConsultaDetPersonasMoralesResponseBean consultaPersonaMoral(ConsultaDetPersonasMoralesRequestBean consultaDetPersonasMorales)throws BusinessException{
		this.info("CONSULTAR PERSONA MORAL");
		ConsultaDetPersonasMoralesResponseBean response = new ConsultaDetPersonasMoralesResponseBean();
		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			Object obj = ic.lookup(JNDI_PERSONAS_FM);
			obj = PortableRemoteObject.narrow(obj, BOConsultaDatosPersona.class);
			//Valida Instancia de BO
			if(obj instanceof BOConsultaDatosPersona){
				//Obtiente BO
				boConsultaDatosPersona = (BOConsultaDatosPersona) obj; 
			}
			//Consulta BO
			response = boConsultaDatosPersona.consultaDetallePersonasMorales(null,consultaDetPersonasMorales);
		}catch (NamingException e) {
			info(""+e);
			throw new BusinessException(e.getMessage());
		} 
		return response;
	}
	
	/**
	 * PEL2
	 * Transacción para consulta de persona física y física con actividad empresarial
	 * @param consultaDetPersonasFisicas
	 * @return ConsultaDetPersonasFisicasResponseBean
	 * @throws BusinessException
	 */
	@Override
	public ConsultaDetPersonasFisicasResponseBean consultaPersonaFisicas(ConsultaDetPersonasFisicasRequestBean consultaDetPersonasFisicas) throws BusinessException{
		this.info("CONSULTA PERSONA FISICA");
		ConsultaDetPersonasFisicasResponseBean response = new ConsultaDetPersonasFisicasResponseBean();
		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			Object obj = ic.lookup(JNDI_PERSONAS_FM);
			obj = PortableRemoteObject.narrow(obj, BOConsultaDatosPersona.class);
			//Valida Instancia de BO
			if(obj instanceof BOConsultaDatosPersona){
				//Obtiente BO
				boConsultaDatosPersona = (BOConsultaDatosPersona) obj; 
			}
			//COnsulta BO
			response = boConsultaDatosPersona.consultaDetallePersonasFisicas(null,consultaDetPersonasFisicas);
			return response;
		}catch (NamingException e) {
			info(""+e);
			throw new BusinessException(e.getMessage());
		} catch (BusinessException e) {
			info(""+e);
			throw new BusinessException(e.getCode(),e.getMessage());
		}
	}
	
	/**
	 * Método que hará el llamado para la transacción MI08 : Consulta de estado de documentos
	 * @param beanConsultaEstadoDocumentosRequest
	 * @return BeanConsultaEstadoDocumentosResultado
	 * @throws ExceptionDataAccess
	 */
	@Override
	public BeanConsultaEstadoDocumentosResultado consultaEstadoDocumentos(BeanConsultaEstadoDocumentosRequest beanConsultaEstadoDocumentosRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		BeanConsultaEstadoDocumentosResultado resultado = new BeanConsultaEstadoDocumentosResultado();
		info("DAOConsultaTablaParametrosImpl::consultaEstadoDocumentos()");
		//StringBuilder para contruír trama
		final StringBuilder trama = new StringBuilder();
		//Inicia entramado
		trama.append(StringUtils.leftPad(beanConsultaEstadoDocumentosRequest.getCodigoEstadoDocumento(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaEstadoDocumentosRequest.getRellamada().getMasDatos().toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanConsultaEstadoDocumentosRequest.getRellamada().getMemento(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
			
		final String mensajeTrama = trama.toString(); 
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI08_CODE_OPERATION, MI08, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		//Código de error de la transacción
		resultado.setCodError(errores.getCodError());
		//Mensaje de error de la transacción
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanConsultaEstadoDocumentosResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
			//Desentramado
			for (String item : cadena){
				info(MIContratosConstantes.INFORMACION_ITEM+ item);
				BeanConsultaEstadoDocumentosResponse bean = new BeanConsultaEstadoDocumentosResponse();			
				bean.setNumeroOcurrencias(StringUtils.substring(item, 11, 14).trim());
				bean.setCodigoEstadoDocumento(StringUtils.substring(item, 14, 16).trim());
				bean.setDescripcionEstadoDocumento(StringUtils.substring(item, 16, 66).trim());
				//Se añade bean a la lista
				response.add(bean);
		}
		if(cadena.length>0){
			resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 66,	67));
			//Se verifíca si hay mas datos
			if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
				//Se settea el memento
				resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],67, 69));
			}
		}
		//Se settea la lista
		resultado.setListaConsultaEstadoDocumento(response);
		}
		return resultado;
		
	}

	/**
	 * Método que define la función que ejecutará el consumo de la transacción MI09 : Consulta de catalogos estructurales
	 * @param beanCatalogosEstructuralesRequest
	 * @param session
	 * @return BeanCatalogosEstructuralesResultado
	 * @throws ExceptionDataAccess
	 */
	@Override
	public BeanCatalogosEstructuralesResultado consultaCatalogosEstructurales(BeanCatalogosEstructuralesRequest beanCatalogosEstructuralesRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		BeanCatalogosEstructuralesResultado resultado = new BeanCatalogosEstructuralesResultado();
		info("DAOConsultaTablaParametrosImpl::consultaCatalogosEstructurales()");
		//StringBuilder para construír la trama
		final StringBuilder trama = new StringBuilder();
		//Inicia entramado
		trama.append(StringUtils.leftPad(beanCatalogosEstructuralesRequest.getCodigoCatalogo(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCatalogosEstructuralesRequest.getCodigoEstructuralInversion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCatalogosEstructuralesRequest.getRellamada().getMasDatos().toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanCatalogosEstructuralesRequest.getRellamada().getMemento(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		//Trama construída
		final String mensajeTrama = trama.toString(); 
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI09_CODE_OPERATION, MI09, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		//Código de error de la transacción
		resultado.setCodError(errores.getCodError());
		//Mensaje de error de la transacción
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanCatalogosEstructuralesResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
			//Desentramado
			for (String item : cadena){
				info(MIContratosConstantes.INFORMACION_ITEM+ item);
				BeanCatalogosEstructuralesResponse bean = new BeanCatalogosEstructuralesResponse();
			
				bean.setCodigoEstructuralInversion(StringUtils.substring(item, 11, 13).trim());
				bean.setDescripcionEstructuralInversion(StringUtils.substring(item, 13, 63).trim());
				//Se añade bean a la lista
				response.add(bean);
		}
		if(cadena.length>0){
			resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 63,	64));
			//Se verifíca si hay más datos
			if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
				//Se settea el memento
				resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],64,	66 ));
			}
		}
		//Se settea la lista
		resultado.setListaCatalogosEstructurales(response);
		}
		return resultado;
	}
}