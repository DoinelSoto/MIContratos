package mx.isban.micontratos.gestionprecontratos.dao;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanBajaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPreContratoRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPrecontratoResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanModificacionTablaAuxiliarRequest;


/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * interfaz que define los métodos en cuya implementación se ejecutarán el consumo de las transacciones de gestión de precontratos
 *
 */
public interface DAOGestionPreContratos {
	

	/**
	 * Método que ejecutará el consumo de la transacción MI10 : Alta en tabla auxiliar
	 * @param beanAltaTablaAuxiliarRequestPrueba
	 * @param session
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess 
	 */
	ResponseGenerico altaDatosAuxiliar(BeanAltaTablaAuxiliarRequest beanAltaTablaAuxiliarRequestPrueba, ArchitechSessionBean session) throws ExceptionDataAccess;


	/**
	 * Método que ejecutará el consumo de la transacción MI11 : Alta de precontrato global
	 * @param beanAltaPreContratoGlobalPrueba
	 * @param session
	 * @return BeanAltaPreContratoGlobalResultado
	 */
	BeanAltaPreContratoGlobalResultado altaFolioPreContrato(BeanAltaPreContratoGlobalRequest beanAltaPreContratoGlobalPrueba,ArchitechSessionBean session) throws ExceptionDataAccess;

	
	/**
	 * Método que ejecutará el consumo de la transacción MI12 : Modificación de tabla auxiliar
	 * @param beanModificacionTablaAuxiliarRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico modificacionDatosAuxiliar(BeanModificacionTablaAuxiliarRequest beanModificacionTablaAuxiliarRequest,ArchitechSessionBean session) throws ExceptionDataAccess;


	/**
	 * Método que ejecutará el consumo de la transacción MI13 : Baja de tabla auxiliar
	 * @param beanBajaTablaAuxiliarRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico bajaDatosAuxiliar(BeanBajaTablaAuxiliarRequest beanBajaTablaAuxiliarRequest,ArchitechSessionBean session) throws ExceptionDataAccess;


	/**
	 * Método que ejecutará el consumo de la transacción MI14 : Consulta de lista de precontrato
	 * @param beanListaPreContratoRequest
	 * @param session
	 * @return BeanListaPrecontratoResultado
	 */
	BeanListaPrecontratoResultado consultaPreContratos(BeanListaPreContratoRequest beanListaPreContratoRequest, ArchitechSessionBean session) throws ExceptionDataAccess;

	
	
	/**
	 * Método que ejecutará el consumo de la transacción MI15 : Consulta de tabla auxiliar
	 * @param beanConsultaTablaAuxiliar
	 * @param session
	 * @return BeanConsultaTablaAuxiliarResultado
	 */
	BeanConsultaTablaAuxiliarResultado detallePreContratos(BeanConsultaTablaAuxiliarRequest beanConsultaTablaAuxiliar, ArchitechSessionBean session) throws ExceptionDataAccess;
	
}
