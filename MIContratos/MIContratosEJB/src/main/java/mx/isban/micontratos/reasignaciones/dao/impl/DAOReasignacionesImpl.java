package mx.isban.micontratos.reasignaciones.dao.impl;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.reasignaciones.BeanAltaSubEstatusRequest;
import mx.isban.micontratos.reasignaciones.dao.DAOReasignaciones;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.SingletonIDA;

import org.apache.commons.lang.StringUtils;

/**
 * @author everis
 * Clase con los métodos que consumirán las transacciones : MI22
 */
@Stateless
@Local(DAOReasignaciones.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOReasignacionesImpl extends Architech implements DAOReasignaciones{
	//Código de la transacción MI22
	private static final String MI22 = "MI22";
	//Código de operación de la transacción MI22
	private static final String MI22_CODE_OPERATION = "MI22_ALTA_SUBESTATUS";
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 9062306226660675139L;

	/**
	 *  Método que define la función que ejecutará el consumo de la transacción MI22 : alta de subestatus
	 * @param beanAltaSubEstatusResultado
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess
	 */
	@Override
	public ResponseGenerico traspasoEjecutivo(BeanAltaSubEstatusRequest beanAltaSubEstatusRequest,ArchitechSessionBean session) throws ExceptionDataAccess{
		this.info("DAOReasignacionesImpl::traspasoEjecutivo()");	
		//Se llama al método que devolverá la trama construída
		final String mensajeTrama = construirTramaEntradaAltaSubEstatus(beanAltaSubEstatusRequest);		
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI22_CODE_OPERATION, MI22, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}

	/**
	 * Método que construye la trama de entrada para la transacción MI22 : Alta de estatus
	 * @param beanAltaSubEstatusRequest
	 * @return String
	 */
	private String construirTramaEntradaAltaSubEstatus(
			BeanAltaSubEstatusRequest beanAltaSubEstatusRequest) {
		//StringBuilder para construír trama
		final StringBuilder tramaEntrada = new StringBuilder();
		//Inicia entramado
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getMulticanalidad().getEmpresaConexion(),4 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getMulticanalidad().getCanalOperacion(),2 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getMulticanalidad().getCanalComercializacion(),2 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getTipoConsulta(),2 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getEmpresaContrato(),4 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getCentroContrato(),4 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.rightPad(beanAltaSubEstatusRequest.getNumeroContrato(),12 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getCodigoProducto(),2 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getCodigoSubProducto(),4 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getIdentificadorEmpresaContrato(),4 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.rightPad(beanAltaSubEstatusRequest.getDescripcionAlias(),15 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getIdCliente(),8 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.rightPad(beanAltaSubEstatusRequest.getNuevoEjecutivo(),8 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanAltaSubEstatusRequest.getNuevoCentroCostoContrato(),4 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		//Retornar trama construída
		return tramaEntrada.toString();
	}
}