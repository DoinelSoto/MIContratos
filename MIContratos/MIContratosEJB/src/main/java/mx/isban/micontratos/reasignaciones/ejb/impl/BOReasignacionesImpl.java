package mx.isban.micontratos.reasignaciones.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.reasignaciones.BeanAltaSubEstatusRequest;
import mx.isban.micontratos.reasignaciones.dao.DAOReasignaciones;
import mx.isban.micontratos.reasignaciones.ejb.BOReasignaciones;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosValidator;

/**
 * @author everis
 * Classe con los métodos que harán el llamado a los métodos que
 * consumirán las transacciones: MI22
 */
@Stateless
@Remote(BOReasignaciones.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOReasignacionesImpl extends Architech implements BOReasignaciones{
	
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -8240271014092658528L;
	
	/**
	 * Instancia de DAOReasignaciones
	 */
	@EJB
	private DAOReasignaciones daoReasignaciones;
	
	/**
	 * Llamado a el consumo de la transacción MI22 : alta de subestatus
	 * @param beanAltaSubEstatusRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws BusinessException
	 */
	@Override
	public ResponseGenerico traspasoEjecutivo(BeanAltaSubEstatusRequest beanAltaSubEstatusRequest, ArchitechSessionBean session )  throws BusinessException {
		//Campos obligatorios
		String [] camposObligatorios={beanAltaSubEstatusRequest.getTipoConsulta(),beanAltaSubEstatusRequest.getNuevoEjecutivo(),
				beanAltaSubEstatusRequest.getNuevoCentroCostoContrato()};
		//Longitudes de campos obligatorios
		int [] longitudes ={2,8,4};
		//Campos no obligatorios
		String [] camposNoObligatorios={beanAltaSubEstatusRequest.getIdCliente(),beanAltaSubEstatusRequest.getIdentificadorEmpresaContrato(),
				beanAltaSubEstatusRequest.getDescripcionAlias(),beanAltaSubEstatusRequest.getMulticanalidad().getCanalComercializacion(),
				beanAltaSubEstatusRequest.getMulticanalidad().getCanalOperacion(), beanAltaSubEstatusRequest.getMulticanalidad().getEmpresaConexion(),
				beanAltaSubEstatusRequest.getEmpresaContrato(),beanAltaSubEstatusRequest.getCentroContrato(),
				beanAltaSubEstatusRequest.getNumeroContrato(),beanAltaSubEstatusRequest.getCodigoProducto(),
				beanAltaSubEstatusRequest.getCodigoSubProducto()};
		//Longitudes de campos no obligatorios
		int [] longitudesCamposNoObligatorios={8,4,15,2,2,4,4,4,12,2,4};
		
		try {
			//Se validan campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			//Se validan campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesCamposNoObligatorios);
			//Se hace el llamado al método que consumirá la transacción MI22
			ResponseGenerico response=daoReasignaciones.traspasoEjecutivo(beanAltaSubEstatusRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + " la ejecución de la transacción.");
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
}