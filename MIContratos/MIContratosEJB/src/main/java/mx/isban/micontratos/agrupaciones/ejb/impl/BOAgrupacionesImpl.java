package mx.isban.micontratos.agrupaciones.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.agrupaciones.dao.DAOConsultaAgrupaciones;
import mx.isban.micontratos.agrupaciones.ejb.BOConsultaAgrupaciones;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanBajaContratosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorResultado;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosResultado;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosValidator;

/**
 * @author everis
 *	Interfaz que consumira los métodos que consumen las transacción relacionadas a Agrupaciones 
 */

@Stateless
@Remote(BOConsultaAgrupaciones.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOAgrupacionesImpl extends Architech implements BOConsultaAgrupaciones{
	
	@EJB
	private DAOConsultaAgrupaciones daoConsultaAgrupaciones;

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -8725149233569691373L;

	/**
	 * Método que ejecutará el consumo de la transacción MI33 : alta de contrato agrupador
	 * @param beanAltaContratoAgrupadorRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws BusinessException
	 */
	@Override
	public ResponseGenerico altaContratoAgrupador(BeanAltaContratoAgrupadorRequest beanAltaContratoAgrupadorRequest,ArchitechSessionBean session) throws BusinessException{
		
				//Campos oligatorios
				String[] camposObligatorios = {
						beanAltaContratoAgrupadorRequest.getTipoConsulta(), 
						beanAltaContratoAgrupadorRequest.getTipoAgrupacion(), 	
						beanAltaContratoAgrupadorRequest.getFechaComienzoGrupo(), 	
						beanAltaContratoAgrupadorRequest.getFechaFinGrupo()};
				
				//Longitudes de campos no obligatorios
				int[] longitudesObligatorias = {2,3,10,10};
				
				//Campos no oligatorios
				String[] camposNoObligatorios = {
						beanAltaContratoAgrupadorRequest.getMulticanalidad().getEmpresaConexion(), 		
						beanAltaContratoAgrupadorRequest.getMulticanalidad().getCanalOperacion(), 		
						beanAltaContratoAgrupadorRequest.getMulticanalidad().getCanalComercializacion(), 
						beanAltaContratoAgrupadorRequest.getEmpresaContrato(), 			
						beanAltaContratoAgrupadorRequest.getCentroContrato(), 			
						beanAltaContratoAgrupadorRequest.getNumeroContrato(), 			
						beanAltaContratoAgrupadorRequest.getCodigoProducto(), 			
						beanAltaContratoAgrupadorRequest.getCodigoSubProducto(), 		
						beanAltaContratoAgrupadorRequest.getEmpresaContratoInversion(), 
						beanAltaContratoAgrupadorRequest.getAliasContrato(), 			
						beanAltaContratoAgrupadorRequest.getNumeroTelefonico(),						
						beanAltaContratoAgrupadorRequest.getMonedaGrupo(),
						beanAltaContratoAgrupadorRequest.getCodigoEjecutivo(), 	
						beanAltaContratoAgrupadorRequest.getNombreGrupo(), 			
						beanAltaContratoAgrupadorRequest.getDescripcionGrupo(), 	
						beanAltaContratoAgrupadorRequest.getIndicadorTipoGrupo(), 	
						beanAltaContratoAgrupadorRequest.getCodigoGrupoUsuario(),
						beanAltaContratoAgrupadorRequest.getIndicadorUsoGrupo()};
		//Longitudes de campos no obligatorios
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,10,3,8,30,50,1,8,1};
		try {
			//Se valida la longitud de los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			//Se valida la longitud de los campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			//Se manda llamar el método que ejecutará la transacción
			//Se obtiene el response de la transacción
			ResponseGenerico response=daoConsultaAgrupaciones.altaContratoAgrupador(beanAltaContratoAgrupadorRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			//Se retorna el response
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
	
	/**
	 * Método que ejecutará el consumo de la transacción MI32 : Consulta de lista de contratos hijos
	 * @param beanListaContratoAgrupadorRequest
	 * @param session
	 * @return BeanListaContratoAgrupadorResultado
	 * @throws BusinessException
	 */
	@Override
	public BeanListaContratoAgrupadorResultado listaContratosAgrupadores(BeanListaContratoAgrupadorRequest beanListaContratoAgrupadorRequest, ArchitechSessionBean session)throws BusinessException {
		// Campos Obligatorios
		String[] camposObligatorios = { 
				beanListaContratoAgrupadorRequest.getTipoAgrupacion()};
		//Longitudes de campos no obligatorios
		int[] longitudesObligatorias = {3};
		//Campos no obligatorios
		String[] camposNoObligatorios = { 
				beanListaContratoAgrupadorRequest.getMulticanalidad().getEmpresaConexion(),		
				beanListaContratoAgrupadorRequest.getMulticanalidad().getCanalOperacion(),		
				beanListaContratoAgrupadorRequest.getMulticanalidad().getCanalComercializacion(),
				beanListaContratoAgrupadorRequest.getTipoConsulta(),
				beanListaContratoAgrupadorRequest.getEmpresaContrato(),				
				beanListaContratoAgrupadorRequest.getCentroContrato(),				
				beanListaContratoAgrupadorRequest.getNumeroContrato(),				
				beanListaContratoAgrupadorRequest.getCodigoProductoContrato(),		
				beanListaContratoAgrupadorRequest.getCodigoSubProductoContrato(),	
				beanListaContratoAgrupadorRequest.getEmpresaContratoInversion(),  	
				beanListaContratoAgrupadorRequest.getAliasContrato(),				
				beanListaContratoAgrupadorRequest.getRellamada().getMasDatos(),
				beanListaContratoAgrupadorRequest.getRellamada().getMemento()};
		//Longotudes de campos no obligatorios

		int[] longitudesNoObligatorias = {4,2,2,2,4,4,12,2,4,4,15,1,45};
		try {
			//Se validan las longitudes de los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			//Se validan las longitudes de los campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			//Se manda llamar el método que ejecutará la transacción
			//Se obtiene el bean resultado de la transacción
			BeanListaContratoAgrupadorResultado response=daoConsultaAgrupaciones.listaContratosAgrupadores(beanListaContratoAgrupadorRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			//Se retorna el response
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI35 : Alta contratos hijos
	 * @param beanAltaContratosHijosRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws BusinessException
	 */
	@Override
	public ResponseGenerico altaContratoAgrupado(BeanAltaContratosHijosRequest beanAltaContratosHijosRequest,ArchitechSessionBean session) throws BusinessException {
		//Campos obligatorios
		String[] camposObligatorios = { 
				beanAltaContratosHijosRequest.getTipoConsulta(), 
				beanAltaContratosHijosRequest.getTipoAgrupacion()};
		//Longituudes de campos obligatorios
		int[] longitudesObligatorias = {2,3};
		//Campos no obligatorios
		String[] camposNoObligatorios = { 
				beanAltaContratosHijosRequest.getMulticanalidad().getEmpresaConexion(), 		
				beanAltaContratosHijosRequest.getMulticanalidad().getCanalOperacion(), 			
				beanAltaContratosHijosRequest.getMulticanalidad().getCanalComercializacion(), 	
				beanAltaContratosHijosRequest.getEmpresaContrato(), 							
				beanAltaContratosHijosRequest.getCentroContrato(), 								
				beanAltaContratosHijosRequest.getNumeroContrato(), 								
				beanAltaContratosHijosRequest.getCodigoProducto(), 								
				beanAltaContratosHijosRequest.getCodigoSubProducto(), 							
				beanAltaContratosHijosRequest.getEmpresaContratoInversion(),					
				beanAltaContratosHijosRequest.getAliasContrato(), 								
				beanAltaContratosHijosRequest.getEmpresaContratoAgrupador(), 					
				beanAltaContratosHijosRequest.getCentroContratoAgrupado(), 						
				beanAltaContratosHijosRequest.getNumeroContratoAgrupado(), 						
				beanAltaContratosHijosRequest.getCodigoProductoAgrupado(), 						
				beanAltaContratosHijosRequest.getCodigoSubProductoAgrupado(), 					
				beanAltaContratosHijosRequest.getEmpresaContratoInversionAgrupado(), 		
				beanAltaContratosHijosRequest.getAliasContratoAgrupado()};
		//Longituudes de campos no obligatorios
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,4,4,12,2,4,4,15};
		try {
			//Se valida la longitud de los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			//Se valida la longitud de los campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			//Se manda llamar el método que ejecutará la transacción
			//Se obtiene el response de la transacción
			ResponseGenerico response = daoConsultaAgrupaciones.altaContratoAgrupado(beanAltaContratosHijosRequest,session);
			//Se valida código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			//Se retorna el response
			return response;
		} catch (ExceptionDataAccess e) {
			error("", e);
			throw new BusinessException(e.getCode(), e.getMessage());
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI36 : Baja de contratos hijos
	 * @param beanBajaContratosRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	@Override
	public ResponseGenerico bajaContratoAgrupado(BeanBajaContratosRequest beanBajaContratosRequest,ArchitechSessionBean session)throws BusinessException {
		//Campos obligatorios
		String[] camposObligatorios = { 
				beanBajaContratosRequest.getTipoConsulta(), 	
				beanBajaContratosRequest.getTipoAgrupacion()};
		//Longitudes de campos obligatorios
		int[] longitudesObligatorias = {2,3};
		//Campos no obligatorios
		String[] camposNoObligatorios = { 
				beanBajaContratosRequest.getMulticanalidad().getEmpresaConexion(), 				
				beanBajaContratosRequest.getMulticanalidad().getCanalOperacion(), 				
				beanBajaContratosRequest.getMulticanalidad().getCanalComercializacion(), 		
				beanBajaContratosRequest.getEmpresaContrato(), 									
				beanBajaContratosRequest.getCentroContrato(), 									
				beanBajaContratosRequest.getNumeroContrato(), 									
				beanBajaContratosRequest.getCodigoProducto(), 									
				beanBajaContratosRequest.getCodigoSubProducto(), 								
				beanBajaContratosRequest.getEmpresaContratoInversion(), 						
				beanBajaContratosRequest.getAliasContrato(), 									
				beanBajaContratosRequest.getEmpresaContratoAgrupado(),							
				beanBajaContratosRequest.getCentroContratoAgrupado(), 							
				beanBajaContratosRequest.getNumeroContratoAgrupado(), 							
				beanBajaContratosRequest.getCodigoProductoAgrupado(), 							
				beanBajaContratosRequest.getCodigoSubProductoAgrupado(),						
				beanBajaContratosRequest.getEmpresaContratoAgrupado(),							
				beanBajaContratosRequest.getAliasContratoAgrupado(),
				beanBajaContratosRequest.getEmpresaContratoInversionAgrupado()};
		//Longitudes de campos no obligatorios
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,4,4,12,2,4,4,15,4};
		try {
			//Se valida la longitud de los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			//Se valida la longitud de los campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			//Se manda llamar el método que ejecutará la transacción
			//Se obtiene el response de la transacción
			ResponseGenerico response=daoConsultaAgrupaciones.bajaContratoAgrupado(beanBajaContratosRequest, session);
			//Se valida código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			//Se retorna el response
			return response; 
		} catch (ExceptionDataAccess e) {
			error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI34 : Consulta de lista de contratos hijos
	 * @param beanListaContratosHijosRequest
	 * @param session
	 * @return BeanListaContratosHijosResultado
	 * @throws BusinessException
	 */
	@Override
	public BeanListaContratosHijosResultado listarContratosAgrupados(BeanListaContratosHijosRequest beanListaContratosHijosRequest, ArchitechSessionBean session)throws BusinessException {
		//Campos obligatorios
		String[] camposObligatorios = { 
						beanListaContratosHijosRequest.getTipoConsulta()};
		//Longitudes de campos obligatorios
		int[] longitudesObligatorias = {2};
		//Campos no obligatorios
		String[] camposNoObligatorios = { 
				beanListaContratosHijosRequest.getMulticanalidad().getEmpresaConexion(), 		
				beanListaContratosHijosRequest.getMulticanalidad().getCanalOperacion(), 		
				beanListaContratosHijosRequest.getMulticanalidad().getCanalComercializacion(), 	
				beanListaContratosHijosRequest.getEmpresaContrato(), 							
				beanListaContratosHijosRequest.getCentroContrato(), 							
				beanListaContratosHijosRequest.getNumeroContrato(), 							
				beanListaContratosHijosRequest.getCodigoProducto(), 							
				beanListaContratosHijosRequest.getCodigoSubProducto(), 							
				beanListaContratosHijosRequest.getEmpresaContratoInversion(), 					
				beanListaContratosHijosRequest.getAliasContrato(), 								
				beanListaContratosHijosRequest.getTipoAgrupacion(), 							
				beanListaContratosHijosRequest.getRellamada().getMasDatos(), 	
				beanListaContratosHijosRequest.getRellamada().getMemento()};
		//Longitudes de campos no obligatorios
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,3,1,45};
		try {
			//Se validan las longitudes de los campos obligatorios

			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);

			//Se validan las longitudes de los campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			//Se manda llamar el método que ejecutará la transacción
			//Se obtiene en bean resultado de la transacción
			BeanListaContratosHijosResultado response=daoConsultaAgrupaciones.listarContratosAgrupados(beanListaContratosHijosRequest, session);
			//Se valida código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			//Se retorna el response
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
}
