package mx.isban.micontratos.gestioncontratos.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.ConfigFactory;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanCambioNumeroContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanDatosDomicilioResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContactosResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaIntervinientesResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanMantenimientoDatosGeneralesRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionEstatusContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionIndicadorEstadoCuentaRequest;
import mx.isban.micontratos.gestioncontratos.dao.DAOGestionContratos;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.util.SingletonIDA;
import mx.isban.micontratos.validator.MIContratosValidator;

import org.apache.commons.lang.StringUtils;

/**
 * @author everis
 *
 */

@Stateless
@Local(DAOGestionContratos.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGestionContratosImpl extends Architech implements DAOGestionContratos{

	private static final String MI16 = "MI16";

	private static final String MI16_CODE_OPERATION = "MI16_CONSULTA_INVERSION";

	private static final String MI37 = "MI37";

	private static final String MI37_CODE_OPERATION = "MI37_MODIFICACION_INDICADOR_ESTADO_CUENTA";

	private static final String MI31 = "MI31";

	private static final String MI31_CODE_OPERATION = "MI31_MANTENIMIENTO_DATOS_GENERALES";

	private static final String MI24 = "MI24";

	private static final String MI24_CODE_OPERATION = "MI24_CONSULTA_MODIFICACIONES_CONTRATO";

	private static final String MI23 = "MI23";

	private static final String MI23_CODE_OPERATION = "MI23_ALTA_CONTRATO_INVERSION_AMPARO_CHEQUERA";

	private static final String MI21 = "MI21";

	private static final String MI21_CODE_OPERATION = "MI21_CAMBIO_NUMERO_CONTRATO";

	private static final String MI19 = "MI19";

	private static final String MI19_CODE_OPERATION = "MI19_MODIFICACION_CONTRATO_INVERSION";

	private static final String MI18 = "MI18";

	private static final String MI18_CODE_OPERATION = "MI18_ALTA_CONTRATO_INVERSION";
	
	private static final String MI40 = "MI40";

	private static final String MI40_CODE_OPERATION = "MI40_LISTA_CONTRATOS_INVERSION";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3075683631005262741L;
	
	
	/**
	 * Método que ejecutará el consumo de la transacción MI16 : Consulta de inversión
	 */
	@Override
	public BeanConsultaInversionResultado consultaDatosContrato(BeanConsultaInversionRequest beanConsultaInversionRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		BeanConsultaInversionResultado resultado = new BeanConsultaInversionResultado();
		info("DAOGestionContratosImpl::consultaInversion()");
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getTipoConsulta(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getCentroContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getNumeroContrato(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getCodigoProducto(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getCodigoSubProducto(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getEmpresaContratoInversion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanConsultaInversionRequest.getAliasContrato(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getIndicadorInformacionIntervinientes().toUpperCase(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getIndicadorInformacionContactos().toUpperCase(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getIndicadorUsoDomicilios().toUpperCase(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getIndicadorInformacionContratosRelacionados().toUpperCase(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getIndicadorInformacionReferenciaContrato().toUpperCase(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaInversionRequest.getFechaConsulta(),	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));

			
		final String mensajeTrama = trama.toString(); 
		
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI16_CODE_OPERATION, MI16, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanConsultaInversionResponse> response = new ArrayList<>();
			
			List<BeanListaIntervinientesResponse> listaIntervinientes = new ArrayList<>();
			List<BeanDatosDomicilioResponse> listaDomicilios = new ArrayList<>();
			List<BeanListaContactosResponse> listaContactos = new ArrayList<>();
			
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
		
			int posicionActualIntervinientes=801;
			int posicionActualContactos = 1194;
			int posicionActualDomicilios = 1297;
			
			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM+item.length());	
			BeanConsultaInversionResponse bean = new BeanConsultaInversionResponse();
			
			bean.getBeanDatosContratos().setCodigoEmpresa(StringUtils.substring(item,														11	,	15).trim());
			bean.getBeanDatosContratos().setCentroContrato(StringUtils.substring(item,														15	,	19).trim());
			bean.getBeanDatosContratos().setNumeroContrato(StringUtils.substring(item,														19	,	31).trim());
			bean.getBeanDatosContratos().setCodigoProducto(StringUtils.substring(item,														31	,	33).trim());
			bean.getBeanDatosContratos().setCodigoSubProductoContrato(StringUtils.substring(item,											33	,	37).trim());
			bean.getBeanDatosContratos().setCodigoMonedaContrato(StringUtils.substring(item,												37	,	40).trim());
			bean.getBeanDatosContratos().setNumeroFolio(Integer.valueOf(StringUtils.substring(item,											40	,	49).trim()));
			bean.getBeanDatosContratos().setCodigoEmpresaContrato(StringUtils.substring(item,												49	,	53).trim());
			bean.getBeanDatosContratos().setCodigoBanca(StringUtils.substring(item,															53	,	56).trim());
			bean.getBeanDatosContratos().setFechaApertura(StringUtils.substring(item,														56	,	66).trim());
			bean.getBeanDatosContratos().setDescripcionAlias(StringUtils.substring(item,													66	,	81).trim());
			bean.getBeanDatosContratos().setFechaVencimiento(StringUtils.substring(item,													81	,	91).trim());
			bean.getBeanDatosContratos().setCodigoEjecutivo(StringUtils.substring(item,														91	,	99).trim());
			bean.getBeanDatosContratos().setCentroCostosContrato(StringUtils.substring(item,												99	,	103).trim());
			bean.getBeanDatosContratos().setIndicadorTipoCuenta(StringUtils.substring(item,                            						103	,	104).trim());
			bean.getBeanDatosContratos().setIndicadorTipoContrato(StringUtils.substring(item,												104	,	105).trim());
			bean.getBeanDatosContratos().setIndicadorAutorizacionPosicionCortoCapitales(StringUtils.substring(item,							105	,	106).trim());
			bean.getBeanDatosContratos().setIndicadorAutorizacionPosicionCortoFondos(StringUtils.substring(item,							106	,	107).trim());
			bean.getBeanDatosContratos().setIndicadorAutorizacionCruceFondosInversion(StringUtils.substring(item,							107	,	108).trim());
			bean.getBeanDatosContratos().setIndicadorDiscrecionalidad(StringUtils.substring(item,											108	,	109).trim());
			bean.getBeanDatosContratos().setIndicadorTipoCustodaDirecto(StringUtils.substring(item,											109	,	110).trim());
			bean.getBeanDatosContratos().setIndicadorTipoCustodiaRepos(StringUtils.substring(item,											110	,	111).trim());
			bean.getBeanDatosContratos().setIndicadorEstadoCuenta(StringUtils.substring(item,												111	,	112).trim());
			bean.getBeanDatosContratos().setImporteMaximoOperacion(MIContratosValidator.validarCamposFlotante(StringUtils.substring(item,	112	,	127)).trim());
			bean.getBeanDatosContratos().setDivisaMaximaOperacion(StringUtils.substring(item,												127	,	130).trim());
			bean.getBeanDatosContratos().setIndicadorEnvioCorreoCartaConfirmacionCapitales(StringUtils.substring(item,						130	,	131).trim());
			bean.getBeanDatosContratos().setEstatusContrato(StringUtils.substring(item,														131	,	133).trim());
			bean.getBeanDatosContratos().setDetalleEstatus(StringUtils.substring(item,														133	,	135).trim());
			bean.getBeanDatosContratos().setFechaEstado(StringUtils.substring(item,															135	,	145).trim());
			bean.getBeanDatosContratos().setIndicadorContratoAgrupador(StringUtils.substring(item,											145	,	146).trim());
			bean.getBeanDatosContratos().setIndicadorTipoPosicion(StringUtils.substring(item,												146	,	147).trim());
			bean.getBeanDatosContratos().setIndicadorContratoEspejo(StringUtils.substring(item,												147	,	148).trim());
			bean.getBeanDatosContratos().setIndicadorContratoChequera(StringUtils.substring(item,											148	,	149).trim());
			bean.getBeanDatosContratos().setCodigoTipoServicio(StringUtils.substring(item,													149	,	151).trim());
			bean.getBeanDatosContratos().setCodigoAsesorAfinanciero(StringUtils.substring(item,												151	,	159).trim());
			bean.getBeanDatosContratos().setIndicadorBeneficiarios(StringUtils.substring(item,												159	,	160).trim());
			bean.getBeanDatosContratos().setIndicadorProveedoresRecursos(StringUtils.substring(item,										160	,	161).trim());
			bean.getBeanDatosContratos().setIndicadorAccionistas(StringUtils.substring(item,												161	,	162).trim());
			bean.getBeanDatosContratos().setFechaUltimaOperacion(StringUtils.substring(item,												162	,	172).trim());
			bean.getBeanDatosContratos().setFechaUltimaModificacion(StringUtils.substring(item,												172	,	182).trim());
			bean.getBeanDatosContratos().setDescripcionContrato(StringUtils.substring(item,													182	,	282).trim());
			bean.getBeanDatosContratos().setDescripcionFirma(StringUtils.substring(item,													282	,	382).trim());
			bean.getBeanDatosContratos().setDescripcionAreaReferencia(StringUtils.substring(item,											382	,	482).trim());
			bean.getBeanDatosContratos().setDescripcionReferenciaExterna(StringUtils.substring(item,										482	,	582).trim());
			bean.getBeanDatosContratos().setDescripcionTextoLibreSubestatus(StringUtils.substring(item,										582	,	682).trim());
			bean.getBeanDatosContratos().setEmpresaContratoAgrupador(StringUtils.substring(item,											682	,	686).trim());
			bean.getBeanDatosContratos().setCentroContratoAgrupador(StringUtils.substring(item,												686	,	690).trim());
			bean.getBeanDatosContratos().setNumeroContratoAgrupador(StringUtils.substring(item,												690	,	702).trim());
			bean.getBeanDatosContratos().setCodigoProductoAgrupador(StringUtils.substring(item,												702	,	704).trim());
			bean.getBeanDatosContratos().setCodigoSubproductoAgrupador(StringUtils.substring(item,											704	,	708).trim());
			bean.getBeanDatosContratos().setEmpresaContratoAgrupado(StringUtils.substring(item,												708	,	712).trim());
			bean.getBeanDatosContratos().setAliasContratoAgrupador(StringUtils.substring(item,												712	,	727).trim());
			bean.getBeanDatosContratos().setEmpresaContratoEspejo(StringUtils.substring(item,												727	,	731).trim());
			bean.getBeanDatosContratos().setCentroContratoEspejo(StringUtils.substring(item,												731	,	735).trim());
			bean.getBeanDatosContratos().setNumeroContratoEspejo(StringUtils.substring(item,												735	,	747).trim());
			bean.getBeanDatosContratos().setCodigoProductoEspejo(StringUtils.substring(item,												747	,	749).trim());
			bean.getBeanDatosContratos().setCodigoSubProductoEspejo(StringUtils.substring(item,												749	,	753).trim());
			bean.getBeanDatosContratos().setEmpresaContratoAlias(StringUtils.substring(item,												753	,	757).trim());
			bean.getBeanDatosContratos().setAliasContratoEspej(StringUtils.substring(item,													757	,	772).trim());
			bean.getBeanDatosContratos().setEmpresaContratoChequera(StringUtils.substring(item,												772	,	776).trim());
			bean.getBeanDatosContratos().setCentroContratoChequera(StringUtils.substring(item,												776	,	780).trim());
			bean.getBeanDatosContratos().setNumeroContratoChequera(StringUtils.substring(item,												780	,	792).trim());
			bean.getBeanDatosContratos().setCodigoProductoChequera(StringUtils.substring(item,												792	,	794).trim());
			bean.getBeanDatosContratos().setCodigoSubProductoChequera(StringUtils.substring(item,											794	,	798).trim());
			
			bean.setNumeroIntervinientes(StringUtils.substring(item,	798,		801).trim());
			int posicionFinalIntervinientes=posicionActualIntervinientes;
			for(int i=0;i<Integer.valueOf(StringUtils.substring(item,	798,		801).trim());i++){
				BeanListaIntervinientesResponse intervinientes = new BeanListaIntervinientesResponse();	
				posicionFinalIntervinientes+=2;
				intervinientes.setCalidadParticipacionInterviniente(StringUtils.substring(item,					posicionActualIntervinientes	,	posicionFinalIntervinientes).trim());
				posicionActualIntervinientes=posicionFinalIntervinientes;
				posicionFinalIntervinientes+=3;
				intervinientes.setOrdenParticipacionInterviniente(Integer.valueOf(StringUtils.substring(item,		posicionActualIntervinientes	,	posicionFinalIntervinientes)));
				posicionActualIntervinientes=posicionFinalIntervinientes;
				posicionFinalIntervinientes+=8;
				intervinientes.setNumeroPersona(StringUtils.substring(item,										posicionActualIntervinientes	,	posicionFinalIntervinientes).trim());
				posicionActualIntervinientes=posicionFinalIntervinientes;
				listaIntervinientes.add(intervinientes);				
			}
			bean.setListaIntervinientes(listaIntervinientes);
			
			bean.setNumeroContacto(StringUtils.substring(item,	1191,	1194	).trim());
			int posicionFinalContactos = posicionActualContactos;
			for(int i=0;i<Integer.valueOf(StringUtils.substring(item,	1191,	1194	).trim());i++){
				BeanListaContactosResponse contactos = new BeanListaContactosResponse();
				posicionFinalContactos+=2;
				contactos.setTipoContacto(StringUtils.substring(item,					posicionActualContactos	,	posicionFinalContactos).trim());
				posicionActualContactos=posicionFinalContactos;
				posicionFinalContactos+=3;
				contactos.setSecuenciaContacto(Integer.valueOf(StringUtils.substring(item,		posicionActualContactos	,	posicionFinalContactos)));
				posicionActualContactos=posicionFinalContactos;
				listaContactos.add(contactos);
			}
			bean.setListaContactos(listaContactos);			
			
			bean.setNumeroUsosDomicilio(StringUtils.substring(item,				1294,		1297).trim());
			int posicionFinalDomicilios= posicionActualDomicilios;
			for(int i=0;i<Integer.valueOf(StringUtils.substring(item,				1294,		1297).trim());i++){
				BeanDatosDomicilioResponse domicilios = new BeanDatosDomicilioResponse();
				posicionFinalDomicilios+=8;
				domicilios.setNumeroPersonaUsoDomicilio(StringUtils.substring(item,					posicionActualDomicilios	,	posicionFinalDomicilios).trim());
				posicionActualDomicilios=posicionFinalDomicilios;
				posicionFinalDomicilios+=3;
				domicilios.setTipoUsoDomicilio(StringUtils.substring(item,		posicionActualDomicilios	,	posicionFinalDomicilios));
				posicionActualDomicilios=posicionFinalDomicilios;
				posicionFinalDomicilios+=3;
				domicilios.setSecuenciaDomicilio(Integer.valueOf(StringUtils.substring(item,		posicionActualDomicilios	,	posicionFinalDomicilios)));
				posicionActualDomicilios=posicionFinalDomicilios;
				listaDomicilios.add(domicilios);
			}
			bean.setListaDatosDomicilio(listaDomicilios);
			
			response.add(bean);
			
		}
			resultado.setListaConsultaInversion(response);
			}
		return resultado;
	}


	/**
	 * Método que ejecutará el consumo de la transacción MI18 : Alta de contrato de inversión
	 */
	@Override
	public BeanAltaContratoInversionResultado altaContrato(BeanAltaContratoInversionRequest beanAltaContratoInversionRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess {
		BeanAltaContratoInversionResultado resultado = new BeanAltaContratoInversionResultado();
		info("DAOGestionContratosImpl::altaContrato()");
		
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanAltaContratoInversionRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoInversionRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoInversionRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanAltaContratoInversionRequest.getNumeroFolio()),	9, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoInversionRequest.getFechaAlta(),	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		
		final String mensajeTrama = trama.toString(); 
		
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI18_CODE_OPERATION, MI18, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanAltaContratoInversionResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));

			
			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM+item.length());	
			BeanAltaContratoInversionResponse bean = new BeanAltaContratoInversionResponse();
			bean.setEmpresaContrato(StringUtils.substring(item, 11,	15).trim());
			bean.setCentroContrato(StringUtils.substring(item, 15,19).trim());
			bean.setNumeroContrato(StringUtils.substring(item, 19,	31).trim());
			bean.setCodigoProducto(StringUtils.substring(item, 31,	33).trim());
			bean.setCodigoSubProducto(StringUtils.substring(item,	33,	37).trim());
			bean.setCodigoMonedaContrato(StringUtils.substring(item, 37,40).trim());
			bean.setCodigoEmpresaContratoInversion(StringUtils.substring(item, 40,44).trim());
			bean.setAliasContrato(StringUtils.substring(item, 44,	59).trim());
			response.add(bean);
		}
		resultado.setListaAltaContratoInversionResponse(response);
		}
		return resultado;
	}


	/**
	 * Método que ejecutará el consumo de la transacción MI19 : Modificación de estatus de contrato
	 */
	@Override
	public ResponseGenerico cambioEstatusContrato(BeanModificacionEstatusContratoRequest beanModificacionContrato,ArchitechSessionBean sesion) throws ExceptionDataAccess {
		info("DAOGestionPreContratosImpl::cambioEstatusContrato()");

		
		final StringBuilder trama = new StringBuilder();
		trama.append(StringUtils.leftPad(beanModificacionContrato.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getMulticanalidad().getCanalOperacion()	,2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getTipoConsulta(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getCentroContrato()	,4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getNumeroContrato()	,12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getCodigoProducto()	,2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getCodigoSubProducto(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getEmpresaContratoInversion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionContrato.getAliasContrato(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getEstatusContrato(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getDetalleEstatus(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionContrato.getTextoLibreEstatus(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionContrato.getFechaCambio(),	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));

		return SingletonIDA.getSingletonInstance().crearResponse(trama.toString(), MI19_CODE_OPERATION, MI19, this.getLoggingBean(),MIContratosConstantes.MENSAJE_LOG);

	}

	/**
	 * Método que ejecutará el consumo de la transacción MI21 : Cambio de número de contrato
	 */
	@Override
	public ResponseGenerico cambioAliasContrato(
			BeanCambioNumeroContratoRequest beanCambioNumeroContratoRequest,
			ArchitechSessionBean sesion) throws ExceptionDataAccess {
		this.info("DAOGestionContratosImpl::cambioAliasContrato()");

		
		final StringBuilder trama = new StringBuilder();
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getMulticanalidad().getEmpresaConexion(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getMulticanalidad().getCanalOperacion(),	2,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getMulticanalidad().getCanalComercializacion(),	2,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getTipoConsulta(),	2,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getEmpresaContrato(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getCentroContrato(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getNumeroContrato(),	12,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getCodigoProducto(),	2,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getCodigoSubProducto(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanCambioNumeroContratoRequest.getEmpresaContratoInversion(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanCambioNumeroContratoRequest.getAliasContrato(),	15,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanCambioNumeroContratoRequest.getNuevoAliasContrato(),	15,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));

		return SingletonIDA.getSingletonInstance().crearResponse(trama.toString(), MI21_CODE_OPERATION, MI21, this.getLoggingBean(),MIContratosConstantes.MENSAJE_LOG);

	}

	/**
	 * Método que ejecutará el consumo de la transacción MI23 : Alta de contrato de inversión al amparo de una chequera
	 */
	@Override
	public BeanAltaContratosAmparadosChequeraResultado altaCtoAmparadoChequera(
			BeanAltaContratosAmparadosChequeraRequest beanAltaContratosAmparadosChequeraRequest,
			ArchitechSessionBean sesion) throws ExceptionDataAccess {
		BeanAltaContratosAmparadosChequeraResultado respuesta = new BeanAltaContratosAmparadosChequeraResultado();
		info("DAOGestionContratosImpl::altaCtoAmparadoChequera()");
		
		final StringBuilder trama = new StringBuilder();
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getMulticanalidad().getEmpresaConexion(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getMulticanalidad().getCanalOperacion(),	2,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getMulticanalidad().getCanalComercializacion(),	2,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getEmpresaContrato(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getCentroContrato	(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getNumeroContrato(),	12,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getCodigoProducto(),	2,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getCodigoSubProducto(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));		
		trama.append(StringUtils.rightPad(beanAltaContratosAmparadosChequeraRequest.getCodigoEjecutivo(),	8,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getNumeroPersonaTitular(),	8,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getCentroCostosContrato(),	4,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getCodigoBanca(),	3,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosAmparadosChequeraRequest.getFechaAlta(),	10,	 MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));

		
		
		final String mensajeTrama =  trama.toString();
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance().ejecutarTransaccion( mensajeTrama,  MI23_CODE_OPERATION,  MI23,
				this.getLoggingBean(),MIContratosConstantes.MENSAJE_LOG); 

		if(!ConfigFactory.CODE_SUCCESFULLY.equals(responseDTO.getCodeError()) || StringUtils.isBlank(responseDTO.getResponseMessage())){
			this.error("Se generó un error al realizar el alta de contrato de inversión: " + responseDTO.getCodeError());
			respuesta.setCodError(responseDTO.getCodeError());
			respuesta.setMsgError(responseDTO.getMessageError());
			return respuesta;
		}
		
		String respuestaTransaccion = responseDTO.getResponseMessage();
		BeanAltaContratosAmparadosChequeraResultado resultado= new BeanAltaContratosAmparadosChequeraResultado();
		resultado.setCodError(StringUtils.substring(respuestaTransaccion, 8 , 17 ).trim());
		resultado.setMsgError(StringUtils.substring(respuestaTransaccion, 17 , respuestaTransaccion.indexOf(MIContratosConstantes.DELIMITADOR_OBJETO_TRAMA) ).trim());
		
		String cadena = StringUtils.substring(respuestaTransaccion, respuestaTransaccion.indexOf(MIContratosConstantes.DELIMITADOR_OBJETO_TRAMA)+2 , respuestaTransaccion.length()-1 ).trim();
	
		resultado.setEmpresaContrato(StringUtils.substring(cadena,	11,	15 ).trim());
		resultado.setCentroContrato(StringUtils.substring(cadena,	15,	19 ).trim());
		resultado.setNumeroContrato(StringUtils.substring(cadena,	19,	31 ).trim());
		resultado.setCodigoProducto(StringUtils.substring(cadena,	31,	33 ).trim());
		resultado.setCodigoSubProducto(StringUtils.substring(cadena,33,	37 ).trim());
		resultado.setCodigoEmpresaContrato(StringUtils.substring(cadena,37,	41 ).trim());
		resultado.setAliasContrato(StringUtils.substring(cadena,41,	56 ).trim());

		return resultado;
	}


	/**
	 * 
	 * Método que ejecutará el consumo de la transacción MI31 : Mantenimiento de datos generales
	 */
	@Override
	public ResponseGenerico modificacionDatosContrato(
			BeanMantenimientoDatosGeneralesRequest beanMantenimientoDatosGeneralesRequest,
			ArchitechSessionBean sesion) throws ExceptionDataAccess {
		this.info("DAOGestionContratosImpl::modificacionDatosContrato()");
		final StringBuilder trama = new StringBuilder();

		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getMulticanalidad().getEmpresaConexion(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getMulticanalidad().getCanalOperacion(),		2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getMulticanalidad().getCanalComercializacion(),		2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getTipoConsulta(),		2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getEmpresaContrato(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCentroContrato(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getNumeroContrato(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoProducto(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoSubProducto(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getEmpresaContratoInversion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanMantenimientoDatosGeneralesRequest.getAliasContrato(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoBanca(),	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorTipoCuenta(),	1 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorTipoContrato(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorAutorizacionPosicionCortoCapitales(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorAutorizacionPosicionCortoFondos(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorAutorizacionCruceFondosInversion(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorDiscrecionalidad(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorTipoCustodiaDirecto(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorTipoCustodiaRepos(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorEstadoCuenta(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getMontoMaximoOperacionContrato(),	15 , MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getDivisamaximoOperacionContrato(),	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorEnvioCorreoCartaConfirmacionCapitales(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorContratoAgrupado(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getEmpresaContratoAgrupador(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCentroContratoAgrupador(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getNumeroContratoAgrupador(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoProductoContratoAgrupador(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoSubProductoContratoAgrupador(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getIndicadorContratoEspejo(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getEmpresaContratoEspejo(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCentroContratoEspejo(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getNumeroContratoEspejo(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoProductoContratoEspejo(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoSubProductoContratoEspejo(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoTipoServicio(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));		
		trama.append(StringUtils.leftPad(beanMantenimientoDatosGeneralesRequest.getCodigoAfiliacion(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanMantenimientoDatosGeneralesRequest.getDescripcionContrato(),	100, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanMantenimientoDatosGeneralesRequest.getDescripcionFirma(),	100, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanMantenimientoDatosGeneralesRequest.getDescripcionAreaReferencia(),	100, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanMantenimientoDatosGeneralesRequest.getDescripcionAreaReferenciaExterna(),	100, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));

		return SingletonIDA.getSingletonInstance().crearResponse(trama.toString(), MI31_CODE_OPERATION, MI31, this.getLoggingBean(),MIContratosConstantes.MENSAJE_LOG);

	}


	/**
	 * Método que ejecutará el consumo de la transacción MI24 : Consulta de modificaciones del contrato
	 */
	@Override
	public BeanConsultaModificacionesContratoResultado historicoModifContrato(BeanConsultaModificacionesContratoRequest beanConsultaModificacionesContratoRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess{
		this.info("DAOGestionContratosImpl::historicoModifContrato()");
		BeanConsultaModificacionesContratoResultado resultado = new BeanConsultaModificacionesContratoResultado();
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getTipoConsulta(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getCentroContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanConsultaModificacionesContratoRequest.getNumeroContrato(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getCodigoProducto(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getCodigoSubProducto(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getEmpresaContratoInversion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanConsultaModificacionesContratoRequest.getAliasContrato(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getRellamada().getMasDatos().toUpperCase(),	1,  String.valueOf(MIContratosConstantes.NO_HAY_DATOS)));
		trama.append(StringUtils.leftPad(beanConsultaModificacionesContratoRequest.getRellamada().getMemento(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));		
		
		final String mensajeTrama = trama.toString(); 
		
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI24_CODE_OPERATION, MI24, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanConsultaModificacionesContratoResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));

			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM+item.length());	
			BeanConsultaModificacionesContratoResponse bean = new BeanConsultaModificacionesContratoResponse();
			bean.setFechaComienzoValor(StringUtils.substring(item,	11,	21).trim());
			bean.setFechaFinValor(StringUtils.substring(item,	21,	31).trim());
			bean.setCodigoBancaContratoAntiguo(StringUtils.substring(item,	31,	34).trim());
			bean.setCodigoBancaContratoNuevo(StringUtils.substring(item,	34,	37).trim());
			bean.setAliasContratoAntiguo(StringUtils.substring(item,	37,	52).trim());
			bean.setAliasContratoNuevo(StringUtils.substring(item,	52,	67).trim());
			bean.setFechaVencimientoContratoAntiguo(StringUtils.substring(item,	67,	77).trim());
			bean.setFechaVencimientoContratoNuevo(StringUtils.substring(item,	77,	87).trim());
			bean.setCodigoEjecutivoAntiguo(StringUtils.substring(item,	87,	95).trim());
			bean.setCodigoEjecutivoNuevo(StringUtils.substring(item,	95,	103).trim());
			bean.setCentroCostosContratoAntiguo(StringUtils.substring(item,	103,	107).trim());
			bean.setCentroCostosContratoNuevo(StringUtils.substring(item,	107, 111).trim());
			bean.setIndicadorTipoCuentaAntigua(StringUtils.substring(item,	111,	112).trim());
			bean.setIndicadorTipoCuentaNueva(StringUtils.substring(item,	112,	113).trim());
			bean.setIndicadorTipoContratoAntiguo(StringUtils.substring(item,	113,	114).trim());
			bean.setIndicadorTipoContratoNuevo(StringUtils.substring(item,	114,	115).trim());
			bean.setIndicadorAutorizacionPosicionCapitalesAntiguo(StringUtils.substring(item,	115,	116).trim());
			bean.setIndicadorAutorizacionPosicionCapitalesNuevo(StringUtils.substring(item,	116,	117).trim());
			bean.setIndicadorAutorizacionPosicionFondosAntiguo(StringUtils.substring(item,	117,	118).trim());
			bean.setIndicadorAutorizacionPosicionFondosNuevo(StringUtils.substring(item,	118,	119).trim());
			bean.setIndicadorCruceFondosInversionAntiguo(StringUtils.substring(item,	119,	120).trim());
			bean.setIndicadorCruceFondosInversionNuevo(StringUtils.substring(item,	120,	121).trim());
			bean.setIndicadorDiscrecionalidadAntiguo(StringUtils.substring(item,	121,	122).trim());
			bean.setIndicadorDiscrecionalidadNuevo(StringUtils.substring(item,	122,	123).trim());
			bean.setIndicadorTipoCustodiaDirectoAntiguo(StringUtils.substring(item,	123,	124).trim());
			bean.setIndicadorTipoCustodiaDirectoNuevo(StringUtils.substring(item,	124,	125).trim());
			bean.setIndicadorTipoCustodiaReposAntiguo(StringUtils.substring(item,	125,	126).trim());
			bean.setIndicadorTipoCustodiaReposNuevo(StringUtils.substring(item,	126,	127).trim());
			bean.setIndicadorEstadoCuentaAntigua(StringUtils.substring(item,	127,	128).trim());
			bean.setIndicadorEstadoCuentaNueva(StringUtils.substring(item,	128,	129).trim());
			bean.setImporteMaximoOperacionAntiguo(MIContratosValidator.validarCamposFlotante(StringUtils.substring(item,	129,	144).trim()));
			bean.setDivisaMontoMaximoOperacionContratoAntiguo(StringUtils.substring(item,	144,	147).trim());
			bean.setImporteMaximoOperacionNuevo(MIContratosValidator.validarCamposFlotante(StringUtils.substring(item,	147,	162).trim()));
			bean.setDivisaMontoMaximoOperacionContratoNuevo(StringUtils.substring(item,	162,	165).trim());
			bean.setIndicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo(StringUtils.substring(item,	165,	166).trim());
			bean.setIndicadorEnvioCorreoCartaConfirmacionCapitalesNuevo(StringUtils.substring(item,	166,	167).trim());
			bean.setCodigoEstatusAntiguo(StringUtils.substring(item,	167,	169).trim());
			bean.setCodigoEstatusNuevo(StringUtils.substring(item,	169,	171).trim());
			bean.setDetalleEstatusAntiguo(StringUtils.substring(item,	171,	173).trim());
			bean.setDetalleEstatusNuevo(StringUtils.substring(item,	173,	175).trim());
			bean.setIndicadorContratoAgrupadoAntiguo(StringUtils.substring(item,	175,	176).trim());
			bean.setIndicadorContratoAgrupadoNuevo(StringUtils.substring(item,	176,	177).trim());
			bean.setIndicadorTipoPosicionAntiguo(StringUtils.substring(item,	177,	178).trim());
			bean.setIndicadorTipoPosicionNuevo(StringUtils.substring(item,	178,	179).trim());
			bean.setIndicadorContratoEspejoAntiguo(StringUtils.substring(item,	179,	180).trim());
			bean.setIndicadorContratoEspejoNuevo(StringUtils.substring(item,	180,	181).trim());
			bean.setIndicadorContratoAmparadoChequeraAntiguo(StringUtils.substring(item,	181,	182).trim());
			bean.setIndicadorContratoAmparadoChequeraNuevo(StringUtils.substring(item,	182,	183).trim());
			bean.setCodigoTipoServicioAntiguo(StringUtils.substring(item,	183,	185).trim());
			bean.setCodigoTipoServicioNuevo(StringUtils.substring(item,	185,	187).trim());
			bean.setCodigoAsesorFinancieroAntiguo(StringUtils.substring(item,	187,	195).trim());
			bean.setCodigoAsesorFinancieroNuevo(StringUtils.substring(item,	195,	203).trim());

			response.add(bean);
		}
			if(cadena.length>0){
				resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 203,	204));
				if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
					resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],204,	219 ));
				}
			}
		resultado.setListaModificacionesContrato(response);
		}
		return resultado;
	}

	
	/**
	 * Método que ejecutará el consumo de la transacción MI37 : Modificación del indicador de estado de cuenta
	 */
	@Override
	public ResponseGenerico modificaOpcionEdoCuenta(
			BeanModificacionIndicadorEstadoCuentaRequest beanBajaIndicadorEstadoCuentaRequest,
			ArchitechSessionBean sesion) throws ExceptionDataAccess{
		this.info("DAOGestionContratosImpl::modificaOpcionEdoCuenta()");
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getMulticanalidad().getEmpresaConexion(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getMulticanalidad().getCanalOperacion(),		2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getMulticanalidad().getCanalComercializacion(),		2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getTipoConsulta(),		2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getEmpresaContrato(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getCentroContrato(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getNumeroContrato(),		12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getCodigoProducto(),		2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getCodigoSubProducto(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getEmpresaContratoInversion(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanBajaIndicadorEstadoCuentaRequest.getAliasContrato(),		15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getIndicadorEstadoCuenta(),		1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaIndicadorEstadoCuentaRequest.getFecha(),		10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama =trama.toString();		
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI37_CODE_OPERATION, MI37, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}


	/**
	 * Método que ejecutará el consumo de la transacción MI40 : Lista de contratos de inversión
	 */
	@Override
	public BeanListaContratosInversionResultado listarContratosInversion(
			BeanListaContratosInversionRequest beanListaContratosInversionRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess {
		this.info("DAOGestionContratosImpl::listarContratosInversion()");
		BeanListaContratosInversionResultado resultado = new BeanListaContratosInversionResultado();
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanListaContratosInversionRequest.getMulticanalidad().getEmpresaConexion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosInversionRequest.getMulticanalidad().getCanalOperacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosInversionRequest.getMulticanalidad().getCanalComercializacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaContratosInversionRequest.getCodigoUsuarioConexion(), 8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaContratosInversionRequest.getCodigoEjecutivo(), 8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosInversionRequest.getCodigoEmpresaContratoInversion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaContratosInversionRequest.getAliasContrato(), 15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosInversionRequest.getCodigoPersonaTitular(), 8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosInversionRequest.getRellamada().getMasDatos().toUpperCase(), 1, String.valueOf(MIContratosConstantes.NO_HAY_DATOS)));
		trama.append(StringUtils.leftPad(beanListaContratosInversionRequest.getRellamada().getMemento(), 26, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama = trama.toString();
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI40_CODE_OPERATION, MI40, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());

		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanListaContratosInversionResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
			
			for (String item : cadena) {
				info(MIContratosConstantes.INFORMACION_ITEM+item.length());
				BeanListaContratosInversionResponse bean = new BeanListaContratosInversionResponse();
				
				bean.setCodigoPersonaTitular(StringUtils.substring(item, 11, 19).trim());
				bean.setDescripcionContrato(StringUtils.substring(item, 19, 119));
				bean.setEmpresaContrato(StringUtils.substring(item, 119, 123).trim());
				bean.setCentroContrato(StringUtils.substring(item, 123, 127).trim());
				bean.setNumeroContrato(StringUtils.substring(item, 127, 139).trim());
				bean.setCodigoProductoContrato(StringUtils.substring(item, 139, 141).trim());
				bean.setCodigoSubProductoContrato(StringUtils.substring(item, 141, 145).trim());
				bean.setCodigoMonedaContrato(StringUtils.substring(item, 145, 148).trim());
				bean.setNumeroFolioContrato(MIContratosValidator.validarEnteros(StringUtils.substring(item, 148, 157).trim()));
				bean.setCodigoEmpresaContratoInversion(StringUtils.substring(item, 157, 161).trim());
				bean.setAliasContrato(StringUtils.substring(item, 161, 176).trim());
				bean.setCodigoBancaContratoInversion(StringUtils.substring(item, 176, 179).trim());
				bean.setFechaAperturaContrato(StringUtils.substring(item, 179, 189).trim());
				bean.setFechaVencimientoContrato(StringUtils.substring(item, 189, 199).trim());
				bean.setCodigoEjecutivoContrato(StringUtils.substring(item, 199, 207).trim());
				bean.setCentroCostosContrato(StringUtils.substring(item, 207, 211).trim());
				bean.setIndicadorTipoCuenta(StringUtils.substring(item, 211, 212).trim());
				bean.setIndicadorTipoContrato(StringUtils.substring(item, 212, 213).trim());
				bean.setIndicadorAutorizacionPosicionCortoCapitales(StringUtils.substring(item, 213, 214).trim());
				bean.setIndicadorAutorizacionPosicionCortoFondos(StringUtils.substring(item, 214, 215).trim());
				bean.setIndicadorAutorizacionCruceFondosInversion(StringUtils.substring(item, 215, 216).trim());
				bean.setIndicadorDiscrecionalidad(StringUtils.substring(item, 216, 217).trim());
				bean.setIndicadorTipoCustodiaDirecto(StringUtils.substring(item, 217, 218).trim());
				bean.setIndicadorTipoCustodiaRepos(StringUtils.substring(item, 218, 219).trim());
				bean.setIndicadorEstatoCuenta(StringUtils.substring(item, 219, 220).trim());
				bean.setMontoMaximoOperarContrato(MIContratosValidator.validarCamposFlotante(StringUtils.substring(item, 220, 235).trim()));
				bean.setDivisaMontoMaximoOperarContrato(StringUtils.substring(item, 235, 238).trim());
				bean.setIndicadorEnvioCorreoCartaConfirmacionCapitales(StringUtils.substring(item, 238, 239).trim());
				bean.setEstatusContrato(StringUtils.substring(item, 239, 241).trim());
				bean.setDetalleEstatus(StringUtils.substring(item, 241, 243).trim());
				bean.setFechaEstadoContrato(StringUtils.substring(item, 243, 253).trim());
				bean.setIndicadorContratoAgrupado(StringUtils.substring(item, 253, 254).trim());
				bean.setIndicadorTipoPosicion(StringUtils.substring(item, 254, 255).trim());
				bean.setIndicadorContratoEspejo(StringUtils.substring(item, 255, 256).trim());
				bean.setIndicadorContratoAmparadoChequera(StringUtils.substring(item, 256, 257).trim());
				bean.setCodigoTipoServicio(StringUtils.substring(item, 257, 259).trim());
				bean.setCodigoAfiliacion(StringUtils.substring(item, 259, 267).trim());
				bean.setIndicadorBeneficiarios(StringUtils.substring(item, 267, 268).trim());
				bean.setIndicadorProveedoresRecursos(StringUtils.substring(item, 268, 269).trim());
				bean.setIndicadorAccionistas(StringUtils.substring(item, 269, 270).trim());
				bean.setFechaUlitmaOperacion(StringUtils.substring(item, 270, 280).trim());
				bean.setFechaUltimaModificacion(StringUtils.substring(item, 280, 290).trim());
				bean.setIndicadorAutorizacionOperar(StringUtils.substring(item, 290, 291).trim());
				bean.setIndicadorAutorizacionConsultar(StringUtils.substring(item, 291, 292).trim());
				bean.setBranchContrato(StringUtils.substring(item, 292, 294).trim());
				
				response.add(bean);
			}
			if(cadena.length>0){
				resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 294, 295));
				if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
					resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],295,	321));
				}
			}
		resultado.setListaContratoInversion(response);
		}
		return resultado;
	}	

}
