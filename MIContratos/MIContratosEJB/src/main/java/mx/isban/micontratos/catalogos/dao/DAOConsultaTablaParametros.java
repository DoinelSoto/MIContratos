package mx.isban.micontratos.catalogos.dao;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchResult;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIRequest;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasFisicasRequestBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasFisicasResponseBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasMoralesRequestBean;
import mx.isban.mipersonas.datosgenerales.beans.ConsultaDetPersonasMoralesResponseBean;
import mx.isban.mipersonas.localizadores.beans.ConsPersonasBucResponseBean;



/**
 * @author Daniel Hernández Soto
 * Interfaz que define los métodos en cuya implementación se ejecutarán en el consumo de las transacciones de consulta a la tabla de parametros
 *
 */
public interface DAOConsultaTablaParametros {
	
	/**
	 * Método que ejecutará el consumo de la transaccion MI04 : Consulta de catálogos de estatus operativo
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return BeanCatalogoEstatusOperativoResultado
	 * @throws ExceptionDataAccess 
	 */
	BeanCatalogoEstatusOperativoResultado catalogoEstatusOperativos(BeanCatalogoEstatusOperativoRequest beanAltaCatalogosEstatusRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;
	
	/**
	 * Método que ejecutará el consumo de la transaccion MI05 : Consulta de catálogos de Subestaus operativo
	 * @param beanCatalogosSubEstatusRequest
	 * @param sesion
	 * @return BeanCatalogoSubEstatusResultado
	 * @throws ExceptionDataAccess
	 */
	BeanCatalogoSubEstatusResultado catalogoSubEstatus(BeanCatalogoSubEstatusRequest beanCatalogosSubEstatusRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;

	/**
	 * Método que ejecutará el consumo de la transaccion MI06 : Consulta de la lista de tipo de servicios
	 * @param beanListaTiposServicioRequest
	 * @param sesion
	 * @return BeanListaTiposServicioResultado
	 * @throws ExceptionDataAccess
	 */
	BeanListaTiposServicioResultado tipoServicio(BeanListaTiposServicioRequest beanListaTiposServicioRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;

		
	/**
	 * Método que ejecutará el consumo de la transaccion MI07 : Consulta de arbol de tipo de servicios
	 * @param beanArbolTiposServicioRequest
	 * @param sesion
	 * @return BeanArbolTiposServicioResultado
	 * @throws ExceptionDataAccess
	 */
	BeanArbolTiposServicioResultado arbolTipoServicio(BeanArbolTiposServicioRequest beanArbolTiposServicioRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;

	 /**
	 * Metódo que ejecutará el consumo de la transacción MI39 : lista de códigos branch-entidad: 
	 * @param beanConsultarBranchRequest
	 * @param session
	 * @return BeanConsultarBranchResult 
	 * @throws ExceptionDataAccess 
	 */
	BeanConsultarBranchResult consultaBranch(BeanConsultarBranchRequest beanConsultarBranchRequest,ArchitechSessionBean session) throws ExceptionDataAccess;
	
	/**
	 * 
	 * @param beanTipoNecesidadAFIRequest
	 * @param sesion
	 * @return ConsPersonasBucResponseBean
	 * @throws BusinessException
	 */
	ConsPersonasBucResponseBean localizadorPersonasBuc(BeanTipoNecesidadAFIRequest beanTipoNecesidadAFIRequest,ArchitechSessionBean sesion) throws BusinessException;

	/**
	 * PEL1
	 * Transacción para consulta de persona moral. 
	 * @param consultaDetPersonasMorales
	 * @return ConsultaDetPersonasMoralesResponseBean
	 * @throws BusinessException 
	 */
	ConsultaDetPersonasMoralesResponseBean consultaPersonaMoral(ConsultaDetPersonasMoralesRequestBean consultaDetPersonasMorales) throws BusinessException;
	
	/**
	 * PEL2
	 * Transacción para consulta de persona física y física con actividad empresarial
	 * @param consultaDetPersonasFisicas
	 * @return ConsultaDetPersonasFisicasResponseBean
	 * @throws BusinessException 
	 */
	ConsultaDetPersonasFisicasResponseBean consultaPersonaFisicas(ConsultaDetPersonasFisicasRequestBean consultaDetPersonasFisicas) throws BusinessException;


	/**
	 * Método que define la función que ejecutará el consumo de la transacción MI08 : Consulta de Estado de documentos
	 * @param beanConsultaEstadoDocumentosRequest
	 * @param session
	 * @return BeanConsultaEstadoDocumentosResultado
	 * @throws ExceptionDataAccess
	 */
	BeanConsultaEstadoDocumentosResultado consultaEstadoDocumentos(BeanConsultaEstadoDocumentosRequest beanConsultaEstadoDocumentosRequest, ArchitechSessionBean session) throws ExceptionDataAccess;

	/**
	 * Método que define la función que ejecutará el consumo de la transacción MI09 : Consulta de catalogos estructurales
	 * @param beanCatalogosEstructuralesRequest
	 * @param session
	 * @return BeanCatalogosEstructuralesResultado
	 * @throws  ExceptionDataAccess
	 */
	BeanCatalogosEstructuralesResultado consultaCatalogosEstructurales(BeanCatalogosEstructuralesRequest beanCatalogosEstructuralesRequest, ArchitechSessionBean session) throws ExceptionDataAccess;
	
}