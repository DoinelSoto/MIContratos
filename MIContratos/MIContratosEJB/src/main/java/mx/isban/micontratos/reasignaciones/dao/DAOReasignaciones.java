package mx.isban.micontratos.reasignaciones.dao;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.reasignaciones.BeanAltaSubEstatusRequest;

/**
 * 
 * interfaz en la que se declaran los metodos a implementarse para consumir las transacciones de reasignaciones
 *
 */
public interface DAOReasignaciones {
	

	/**
	 *  Método que define la función que ejecutará el consumo de la transacción MI22 : alta de subestatus
	 * @param beanAltaSubEstatusRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess 
	 */
	ResponseGenerico traspasoEjecutivo(BeanAltaSubEstatusRequest beanAltaSubEstatusRequest,ArchitechSessionBean session) throws ExceptionDataAccess;

	
}
