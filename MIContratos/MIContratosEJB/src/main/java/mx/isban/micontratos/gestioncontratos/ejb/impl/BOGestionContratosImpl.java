package mx.isban.micontratos.gestioncontratos.ejb.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanCambioNumeroContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasEntrada;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanDatosContratos;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanMantenimientoDatosGeneralesRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionEstatusContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionIndicadorEstadoCuentaRequest;
import mx.isban.micontratos.gestioncontratos.dao.DAOGestionContratos;
import mx.isban.micontratos.gestioncontratos.ejb.BOGestionContratos;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosValidator;

/**
 * @author Daniel Hernández soto
 *
 */

/**
 * Método que hará la definición para el llamado al método que consumirá la transacción MI18 : Alta de contrato de inversión
 * 
 *
 */
@Stateless
@Remote(BOGestionContratos.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGestionContratosImpl extends Architech implements BOGestionContratos{


	
	/**
	 * Serializacion
	 */
	private static final long serialVersionUID = 4001625547316275139L;
	
	@EJB
	private DAOGestionContratos daoGestionContratos;

	/**
	 * Método que hará la definición para el llamado al método que consumirá la transacción MI16 : Consulta de contrato de inversión
	 */
	@Override
	public BeanConsultaInversionResultado consultaDatosContrato(BeanConsultaInversionRequest beanConsultaInversionRequest,ArchitechSessionBean session) throws BusinessException {
		String[] camposObligatorios = {
				beanConsultaInversionRequest.getTipoConsulta()};
		
		int[] longitudesObligatorias = {
				2};
		String[] camposNoObligatorios = {
				beanConsultaInversionRequest.getMulticanalidad().getEmpresaConexion(),	
				beanConsultaInversionRequest.getMulticanalidad().getCanalOperacion(),	
				beanConsultaInversionRequest.getMulticanalidad().getCanalComercializacion(),	
				beanConsultaInversionRequest.getEmpresaContrato(),							
				beanConsultaInversionRequest.getCentroContrato(),							
				beanConsultaInversionRequest.getNumeroContrato(),							
				beanConsultaInversionRequest.getCodigoProducto(),							
				beanConsultaInversionRequest.getCodigoSubProducto(),						
				beanConsultaInversionRequest.getEmpresaContratoInversion(),					
				beanConsultaInversionRequest.getAliasContrato(),							
				beanConsultaInversionRequest.getIndicadorInformacionIntervinientes(),		
				beanConsultaInversionRequest.getIndicadorInformacionContactos(),			
				beanConsultaInversionRequest.getIndicadorUsoDomicilios(),					
				beanConsultaInversionRequest.getIndicadorInformacionContratosRelacionados(),
				beanConsultaInversionRequest.getIndicadorInformacionReferenciaContrato(),	
				beanConsultaInversionRequest.getFechaConsulta(),};
		
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,1,1,1,1,1,10};
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanConsultaInversionResultado response=daoGestionContratos.consultaDatosContrato(beanConsultaInversionRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
	
	/**
	 * Método que hará la definición para el llamado al método que consumirá la transacción MI18 : Alta de contrato de inversión
	 */
	@Override
	public BeanAltaContratoInversionResultado altaContrato(BeanAltaContratoInversionRequest beanAltaContratoInversionRequest, ArchitechSessionBean session)  throws BusinessException {
		
		String[] camposObligatorios = {
				MIContratosValidator.validarEnteroString(beanAltaContratoInversionRequest.getNumeroFolio()),	
				beanAltaContratoInversionRequest.getFechaAlta()};
				
		int[] longitudesObligatorias = {9,10};
				
		String[] camposNoObligatorios = {
				beanAltaContratoInversionRequest.getMulticanalidad().getEmpresaConexion(),		
				beanAltaContratoInversionRequest.getMulticanalidad().getCanalOperacion(),		
				beanAltaContratoInversionRequest.getMulticanalidad().getCanalComercializacion(),	
		};

		int[] longitudesNoObligatorias = {4,2,2};
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanAltaContratoInversionResultado response=daoGestionContratos.altaContrato(beanAltaContratoInversionRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que hará la definición para el llamado al método que consumirá la transacción MI23 : Alta de contrato de inversión al amparo de una chequera
	 */
	@Override
	public BeanAltaContratosAmparadosChequeraResultado altaCtoAmparadoChequera(BeanAltaContratosAmparadosChequeraRequest beanAltaContratosAmparadosChequeraRequest,ArchitechSessionBean session)  throws BusinessException {
		String [] camposObligatorios={beanAltaContratosAmparadosChequeraRequest.getEmpresaContrato(),beanAltaContratosAmparadosChequeraRequest.getCentroContrato(),
				beanAltaContratosAmparadosChequeraRequest.getNumeroContrato(),beanAltaContratosAmparadosChequeraRequest.getCodigoProducto(),
				beanAltaContratosAmparadosChequeraRequest.getCodigoSubProducto(),beanAltaContratosAmparadosChequeraRequest.getCodigoEjecutivo(),
				beanAltaContratosAmparadosChequeraRequest.getNumeroPersonaTitular(),beanAltaContratosAmparadosChequeraRequest.getCentroCostosContrato(),
				beanAltaContratosAmparadosChequeraRequest.getCodigoBanca(),beanAltaContratosAmparadosChequeraRequest.getFechaAlta()};
		int [] longitudes={4,4,12,2,4,8,8,4,3,10};
		String [] camposNoObligatorios={beanAltaContratosAmparadosChequeraRequest.getMulticanalidad().getCanalComercializacion(),beanAltaContratosAmparadosChequeraRequest.getMulticanalidad().getCanalOperacion(),
				beanAltaContratosAmparadosChequeraRequest.getMulticanalidad().getEmpresaConexion()};
		int [] longitudesCamposNoObligatorios={2,2,4};
		
		
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesCamposNoObligatorios);
			BeanAltaContratosAmparadosChequeraResultado response=daoGestionContratos.altaCtoAmparadoChequera(beanAltaContratosAmparadosChequeraRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que hará la definición para el llamado al método que consumirá la transacción MI19 : Modificación de estatus de contrato
	 */
	@Override
	public ResponseGenerico cambioEstatusContrato(BeanModificacionEstatusContratoRequest beanModificacionEstatusContratoRequest,ArchitechSessionBean session) throws BusinessException {
		String[] camposObligatorios = {
				beanModificacionEstatusContratoRequest.getTipoConsulta(),	
				beanModificacionEstatusContratoRequest.getEstatusContrato(),	
				beanModificacionEstatusContratoRequest.getFechaCambio()};
		int[] longitudesObligatorias = {2,2,10};
		
		String[] camposNoObligatorios = {
				beanModificacionEstatusContratoRequest.getMulticanalidad().getEmpresaConexion(),		
				beanModificacionEstatusContratoRequest.getMulticanalidad().getCanalOperacion(),		
				beanModificacionEstatusContratoRequest.getMulticanalidad().getCanalComercializacion(),
				beanModificacionEstatusContratoRequest.getEmpresaContrato(),			
				beanModificacionEstatusContratoRequest.getCentroContrato(),			
				beanModificacionEstatusContratoRequest.getNumeroContrato(),			
				beanModificacionEstatusContratoRequest.getCodigoProducto(),			
				beanModificacionEstatusContratoRequest.getCodigoSubProducto(),			
				beanModificacionEstatusContratoRequest.getEmpresaContratoInversion(),	
				beanModificacionEstatusContratoRequest.getAliasContrato(),			
				beanModificacionEstatusContratoRequest.getTextoLibreEstatus(),
				beanModificacionEstatusContratoRequest.getDetalleEstatus()};
		
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,50,2};
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			ResponseGenerico response=daoGestionContratos.cambioEstatusContrato(beanModificacionEstatusContratoRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
	
	/**
	 * Método que hará la definición para el llamado al método que consumirá la transacción MI21 : Cambio de número de contrato
	 */
	@Override
	public ResponseGenerico cambioAliasContrato(BeanCambioNumeroContratoRequest beanCambioNumeroContratoRequest, ArchitechSessionBean session) throws BusinessException {
		info("BOGestionContratosImpl::cambioAliasContrato()");
		
		String [] camposObligatorios ={beanCambioNumeroContratoRequest.getTipoConsulta(),beanCambioNumeroContratoRequest.getNuevoAliasContrato()};
		int [] longitudes={2,15};
		String [] camposNoObligatorios={beanCambioNumeroContratoRequest.getMulticanalidad().getCanalComercializacion(),
				beanCambioNumeroContratoRequest.getMulticanalidad().getCanalOperacion(),beanCambioNumeroContratoRequest.getMulticanalidad().getEmpresaConexion(),
				beanCambioNumeroContratoRequest.getEmpresaContratoInversion(),beanCambioNumeroContratoRequest.getAliasContrato(),
				beanCambioNumeroContratoRequest.getEmpresaContrato(),beanCambioNumeroContratoRequest.getCentroContrato(),
				beanCambioNumeroContratoRequest.getNumeroContrato(),beanCambioNumeroContratoRequest.getCodigoProducto(),
				beanCambioNumeroContratoRequest.getCodigoSubProducto()};
		int [] longitudesCamposNoObligatorios={2,2,4,4,15,4,4,12,2,4};
				
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesCamposNoObligatorios);
			ResponseGenerico response=daoGestionContratos.cambioAliasContrato(beanCambioNumeroContratoRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			this.info("======= Se ha ejecutado la transacción MI21 =======");
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}


	/**
	 * Método que hará la definición para el llamado al método que consumirá la transacción MI24 : Consulta de modificaciones del contrato
	 */
	@Override
	public BeanConsultaModificacionesContratoResultado historicoModifContrato(BeanConsultaModificacionesContratoRequest beanConsultaModificacionesContratoRequest,ArchitechSessionBean sesion) throws BusinessException {
		String[] camposObligatorios = {beanConsultaModificacionesContratoRequest.getTipoConsulta()};
		int[] longitudesObligatorias = {2};
		String[] camposNoObligatorios = {beanConsultaModificacionesContratoRequest.getMulticanalidad().getEmpresaConexion(),beanConsultaModificacionesContratoRequest.getMulticanalidad().getCanalOperacion(),beanConsultaModificacionesContratoRequest.getMulticanalidad().getCanalComercializacion(),
										beanConsultaModificacionesContratoRequest.getEmpresaContrato(),beanConsultaModificacionesContratoRequest.getCentroContrato(),beanConsultaModificacionesContratoRequest.getNumeroContrato(),beanConsultaModificacionesContratoRequest.getCodigoProducto(),
										beanConsultaModificacionesContratoRequest.getCodigoSubProducto(),beanConsultaModificacionesContratoRequest.getEmpresaContratoInversion(),beanConsultaModificacionesContratoRequest.getAliasContrato(),
										beanConsultaModificacionesContratoRequest.getRellamada().getMasDatos(),beanConsultaModificacionesContratoRequest.getRellamada().getMemento()};
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,1,15};
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanConsultaModificacionesContratoResultado response=daoGestionContratos.historicoModifContrato(beanConsultaModificacionesContratoRequest, sesion);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			this.info("======= Se ha ejecutado la transacción MI24 =======");
			return response; 
		} catch (ExceptionDataAccess e) {
			error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que hará la definición para el llamado al método que consumirá la transacción MI31 : Mantenimiento de datos generales
	 */
	@Override
	public ResponseGenerico modificacionDatosContrato(BeanMantenimientoDatosGeneralesRequest beanMantenimientoDatosGeneralesRequest,ArchitechSessionBean session) throws BusinessException {
		
		beanMantenimientoDatosGeneralesRequest.setMontoMaximoOperacionContrato(MIContratosValidator.validarPunto(beanMantenimientoDatosGeneralesRequest.getMontoMaximoOperacionContrato()));
		
		String[] camposObligatorios={beanMantenimientoDatosGeneralesRequest.getTipoConsulta(),beanMantenimientoDatosGeneralesRequest.getCodigoBanca(),
				beanMantenimientoDatosGeneralesRequest.getIndicadorTipoCuenta(),beanMantenimientoDatosGeneralesRequest.getIndicadorTipoContrato(),
				beanMantenimientoDatosGeneralesRequest.getIndicadorAutorizacionPosicionCortoCapitales(),beanMantenimientoDatosGeneralesRequest.getIndicadorAutorizacionPosicionCortoFondos(),
				beanMantenimientoDatosGeneralesRequest.getIndicadorAutorizacionCruceFondosInversion(),beanMantenimientoDatosGeneralesRequest.getIndicadorDiscrecionalidad(),
				beanMantenimientoDatosGeneralesRequest.getIndicadorTipoCustodiaDirecto(),beanMantenimientoDatosGeneralesRequest.getIndicadorTipoCustodiaRepos(),
				beanMantenimientoDatosGeneralesRequest.getIndicadorEstadoCuenta(),beanMantenimientoDatosGeneralesRequest.getMontoMaximoOperacionContrato(),
				beanMantenimientoDatosGeneralesRequest.getDivisamaximoOperacionContrato(),beanMantenimientoDatosGeneralesRequest.getIndicadorEnvioCorreoCartaConfirmacionCapitales(),
				beanMantenimientoDatosGeneralesRequest.getIndicadorContratoAgrupado(),beanMantenimientoDatosGeneralesRequest.getIndicadorContratoEspejo(),
				beanMantenimientoDatosGeneralesRequest.getCodigoTipoServicio()};
		int[] longitudesObligatorias={2,3,1,1,1,1,1,1,1,1,1,15,3,1,1,1,2};
	
		
		String[] camposNoObligatorios = {
				beanMantenimientoDatosGeneralesRequest.getMulticanalidad().getEmpresaConexion(),
				beanMantenimientoDatosGeneralesRequest.getMulticanalidad().getCanalOperacion(),
				beanMantenimientoDatosGeneralesRequest.getMulticanalidad().getCanalComercializacion(),
				beanMantenimientoDatosGeneralesRequest.getEmpresaContrato(),
				beanMantenimientoDatosGeneralesRequest.getCentroContrato(),
				beanMantenimientoDatosGeneralesRequest.getNumeroContrato(),
				beanMantenimientoDatosGeneralesRequest.getCodigoProducto(),
				beanMantenimientoDatosGeneralesRequest.getCodigoSubProducto(),
				beanMantenimientoDatosGeneralesRequest.getEmpresaContratoInversion(),
				beanMantenimientoDatosGeneralesRequest.getAliasContrato(),
				beanMantenimientoDatosGeneralesRequest.getEmpresaContratoAgrupador(),
				beanMantenimientoDatosGeneralesRequest.getCentroContratoAgrupador(),	
				beanMantenimientoDatosGeneralesRequest.getNumeroContratoAgrupador(),
				beanMantenimientoDatosGeneralesRequest.getCodigoProductoContratoAgrupador(),	
				beanMantenimientoDatosGeneralesRequest.getCodigoSubProductoContratoAgrupador(),				
				beanMantenimientoDatosGeneralesRequest.getEmpresaContratoEspejo(),
				beanMantenimientoDatosGeneralesRequest.getCentroContratoEspejo(),
				beanMantenimientoDatosGeneralesRequest.getNumeroContratoEspejo(),
				beanMantenimientoDatosGeneralesRequest.getCodigoProductoContratoEspejo(),
				beanMantenimientoDatosGeneralesRequest.getCodigoSubProductoContratoEspejo(),
				beanMantenimientoDatosGeneralesRequest.getCodigoAfiliacion(),
				beanMantenimientoDatosGeneralesRequest.getDescripcionContrato(),
				beanMantenimientoDatosGeneralesRequest.getDescripcionFirma(),
				beanMantenimientoDatosGeneralesRequest.getDescripcionAreaReferencia(),
				beanMantenimientoDatosGeneralesRequest.getDescripcionAreaReferenciaExterna()};
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,4,4,12,2,4,4,4,12,2,4,8,100,100,100,100};
		
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);		
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);			
			ResponseGenerico response=daoGestionContratos.modificacionDatosContrato(beanMantenimientoDatosGeneralesRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
	

	/**
	 * Método que hará la definición para el llamado al método que consumirá la transacción MI37 : Mantenimiento de datos generales
	 */
	@Override
	public ResponseGenerico modificaOpcionEdoCuenta(BeanModificacionIndicadorEstadoCuentaRequest beanBajaIndicadorEstadoCuentaRequest,ArchitechSessionBean sesion) throws BusinessException {
		String[] camposObligatorios = {beanBajaIndicadorEstadoCuentaRequest.getTipoConsulta()};
		int[] longitudes ={2};
		String[] camposNoObligatorios = {beanBajaIndicadorEstadoCuentaRequest.getMulticanalidad().getEmpresaConexion(),beanBajaIndicadorEstadoCuentaRequest.getMulticanalidad().getCanalOperacion(),beanBajaIndicadorEstadoCuentaRequest.getMulticanalidad().getCanalComercializacion(),
				beanBajaIndicadorEstadoCuentaRequest.getTipoConsulta(),beanBajaIndicadorEstadoCuentaRequest.getEmpresaContrato(),beanBajaIndicadorEstadoCuentaRequest.getCentroContrato(),beanBajaIndicadorEstadoCuentaRequest.getNumeroContrato(),
				beanBajaIndicadorEstadoCuentaRequest.getCodigoProducto(),beanBajaIndicadorEstadoCuentaRequest.getCodigoSubProducto(),beanBajaIndicadorEstadoCuentaRequest.getEmpresaContratoInversion(),beanBajaIndicadorEstadoCuentaRequest.getAliasContrato(),
				beanBajaIndicadorEstadoCuentaRequest.getIndicadorEstadoCuenta(),beanBajaIndicadorEstadoCuentaRequest.getFecha()};		
		int[] longitudes2 = {4,2,2,2,4,4,12,2,4,4,15,1,10};
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudes2);
			ResponseGenerico response=daoGestionContratos.modificaOpcionEdoCuenta(beanBajaIndicadorEstadoCuentaRequest, sesion);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método conversor de cuentas que hará el llamado al método que consumirá la transacción MI16.
	 * @param beanConversorCuentasRequest
	 * @param sesion
	 * @return BeanConversorCuentasResultado
	 */
	@Override
	public BeanConversorCuentasResultado conversorCuentas(BeanConversorCuentasRequest beanConversorCuentasRequest,ArchitechSessionBean sesion) throws BusinessException {
		BeanConversorCuentasResultado request = new BeanConversorCuentasResultado();
		List<BeanConversorCuentasResponse> requestList = new ArrayList<>();
		if(beanConversorCuentasRequest!=null && beanConversorCuentasRequest.getContratos().size()>10){
			request.setCodError("");
			request.setMsgError("EL CONVERSOR PROCESA COMO MAXIMO 10 CONTRATOS");
			return request;
		}

		try {
			for(int i=0;i<beanConversorCuentasRequest.getContratos().size();i++){
				BeanConversorCuentasEntrada objIte = beanConversorCuentasRequest.getContratos().get(i);

				String[] camposNoObligatorios = {
						beanConversorCuentasRequest.getMulticanalidad().getEmpresaConexion(),
						beanConversorCuentasRequest.getMulticanalidad().getCanalOperacion(),
						beanConversorCuentasRequest.getMulticanalidad().getCanalComercializacion(),
						objIte.getEmpresaContrato(),
						objIte.getCentroContrato(),
						objIte.getNumeroContrato(),
						objIte.getCodigoProducto(),
						objIte.getCodigoSubProducto(),
						objIte.getCodigoEmpresaContratoInversion(),
						objIte.getAliasContrato()};
				int[] longitudesNoOblig = {4,2,2,4,4,12,2,4,4,15};
				
				String[] camposObliAltair = {
						objIte.getEmpresaContrato(),
						objIte.getCentroContrato(),
						objIte.getNumeroContrato(),
						objIte.getCodigoProducto(),
						objIte.getCodigoSubProducto()};
				
				int[] longitudesAlt = {4,4,12,2,4};
				
				String[] camposObliNoAltair = {
						objIte.getCodigoEmpresaContratoInversion(),
						objIte.getAliasContrato()};
				
				int[] longitudesNoAlt = {4,15};

				MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoOblig);
				boolean altair = false;

				if(objIte.getEmpresaContrato().trim().length()>0 || objIte.getCentroContrato().trim().length()>0 || objIte.getNumeroContrato().trim().length()>0 ||
						objIte.getCodigoProducto().trim().length()>0 || objIte.getCodigoSubProducto().trim().length()>0){
					altair = true;
					
					MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObliAltair, longitudesAlt);

				}else if (objIte.getCodigoEmpresaContratoInversion().trim().length()>0 || objIte.getAliasContrato().trim().length()>0) {
					altair =false;
					MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObliNoAltair, longitudesNoAlt);
				}

				BeanConsultaInversionRequest entrada = new BeanConsultaInversionRequest();
				entrada = getBeanEntrada(objIte,beanConversorCuentasRequest,altair);
				
				BeanConsultaInversionResultado resp = consultaDatosContrato(entrada,null);
				request.setMsgError(resp.getMsgError());
				request.setCodError(resp.getCodError());
				
				if(resp!=null && "MIA0000".equals(resp.getCodError())){
					BeanConsultaInversionResponse objRes = resp.getListaConsultaInversion().get(0);
					BeanDatosContratos objResDos = objRes.getBeanDatosContratos();
					
					BeanConversorCuentasResponse obj = new BeanConversorCuentasResponse();
					obj= getBeanConversorCuentasResponse(objResDos);

					requestList.add(obj);
				}
				request.setBeanconversorCuentas(requestList);
			}
			return request;
		}catch (BusinessException e){
			error("", e);
			throw new BusinessException(e.getCode(),e.getMessage());
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	
	}
	
	/**
	 * Método que construye el bean de contrato de salida para el conversor de cuentas
	 * @param objResDos
	 * @return BeanConversorCuentasResponse
	 */
	public BeanConversorCuentasResponse getBeanConversorCuentasResponse(BeanDatosContratos objResDos){
		BeanConversorCuentasResponse obj = new BeanConversorCuentasResponse();
		
		obj.setCodigoEmpresa(objResDos.getCodigoEmpresa());
		obj.setCentroContrato(objResDos.getCentroContrato());
		obj.setNumeroContrato(objResDos.getNumeroContrato());
		obj.setCodigoProductoContrato(objResDos.getCodigoProducto());
		obj.setCodigoSubProductoContrato(objResDos.getCodigoSubProductoContrato());
		obj.setCodigoMonedaContrato(objResDos.getCodigoMonedaContrato());
		obj.setFolioProvieneContrato(objResDos.getNumeroFolio());
		obj.setCodEmpresaContratoInversion(objResDos.getCodigoEmpresaContrato());
		obj.setCodigoBancaContrato(objResDos.getCodigoBanca());
		obj.setFechaAperturaContrato(objResDos.getFechaApertura());
		obj.setAliasContrato(objResDos.getDescripcionAlias());
		obj.setFechaVencimientoContrato(objResDos.getFechaVencimiento());
		obj.setCodigoEjecutivoContrato(objResDos.getCodigoEjecutivo());
		obj.setCentroCostosContrato(objResDos.getCentroCostosContrato());
		obj.setIndicadorTipoCuenta(objResDos.getIndicadorTipoCuenta());
		obj.setIndicadorTipoContrato(objResDos.getIndicadorTipoContrato());
		obj.setIndAutPosicionCortoCapitales(objResDos.getIndicadorAutorizacionPosicionCortoCapitales());
		obj.setIndAutPosicionCortoFondos(objResDos.getIndicadorAutorizacionPosicionCortoFondos());
		obj.setIndAutPosicionFondosInvers(objResDos.getIndicadorAutorizacionPosicionCortoFondos());
		obj.setIndicadorDiscrecionalidad(objResDos.getIndicadorDiscrecionalidad());
		obj.setIndicadorTipoCustodiaDirecto(objResDos.getIndicadorTipoCustodaDirecto());
		obj.setIndicadorTipoCustodiaRepos(objResDos.getIndicadorTipoCustodiaRepos());
		obj.setIndicadorEstadoCuenta(objResDos.getIndicadorEstadoCuenta());
		obj.setMontoMaxOperarContrato(objResDos.getImporteMaximoOperacion());
		obj.setDivisaMontoMaxContrato(objResDos.getDivisaMaximaOperacion());
		obj.setIndicadorEnvioCorreo(objResDos.getIndicadorEnvioCorreoCartaConfirmacionCapitales());
		obj.setEstatusContrato(objResDos.getEstatusContrato());
		obj.setDetalleEstatus(objResDos.getDetalleEstatus());
		obj.setFechaEstadoContrato(objResDos.getFechaEstado());
		obj.setIndicadorContratoAgrupado(objResDos.getIndicadorContratoAgrupador());
		obj.setIndicadorTipoPosicion(objResDos.getIndicadorTipoPosicion());
		obj.setIndicadorContratoEspejo(objResDos.getIndicadorContratoEspejo());
		obj.setIndCtoAmparadoChequera(objResDos.getIndicadorContratoChequera());
		obj.setCodigoTipoServicio(objResDos.getCodigoTipoServicio());
		obj.setCodigoAfi(objResDos.getCodigoAsesorAfinanciero());
		obj.setIndicadorBeneficiario(objResDos.getIndicadorBeneficiarios());
		obj.setIndicadorProveedoresRecursos(objResDos.getIndicadorProveedoresRecursos());
		obj.setIndicadorAccionistas(objResDos.getIndicadorAccionistas());
		obj.setFechaUltimaOperacion(objResDos.getFechaUltimaOperacion());
		obj.setFechaUltimaModificacion(objResDos.getFechaUltimaModificacion());
		obj.setDescripcionSubEstatus(objResDos.getDescripcionTextoLibreSubestatus());
		
		return obj;
		
	}
	/**
	 * 
	 * @param objIte
	 * @param beanConversorCuentasRequest
	 * @param altair
	 * @return BeanConsultaInversionRequest
	 */
	public BeanConsultaInversionRequest getBeanEntrada(BeanConversorCuentasEntrada objIte, BeanConversorCuentasRequest beanConversorCuentasRequest,boolean altair ){
		BeanConsultaInversionRequest entrada = new BeanConsultaInversionRequest();

		MulticanalidadGenerica multi = new MulticanalidadGenerica();
		
		if(altair){
			entrada.setEmpresaContrato(objIte.getEmpresaContrato());
			entrada.setCentroContrato(objIte.getCentroContrato());
			entrada.setNumeroContrato(objIte.getNumeroContrato());
			entrada.setCodigoProducto(objIte.getCodigoProducto());
			entrada.setCodigoSubProducto(objIte.getCodigoSubProducto());
			entrada.setTipoConsulta("01");
			entrada.setEmpresaContratoInversion("");
			entrada.setAliasContrato("");
		}else{
			entrada.setEmpresaContratoInversion(objIte.getCodigoEmpresaContratoInversion());
			entrada.setAliasContrato(objIte.getAliasContrato());
			entrada.setTipoConsulta("02");

			entrada.setEmpresaContrato("");
			entrada.setCentroContrato("");
			entrada.setNumeroContrato("");
			entrada.setCodigoProducto("");
			entrada.setCodigoSubProducto("");
		}
		multi.setEmpresaConexion(beanConversorCuentasRequest.getMulticanalidad().getEmpresaConexion());
		multi.setCanalComercializacion(beanConversorCuentasRequest.getMulticanalidad().getCanalComercializacion());
		multi.setCanalOperacion(beanConversorCuentasRequest.getMulticanalidad().getCanalOperacion());
		entrada.setMulticanalidad(multi);
		entrada.setIndicadorInformacionContactos(MIContratosConstantes.NO_HAY_DATOS);
		entrada.setIndicadorInformacionContratosRelacionados(MIContratosConstantes.NO_HAY_DATOS);
		entrada.setIndicadorInformacionIntervinientes(MIContratosConstantes.NO_HAY_DATOS);
		entrada.setIndicadorInformacionReferenciaContrato(MIContratosConstantes.NO_HAY_DATOS);
		entrada.setIndicadorUsoDomicilios(MIContratosConstantes.NO_HAY_DATOS);
		entrada.setFechaConsulta("");
		
		return entrada;
	}
	
	/**
	 * Método que llamará al método que consume la transacción MI40: Listar contratos de inversión
	 * @param beanListaContratosInversionRequest
	 * @param sesion
	 * @return BeanListaContratosInversionResultado
	 * @throws BusinessException
	 */
	@Override
	public BeanListaContratosInversionResultado listarContratosInversion(
			BeanListaContratosInversionRequest beanListaContratosInversionRequest,
			ArchitechSessionBean sesion) throws BusinessException {
		
		String [] camposObligatorios = {
				beanListaContratosInversionRequest.getCodigoUsuarioConexion()};
		int [] longitudesCamposObligatorios = {8};
		String [] camposNoObligatorios ={beanListaContratosInversionRequest.getIndicadorTodosCampos(),
				beanListaContratosInversionRequest.getMulticanalidad().getEmpresaConexion(),
				beanListaContratosInversionRequest.getMulticanalidad().getCanalOperacion(),
				beanListaContratosInversionRequest.getMulticanalidad().getCanalComercializacion(),
				beanListaContratosInversionRequest.getCodigoEmpresaContratoInversion(),
				beanListaContratosInversionRequest.getAliasContrato(),
				beanListaContratosInversionRequest.getCodigoPersonaTitular(),
				beanListaContratosInversionRequest.getRellamada().getMemento(),
				beanListaContratosInversionRequest.getRellamada().getMasDatos(),
				beanListaContratosInversionRequest.getCodigoEjecutivo()};
		int [] longitudesCamposNoObligatorios={1,4,2,2,4,15,8,26,1,8};
		
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesCamposObligatorios);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesCamposNoObligatorios);
			BeanListaContratosInversionResultado resultado = new BeanListaContratosInversionResultado();
			BeanListaContratosInversionResultado acumulado = new BeanListaContratosInversionResultado();
			List<BeanListaContratosInversionResponse> lista = new ArrayList<>();
			do {
				acumulado = daoGestionContratos.listarContratosInversion(beanListaContratosInversionRequest, sesion);
				lista.addAll(acumulado.getListaContratoInversion());
				beanListaContratosInversionRequest.setRellamada(acumulado.getRellamada());
			} while (MIContratosConstantes.SI_HAY_DATOS.equals(resultado.getRellamada().getMasDatos()) && 
					MIContratosConstantes.SI_HAY_DATOS.equalsIgnoreCase(beanListaContratosInversionRequest.getIndicadorTodosCampos()));
			resultado.setListaContratoInversion(lista);
			resultado.setRellamada(acumulado.getRellamada());
			resultado.setCodError(acumulado.getCodError());
			resultado.setMsgError(acumulado.getMsgError());
			resultado.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(resultado.getCodError(), resultado.getMsgError()));
			return resultado;
		} catch (ExceptionDataAccess e) {
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}		
		
	}		
}
