package mx.isban.micontratos.catalogos.dao.impl;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaEstatusContratosRequest;
import mx.isban.micontratos.beans.catalogos.BeanModificacionEstatusTablaCatalogoRequest;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.catalogos.dao.DAOConsultaCatalogos;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.SingletonIDA;

import org.apache.commons.lang.StringUtils;

/**
 * @author Daniel Hernández Soto
 *
 */

@Stateless
@Local(DAOConsultaCatalogos.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaCatalogosImpl extends Architech implements DAOConsultaCatalogos{
	//Código de la transacción MI29
	private static final String MI29 = "MI29";
	//Código de operación de la transacción MI29
	private static final String MI29_CODE_OPERATION = "MI29_BAJA_CATALOGO_SUBESTATUS";
	//Código de la transacción MI28
	private static final String MI28 = "MI28";
	//Código de operación de la transacción MI28
	private static final String MI28_CODE_OPERATION = "MI28_SALTA_CATALOGO_SUBESTATUS";
	//Código de la transacción MI27
	private static final String MI27 = "MI27";
	//Código de operación de la transacción MI27
	private static final String MI27_CODE_OPERATION = "MI27_MODIFICACION_CATALOGO_ESTATUS";
	//Código de operación de la transacción MI26
	private static final String MI26_CODE_OPERATION = "MI26_BAJA_ESTATUS";
	//Código de la transacción MI26
	private static final String MI26 = "MI26";
	//Código de la transacción MI25
	private static final String MI25 = "MI25";
	//Código de operación de la transacción MI25
	private static final String MI25_CODE_OPERATION = "MI25_ALTA_CATALOGO_ESTATUS";

	
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -3317858687331243665L;

	
	/**
	 * Metódo que ejecutará el consumo de la transacción MI25 : Alta de catalogos de estatus
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess
	 */
	@Override
	public ResponseGenerico altaEstatusOperativo(BeanAltaCatalogosEstatusRequest beanAltaCatalogosEstatusRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess {

		this.info("DAOConsultaCatalogosImpl::altaCatalogosEstatus()");
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getCodigoEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaCatalogosEstatusRequest.getDescripcionEstatus(), 50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorOperativo(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorBloqueo(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorEstatusCancelado(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorBloqueoLegal(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorInversiones(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorDetalle(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorBloqueable(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorCancelable(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosEstatusRequest.getIndicadorValidacionDocumentos(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
	
		
		final String mensajeTrama =  trama.toString();
		info("MI25 Request: " + mensajeTrama);

		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI25_CODE_OPERATION, MI25, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);

	}


	/**
	 * Metódo que ejecutará el consumo de la transacción MI26 : baja  de estatus
	 * @param beanBajaEstatusContratosRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess
	 */
	@Override
	public ResponseGenerico bajaEstatusOperativo(BeanBajaEstatusContratosRequest beanBajaEstatusContratosRequest,ArchitechSessionBean session) throws ExceptionDataAccess {

		this.info("DAOConsultaCatalogosImpl::bajaEstatusContratos()");
		final StringBuilder trama = new StringBuilder();
		trama.append(StringUtils.leftPad(beanBajaEstatusContratosRequest.getCodigoEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		 
		final String mensajeTrama = trama.toString();
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI26_CODE_OPERATION, MI26, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}

	/**
	 * Metódo que ejecutará el consumo de la transacción MI27 : Modificación de catalogos de estatus
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess
	 */
	@Override
	public ResponseGenerico modificarEstatusOperativo(BeanModificacionEstatusTablaCatalogoRequest beanModificacionEstatusTablaCatalogoRequest, ArchitechSessionBean session) throws ExceptionDataAccess {

		this.info("DAOConsultaCatalogosImpl::modificacionCatalogosEstatus()");
	
		final StringBuilder trama = new StringBuilder();
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getCodigoEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionEstatusTablaCatalogoRequest.getDescripcionEstatus(), 50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorOperativo(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorBloqueo(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorCancelacion(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorBloqueoLegal(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorInversion(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorDetalle(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorBloqueable(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));	
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorCancelable(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstatusTablaCatalogoRequest.getIndicadorValidacionDocumentos(), 1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama =  trama.toString();

		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI27_CODE_OPERATION, MI27, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);

	}



	/**
	 * Metódo que ejecutará el consumo de la transacción MI28 : Alta de catalogos de SubEstatus
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess
	 */
	@Override
	public ResponseGenerico altaSubEstatusOperativo(BeanAltaCatalogosSubEstatusRequest beanAltaCatalogosSubEstatusRequest,ArchitechSessionBean session) throws ExceptionDataAccess {

		this.info("DAOConsultaCatalogosImpl::altaSubEstatusOperativo()");
		
		final StringBuilder trama = new StringBuilder();
		trama.append(StringUtils.leftPad(beanAltaCatalogosSubEstatusRequest.getCodigoEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaCatalogosSubEstatusRequest.getCodigoSubEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaCatalogosSubEstatusRequest.getDescripcionSubEstatus(), 50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama =  trama.toString();

		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI28_CODE_OPERATION, MI28, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}


	/**
	 * Metódo que ejecutará el consumo de la transacción MI29 : Baja de catalogos de SubEstatus
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess
	 */
	@Override
	public ResponseGenerico bajaSubEstatusOperativo(BeanBajaCatalogosSubEstatusRequest beanBajaCatalogosSubEstatusRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		this.info("DAOConsultaCatalogosImpl::bajaSubEstatusOperativo()");
		
		final StringBuilder trama = new StringBuilder();
		trama.append(StringUtils.leftPad(beanBajaCatalogosSubEstatusRequest.getCodigoEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaCatalogosSubEstatusRequest.getCodigoSubEstatus(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama = trama.toString();
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI29_CODE_OPERATION, MI29, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);

	}

}