package mx.isban.micontratos.gestioncontratos.dao;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratoInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanAltaContratosAmparadosChequeraResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanCambioNumeroContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaModificacionesContratoResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContratosInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanMantenimientoDatosGeneralesRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionEstatusContratoRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanModificacionIndicadorEstadoCuentaRequest;


/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * interfaz que define los métodos en cuya implementación se ejecutarán el consumo de las transacciones de gestión de contratos
 *
 */
public interface DAOGestionContratos {
	
   

	/**
	 * Método que ejecutará el consumo de la transacción MI16 : Consulta de inversión
	 * @param beanConsultaInversionRequest
	 * @param session
	 * @return BeanConsultaInversionResultado
	 */
	BeanConsultaInversionResultado consultaDatosContrato(BeanConsultaInversionRequest beanConsultaInversionRequest,ArchitechSessionBean session) throws ExceptionDataAccess;


	/**
	 * Método que ejecutará el consumo de la transacción MI18 : Alta de contrato de inversión
	 * @param beanAltaContratoInversionRequest
	 * @param sesion
	 * @return BeanAltaContratoInversionResultado
	 */
	BeanAltaContratoInversionResultado altaContrato(BeanAltaContratoInversionRequest beanAltaContratoInversionRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;

	
	/**
	 * Método que ejecutará el consumo de la transacción MI19 : Modificación de estatus de contrato
	 * @param beanAltaContratoInversionRequest
	 * @param sesion
	 * @return ResponseGenerico
	 */
	ResponseGenerico cambioEstatusContrato(BeanModificacionEstatusContratoRequest beanAltaContratoInversionRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;


	/**
	 *  Método que ejecutará el consumo de la transacción MI21 : Cambio de número de contrato
	 * @param beanCambioNumeroContratoRequest
	 * @param sesion
	 * @return ResponseGenerico
	 */
	ResponseGenerico cambioAliasContrato(BeanCambioNumeroContratoRequest beanCambioNumeroContratoRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;


	/**
	 * Método que ejecutará el consumo de la transacción MI23 : Alta de contrato de inversión al amparo de una chequera
	 * @param beanAltaContratosAmparadosChequeraRequest
	 * @param sesion
	 * @return BeanAltaContratosAmparadosChequeraResultado
	 */
	BeanAltaContratosAmparadosChequeraResultado altaCtoAmparadoChequera(BeanAltaContratosAmparadosChequeraRequest beanAltaContratosAmparadosChequeraRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;


	/**
	 * Método que ejecutará el consumo de la transacción MI24 : Consulta de modificaciones del contrato
	 * @param beanConsultaModificacionesContratoRequest
	 * @param sesion
	 * @return BeanConsultaModificacionesContratoResultado
	 */
	BeanConsultaModificacionesContratoResultado historicoModifContrato(BeanConsultaModificacionesContratoRequest beanConsultaModificacionesContratoRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;

	/**
	 * Método que ejecutará el consumo de la transacción MI31 : Mantenimiento de datos generales
	 * @param beanMantenimientoDatosGeneralesRequest
	 * @param sesion
	 * @return ResponseGenerico
	 */
	ResponseGenerico modificacionDatosContrato(BeanMantenimientoDatosGeneralesRequest beanMantenimientoDatosGeneralesRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;


	/**
	 * Método que ejecutará el consumo de la transacción MI37 : Modificación del indicador de estado de cuenta
	 * @param beanBajaIndicadorEstadoCuentaRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess
	 */
	ResponseGenerico modificaOpcionEdoCuenta(BeanModificacionIndicadorEstadoCuentaRequest beanBajaIndicadorEstadoCuentaRequest,ArchitechSessionBean sesion) throws ExceptionDataAccess;
	
	/**
	 * Método que ejecutará el consumo de la transacción MI40 : Listar contratos de inversión
	 * @param beanListaContratosInversionRequest
	 * @param session
	 * @return BeanListaContratosInversionResultado
	 * @throws ExceptionDataAccess
	 */
	BeanListaContratosInversionResultado listarContratosInversion(BeanListaContratosInversionRequest beanListaContratosInversionRequest, ArchitechSessionBean session) throws ExceptionDataAccess;
	
}
