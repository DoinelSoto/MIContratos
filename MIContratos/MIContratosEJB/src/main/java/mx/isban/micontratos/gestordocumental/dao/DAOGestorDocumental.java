package mx.isban.micontratos.gestordocumental.dao;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanModificacionEstadosDocumentosRequest;


/**
 * @author everis
 *
 */
public interface DAOGestorDocumental{
	

	/**
	 * Metódo que ejecutará el consumo de la transacción MI17 : consulta de tabla auxiliar de gestor de documentos
	 * @param beanConsultaTablaAuxiliarGestorRequest
	 * @param session
	 * @return BeanConsultaTablaAuxiliarGestorResultado
	 * @throws ExceptionDataAccess 
	 */	
	BeanConsultaTablaAuxiliarGestorResultado listarDocumentosContrato(BeanConsultaTablaAuxiliarGestorRequest beanConsultaTablaAuxiliarGestorRequest,ArchitechSessionBean session) throws ExceptionDataAccess;

	
	/**
	 * Metódo que ejecutará el consumo de la transacción MI38 : consulta de indicador de obligatoriedad
	 * @param beanIndicadorObligatoriedadRequest
	 * @param session
	 * @return BeanIndicadorObligatoriedadResultado
	 */
	BeanIndicadorObligatoriedadResultado consultarVencimientoContrato(BeanIndicadorObligatoriedadRequest beanIndicadorObligatoriedadRequest,ArchitechSessionBean session)throws ExceptionDataAccess;



	/**
	 * Metódo que ejecutará el consumo de la transacción MI20 : Modificacion de estado de documentos 
	 * @param beanModificacionEstadosDocumentosRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico modificarEstatusDocumento( BeanModificacionEstadosDocumentosRequest beanModificacionEstadosDocumentosRequest,ArchitechSessionBean session)throws ExceptionDataAccess;
	
}
