package mx.isban.micontratos.util;

/**
 * @author everis
 *
 */
public final class MIContratosConstantes {

	/**
	 * Property del relleno para el entramado
	 */
	public static final String RELLENO_ENTRADA=" ";

	/**
	 * Relleno alfanúmerico de entrada
	 */
	public static final String RELLENO_ALFANUMERICO_ENTRADA=" ";
	
	/**
	 * Relleno de indicador de no datos
	 */
	public static final String NO_HAY_DATOS="N";
	
	/**
	 * Relleno de indicador de si datos
	 */
	public static final String SI_HAY_DATOS="S";
	
	/**
	 * Property del relleno para el entramado
	 */
	public static final String RELLENO_NUMERICO_ENTRADA="0";

	/**
	 * Property del separador para el destramado
	 */
	public static final String SEPARADOR_SALIDA="@";

	/**
	 * Mensaje de entrada - log info - DAO
	 */
	public static final String MENSAJE_ENTRADA="La trama de ENTRADA es la siguiente:";
	/**
	 * Mensaje de salida - log info - DAO
	 */
	public static final String MENSAJE_SALIDA="La trama de SALIDA es la siguiente:";
	/**
	 * Mensaje error - log info - DAO
	 */
	public static final String MENSAJE_ERROR="Se generó un error al realizar ";
	/**
	 * Mensaje formato JSON - log info - DAO
	 */
	public static final String MENSAJE_JSON="El resultado en formato JSON es: ";
	/**
	 * Mensaje resultado retornado - log info - DAO
	 */
	public static final String MENSAJE_RESULTADO="El resultado retornado es: ";
	/**
	 * Mensaje respuesta OK - log info - DAO
	 */
	public static final String MENSAJE_RESPUESTA_OK="Respuesta OK: ";
	/**
	 * Mensaje error - log info - BO
	 */
	public static final String MENSAJE_ERROR_EJECUCION="Se presentó un error al ejecutar";
	/**
	 * Respuesta complementaria
	 */
	public static final String MENSAJE_EJECUCION = " la ejecución de la transacción."; 
	/**
	 *
	* Constante de Error
	 */
	public static final String ER = "ER";

	/**
	 * Delimitador ERMIE
	 */
	public static final String DELIMITADOR_ERROR = "ERMI";
	/**
	 * Caracter que finaliza una salida de datos para indicar que existe mas para desentramar
	 */
	public static final String DELIMITADOR_OBJETO_TRAMA = "Ó";
	
	/**
	 *
	* Identificador del ERROR en longitudes maximas
	 */
	public static final String MIC0001_FAULT_CODE = "MIC0001_FAULT_CODE";
	/**
	 * Menssage para ERROR en longitudes maximas
	 */
	public static final String MIC0001_FAULT_MESSAGE = "MIC0001_FAULT_MESSAGE";
	/**
	 *
	* Identificador del ERROR en campos vacios
	 */
	public static final String MIC0002_FAULT_CODE = "MIC0002_FAULT_CODE";
	/**
	* Message del ERROR en campos vacios
	 */
	public static final String MIC0002_FAULT_MESSAGE = "MIC0002_FAULT_MESSAGE";
	/**
	 *
	* Identificador del ERROR en campos numericos
	 */
	public static final String MIC0003_FAULT_CODE = "MIC0003_FAULT_CODE";
	/**
	* Message del ERROR en campos numericos
	 */
	public static final String MIC0003_FAULT_MESSAGE = "MIC0003_FAULT_MESSAGE";
	
	/**
	 * Ruta del properties del catalogo de errores
	 */
	public static final String RUTA_PROPERTIES_ERRORES = "RUTA_PROPERTIES_ERRORES";
	/**
	 * Ruta del properties de constantes
	 */
	public static final String RUTA_PROPERTIES_CONSTANTES="RUTA_PROPERTIES_CONSTANTES";
	
	/**
	 * Informacion item
	 */
	public static final String INFORMACION_ITEM = "Item:";
	
	/**
	 * Mensaje general de error
	 */
	public static final String MENSAJE_LOG = "Hubo un error en la ejecución de la transacción";
	
	/**
	 * Constante cadena vacía
	 */
	public static final String CADENA_VACIA = "";

	/**
	 * Constructor privado
	 */
	private MIContratosConstantes(){
		
	}
}
