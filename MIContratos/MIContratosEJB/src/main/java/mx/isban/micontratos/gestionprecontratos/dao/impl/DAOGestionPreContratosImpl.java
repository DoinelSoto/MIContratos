package mx.isban.micontratos.gestionprecontratos.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanBajaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarResponse;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPreContratoRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPrecontratoResponse;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPrecontratoResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanModificacionTablaAuxiliarRequest;
import mx.isban.micontratos.gestionprecontratos.dao.DAOGestionPreContratos;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.util.SingletonIDA;
import mx.isban.micontratos.validator.MIContratosValidator;

import org.apache.commons.lang.StringUtils;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * Clase que contiene el código para ejecutar las transacciones de gestion de precontratos
 * @author everis
 *
 */
@Stateless
@Local(DAOGestionPreContratos.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGestionPreContratosImpl extends Architech implements DAOGestionPreContratos{

	private static final String MI13 = "MI13";

	private static final String MI13_CODE_OPERATION = "MI13_BAJA_TABLA_AUXILIAR";

	private static final String MI14_CODE_OPERATION = "CONSULTA_DE_LISTA_PRECONTRATO";

	private static final String MI14 = "MI14";

	private static final String MI15 = "MI15";

	private static final String MI15_CODE_OPERATION = "CONSULTA_DE_TABLA_AUXILIAR";

	private static final String MI12 = "MI12";

	private static final String MI12_CODE_OPERATION = "MI12_MODIFICACION_TABLA_AUXILIAR";

	private static final String MI11 = "MI11";

	private static final String MI11_CODE_OPERATION = "MI11_ALTA_PRECONTRATO_GLOBAL";

	private static final String MI10 = "MI10";

	private static final String MI10_CODE_OPERATION = "MI10_ALTA_TABLA_AUXILIAR";
	/**
	 * 
	 */
	private static final long serialVersionUID = -7709434749565934008L;
	

	/**
	 * Método que ejecutará el consumo de la transacción MI10 : Alta en tabla auxiliar
	 */
	@Override
	public ResponseGenerico altaDatosAuxiliar(BeanAltaTablaAuxiliarRequest beanAltaTablaAuxiliarRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		info("DAOGestionPreContratosImpl::altaDatosAuxiliar()");

		
		final StringBuilder trama = new StringBuilder();	
		
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getMulticanalidad().getEmpresaConexion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getMulticanalidad().getCanalOperacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getMulticanalidad().getCanalComercializacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanAltaTablaAuxiliarRequest.getNumeroFolio()), 9, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCodigoTipoRegistroGuardado(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCalidadParticipacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getNumeroPersona(), 8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getTipoDomicilio(), 3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanAltaTablaAuxiliarRequest.getSecuenciaDomicilio()), 3, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getIdentificadorTipoFirma(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCodigoTipoPoder(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getFolioPoder(), 30, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getPorcentajeParticipacion(), 5, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getPorcentajeIpab(),  5 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getTipoContacto(), 	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getPersonalidadJuridica(), 	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getNombreContacto(),	40, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getPrimerApellidoContacto(), 	20, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getSegundoApellidoContacto(), 	20, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getFechaNacimiento(), 	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getTipoDocumento(), 	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getNumeroDocumento(), 	20, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getNacionalidadContacto(), 	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getPaisNacimientoContacto(), 	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getPaisContacto(), 	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getNombreCalle(), 	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getNumeroExteriorCalle(), 	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getNumeroInteriorCalle(), 	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getNumeroTelefonicoContacto(), 	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getColonia(), 30, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCodigoPostal(), 	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCiudadPoblacionLocalidad(), 	7, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCodigoProvincia(), 	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getDelegacionMunicipio(), 	5, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getEmailContacto(), 	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getEmpresaContratoRelacionado(), 	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCentroContratoRelacionado(), 	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getNumeroContratoRelacionado(), 	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCodigoProductoContratoRelacionado(), 	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getCodigoSubproductoContratoRelacionado(), 	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getDescripcionAsociadaReferencia(), 	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaTablaAuxiliarRequest.getDescripcionAsociadaReferencia2(), 	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaTablaAuxiliarRequest.getIndicadorValidacionFolio(), 	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama = trama.toString();
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI10_CODE_OPERATION, MI10, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);

	}


	/**
	 * Método que ejecutará el consumo de la transacción MI11 : Alta de precontrato global
	 */
	@Override
	public BeanAltaPreContratoGlobalResultado altaFolioPreContrato(BeanAltaPreContratoGlobalRequest beanAltaPreContratoGlobalRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
	
		info("DAOGestionPreContratosImpl::altaPrecontratoGlobal()");
		
		final StringBuilder trama = new StringBuilder();	
		
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCodigoProductoContrato(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCodigoSubproductoContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCodigoMonedaContrato(),	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCodigoCliente(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getCodigoEjecutivoContrato(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCodigoBancaContratoInversion(),	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCodigoEmpresaContratoInversion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCentroCostosContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorTipoCuenta(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorTipoContrato(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorAutorizacionPosicionCortoCapitales(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorAutorizacionPosicionCortoFondos(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorAutorizacionCruceFondosInversion(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorDiscrecionalidad(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorTipoCustodiaDirecto(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorTipoCustodiaRepos(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getMontoMaximoOperarContrato(), 15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA)); 
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDivisaMontoMaximoOperarContrato(),	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorEnvioCorreoCartaConfirmacionCapitales(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorContratoAgrupado(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getEmpresaContratoAgrupador(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCentroContratoAgrupador(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getContratoAgrupador(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getProductoContratoAgrupador(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getSubproductoContratoAgrupador(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorContratoEspejo(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getEmpresaContratoEspejo(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getCentroContratoEspejo(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getContratoEspejo(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getProductoContratoEspejo(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getSubProductoContratoEspejo(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaPreContratoGlobalRequest.getIndicadorTipos(),	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getCodigoTipoServicio(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getCodigoAsesorFianciero(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanAltaPreContratoGlobalRequest.getSecuenciaDomicilioPrincipal()),	3, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanAltaPreContratoGlobalRequest.getSecuenciaDomicilioEnvio()),	3, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanAltaPreContratoGlobalRequest.getSecuenciaDomicilioFiscal()),	3, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDescripcionCont1(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDescripcionCont2(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDescripcionFirm1(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDescripcionFirm2(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDescripcionArre1(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDescripcionArre2(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDescripcionReex1(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaPreContratoGlobalRequest.getDescripcionReex2(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama =  trama.toString();
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion( mensajeTrama,  MI11_CODE_OPERATION,  MI11, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG); 	
		
		String respuestaTransaccion = responseDTO.getResponseMessage();
		BeanAltaPreContratoGlobalResultado resultado= new BeanAltaPreContratoGlobalResultado();
		resultado.setCodError(StringUtils.substring(respuestaTransaccion, 8 , 17 ).trim());
		resultado.setMsgError(StringUtils.substring(respuestaTransaccion, 17 , respuestaTransaccion.indexOf(MIContratosConstantes.DELIMITADOR_OBJETO_TRAMA) ).trim());
		
		String cadena = StringUtils.substring(respuestaTransaccion, respuestaTransaccion.indexOf(MIContratosConstantes.DELIMITADOR_OBJETO_TRAMA)+2 , respuestaTransaccion.length()-1 ).trim();
		resultado.setNumeroFolio(MIContratosValidator.validarEnteros(StringUtils.substring(cadena,	11,	20 ).trim()));
		return resultado;
	}
	
	/**
	 * Método que ejecutará el consumo de la transacción MI12 : Modificación de tabla auxiliar
	 */
	@Override
	public ResponseGenerico modificacionDatosAuxiliar(BeanModificacionTablaAuxiliarRequest beanModificacionTablaAuxiliarRequest,ArchitechSessionBean session) throws ExceptionDataAccess {

		info("DAOGestionPreContratosImpl::modificacionDatosAuxiliar()");
		final StringBuilder trama = new StringBuilder();	
		
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getMulticanalidad().getEmpresaConexion(),4,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getMulticanalidad().getCanalOperacion(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getMulticanalidad().getCanalComercializacion(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanModificacionTablaAuxiliarRequest.getNumeroFolio()),9,MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getCodigoTipoRegistroGuardado(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanModificacionTablaAuxiliarRequest.getSecuencialTipoRegistro()),3,MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getCalidadParticipacion(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getNumeroPersona(),8,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getTipoDomicilio(),3,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanModificacionTablaAuxiliarRequest.getSecuenciaDomicilio()),3,MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getIdentificadorTipoFirma(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getCodigoTipoPoder(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getFolioPoder(),30,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getPorcentajeParticipacion(),5,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getPorcentajeIpab(),5,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getTipoContacto(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getPersonalidadJuridica(),1,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getNombreContacto(),40,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getPrimerApellidoContacto(),20,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getSegundoApellidoContacto(),20,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getFechaNacimiento(),10,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getTipoDocumento(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getNumeroDocumento(),20,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getNacionalidadContacto(),3,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getPaisNacionalidad(),3,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getPaisContacto(),3,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getNombreCalle(),50,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getNumeroExteriorCalle(),15,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getNumeroInteriorCalle(),15,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getNumeroTelefonicoContacto(),10,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getColonia(),30,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getCodigoPostal(),8,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getLocalidad(),7,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getCodigoProvincia(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getDelegacion(),5,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getEmailContacto(),50,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getEmpresaContratoRelacionado(),4,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getCentroContratoRelacionado(),4,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getNumeroContratoRelacionado(),12,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getCodigoProductoContratoRelacionado(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionTablaAuxiliarRequest.getCodigoSubproductoContratoRelacionado(),4,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getDescripcionAsociadaReferencia(),50,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionTablaAuxiliarRequest.getDescripcionAsociadaReferencia2(),50,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));

		
		final String mensajeTrama = trama.toString();
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI12_CODE_OPERATION, MI12, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);

	}

	
	/**
	 * Método que ejecutará el consumo de la transacción MI13 : Baja de tabla auxiliar
	 */
	@Override
	public ResponseGenerico bajaDatosAuxiliar(BeanBajaTablaAuxiliarRequest beanBajaTablaAuxiliarRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		
		final StringBuilder trama = new StringBuilder();	

		trama.append(StringUtils.leftPad(beanBajaTablaAuxiliarRequest.getMulticanalidad().getEmpresaConexion(),4,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaTablaAuxiliarRequest.getMulticanalidad().getCanalOperacion(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaTablaAuxiliarRequest.getMulticanalidad().getCanalComercializacion(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanBajaTablaAuxiliarRequest.getNumeroFolio()),9,MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaTablaAuxiliarRequest.getCodigoTipoRegistroGuardado(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaTablaAuxiliarRequest.getSecuencialTipoRegistro(),3,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));

		final String mensajeTrama = trama.toString();
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI13_CODE_OPERATION, MI13, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);

	}

	

	/**
	 * Método que ejecutará el consumo de la transacción MI14 : Consulta de lista de precontrato
	 */
	@Override
	public BeanListaPrecontratoResultado consultaPreContratos(BeanListaPreContratoRequest beanListaPreContratoRequest,ArchitechSessionBean session) throws ExceptionDataAccess {		
		info("DAOGestionPreContratosImpl::consultaPreContratos()");
		BeanListaPrecontratoResultado resultado = new BeanListaPrecontratoResultado();
		final StringBuilder trama = new StringBuilder();	
		trama.append(StringUtils.leftPad(beanListaPreContratoRequest.getMulticanalidad().getEmpresaConexion(),4,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaPreContratoRequest.getMulticanalidad().getCanalOperacion(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaPreContratoRequest.getMulticanalidad().getCanalComercializacion(),2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanListaPreContratoRequest.getNumeroFolio()),9,MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaPreContratoRequest.getCodigoEjecutivo(),8,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaPreContratoRequest.getNumeroCliente(),8,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaPreContratoRequest.getFechaAperturaFolio(),10,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad((beanListaPreContratoRequest.getRellamada().getMasDatos()).toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanListaPreContratoRequest.getRellamada().getMemento(), 9, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
	

		final String mensajeTrama = trama.toString(); 
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI14_CODE_OPERATION, MI14, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanListaPrecontratoResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));

			for (String item : cadena){
			BeanListaPrecontratoResponse bean = new BeanListaPrecontratoResponse();

			bean.setNumeroFolio(Integer.valueOf(StringUtils.substring(item,	11,	20	 ).trim()));
			bean.setCodigoEmpresa(StringUtils.substring(item,20,	24		).trim());
			bean.setCodigoProducto(StringUtils.substring(item,	24,	26	).trim());
			bean.setCodigoSubProductoContrato(StringUtils.substring(item,26, 30 ).trim());
			bean.setCodigoMonedaContrato(StringUtils.substring(item,30,	33		).trim());
			bean.setNumeroPersona(StringUtils.substring(item,	33,	41	).trim());
			bean.setCodigoEmpresaContrato(StringUtils.substring(item,	41,	45	).trim());
			bean.setCodigoBanca(StringUtils.substring(item,	45,	48	 ).trim());
			bean.setFechaApertura(StringUtils.substring(item,	48,	58	).trim());
			bean.setCodigoEjecutivo(StringUtils.substring(item,	58,	66	 ).trim());
			bean.setCentroCostosContrato(StringUtils.substring(item,	66,	70	 ).trim());
			bean.setTipoCuenta(StringUtils.substring(item,	70,	71	 ).trim());
			bean.setIndicadorTipoContrato(StringUtils.substring(item,	71,	72	 ).trim());
			bean.setIndicadorAutorizacionPosicionCortoCapitales(StringUtils.substring(item,	72,	73	 ).trim());
			bean.setIndicadorAutorizacionPosicionCortoFondos(    StringUtils.substring(item,	73,	74	 ).trim());
			bean.setIndicadorAutorizacionCruceFondosInversion(StringUtils.substring(item,	74,	75	 ).trim());
			bean.setIndicadorDiscrecionalidad(StringUtils.substring(item,	75,	76	 ).trim());
			bean.setIndicadorTipoCustodiaDirecto(StringUtils.substring(item,	76,	77	 ).trim());
			bean.setIndicadorTipoCustodiaRepos(StringUtils.substring(item,	77,	78	 ).trim());
			bean.setImporteMaximoOperacion(MIContratosValidator.validarCamposFlotante(StringUtils.substring(item,	78,	93	 ).trim()));
			bean.setDivisaMaximaOperacion(StringUtils.substring(item,	93,	96	 ).trim());
			bean.setIndicadorEnvioCorrreoCartaConfirmacionCapitales(StringUtils.substring(item,	96,	97	 ).trim());
			bean.setIndicadorContratoAgrupado(StringUtils.substring(item,	97,	98	 ).trim());
			bean.setIndicadorTipoPosicion(StringUtils.substring(item,	98,	99	 ).trim());
			bean.setIndicadorContratoEspejo(StringUtils.substring(item,	99,	100	 ).trim());
			bean.setCodigoTipoServicio(StringUtils.substring(item,	100,	102	 ).trim());
			bean.setCodigoAsesorFinanciero(StringUtils.substring(item,	102,	110	 ).trim());

			response.add(bean);
		}
		if(cadena.length>0){
			resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 110,	111));
			if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
			resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],111,	120 ));
			
			}
		}
		resultado.setListaPrecontrato(response);
		}
		return resultado;
	}


	/**
	 * Método que ejecutará el consumo de la transacción MI15 : Consulta de tabla auxiliar
	 */
	@Override
	public BeanConsultaTablaAuxiliarResultado detallePreContratos(BeanConsultaTablaAuxiliarRequest beanConsultaTablaAuxiliarRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		info("DAOGestionPreContratosImpl::detallePreContratos()");
		BeanConsultaTablaAuxiliarResultado resultado = new BeanConsultaTablaAuxiliarResultado();
		final StringBuilder trama = new StringBuilder();			

		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarRequest.getMulticanalidad().getEmpresaConexion(),	4,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarRequest.getMulticanalidad().getCanalOperacion(),	2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarRequest.getMulticanalidad().getCanalComercializacion(),	2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanConsultaTablaAuxiliarRequest.getNumeroFolio()),	9,MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarRequest.getCodigoTipoRegistro(),	2,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarRequest.getRellamada().getMasDatos().toUpperCase(), 1, String.valueOf(MIContratosConstantes.NO_HAY_DATOS)));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarRequest.getRellamada().getMemento(), 14, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
	
		final String mensajeTrama = trama.toString(); 
		
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI15_CODE_OPERATION, MI15, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanConsultaTablaAuxiliarResponse> response = new ArrayList<>();
			String[] cadena = responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA);
			String reemplazar = cadena[0] + MIContratosConstantes.SEPARADOR_SALIDA + cadena[1]+ MIContratosConstantes.SEPARADOR_SALIDA + cadena[2];
			String res = responseDTO.getResponseMessage().replace(reemplazar,MIContratosConstantes.CADENA_VACIA);
			String[] mensajeRespuesta = MIContratosValidator.getSingletonInstance().separarSalida(res, 494);
			
			for (String item : mensajeRespuesta){
			BeanConsultaTablaAuxiliarResponse bean = new BeanConsultaTablaAuxiliarResponse();
			bean.setNumeroFolio(MIContratosValidator.validarEnteros(StringUtils.substring(item,						12,		 21).trim()));
			bean.setCodigoTipoRegistro(StringUtils.substring(item,													21,		 23).trim());
			bean.setNumeroSecuencial(MIContratosValidator.validarEnteros(StringUtils.substring(item,				23,		 26).trim()));
			bean.setCalidadParticipacion(StringUtils.substring(item,												26,		 28).trim());
			bean.setNumeroPersona(StringUtils.substring(item,														28,		 36).trim());
			bean.setTipoDomicilio(StringUtils.substring(item,														36,		 39).trim());
			bean.setSecuenciaDomicilio(MIContratosValidator.validarEnteros(StringUtils.substring(item,				39,		 42).trim()));
			bean.setIdentificadorTipoFirma(StringUtils.substring(item,												42,		 44).trim());
			bean.setCodigoTipoPoder(StringUtils.substring(item,														44,		 46).trim());
			bean.setFolioPoder(StringUtils.substring(item,															46,		 76).trim());
			bean.setPorcentajeParticipacion(MIContratosValidator.validarCamposFlotante(StringUtils.substring(item,	76,		 81).trim()));
			bean.setPorcentajeIpab(MIContratosValidator.validarCamposFlotante(StringUtils.substring(item,			81,		 86).trim()));
			bean.setTipoContacto(StringUtils.substring(item,														86,		 88).trim());
			bean.setTipoPersonalidadJuridica(StringUtils.substring(item,											88,		 89).trim());
			bean.setNombreContacto(StringUtils.substring(item,														89,		129).trim());
			bean.setPrimerApellidoContacto(StringUtils.substring(item,												129,	149).trim());
			bean.setSegundoApellidoContacto(StringUtils.substring(item,												149,	169).trim());
			bean.setFechaNacimiento(StringUtils.substring(item,														169,	179).trim());
			bean.setTipoDocumento(StringUtils.substring(item,														179,	181).trim());
			bean.setNumeroDocumento(StringUtils.substring(item,														181,	201).trim());
			bean.setNacionalidadContacto(StringUtils.substring(item,												201,	204).trim());
			bean.setPaisNacimientoContacto(StringUtils.substring(item,												204	,	207).trim());
			bean.setPaisContacto(StringUtils.substring(item,													207,	210).trim());
			bean.setNombreCalle(StringUtils.substring(item,															210,	260).trim());
			bean.setNumeroExterior(StringUtils.substring(item,														260,	275).trim());
			bean.setNumeroInteriorCalle(StringUtils.substring(item,													275,	290).trim());
			bean.setNumeroTelefonicoContacto(StringUtils.substring(item,											290,	300).trim());
			bean.setNombreAsentamiento(StringUtils.substring(item,													300,	330).trim());
			bean.setCodigoPostal(StringUtils.substring(item,														330,	338).trim());
			bean.setNombreLocalidad(StringUtils.substring(item,														338,	345).trim());
			bean.setNombreProvincia(StringUtils.substring(item,														345,	347).trim());
			bean.setNombreMunicipio(StringUtils.substring(item,														347	,	352).trim());
			bean.setEmailContacto(StringUtils.substring(item,														352	,	402).trim());
			bean.setEmpresaContratoRelacionado(StringUtils.substring(item,											402,	406).trim());
			bean.setCentroContratoRelacionado(StringUtils.substring(item,											406,	410).trim());
			bean.setNumeroContratoRelacionado(StringUtils.substring(item,											410	,	422).trim());
			bean.setCodigoProducto(StringUtils.substring(item,														422,	424).trim());
			bean.setCodigoSubProducto(StringUtils.substring(item,													424	,	428).trim());
			bean.setDescripcion(StringUtils.substring(item,															428,	478).trim());

			response.add(bean);
		}
			if(mensajeRespuesta.length>0){
				resultado.getRellamada().setMasDatos(StringUtils.substring(mensajeRespuesta[mensajeRespuesta.length-1], 478,	479));
				if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
				resultado.getRellamada().setMemento(StringUtils.substring(mensajeRespuesta[mensajeRespuesta.length-1],	479,	493 ));
				}
			}			
		resultado.setListaTablaAuxiliar(response);
		}
		return resultado;
	}
	
}
