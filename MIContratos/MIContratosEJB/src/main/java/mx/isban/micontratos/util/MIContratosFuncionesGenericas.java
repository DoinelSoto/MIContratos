package mx.isban.micontratos.util;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author everis
 * Clase con funciones para validar errores
 */
public final class MIContratosFuncionesGenericas extends Architech{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 1531937879135733538L;
	private static MIContratosFuncionesGenericas validator;

	/**
	 * Constructor
	 */
	private MIContratosFuncionesGenericas() {
	}

	/**
	 * /**
	 * 
	 * @return MIPerfilamientoValidator La instancia singleton
	 */
	public static MIContratosFuncionesGenericas getSingletonInstance() {
		if (validator == null) {
			validator = new MIContratosFuncionesGenericas();
		}
		return validator;
	}

	/**
	 * Le quita los mensajes de error a la respuesta del IDA
	 * 
	 * @param cadena
	 * @return respuesta
	 */
	public String[] getCadena(String[] cadena) {
		String[] respuesta = new String[cadena.length - 3];
		System.arraycopy(cadena, 3, respuesta, 0, cadena.length - 3);
		return respuesta;

	}

	/**
	 * Separa la salida
	 * 
	 * @param tramaSalida
	 * @param size
	 * @return cadena
	 */
	public String[] separarSalida(String trama, int size) {
		String tramaSalida = trama;
		String[] cadena = new String[tramaSalida.length() / size];
		for (int i = 0; tramaSalida.length() > size; i++) {
			cadena[i] = StringUtils.substring(tramaSalida, 0, size);
			tramaSalida = tramaSalida.substring(size);
		}
		return cadena;
	}

	/**
	 * 
	 * @param codError
	 *            código de error
	 * @param msgError
	 *            mensaje de error
	 * @return String
	 * @throws BusinessException
	 */
	public String validarCodError(String codError, String msgError) throws BusinessException {
		if (codError.startsWith(MIContratosConstantes.ER)) { 
			this.info("Se ha ejecutado la transacción");	
			throw new BusinessException(codError.substring(2), msgError); 
		}
		return codError.substring(2);
	}
}
