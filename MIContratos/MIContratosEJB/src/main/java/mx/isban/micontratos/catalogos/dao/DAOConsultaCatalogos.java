package mx.isban.micontratos.catalogos.dao;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaEstatusContratosRequest;
import mx.isban.micontratos.beans.catalogos.BeanModificacionEstatusTablaCatalogoRequest;
import mx.isban.micontratos.beans.generales.ResponseGenerico;


/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * interfaz que define los métodos en cuya implementación se ejecutarán el consumo de las transacciones de agrupaciones
 *
 */
public interface DAOConsultaCatalogos {
	
	/**
	 * Metódo que ejecutará el consumo de la transacción MI25 : Alta de catalogos de estatus
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return ResponseGenerico 
	 */
	ResponseGenerico altaEstatusOperativo(BeanAltaCatalogosEstatusRequest beanAltaCatalogosEstatusRequest,ArchitechSessionBean sesion)throws ExceptionDataAccess;

	
	/**
	 * Metódo que ejecutará el consumo de la transacción MI26 : Baja de estatus de contratos
	 * @param beanBajaEstatusContratosRequest
	 * @param session
	 * @return ResponseGenerico 
	 * @throws ExceptionDataAccess 
	 */
	ResponseGenerico bajaEstatusOperativo(BeanBajaEstatusContratosRequest beanBajaEstatusContratosRequest,ArchitechSessionBean session) throws ExceptionDataAccess;

	
	/**
	 * Metódo que ejecutará el consumo de la transacción MI27 : Modificación de estatus de la tabla catálogo
	 * @param beanModificacionEstatusTablaCatalogoRequest
	 * @param session
	 * @return ResponseGenerico 
	 * @throws ExceptionDataAccess 
	 */
	ResponseGenerico modificarEstatusOperativo(BeanModificacionEstatusTablaCatalogoRequest beanModificacionEstatusTablaCatalogoRequest,ArchitechSessionBean session) throws ExceptionDataAccess;

	
	/**
	 * Metódo que ejecutará el consumo de la transacción MI28 : Alta de catálogos de subEstatus
	 * @param beanAltaCatalogosSubEstatusRequest
	 * @param session
	 * @return ResponseGenerico 
	 * @throws ExceptionDataAccess 
	 */
	ResponseGenerico altaSubEstatusOperativo(BeanAltaCatalogosSubEstatusRequest beanAltaCatalogosSubEstatusRequest,ArchitechSessionBean session) throws ExceptionDataAccess;

	
	/**
	 * Metódo que ejecutará el consumo de la transacción MI29 : Baja de subestatus de la tabla de catálogos
	 * @param beanBajaCatalogosSubEstatusRequest
	 * @param session
	 * @return ResponseGenerico 
	 * @throws ExceptionDataAccess 
	 */
	ResponseGenerico bajaSubEstatusOperativo(BeanBajaCatalogosSubEstatusRequest beanBajaCatalogosSubEstatusRequest,ArchitechSessionBean session) throws ExceptionDataAccess;

	

}
