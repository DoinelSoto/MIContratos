package mx.isban.micontratos.agrupaciones.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.micontratos.agrupaciones.dao.DAOConsultaAgrupaciones;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanBajaContratosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorResponse;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorResultado;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosResponse;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosResultado;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.util.SingletonIDA;

import org.apache.commons.lang.StringUtils;

/**
 * @author everis
 *
 */

/**
 *	Interfaz con los métodos que consumirán las transacciones de agrupaciones
 * @author everis
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOConsultaAgrupacionesImpl extends Architech implements DAOConsultaAgrupaciones{
	//Código de transacción MI36
	private static final String MI36 = "MI36";
	//Codigo de operación de la TRX MI36
	private static final String MI36_CODE_OPERATION = "MI36_BAJA_CONTRATOS_HIJOS";
	//Código de transacción MI34
	private static final String MI34 = "MI34";
	//Codigo de operacion de la TRX 34
	private static final String MI34_CODE_OPERATION = "MI34_CONSULTA_LISTA_CONTRATOS_HIJOS";
	//Código de transacción MI33
	private static final String MI33 = "MI33";
	//Codigo de operacion de la TRX 33
	private static final String MI33_CODE_OPERATION = "MI33_ALTA_CONTRATO_AGRUPADOR";
	//Código de transacción 32
	private static final String MI32 = "MI32";
	//Codigo de operacion de la TRX 32
	private static final String MI32_CODE_OPERATION = "MI32_CONSULTA_LISTA_CONTRATO_AGRUPADOR";
	//Código de transacción MI35
	private static final String MI35 = "MI35";
	//Codigo de operacion de la TRX 35
	private static final String MI35_CODE_OPERATION = "MI35_ALTA_DE_LOS_CONTRATOS_HIJO";
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7683828708341835137L;

	/**
	 * Método que consumirá la ejecución de la transacción MI32 : Consulta de lista de contrato agrupador
	 * @param beanListaContratoAgrupadorRequest
	 * @param session
	 * @return BeanListaContratoAgrupadorResultado
	 * @throws ExceptionDataAccess 
	 */
	@Override
	public BeanListaContratoAgrupadorResultado listaContratosAgrupadores(BeanListaContratoAgrupadorRequest beanListaContratoAgrupadorRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		//Instancia del bean que se retornará
		BeanListaContratoAgrupadorResultado respuesta = new BeanListaContratoAgrupadorResultado();
		//StringBuilder para construír trama
		final StringBuilder trama = new StringBuilder();

		//Comienza entramado del bean de entrada
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getTipoConsulta(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaContratoAgrupadorRequest.getTipoAgrupacion(),	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getCentroContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getNumeroContrato(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getCodigoProductoContrato(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getCodigoSubProductoContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getEmpresaContratoInversion(),4	, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaContratoAgrupadorRequest.getAliasContrato(),15	, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanListaContratoAgrupadorRequest.getRellamada().getMasDatos()).toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanListaContratoAgrupadorRequest.getRellamada().getMemento(), 45, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		//Trama construída
		final String mensajeTrama = trama.toString(); 
		//Se ejecuta la transacción
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI32_CODE_OPERATION, MI32, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		//Código de error de la transacción
		respuesta.setCodError(errores.getCodError());
		//Mensaje de error de la transacción
		respuesta.setMsgError(errores.getMsgError());
		
		//Se valida si hubo algún error durante la transacción
		if(!respuesta.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanListaContratoAgrupadorResponse> response = new ArrayList<>();
			//Se aplica un split a la trama de respuesta
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
			
			//Comienza el desentramado
			for (String item : cadena){
				info(MIContratosConstantes.INFORMACION_ITEM + item);
				
				BeanListaContratoAgrupadorResponse bean = new BeanListaContratoAgrupadorResponse();
				bean.setEmpresaContrato(StringUtils.substring(item,				11	,	15).trim());
				bean.setCentroContrato(StringUtils.substring(item,				15	,	19).trim());
				bean.setNumeroContrato(StringUtils.substring(item,				19	,	31).trim());
				bean.setCodigoProductoContrato(StringUtils.substring(item,		31	,	33).trim());
				bean.setCodigoSubProductoContrato(StringUtils.substring(item,	33	,	37).trim());
				bean.setEmpresaContratoInversion(StringUtils.substring(item,	37	,	41).trim());
				bean.setAliasContrato(StringUtils.substring(item,				41	,	56).trim());
				bean.setCodigoEjecutivo(StringUtils.substring(item,				56	,	64).trim());
				bean.setNumeroTelefonico(StringUtils.substring(item,			64	,	74).trim());
				bean.setModedaGrupo(StringUtils.substring(item, 				74	,	77).trim());
				bean.setNombreGrupo(StringUtils.substring(item,					77	,	107).trim());
				bean.setDescripcionGrupo(StringUtils.substring(item,			107	,	157).trim());
				bean.setIndicadorTipoGrupo(StringUtils.substring(item,			157	,	158).trim());
				bean.setIdentificadorGrupoUsuario(StringUtils.substring(item,	158	,	166).trim());
				bean.setFechaComienzoGrupo(StringUtils.substring(item, 			166	,	176).trim());
				bean.setFechaFinGrupo(StringUtils.substring(item,				176	,	186).trim());
				bean.setIndicadorUsoGrupo(StringUtils.substring(item,			186	,	187).trim());

				//Se añade el bean construido a la lista
				response.add(bean);
			}
			if(cadena.length>0){
				//Se guarda el indicador de mas datos
				respuesta.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 187,188	));
				//Se valida si el indicador de mas datos es S
				if(respuesta.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
					respuesta.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],188,233	 ));
				}
			}
		//Se guarda la lista en el bean de respuesta
		respuesta.setListaContratoAgrupador(response);
		}
		//Se devuelve el bean de respuesta
		return respuesta;
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI33 : alta de contrato agrupador
	 * @param beanAltaContratoAgrupadorRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess 
	 */
	@Override
	public ResponseGenerico altaContratoAgrupador(
			BeanAltaContratoAgrupadorRequest beanAltaContratoAgrupadorRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess {
		this.info("DAOPersonasImpl::altaContratoAgrupador()");
		//StringBuilder para construír trama		
		final StringBuilder trama = new StringBuilder();
		
		//Comienza entramado del bean de entrada
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getMulticanalidad().getEmpresaConexion(), 	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getMulticanalidad().getCanalOperacion(), 	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getMulticanalidad().getCanalComercializacion(), 	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getTipoConsulta(), 	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getEmpresaContrato(), 	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getCentroContrato(), 	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratoAgrupadorRequest.getNumeroContrato(), 	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getCodigoProducto(), 	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getCodigoSubProducto(), 	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getEmpresaContratoInversion(), 	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratoAgrupadorRequest.getAliasContrato(), 	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getTipoAgrupacion(), 	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratoAgrupadorRequest.getCodigoEjecutivo(), 	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getNumeroTelefonico(), 	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getMonedaGrupo(), 	3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratoAgrupadorRequest.getNombreGrupo(), 	30, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratoAgrupadorRequest.getDescripcionGrupo(), 	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getIndicadorTipoGrupo(), 	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratoAgrupadorRequest.getCodigoGrupoUsuario(), 	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getFechaComienzoGrupo(), 	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getFechaFinGrupo(), 	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratoAgrupadorRequest.getIndicadorUsoGrupo(), 	1, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		//Trama construída
		final String mensajeTrama = trama.toString();
		//Se ejecuta la transacción
		//Se crea el bean reponse
		//Se devuelve el bean response
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI33_CODE_OPERATION, MI33, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI34 : consulta de la lista de contratos hijos
	 * @param beanListaContratosHijosRequest
	 * @param session
	 * @return BeanListaContratosHijosResultado
	 * @throws ExceptionDataAccess
	 */
	@Override
	public BeanListaContratosHijosResultado listarContratosAgrupados(
			BeanListaContratosHijosRequest beanListaContratosHijosRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess {
		info("DAOConsultaAgrupacionesImpl::listarContratosAgrupados()");
		BeanListaContratosHijosResultado resultado= new BeanListaContratosHijosResultado();
		//StringBuilder para construír trama
		final StringBuilder trama = new StringBuilder();

		//Comienza el entramado del bean de entrada
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getMulticanalidad().getEmpresaConexion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getMulticanalidad().getCanalOperacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getMulticanalidad().getCanalComercializacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getTipoConsulta(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getEmpresaContrato(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getCentroContrato(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaContratosHijosRequest.getNumeroContrato(), 12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getCodigoProducto(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getCodigoSubProducto(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getEmpresaContratoInversion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanListaContratosHijosRequest.getAliasContrato(), 15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getTipoAgrupacion(), 3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanListaContratosHijosRequest.getRellamada().getMasDatos()).toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanListaContratosHijosRequest.getRellamada().getMemento(), 45, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		//Trama construída
		final String mensajeTrama = trama.toString(); 
		//Se ejecuta la transacción
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI34_CODE_OPERATION, MI34, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		//Código de error de la transacción
		resultado.setCodError(errores.getCodError());
		//Mensaje de error de la transacción
		resultado.setMsgError(errores.getMsgError());
		
		//Se valida si hubo algún error durante la transacción		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanListaContratosHijosResponse> response = new ArrayList<>();
			//Se aplica un split a la trama de respuesta
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));
			
			//Comienza desentramado
			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM + item);
			BeanListaContratosHijosResponse bean = new BeanListaContratosHijosResponse();
			
			bean.setIdentificadorEmpresa(StringUtils.substring(item, 11,	15	).trim());
			bean.setIdentificadorCentroContrato(StringUtils.substring(item, 15,	19	).trim());
			bean.setIdentificadorContrato(StringUtils.substring(item, 19,	31	).trim());
			bean.setCodigoProductoContrato(StringUtils.substring(item, 31,	33	).trim());
			bean.setCodigoSubproductoContrato(StringUtils.substring(item, 33,	37	).trim());
			bean.setIdentificadorEmpresaContrato(StringUtils.substring(item, 37,	41).trim());
			bean.setDescripcionAlias(StringUtils.substring(item, 41,	56	).trim());
			
			//Se añade el bean construido a la lista
			response.add(bean);
			}
			
			if(cadena.length>0){
				//Se guarda el indicador de mas datos
				resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 56,	57));
				//Se valida si el indicador de mas datos es S
				if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){ 
					resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1],11,	56 ));
				}
			}
			//Se guarda la lista en el bean de resultado
			resultado.setListaContratosHijos(response);
		}
		//Se devuelve el bean de resultado
		return resultado;
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI35 : Alta contratos hijos
	 * @param beanAltaContratosHijosRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess
	 */
	@Override
	public ResponseGenerico altaContratoAgrupado(BeanAltaContratosHijosRequest beanAltaContratosHijosRequest,ArchitechSessionBean session) throws ExceptionDataAccess {
		info("DAOConsultaAgrupacionesImpl::altaContratosHijos()");
		final StringBuilder trama = new StringBuilder();

		//Comienza el entramado del bean de entrada
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getMulticanalidad().getEmpresaConexion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getMulticanalidad().getCanalOperacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getMulticanalidad().getCanalComercializacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getTipoConsulta(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getEmpresaContrato(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getCentroContrato(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratosHijosRequest.getNumeroContrato(), 12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getCodigoProducto(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getCodigoSubProducto(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getEmpresaContratoInversion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratosHijosRequest.getAliasContrato(), 15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getTipoAgrupacion(), 3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getEmpresaContratoAgrupador(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getCentroContratoAgrupado(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratosHijosRequest.getNumeroContratoAgrupado(), 12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getCodigoProductoAgrupado(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getCodigoSubProductoAgrupado(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanAltaContratosHijosRequest.getEmpresaContratoInversionAgrupado(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanAltaContratosHijosRequest.getAliasContratoAgrupado(), 15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama =  trama.toString();

		//Se ejecuta la transacción
		//Se crea el bean reponse
		//Se devuelve el bean response
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI35_CODE_OPERATION, MI35, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI36 : Baja de contratos hijos
	 * @param beanBajaContratosRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess 
	 */
	@Override
	public ResponseGenerico bajaContratoAgrupado(
			BeanBajaContratosRequest beanBajaContratosRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess {
		info("DAOConsultaAgrupacionesImpl::bajaContratosHijos()");
		final StringBuilder trama = new StringBuilder();
		
		//Comienza entramado del bean de entrada
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getMulticanalidad().getEmpresaConexion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getMulticanalidad().getCanalOperacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getMulticanalidad().getCanalComercializacion(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getTipoConsulta(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getEmpresaContrato(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getCentroContrato(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanBajaContratosRequest.getNumeroContrato(), 12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getCodigoProducto(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getCodigoSubProducto(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getEmpresaContratoInversion(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanBajaContratosRequest.getAliasContrato(), 15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getTipoAgrupacion(), 3, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getEmpresaContratoAgrupado(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getCentroContratoAgrupado(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanBajaContratosRequest.getNumeroContratoAgrupado(), 12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getCodigoProductoAgrupado(), 2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getCodigoSubProductoAgrupado(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanBajaContratosRequest.getEmpresaContratoInversionAgrupado(), 4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanBajaContratosRequest.getAliasContratoAgrupado(), 15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama =  trama.toString();
	
		//Se ejecuta la transacción
		//Se crea el bean reponse
		//Se devuelve el bean response
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI36_CODE_OPERATION, MI36, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}

}
