package mx.isban.micontratos.catalogos.ejb.impl;


import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaEstatusContratosRequest;
import mx.isban.micontratos.beans.catalogos.BeanModificacionEstatusTablaCatalogoRequest;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.catalogos.dao.DAOConsultaCatalogos;
import mx.isban.micontratos.catalogos.ejb.BOConsultaCatalogos;
import mx.isban.micontratos.catalogos.ejb.BOConsultaTablaParametros;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosValidator;

/**
 * @author everis
 * Clase con los métodos que haran el llamado a los métodos que consumen las transacciones: MI25,
 * MI26,MI27,MI28, MI29
 */

@Stateless
@Remote(BOConsultaCatalogos.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOCatalogosImpl extends Architech implements BOConsultaCatalogos{



	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -2619615501114131344L;
	/**
	 * Instancia de DAOConsultaCatalogos
	 */
	@EJB
	private DAOConsultaCatalogos daoConsultaCatalogos;
	
	/**
	 * Instancia de BOConsultaTablaParametros
	 */
	@EJB
	private BOConsultaTablaParametros boConsultaTablaParametros;

	

	/**
	 * Metódo que ejecutará el consumo de la transacción MI25 : Alta de catalogos de estatus
	 * @param beanAltaCatalogosEstatusRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws BusinessException
	 */
	@Override
	public ResponseGenerico altaEstatusOperativo(BeanAltaCatalogosEstatusRequest beanAltaCatalogosEstatusRequest, ArchitechSessionBean session) throws BusinessException {
		//Campos obligatorios
		String [] camposObligatorios={beanAltaCatalogosEstatusRequest.getCodigoEstatus(),beanAltaCatalogosEstatusRequest.getDescripcionEstatus(),
				beanAltaCatalogosEstatusRequest.getIndicadorDetalle(),beanAltaCatalogosEstatusRequest.getIndicadorOperativo(),
				beanAltaCatalogosEstatusRequest.getIndicadorBloqueo(),beanAltaCatalogosEstatusRequest.getIndicadorEstatusCancelado(),
				beanAltaCatalogosEstatusRequest.getIndicadorBloqueoLegal(),beanAltaCatalogosEstatusRequest.getIndicadorInversiones(),
				beanAltaCatalogosEstatusRequest.getIndicadorBloqueable(),beanAltaCatalogosEstatusRequest.getIndicadorCancelable(),
				beanAltaCatalogosEstatusRequest.getIndicadorValidacionDocumentos()};
		//Longitudes de campos obligatorios
		int [] longitudes={2,50,1,1,1,1,1,1,1,1,1};
		
		try {
			//Se validan los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			ResponseGenerico response=daoConsultaCatalogos.altaEstatusOperativo(beanAltaCatalogosEstatusRequest, session);
			//Validación del código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que hará el llamado a la transacción MI28 : Alta de catálogos de subEstatu
	 * @param beanAltaCatalogosEstatusRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws BusinessException
	 */
	@Override
	public ResponseGenerico altaSubEstatusOperativo(BeanAltaCatalogosSubEstatusRequest beanAltaCatalogosEstatusRequest, ArchitechSessionBean session) throws BusinessException {
		//Campos obligatorios
		String [] camposObligatorios ={beanAltaCatalogosEstatusRequest.getCodigoEstatus(),beanAltaCatalogosEstatusRequest.getCodigoSubEstatus(),
				beanAltaCatalogosEstatusRequest.getDescripcionSubEstatus()};
		//Longitudes de campos obligatorios
		int [] longitudes ={2,2,50};
		try {
			//Se validan los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			ResponseGenerico response=daoConsultaCatalogos.altaSubEstatusOperativo(beanAltaCatalogosEstatusRequest, session);
			//Validación del código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que hará el llamado a la transacción MI29 : Baja de subestatus de la tabla de catálogo
	 * @param beanAltaCatalogosEstatusRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	@Override
	public ResponseGenerico bajaSubEstatusOperativo(BeanBajaCatalogosSubEstatusRequest beanAltaCatalogosEstatusRequest, ArchitechSessionBean session) throws BusinessException {
		//Campos obligatorios
		String[] camposObligatorios = {
				beanAltaCatalogosEstatusRequest.getCodigoEstatus(),
				beanAltaCatalogosEstatusRequest.getCodigoSubEstatus() };
		//Longitudes de campos obligatorios
		int[] longitudesCamposObligatorios = {2,2};
		try {
			//Se validan los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesCamposObligatorios);
			ResponseGenerico response=daoConsultaCatalogos.bajaSubEstatusOperativo(beanAltaCatalogosEstatusRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
		
	/**
	 * Método que hará el llamado a la transacción MI26 : baja de catalogos de subestatus
	 * @param beanBajaEstatusContratosRequest
	 * @param session
	 * @return ResponseGenerico 
	 */
	@Override
	public ResponseGenerico bajaEstatusOperativo(BeanBajaEstatusContratosRequest beanBajaEstatusContratosRequest, ArchitechSessionBean session) throws BusinessException {
		//Campos obligatorios
		String [] camposObligatorios ={beanBajaEstatusContratosRequest.getCodigoEstatus()};
		//Longitudes de campos obligatorios
		int [] longitudes={2};
		try {
			//Se validan los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			ResponseGenerico response=daoConsultaCatalogos.bajaEstatusOperativo(beanBajaEstatusContratosRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response;
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que hará el llamado a la transacción MI27 : Modificación del catalogo de estatus
	 * @param beanModificacionEstatusTablaCatalogoRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	@Override
	public ResponseGenerico modificarEstatusOperativo(BeanModificacionEstatusTablaCatalogoRequest beanModificacionEstatusTablaCatalogoRequest,ArchitechSessionBean session) throws BusinessException {
		//Campos obligatorios
		String [] camposObligatorios={beanModificacionEstatusTablaCatalogoRequest.getCodigoEstatus(),beanModificacionEstatusTablaCatalogoRequest.getDescripcionEstatus(),
				beanModificacionEstatusTablaCatalogoRequest.getIndicadorDetalle(),beanModificacionEstatusTablaCatalogoRequest.getIndicadorOperativo(),
				beanModificacionEstatusTablaCatalogoRequest.getIndicadorBloqueo(),beanModificacionEstatusTablaCatalogoRequest.getIndicadorCancelacion(),
				beanModificacionEstatusTablaCatalogoRequest.getIndicadorBloqueoLegal(),beanModificacionEstatusTablaCatalogoRequest.getIndicadorInversion(),
				beanModificacionEstatusTablaCatalogoRequest.getIndicadorBloqueable(),beanModificacionEstatusTablaCatalogoRequest.getIndicadorCancelable(),
				beanModificacionEstatusTablaCatalogoRequest.getIndicadorValidacionDocumentos()};
		//Longitudes de campos obligatorios
		int [] longitudes={2,50,1,1,1,1,1,1,1,1,1};
		try {
			//Se validan los campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			ResponseGenerico response=daoConsultaCatalogos.modificarEstatusOperativo(beanModificacionEstatusTablaCatalogoRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError()));
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
}