package mx.isban.micontratos.gestordocumental.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorResponse;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadResponse;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanModificacionEstadosDocumentosRequest;
import mx.isban.micontratos.gestordocumental.dao.DAOGestorDocumental;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.util.SingletonIDA;

import org.apache.commons.lang.StringUtils;

/**
 * @author everis
 *
 */

@Stateless
@Local(DAOGestorDocumental.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOGestorDocumentalImpl extends Architech implements DAOGestorDocumental{
	
	private static final String MI38 = "MI38";

	private static final String MI38_CODE_OPERATION = "MI38_CONSULTA_INDICADOR_OBLIGATORIEDAD";

	private static final String MI17_CODE_OPERATION = "MI17_CONSULTA_TABLA:AUXILIAR_TABLA_GESTOR_DOCUMENTOS";

	private static final String MI17 = "MI17";

	private static final String MI20 = "MI20";

	private static final String MI20_CODE_OPERATION = "MI20_MODIFICACION_ESTADO_DOCUMENTOS";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7123515720390068015L;
	
	/**
	 * Metódo que ejecutará el consumo de la transacción MI17 : consulta de tabla auxiliar de gestor de documentos
	 */
	@Override
	public BeanConsultaTablaAuxiliarGestorResultado listarDocumentosContrato(
			BeanConsultaTablaAuxiliarGestorRequest beanConsultaTablaAuxiliarGestorRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess{
		info("DAOGestorDocumentalImpl::listarDocumentosContrato()");
		BeanConsultaTablaAuxiliarGestorResultado resultado = new BeanConsultaTablaAuxiliarGestorResultado();
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getTipoConsulta(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getCentroContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanConsultaTablaAuxiliarGestorRequest.getNumeroContrato(), 12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getCodigoProducto(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getCodigoSubProducto(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getIdentificadorEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanConsultaTablaAuxiliarGestorRequest.getDescripcionAlias(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getCalidadParticipacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getNumeroPersona(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanConsultaTablaAuxiliarGestorRequest.getCodigoDocumento(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(String.valueOf(beanConsultaTablaAuxiliarGestorRequest.getRellamada().getMasDatos()).toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanConsultaTablaAuxiliarGestorRequest.getRellamada().getMemento(), 44, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
	
		
		final String mensajeTrama = trama.toString(); 
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI17_CODE_OPERATION, MI17, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanConsultaTablaAuxiliarGestorResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));

			
			for (String item : cadena){
			info(MIContratosConstantes.INFORMACION_ITEM+item.length());	
			BeanConsultaTablaAuxiliarGestorResponse bean = new BeanConsultaTablaAuxiliarGestorResponse();
			bean.setCalidadParticipacion(StringUtils.substring(item,	11,	13 ).trim());
			bean.setNumeroPersona(StringUtils.substring(item,	13,	21 ).trim());
			bean.setCodigoDocumento(StringUtils.substring(item,	21,	29 ).trim());
			bean.setFechaAlta(StringUtils.substring(item,	29,	39 ).trim());
			bean.setComentario(StringUtils.substring(item,	39,	89 ).trim());
			bean.setCodigoEstadoDocumento(StringUtils.substring(item,	89,	91 ).trim());
			bean.setFechaEstado(StringUtils.substring(item,	91,	101 ).trim());
			bean.setFechaVencimientoDocumento(StringUtils.substring(item,	101,	111).trim());
			bean.setIndicadorObligatoriedad(StringUtils.substring(item,	111,	112).trim());

			response.add(bean);
		}
		if(cadena.length>0){
			resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1], 112,	113));
			if(resultado.getRellamada().getMasDatos().equals(MIContratosConstantes.SI_HAY_DATOS)){
			resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1], 113,	157 ).trim());
			}
		}
		resultado.setListaTablaAuxiliarGestor(response);
		}
		return resultado;
	}


	/**
	 * Método que hará el consumo de la transacción MI38 : Consulta indicador de obligatoriedad
	 * @throws ExceptionDataAccess 
	 */
	@Override
	public BeanIndicadorObligatoriedadResultado consultarVencimientoContrato(
			BeanIndicadorObligatoriedadRequest beanIndicadorObligatoriedadRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess {
		
		info("DAOGestorDocumentalImpl::consultarVencimientoContrato()");
		BeanIndicadorObligatoriedadResultado resultado = new BeanIndicadorObligatoriedadResultado();
		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getTipoConsulta(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getEmpresaContrato(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getCentroContrato(),		4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanIndicadorObligatoriedadRequest.getNumeroContrato(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getCodigoProducto(),		2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getCodigoSubProducto(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getCodigoEmpresaContratoInversion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanIndicadorObligatoriedadRequest.getAliasContrato(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getRellamada().getMasDatos().toUpperCase(), 1, MIContratosConstantes.NO_HAY_DATOS));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getRellamada().getMemento(), 44, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanIndicadorObligatoriedadRequest.getFechaProceso(),	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		
		final String mensajeTrama = trama.toString(); 
		
		final ResponseMessageCicsDTO responseDTO = SingletonIDA.getSingletonInstance()
				.ejecutarTransaccion(mensajeTrama.toString(), MI38_CODE_OPERATION, MI38, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
		ResponseGenerico errores=SingletonIDA.getSingletonInstance().crearResponse(responseDTO);
		resultado.setCodError(errores.getCodError());
		resultado.setMsgError(errores.getMsgError());
		
		if(!resultado.getCodError().startsWith(MIContratosConstantes.ER)){	
			List<BeanIndicadorObligatoriedadResponse> response = new ArrayList<>();
			String[] cadena = MIContratosFuncionesGenericas
					.getSingletonInstance().getCadena(responseDTO.getResponseMessage().split(MIContratosConstantes.SEPARADOR_SALIDA));

			
			for (String item : cadena){
					info("Item:" + item);
					BeanIndicadorObligatoriedadResponse bean = new BeanIndicadorObligatoriedadResponse();
					bean.setCalidadParticipacion(StringUtils.substring(item,11,13 ).trim());
					bean.setNumeroPersona(StringUtils.substring(item,13,21 ).trim());
					bean.setCodigoDocumento(StringUtils.substring(item,21,29 ).trim());
					bean.setIndicadorObligatoriedad(StringUtils.substring(item,29,30 ).trim());
					bean.setComentarios(StringUtils.substring(item,30,80 ).trim());
					bean.setCodigoEstadoDocumentos(StringUtils.substring(item,80,82 ).trim());
					response.add(bean);
			}
			if(cadena.length>0){
					resultado.getRellamada().setMasDatos(StringUtils.substring(cadena[cadena.length-1],82,	83 ).trim());
					if("S".equals(resultado.getRellamada().getMasDatos())){
					resultado.getRellamada().setMemento(StringUtils.substring(cadena[cadena.length-1], 83,127	).trim());
					}
				}
				resultado.setListaIndicadorObligatoriedad(response);
				}
				return resultado;
	}

	/**
	 * Metódo que ejecutará el consumo de la transacción MI20 : Modificacion de estado de documentos
	 */
	@Override
	public ResponseGenerico modificarEstatusDocumento(
			BeanModificacionEstadosDocumentosRequest beanModificacionEstadosDocumentosRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess {

		this.info("DAOGestorDocumentalImpl::modificarEstatusDocumento()");

		final StringBuilder trama = new StringBuilder();
		
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getTipoConsulta(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getCentroContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionEstadosDocumentosRequest.getNumeroContrato(),	12, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getCodigoProducto(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getCodigoSubProducto(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getIdentificadorEmpresaContrato(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionEstadosDocumentosRequest.getDescripcionAlias(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getNumeroPersona(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getCalidadParticipacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionEstadosDocumentosRequest.getCodigoDocumento(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getCodigoEstadoNuevo(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.rightPad(beanModificacionEstadosDocumentosRequest.getComentario(),	50, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		trama.append(StringUtils.leftPad(beanModificacionEstadosDocumentosRequest.getFechaEstadoDocumento(),	10, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		
		final String mensajeTrama = trama.toString();
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI20_CODE_OPERATION, MI20, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}
}
