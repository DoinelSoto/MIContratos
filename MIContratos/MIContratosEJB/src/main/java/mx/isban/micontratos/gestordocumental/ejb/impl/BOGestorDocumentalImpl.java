package mx.isban.micontratos.gestordocumental.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanModificacionEstadosDocumentosRequest;
import mx.isban.micontratos.gestordocumental.dao.DAOGestorDocumental;
import mx.isban.micontratos.gestordocumental.ejb.BOGestorDocumental;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosValidator;

/**
 * @author everis
 *
 */

@Stateless
@Remote(BOGestorDocumental.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGestorDocumentalImpl extends Architech implements BOGestorDocumental{
	

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -2728883177533611651L;
	
	@EJB
	private DAOGestorDocumental daoGestorDocumental;

	/**
	 * Metódo que ejecutará el consumo de la transacción MI38 : consulta de indicador de obligatoriedad
	 */
	@Override
	public BeanIndicadorObligatoriedadResultado consultarVencimientoContrato(BeanIndicadorObligatoriedadRequest beanIndicadorObligatoriedadRequest, ArchitechSessionBean sesion) throws BusinessException{
		String [] camposObligatorios={beanIndicadorObligatoriedadRequest.getTipoConsulta()};
		int [] longitudes={2};
		String [] camposNoObligatorios ={beanIndicadorObligatoriedadRequest.getMulticanalidad().getCanalComercializacion(),
				beanIndicadorObligatoriedadRequest.getMulticanalidad().getCanalOperacion(),beanIndicadorObligatoriedadRequest.getMulticanalidad().getEmpresaConexion(),
				beanIndicadorObligatoriedadRequest.getRellamada().getMasDatos(),beanIndicadorObligatoriedadRequest.getRellamada().getMemento(),
				beanIndicadorObligatoriedadRequest.getCodigoEmpresaContratoInversion(),beanIndicadorObligatoriedadRequest.getAliasContrato(),
				beanIndicadorObligatoriedadRequest.getFechaProceso(),beanIndicadorObligatoriedadRequest.getEmpresaContrato(),
				beanIndicadorObligatoriedadRequest.getCentroContrato(),beanIndicadorObligatoriedadRequest.getNumeroContrato(),
				beanIndicadorObligatoriedadRequest.getCodigoProducto(),beanIndicadorObligatoriedadRequest.getCodigoSubProducto()};
		int [] longitudesCamposNoObligatorios={2,2,4,1,44,4,15,10,4,4,12,2,4};
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesCamposNoObligatorios);
			BeanIndicadorObligatoriedadResultado response=daoGestorDocumental.consultarVencimientoContrato(beanIndicadorObligatoriedadRequest, sesion);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Metódo que ejecutará el consumo de la transacción MI17 : consulta de tabla auxiliar de gestor de documentos
	 */
	@Override
	public BeanConsultaTablaAuxiliarGestorResultado listarDocumentosContrato(BeanConsultaTablaAuxiliarGestorRequest beanConsultaTablaAuxiliarGestorRequest,ArchitechSessionBean sesion) throws BusinessException{
		String[] camposObligatorios ={
				beanConsultaTablaAuxiliarGestorRequest.getTipoConsulta()};
		
		int[] longitudesObligatorias = {2};
		
		String[] camposNoObligatorios = {
				beanConsultaTablaAuxiliarGestorRequest.getMulticanalidad().getEmpresaConexion(),		
				beanConsultaTablaAuxiliarGestorRequest.getMulticanalidad().getCanalOperacion(),			
				beanConsultaTablaAuxiliarGestorRequest.getMulticanalidad().getCanalComercializacion(),
				beanConsultaTablaAuxiliarGestorRequest.getEmpresaContrato(),				
				beanConsultaTablaAuxiliarGestorRequest.getCentroContrato(),					
				beanConsultaTablaAuxiliarGestorRequest.getNumeroContrato(),										
				beanConsultaTablaAuxiliarGestorRequest.getCodigoProducto(),					
				beanConsultaTablaAuxiliarGestorRequest.getCodigoSubProducto(),				
				beanConsultaTablaAuxiliarGestorRequest.getIdentificadorEmpresaContrato(),	
				beanConsultaTablaAuxiliarGestorRequest.getDescripcionAlias(),				
				beanConsultaTablaAuxiliarGestorRequest.getCalidadParticipacion(),			
				beanConsultaTablaAuxiliarGestorRequest.getNumeroPersona(),					
				beanConsultaTablaAuxiliarGestorRequest.getCodigoDocumento(),				
				beanConsultaTablaAuxiliarGestorRequest.getRellamada().getMasDatos(),			
				beanConsultaTablaAuxiliarGestorRequest.getRellamada().getMemento()};
		
		int[] longitudesNoObligatorias = {4,2,2,4,4,12,2,4,4,15,2,8,8,1,44};
		
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanConsultaTablaAuxiliarGestorResultado response=daoGestorDocumental.listarDocumentosContrato(beanConsultaTablaAuxiliarGestorRequest, sesion);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}


	/**
	 * Método que hará el llamado al método que consumirá la transacción MI20 : Modificacion de estado de documentos
	 */
	@Override
	public ResponseGenerico modificarEstatusDocumento(BeanModificacionEstadosDocumentosRequest beanModificacionEstadosDocumentosRequest,ArchitechSessionBean session) throws BusinessException {
		String[] camposObligatorios = { beanModificacionEstadosDocumentosRequest.getTipoConsulta(), beanModificacionEstadosDocumentosRequest.getNumeroPersona(),
				beanModificacionEstadosDocumentosRequest.getCalidadParticipacion(),
				beanModificacionEstadosDocumentosRequest.getCodigoDocumento(), beanModificacionEstadosDocumentosRequest.getCodigoEstadoNuevo(),
				beanModificacionEstadosDocumentosRequest.getComentario(), beanModificacionEstadosDocumentosRequest.getFechaEstadoDocumento() };
		int[] longitudes = { 2, 8, 2, 8, 2, 50, 10};
		
		String[] camposNoObligatorios = { beanModificacionEstadosDocumentosRequest.getMulticanalidad().getEmpresaConexion(),
				beanModificacionEstadosDocumentosRequest.getMulticanalidad().getCanalOperacion(),
				beanModificacionEstadosDocumentosRequest.getMulticanalidad().getCanalComercializacion(),
				beanModificacionEstadosDocumentosRequest.getEmpresaContrato(), beanModificacionEstadosDocumentosRequest.getCentroContrato(),
				beanModificacionEstadosDocumentosRequest.getNumeroContrato(), beanModificacionEstadosDocumentosRequest.getCodigoProducto(),
				beanModificacionEstadosDocumentosRequest.getCodigoSubProducto(),beanModificacionEstadosDocumentosRequest.getIdentificadorEmpresaContrato()
				,beanModificacionEstadosDocumentosRequest.getDescripcionAlias()};
		int[] longitudesDeCamposNoObligatorios = { 4, 2, 2 , 4, 4, 12, 2, 4 ,4,15};
		
		try {
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios,longitudesDeCamposNoObligatorios);
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudes);
			ResponseGenerico response=daoGestorDocumental.modificarEstatusDocumento(beanModificacionEstadosDocumentosRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

}
