package mx.isban.micontratos.relacionclientecontrato.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanDatosDomicilioResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaContactosResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanListaIntervinientesResponse;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanCambioTitularRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionContacto;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionInterviniente;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionUsosDomicilio;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosRequest;
import mx.isban.micontratos.relacionclientecontrato.dao.DAORelacionClienteContrato;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.SingletonIDA;
import mx.isban.micontratos.validator.MIContratosValidator;
import mx.isban.miperfilamiento.niveldeacceso.beans.BeanValidacionNivelAcceso;
import mx.isban.miperfilamiento.niveldeacceso.beans.BeanValidacionNivelAccesoRequest;
import mx.isban.miperfilamiento.niveldeacceso.beans.BeanValidacionNivelAccesoResponse;
import mx.isban.miperfilamiento.niveldeacceso.ejb.BONivelAcceso;
import mx.isban.miperfilamiento.util.DatosContratoPadre;
import mx.isban.miperfilamiento.util.DatosContratosValidacion;
import mx.isban.miperfilamiento.util.DatosMulticanalidad;
import mx.isban.miperfilamiento.util.DatosProductoContrato;
import mx.isban.mipersonas.base.beans.ReposicionBean;
import mx.isban.mipersonas.contratos.beans.BeanProductoSubproducto;
import mx.isban.mipersonas.contratos.beans.ConsDetInterRequestBean;
import mx.isban.mipersonas.contratos.beans.ConsDetInterResponseBean;
import mx.isban.mipersonas.contratos.beans.ConsDetalleContacRequestBean;
import mx.isban.mipersonas.contratos.beans.ConsultaContratosClienteRequestBean;
import mx.isban.mipersonas.contratos.beans.ConsultaContratosClienteResponseBean;
import mx.isban.mipersonas.contratos.beans.ConsultaContratosClienteResponseRegBean;
import mx.isban.mipersonas.contratos.beans.DetalleContactoABean;
import mx.isban.mipersonas.contratos.ejb.BOConsultaContactosContrato;
import mx.isban.mipersonas.domicilios.beans.ConsListaUsosDomiciliosRequestBean;
import mx.isban.mipersonas.domicilios.beans.RegUsoDomiciliosBean;
import mx.isban.mipersonas.domicilios.beans.ConsListaUsosDomiciliosResponseBean;
import mx.isban.mipersonas.base.beans.MulticanalidadBean;
import mx.isban.mipersonas.domicilios.ejb.BOConsultaDomicilios;

import org.apache.commons.lang.StringUtils;

/**
 * @author everis
 *
 */

@Stateless
@Local(DAORelacionClienteContrato.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAORelacionClienteContratoImpl  extends Architech implements DAORelacionClienteContrato{

	private static final String MI30 = "MI30";

	private static final String MI30_CODE_OPERATION = "MI30_CAMBIO_TITULAR_CONTRATO";

	private static final String OD39_89 = "89";
	
	private static final String OD39_1103 = "1103";
	
	private static final String JNDI_PERSONAS_CONSULTACONTRATOS = "java:global/MIPersonasEAREJB/MIPersonasEJB/BOConsultaContactosContratoImpl!mx.isban.mipersonas.contratos.ejb.BOConsultaContactosContrato";
	
	private static final String JNDI_PERSONAS_DOMICILIOS = "java:global/MIPersonasEAREJB/MIPersonasEJB/BOConsultaDomiciliosImpl!mx.isban.mipersonas.domicilios.ejb.BOConsultaDomicilios";
	
	private static final String JNDI_PERFILAMIENTO_NIVELACCESO = "java:global/MIPerfilamientoEAREJB/MIPerfilamientoEJB/BONivelAccesoImpl!mx.isban.miperfilamiento.niveldeacceso.ejb.BONivelAcceso";
	
	private BOConsultaContactosContrato boConsultaContactosContrato;

	private BOConsultaDomicilios boConsultaDomicilios; 
	
	private BONivelAcceso boNivelAcceso;
	/**
	 * Serializacion
	 */
	private static final long serialVersionUID = -4930416111895358803L;
	
	/**
	 * Método que define la función que ejecutará el consumo de la transacción MI30 : Cambio de titular de contrato
	 * @param beanCambioTitularRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess 
	 * 
	 */
	@Override 
	public ResponseGenerico modificarTitularContrato(
			BeanCambioTitularRequest beanCambioTitularRequest,
			ArchitechSessionBean session) throws ExceptionDataAccess {
		this.info("DAOPersonasImpl::modificarTitularContrato()");
		final String mensajeTrama = construirTramaCambioTitularContrato(beanCambioTitularRequest);
		return SingletonIDA.getSingletonInstance().crearResponse(mensajeTrama, MI30_CODE_OPERATION, MI30, this.getLoggingBean(), MIContratosConstantes.MENSAJE_LOG);
	}

	/**
	 * Método que construirá la trama con los parámetros de entrada para la transacción MI30 : Cambio de titular de contrato
	 * @param beanCambioTitularRequest
	 * @return String
	 */
	private String construirTramaCambioTitularContrato(
			BeanCambioTitularRequest beanCambioTitularRequest) {
		
		final StringBuilder tramaEntrada = new StringBuilder();
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getMulticanalidad().getEmpresaConexion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getMulticanalidad().getCanalOperacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getMulticanalidad().getCanalComercializacion(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getTipoConsulta(),	2, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getEmpresaContrato(),	4 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getCentroContrato(),	4 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.rightPad(beanCambioTitularRequest.getNumeroContrato(),	12 ,MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getCodigoProducto(),2 , MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getCodigoSubProducto(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getEmpresaContratoInversion(),	4, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.rightPad(beanCambioTitularRequest.getAliasContrato(),	15, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getNumeroPersona(),	8, MIContratosConstantes.RELLENO_ALFANUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getSecuenciaDomicilioPrincipal(),	3, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getSecuenciaDomicilioEnvio(),	3, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));
		tramaEntrada.append(StringUtils.leftPad(beanCambioTitularRequest.getSecuenciaDomicilioFiscal(),	3, MIContratosConstantes.RELLENO_NUMERICO_ENTRADA));

		return tramaEntrada.toString();
	}
	
	/**
	 * Método que construirá consumira las transacciones OD39 
	 * @param beanLocalizadorContratosRequest
	 * @param sesion
	 * @return ConsultaContratosClienteResponseBean
	 * @throws BusinessException
	 */
	@Override 
	public ConsultaContratosClienteResponseBean localizadorContratos(BeanLocalizadorContratosRequest beanLocalizadorContratosRequest,ArchitechSessionBean sesion) throws BusinessException {
		this.info("DAOPersonasImpl::localizadorContratosWS Personas()");

		//Nueva Instancia del Objeto Reponse
		ConsultaContratosClienteResponseBean response = new ConsultaContratosClienteResponseBean();
		ConsultaContratosClienteResponseBean resultado = new ConsultaContratosClienteResponseBean();
		resultado.setListaContratos(new ArrayList<ConsultaContratosClienteResponseRegBean>());
		
		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			Object obj = ic.lookup(JNDI_PERSONAS_CONSULTACONTRATOS);
			obj = PortableRemoteObject.narrow(obj, BOConsultaContactosContrato.class);
			//Valida Instancia de BO
			if(obj instanceof BOConsultaContactosContrato){
				//Obtiente BO
				boConsultaContactosContrato = (BOConsultaContactosContrato) obj; 
			}
			String masDatos="N";
			String memento="";
			do{
				
				//Consulta BO ------>
				ConsultaContratosClienteRequestBean objRequest = new ConsultaContratosClienteRequestBean();
				objRequest.setNumeroPersona(beanLocalizadorContratosRequest.getNumeroPersona());
				objRequest.setCalidadParticipacion(beanLocalizadorContratosRequest.getCalidadPaticipacion());
				objRequest.setCanalComercio("");
				objRequest.setCanalOperacion("");
				objRequest.setEmpresaConexion("");
				objRequest.setEntidadPersona("");
				ReposicionBean repoBean= new ReposicionBean();
				repoBean.setCadenaRepo(memento);
				repoBean.setMasDatos(masDatos);
				objRequest.setRepo(repoBean);
				
				BeanProductoSubproducto[] lstProd = new BeanProductoSubproducto[10];
				BeanProductoSubproducto prod= new BeanProductoSubproducto();
				prod.setCodigoProducto(OD39_89);
				prod.setCodigoSubproducto(OD39_1103);
				lstProd[0] = prod;
				for(int i=1;i<10;i++){
					BeanProductoSubproducto prodTmp= new BeanProductoSubproducto();
					prodTmp.setCodigoProducto("");
					prodTmp.setCodigoSubproducto("");
					lstProd[i] = prodTmp;
				}
				objRequest.setListaProductoSubproducto(lstProd);		
				response = boConsultaContactosContrato.consultaContratosCliente(objRequest, null);				
				resultado.setCodError(response.getCodError());
				resultado.setMsgError(response.getMsgError());
				resultado.getListaContratos().addAll(response.getListaContratos());
				resultado.setRepo(response.getRepo());
				
			
				masDatos = response.getRepo().getMasDatos();
				memento="";
				if(MIContratosConstantes.SI_HAY_DATOS.equals(masDatos)){
					memento=response.getRepo().getCadenaRepo();
				}
			}while(MIContratosConstantes.SI_HAY_DATOS.equals(masDatos));			
		} catch (NamingException e) {
			//Excepcion de JNDI No se encontro el BO
			this.error("",e);
			showException(e);
		} catch (BusinessException e) {
			//La BusinessException en el BO
			this.error("",e);
			showException(e);
			throw new BusinessException(e.getCode(),e.getMessage());
		}
		return resultado;
	}

	
	/**
	 * Método que construirá consumira las transacciones OD39 
	 * @param ejecutivo
	 * @param tipoConsulta
	 * @param list
	 * @param session
	 * @return BeanValidacionNivelAccesoResponse
	 * @throws BusinessException
	 */
	@Override 
	public BeanValidacionNivelAccesoResponse validacioNivelAcceso(String ejecutivo, String tipoConsulta, List<ConsultaContratosClienteResponseRegBean> list,ArchitechSessionBean session) throws BusinessException {
		this.info("DAOPersonasImpl::localizadorContratosWS Personas()");
		//Nueva Instancia del Objeto Reponse
		BeanValidacionNivelAccesoResponse response = new BeanValidacionNivelAccesoResponse();
		List<BeanValidacionNivelAcceso> lst = new ArrayList<>();
		response.setListaValNivelAcceso(lst);
		
		//Archivo properties
		Properties properties=MIContratosValidator.getSingletonInstance().getProperties();

		//Se obtiene valor del contador de iteracion desde el archivo properties
		int contIteracion = Integer.parseInt(properties.getProperty("CONT_ITERACION"));

		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			Object obj = ic.lookup(JNDI_PERFILAMIENTO_NIVELACCESO);
			obj = PortableRemoteObject.narrow(obj, BONivelAcceso.class);
			//Valida Instancia de BO
			if(obj instanceof BONivelAcceso){
				//Obtiente BO
				boNivelAcceso = (BONivelAcceso) obj; 
			}
			Integer tamanio = list.size()/contIteracion;
			Integer restante = list.size()%contIteracion;
			
			if(restante>0){
				tamanio ++;
			}
		
			for(int i=0;i<tamanio;i++){
				BeanValidacionNivelAccesoRequest req = new BeanValidacionNivelAccesoRequest();
				req.setCodigoEjecutivo(ejecutivo);
				req.setTipoConsulta(tipoConsulta);
				
				DatosMulticanalidad multi = new DatosMulticanalidad();
				multi.setCanalComercializacion("");
				multi.setCanalOperacion("");
				multi.setEmpresaConexion("");
				req.setMulticanalidad(multi);
				DatosContratosValidacion[] validacion = req.getDatosContratosValidacion();
				
				validacion=getListaValidaciones(validacion,list,contIteracion,i);
				req.setDatosContratosValidacion(validacion);
				
				BeanValidacionNivelAccesoResponse respuesta = boNivelAcceso.validacionNivelAcceso(session,req);
				if(respuesta.getListaValNivelAcceso()!=null){
		
					List<BeanValidacionNivelAcceso> lstFin = response.getListaValNivelAcceso();
					lstFin.addAll(respuesta.getListaValNivelAcceso());
					response.setListaValNivelAcceso(lstFin);
				}
			}
		} catch (NamingException e) {
			//Excepcion de JNDI No se encontro el BO
			this.error("",e);
			showException(e);
		} catch (BusinessException e) {
			//La BusinessException en el BO
			this.error("",e);
			showException(e);
			throw new BusinessException(e.getCode(),e.getMessage());
		}
		return response;
	}
	
	/**
	 * Método que devuelve un arreglo de contratos validados
	 * @param valid
	 * @param list
	 * @param contIteracion
	 * @param i
	 * @return DatosContratosValidacion[]
	 */
	public DatosContratosValidacion[] getListaValidaciones(DatosContratosValidacion[] valid,List<ConsultaContratosClienteResponseRegBean> list, int contIteracion, int i){
		DatosContratosValidacion[] validacion = valid.clone();
				for(int j=0;j<10;j++){
					DatosContratosValidacion listUno = new DatosContratosValidacion();
					listUno.setAliasContrato("");
					
					DatosContratoPadre padreUno = new DatosContratoPadre();
					if(j<contIteracion){
						if(list.size()>((i*contIteracion)+j)){
							ConsultaContratosClienteResponseRegBean objeto = list.get((i*contIteracion)+j);
							padreUno.setCentroContrato(objeto.getCentroAlta());
							padreUno.setEmpresaContrato(objeto.getEntidad());
							padreUno.setNumeroContrato(objeto.getCuenta());
							DatosProductoContrato prodUno = new DatosProductoContrato();
							prodUno.setCodigoProductoContrato(objeto.getProducto());
							prodUno.setCodigoSubproductoContrato(objeto.getSubproducto());
							padreUno.setDatosProductoContrato(prodUno);
							listUno.setDatosContratoPadre(padreUno);
						}else{	
							padreUno.setCentroContrato("");
							padreUno.setEmpresaContrato("");
							padreUno.setNumeroContrato("");
							
							DatosProductoContrato prodUno = new DatosProductoContrato();
							prodUno.setCodigoProductoContrato("");
							prodUno.setCodigoSubproductoContrato("");
							padreUno.setDatosProductoContrato(prodUno);
							listUno.setDatosContratoPadre(padreUno);
						}	
					}else{	
						padreUno.setCentroContrato("");
						padreUno.setEmpresaContrato("");
						padreUno.setNumeroContrato("");
						
						DatosProductoContrato prodUno = new DatosProductoContrato();
						prodUno.setCodigoProductoContrato("");
						prodUno.setCodigoSubproductoContrato("");
						padreUno.setDatosProductoContrato(prodUno);
						listUno.setDatosContratoPadre(padreUno);
					}	

					validacion[j] =listUno;	
				}
				return validacion;
	}
	
	/**
	 * Método que devuelve una lista de intervinientes detallada
	 * @param contrato
	 * @param session
	 * @return List
	 * @throws BusinessException
	 */
	public List<BeanDetalleContratoInversionInterviniente> consultaDetIntervinientes(BeanConsultaInversionResponse contrato, ArchitechSessionBean session) throws BusinessException{
		this.info("DAORelacionClienteContrato::consultaDetIntervinientes Personas");
		List<BeanDetalleContratoInversionInterviniente> lstResponse  = new ArrayList<>();

		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			Object obj = ic.lookup(JNDI_PERSONAS_CONSULTACONTRATOS);
			obj = PortableRemoteObject.narrow(obj, BOConsultaContactosContrato.class);
			//Valida Instancia de BO
			if(obj instanceof BOConsultaContactosContrato){
				//Obtiente BO
				boConsultaContactosContrato = (BOConsultaContactosContrato) obj; 
			}
			
			List<BeanListaIntervinientesResponse> lstInterv= contrato.getListaIntervinientes();
			for(int i=0;i<lstInterv.size();i++){
				BeanListaIntervinientesResponse intervin = lstInterv.get(i);

				// validar intervinientes TI Calidad Participacion Interviniente
					ConsDetInterRequestBean objEnt = new ConsDetInterRequestBean();
					objEnt.setCalidadParticipacion(intervin.getCalidadParticipacionInterviniente());
					objEnt.setCodigoProducto(contrato.getBeanDatosContratos().getCodigoProducto());
					objEnt.setCodigoSubProducto(contrato.getBeanDatosContratos().getCodigoSubProductoContrato());
					objEnt.setEntidadContrato(contrato.getBeanDatosContratos().getCodigoEmpresa());
					MulticanalidadBean multi = new MulticanalidadBean();
					multi.setCanalComercial("");
					multi.setCanalOperacion("");
					multi.setEntidad("");
					objEnt.setMulticanalidad(multi);
					objEnt.setNumeroContrato(contrato.getBeanDatosContratos().getNumeroContrato());
					objEnt.setNumeroPersona(intervin.getNumeroPersona());
					objEnt.setOficinaContrato(contrato.getBeanDatosContratos().getCentroContrato());
	
					ConsDetInterResponseBean result = boConsultaContactosContrato.consultaDetalleInterviniente(objEnt, session);
					BeanDetalleContratoInversionInterviniente reg = new BeanDetalleContratoInversionInterviniente();
					reg.setCalidadParticipacion(intervin.getCalidadParticipacionInterviniente());
					reg.setCodigoPoder(result.getCodigoPoder());
					reg.setDomicilioVinculador(result.getDomicilioVinculado());
					reg.setFolioPoder(result.getFolioPoder());
					reg.setIdentificacionTipoFirma(result.getIdentificacionFirma());
					reg.setNumeroPersona(intervin.getNumeroPersona());
					reg.setOrdenParticipacion(intervin.getOrdenParticipacionInterviniente());
					reg.setPorcentajeIpab(MIContratosValidator.validarCamposFlotante(result.getPorcIPAB()));
					reg.setPorcentajeParticipacion(MIContratosValidator.validarCamposFlotante(result.getPorcParticipacion()));
					reg.setSecuenciaDomicilio(result.getSecuenciaDomicilio());
					
					lstResponse.add(reg);
				//Cerrar validación
			}

		} catch (NamingException e) {
			//Excepcion de JNDI No se encontro el BO
			this.error("",e);
			showException(e);
		}
		return lstResponse;
	}
	
	/**
	 * Método que devuelve una lista detallada de contactos de inversión
	 * @param contrato
	 * @param numeroPersona
	 * @param session
	 * @return List
	 * @throws BusinessException
	 */
	public List<BeanDetalleContratoInversionContacto> consultaDetContactos(BeanConsultaInversionResponse contrato, String numeroPersona,ArchitechSessionBean session) throws BusinessException{
		this.info("DAORelacionClienteContrato::consultaDetContactos Personas"+contrato.getListaContactos().size());
		List<BeanDetalleContratoInversionContacto> lstResponse  = new ArrayList<>();

		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			Object obj = ic.lookup(JNDI_PERSONAS_CONSULTACONTRATOS);
			obj = PortableRemoteObject.narrow(obj, BOConsultaContactosContrato.class);
			//Valida Instancia de BO
			if(obj instanceof BOConsultaContactosContrato){
				//Obtiente BO
				boConsultaContactosContrato = (BOConsultaContactosContrato) obj;	
			}
				
			List<BeanListaContactosResponse> lstCont= contrato.getListaContactos();
			for(int i=0;i<lstCont.size();i++){
				BeanListaContactosResponse contacto = lstCont.get(i);
				
				ConsDetalleContacRequestBean objEnt = new ConsDetalleContacRequestBean(); 
				objEnt.setCodigoProducto(contrato.getBeanDatosContratos().getCodigoProducto());
				objEnt.setCodigoSubProducto(contrato.getBeanDatosContratos().getCodigoSubProductoContrato());
				objEnt.setEntidadContrato(contrato.getBeanDatosContratos().getCodigoEmpresa());
				MulticanalidadBean multi = new MulticanalidadBean();
				multi.setCanalComercial("");
				multi.setCanalOperacion("");
				multi.setEntidad("");
				objEnt.setMulticanalidad(multi);
				objEnt.setNumeroContrato(contrato.getBeanDatosContratos().getNumeroContrato());
				objEnt.setNumeroPersona(numeroPersona);
				objEnt.setOficinaContrato(contrato.getBeanDatosContratos().getCentroContrato());
				objEnt.setSecuenciaContacto(Integer.toString(contacto.getSecuenciaContacto()));//Detalle: consultaDetalleContacto rellena a la derecha
					
				DetalleContactoABean result = boConsultaContactosContrato.consultaDetalleContacto(objEnt, session);
				
				BeanDetalleContratoInversionContacto reg = new BeanDetalleContratoInversionContacto();
				reg.setApeMaternoContact(result.getaMaterno());
				reg.setApePaterContact(result.getaPaterno());
				reg.setAsentamientoColonia(result.getAsentamiento());
				reg.setCalleAvenidaVia(result.getCalle());
				reg.setCiudadPoblacion(result.getCiudad());
				reg.setCodigoPostal(result.getCp());
				reg.setCurp(result.getCurp());
				reg.setDelegacionMunicipioContact(result.getDelMunicipio());
				reg.setDenomRazonSocial(result.getRazonSocial());
				reg.setDescDelegacionMunicipioContacto(result.getDescDelMunicipio());
				reg.setDescEntidadFederatContacto(result.getDescEntidadFe());
				reg.setDescripcionCiudadPoblacion(result.getCiudad());
				reg.setEmailContacto(result.getCorreo());
				reg.setEntidadFederatDomicContacto(result.getCodEntidadFe());
				reg.setFechaNacimientoContacto(result.getFechaNac());
				reg.setNacionalidadContacto(result.getNacionalidad());
				reg.setNombreContact(result.getNombre());
				reg.setNumeroExterior(result.getNumExterior());
				reg.setNumeroIdentificacionFiscal(result.getIndentificacionFiscal());
				reg.setNumeroInterior(result.getNumInterior());
				reg.setPaisDomiclio(result.getPaisDomicilio());
				reg.setPaisNacimientoContacto(result.getPaisNacimiento());
				reg.setPorcentajeParticpacion(MIContratosValidator.validarCamposFlotante(result.getPorcParticipacion()));
				reg.setPrefijoTelefono(result.getLada());
				reg.setTelefonoContacto(result.getTelefono());
				reg.setRfc(result.getRfc());
				reg.setSecunciaContacto(contacto.getSecuenciaContacto());
				reg.setTipoContacto(contacto.getTipoContacto());
				reg.setTipPersJuridConta(Integer.toString(result.getTipoPersona()));
				
				lstResponse.add(reg);
			
			}
		} catch (NamingException e) {
			//Excepcion de JNDI No se encontro el BO
			this.error("",e);
			showException(e);
		}
		return lstResponse;
	}

	/**
	 * Método que devuelve una lista detallada de usos domicilio
	 * @param contrato
	 * @param session
	 * @return List
	 * @throws BusinessException
	 */
	public List<BeanDetalleContratoInversionUsosDomicilio> listadoDomicilios(BeanConsultaInversionResponse contrato, ArchitechSessionBean session) throws BusinessException{
		this.info("DAORelacionClienteContrato::listadoDomicilios Personas"+contrato.getListaDatosDomicilio());
		List<BeanDetalleContratoInversionUsosDomicilio> lstResponse  = new ArrayList<>();

		try {
			//Obtener el contexto
			InitialContext ic = new InitialContext();
			Object obj = ic.lookup(JNDI_PERSONAS_DOMICILIOS);
			obj = PortableRemoteObject.narrow(obj, BOConsultaDomicilios.class);
			//Valida Instancia de BO
			if(obj instanceof BOConsultaDomicilios){
				//Obtiente BO
				boConsultaDomicilios = (BOConsultaDomicilios) obj;	
			}
				
			List<BeanDatosDomicilioResponse> lstDom= contrato.getListaDatosDomicilio();
			for(int i=0;i<lstDom.size();i++){
				BeanDatosDomicilioResponse domicilio = lstDom.get(i);
				ConsListaUsosDomiciliosRequestBean objEnt = new ConsListaUsosDomiciliosRequestBean();
				objEnt.setCodEntidad(contrato.getBeanDatosContratos().getCodigoEmpresa());
				objEnt.setCodOficina(contrato.getBeanDatosContratos().getCentroContrato());
				objEnt.setCodProducto(contrato.getBeanDatosContratos().getCodigoProducto());
				objEnt.setCodSubProducto(contrato.getBeanDatosContratos().getCodigoSubProductoContrato());
				MulticanalidadBean multi = new MulticanalidadBean();
				multi.setCanalComercial("");
				multi.setCanalOperacion("");
				multi.setEntidad("");
				objEnt.setDatosMulticanalidad(multi);
				objEnt.setNumCont(contrato.getBeanDatosContratos().getNumeroContrato());
				objEnt.setNumPers(domicilio.getNumeroPersonaUsoDomicilio());
				objEnt.setTipoUso(domicilio.getTipoUsoDomicilio());
				
				ConsListaUsosDomiciliosResponseBean result = boConsultaDomicilios.consultaUsosDomicilio(session,objEnt);
				for(int j=0;j<result.getListaUsoDomicilios().size();j++){
					RegUsoDomiciliosBean reg = result.getListaUsoDomicilios().get(j);
					BeanDetalleContratoInversionUsosDomicilio regAgrega = new BeanDetalleContratoInversionUsosDomicilio();
					regAgrega.setNombreDomicilio(reg.getNomCalle());
					regAgrega.setNumeroPersona(reg.getNumPers());
					regAgrega.setSecuenciaDomicilio(reg.getSecDomi());
					regAgrega.setTipoDomicilio(reg.getTipDomi());
					regAgrega.setTipoUsoDomicilio(reg.getTipoUso());
					lstResponse.add(regAgrega);
				}
			
			}
		} catch (NamingException e) {
			//Excepcion de JNDI No se encontro el BO
			this.error("",e);
			showException(e);
		}
		return lstResponse;
	}
}