package mx.isban.micontratos.relacionclientecontrato.ejb.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanDatosContratos;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResultado;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasEntrada;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasRequest;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasResponse;
import mx.isban.micontratos.beans.gestioncontratos.BeanConversorCuentasResultado;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanCambioTitularRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionContacto;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionInterviniente;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionResultado;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionUsosDomicilio;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosResponse;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosResultado;
import mx.isban.micontratos.gestioncontratos.ejb.BOGestionContratos;
import mx.isban.micontratos.relacionclientecontrato.dao.DAORelacionClienteContrato;
import mx.isban.micontratos.relacionclientecontrato.ejb.BORelacionClienteContrato;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosValidator;
import mx.isban.miperfilamiento.niveldeacceso.beans.BeanValidacionNivelAcceso;
import mx.isban.miperfilamiento.niveldeacceso.beans.BeanValidacionNivelAccesoResponse;
import mx.isban.mipersonas.contratos.beans.ConsultaContratosClienteResponseBean;
import mx.isban.mipersonas.contratos.beans.ConsultaContratosClienteResponseRegBean;

/**
 * @author everis
 *
 */

@Stateless
@Remote(BORelacionClienteContrato.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BORelacionClienteContratoImpl extends Architech implements BORelacionClienteContrato {
	

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -5691697572501307679L;
	
	/**
	 * Instancia de DAORelacionClienteContrato
	 */
	@EJB
	private DAORelacionClienteContrato daoRelacionClienteContrato;
	
	/**
	 * Instancia de BOGestionContratos
	 */
	@EJB
	private BOGestionContratos boGestionContratos;


	/**
	 * 	Método que hará el llamado a el consumo de la transacción MI30 : Cambio de titular de contrato
	 * @param beanCambioTitularRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws BusinessException
	 */
	@Override
	public ResponseGenerico modificarTitularContrato(BeanCambioTitularRequest beanCambioTitularRequest,ArchitechSessionBean session) throws BusinessException {
		//Campos obligatorios
		String[] camposObligatorios = {beanCambioTitularRequest.getTipoConsulta()};
		//Longitudes de campos obligatorios
		int[] longitudesObligatorias={2};
		//Campos no obligatorios
		String[] camposNoObligatorios = { beanCambioTitularRequest.getMulticanalidad().getEmpresaConexion(),
				beanCambioTitularRequest.getMulticanalidad().getCanalOperacion(),
				beanCambioTitularRequest.getMulticanalidad().getCanalComercializacion(), 
				beanCambioTitularRequest.getEmpresaContrato(),
				beanCambioTitularRequest.getCentroContrato(),
				beanCambioTitularRequest.getNumeroContrato(),
				beanCambioTitularRequest.getCodigoProducto(),
				beanCambioTitularRequest.getCodigoSubProducto(),
				beanCambioTitularRequest.getEmpresaContratoInversion(),
				beanCambioTitularRequest.getAliasContrato(),
				beanCambioTitularRequest.getSecuenciaDomicilioEnvio(),
				beanCambioTitularRequest.getSecuenciaDomicilioFiscal(),
				beanCambioTitularRequest.getNumeroPersona(),
				beanCambioTitularRequest.getSecuenciaDomicilioPrincipal()
				};
		//Longitudes de campos no obligatorios
		int[] longitudesDeCamposNoObligatorios = {4,2,2,4,4,12,2,4,4,15,3,3,8,3};
		
		try {
			//Se validan campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesDeCamposNoObligatorios);
			//Se validan campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			ResponseGenerico response=daoRelacionClienteContrato.modificarTitularContrato(beanCambioTitularRequest, session);
			//Se valida el código de error
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + " la ejecución de la transacción.");
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
	
	/**
	 * Metódo que ejecutará el consumo de la transacción OD39 y WS perfilamiento: Localizador de contratos por cliente
	 * @param beanLocalizadorContratosRequest
	 * @param sesion 
	 * @return BeanLocalizadorContratosResultado
	 * @throws BusinessException
	 */
	@Override
	public BeanLocalizadorContratosResultado localizadorContratos(BeanLocalizadorContratosRequest beanLocalizadorContratosRequest,ArchitechSessionBean sesion) throws BusinessException {
		BeanLocalizadorContratosResultado response = new BeanLocalizadorContratosResultado();
		//Campos  obligatorios
		String[] camposObligatorios = {beanLocalizadorContratosRequest.getNumeroPersona(),beanLocalizadorContratosRequest.getCalidadPaticipacion(),
				beanLocalizadorContratosRequest.getCodigoEjecutivo()};
		//Longitudes de campos obligatorios 
		int[] longitudesObligatorias={8,2,8};
		//Campos no obligatorios
		String[] camposNoObligatorios = {beanLocalizadorContratosRequest.getMulticanalidad().getEmpresaConexion(),beanLocalizadorContratosRequest.getMulticanalidad().getCanalOperacion(),
				beanLocalizadorContratosRequest.getMulticanalidad().getCanalComercializacion(),beanLocalizadorContratosRequest.getEntidadPersona()};
		//Longitudes de campos no obligatorios
		int[] longitudesDeCamposNoObligatorios = {4,2,2,4};
				
		try {
			//Se validan campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			//Se validan campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesDeCamposNoObligatorios);			
			
			ConsultaContratosClienteResponseBean responseOdTreNueve = daoRelacionClienteContrato.localizadorContratos(beanLocalizadorContratosRequest, sesion);
			info("Response: " + responseOdTreNueve);
			
			if(responseOdTreNueve.getListaContratos()!=null && !responseOdTreNueve.getListaContratos().isEmpty()){
				//Se valida el nivel de acceso en perfilamiento
				BeanValidacionNivelAccesoResponse responsePerfil = daoRelacionClienteContrato.validacioNivelAcceso(beanLocalizadorContratosRequest.getCodigoEjecutivo(), "01", responseOdTreNueve.getListaContratos(),null);
				//Código de error
				response.setCodError(responsePerfil.getCodError());
				//Mensaje de error
				response.setMsgError(responsePerfil.getMsgError());
				if(responsePerfil.getListaValNivelAcceso()!=null && !responsePerfil.getListaValNivelAcceso().isEmpty()){
					response=getResponseLocalizadorContratos(responsePerfil, responseOdTreNueve, sesion);
				}
			}
			return response;
		} catch (BusinessException e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + " la ejecución de la transacción OD39.");
			this.error("",e);
			showException(e);
			throw new BusinessException(e.getCode(),e.getMessage()); 
		}catch (MIContratosException e){
			this.error("", e);
			showException(e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	
	}
	
	/**
	 * Método que devuelve el bean resultado del localizador de contratos
	 * @param responsePerfil
	 * @param responseOdTreNueve
	 * @param sesion
	 * @return BeanLocalizadorContratosResultado
	 * @throws BusinessException
	 */
	public BeanLocalizadorContratosResultado getResponseLocalizadorContratos(BeanValidacionNivelAccesoResponse responsePerfil,ConsultaContratosClienteResponseBean responseOdTreNueve, ArchitechSessionBean sesion ) throws BusinessException{
		BeanLocalizadorContratosResultado response = new BeanLocalizadorContratosResultado();
		List<BeanLocalizadorContratosResponse> localizadorContratoFin = new ArrayList<>();	
		BeanConversorCuentasRequest entradaCon = new BeanConversorCuentasRequest();
		List<BeanConversorCuentasEntrada> beanconversorCuentas = new ArrayList<>();
		//Se itera para todos los contratos validados
		for(int i=0;i<responsePerfil.getListaValNivelAcceso().size();i++){
			BeanValidacionNivelAcceso objRest= responsePerfil.getListaValNivelAcceso().get(i);
			
				//Se valida que se informe el indicador de consulta o el indiicador de operación
				if("S".equals(objRest.getIndicadorConsulta()) || "S".equals(objRest.getIndicadorOperacion())){
					BeanConversorCuentasEntrada reg = new BeanConversorCuentasEntrada();
					
					reg.setCodigoEmpresaContratoInversion("");				
					reg.setAliasContrato("");					

					ConsultaContratosClienteResponseRegBean objRestDos = responseOdTreNueve.getListaContratos().get(i);
					
					reg.setCentroContrato(objRestDos.getCentroAlta());
					reg.setCodigoProducto(objRestDos.getProducto());
					reg.setCodigoSubProducto(objRestDos.getSubproducto());
					reg.setEmpresaContrato(objRestDos.getEntidad());
					reg.setNumeroContrato(objRestDos.getCuenta());
	
					beanconversorCuentas.add(reg);
					
				}
				//Si valida que la lista ya cuente con 10 contratos
				if(beanconversorCuentas.size()==10 || i == responsePerfil.getListaValNivelAcceso().size()-1){
					
					MulticanalidadGenerica multi = new MulticanalidadGenerica();
					multi.setCanalComercializacion("");
					multi.setCanalOperacion("");
					multi.setEmpresaConexion("");
					entradaCon.setContratos(beanconversorCuentas);
					entradaCon.setMulticanalidad(multi);
					//Consulta al BOGestionContratos
					BeanConversorCuentasResultado conversor= boGestionContratos.conversorCuentas(entradaCon, sesion);
					//Código de error
					response.setCodError(conversor.getCodError());
					//Mensaje de error
					response.setMsgError(conversor.getMsgError());
					//Se añaden todos los contratos a la lista final del localizador de contratos
					localizadorContratoFin.addAll(getListaLocalizadorCOntratosResponse(conversor));
					
					beanconversorCuentas= new ArrayList<>();	
				}					
		}
		response.setLocalizadorContratos(localizadorContratoFin);
		return response;
	}
	/**
	 * Método que devuelve una lista de contratos
	 * @param conversor
	 * @return List
	 */
	public 	List<BeanLocalizadorContratosResponse> getListaLocalizadorCOntratosResponse(BeanConversorCuentasResultado conversor){
		List<BeanLocalizadorContratosResponse> localizadorContratoFin = new ArrayList<>();
		
		if(conversor.getBeanconversorCuentas()!=null && !conversor.getBeanconversorCuentas().isEmpty()){
			for(int j=0;j<conversor.getBeanconversorCuentas().size();j++){
				BeanConversorCuentasResponse reg = conversor.getBeanconversorCuentas().get(j);
				
				BeanLocalizadorContratosResponse regFin = new BeanLocalizadorContratosResponse();
				regFin.setIndicadorOperacion("S");
				regFin.setIndicadorConsulta("S");
				regFin.setCodigoEmpresa(reg.getCodEmpresaContratoInversion());
				regFin.setCentroContrato(reg.getCentroContrato());
				regFin.setNumeroContrato(reg.getNumeroContrato());
				regFin.setCodigoProductoContrato(reg.getCodigoProductoContrato());
				regFin.setCodigoSubProductoContrato(reg.getCodigoSubProductoContrato());
				regFin.setCodigoMonedaContrato(reg.getCodigoMonedaContrato());
				regFin.setFolioProvieneContrato(reg.getFolioProvieneContrato());
				regFin.setCodEmpresaContratoInversion(reg.getCodEmpresaContratoInversion());
				regFin.setCodigoBancaContrato(reg.getCodigoBancaContrato());
				regFin.setFechaAperturaContrato(reg.getFechaAperturaContrato());
				regFin.setAliasContrato(reg.getAliasContrato());
				regFin.setFechaVencimientoContrato(reg.getFechaVencimientoContrato());
				regFin.setCodigoEjecutivoContrato(reg.getCodigoEjecutivoContrato());
				regFin.setCentroCostosContrato(reg.getCentroCostosContrato());
				regFin.setIndicadorTipoCuenta(reg.getIndicadorTipoCuenta());
				regFin.setIndicadorTipoContrato(reg.getIndicadorTipoContrato());
				regFin.setIndAutPosicionCortoCapitales(reg.getIndAutPosicionCortoCapitales());
				regFin.setIndAutPosicionCortoFondos(reg.getIndAutPosicionCortoFondos());
				regFin.setIndAutPosicionFondosInvers(reg.getIndAutPosicionFondosInvers());
				regFin.setIndicadorDiscrecionalidad(reg.getIndicadorDiscrecionalidad());
				regFin.setIndicadorTipoCustodiaDirecto(reg.getIndicadorTipoCustodiaDirecto());
				regFin.setIndicadorTipoCustodiaRepos(reg.getIndicadorTipoCustodiaRepos());
				regFin.setIndicadorEstadoCuenta(reg.getIndicadorEstadoCuenta());
				regFin.setMontoMaxOperarContrato(reg.getMontoMaxOperarContrato());
				regFin.setDivisaMontoMaxContrato(reg.getDivisaMontoMaxContrato());
				regFin.setIndicadorEnvioCorreo(reg.getIndicadorEnvioCorreo());
				regFin.setEstatusContrato(reg.getEstatusContrato());
				regFin.setDetalleEstatus(reg.getDetalleEstatus());
				regFin.setFechaEstadoContrato(reg.getFechaEstadoContrato());
				regFin.setIndicadorContratoAgrupado(reg.getIndicadorContratoAgrupado());
				regFin.setIndicadorTipoPosicion(reg.getIndicadorTipoPosicion());
				regFin.setIndicadorContratoEspejo(reg.getIndicadorContratoEspejo());
				regFin.setIndCtoAmparadoChequera(reg.getIndCtoAmparadoChequera());
				regFin.setCodioTipoServicio(reg.getCodigoTipoServicio());
				regFin.setCodigoAfi(reg.getCodigoAfi());
				regFin.setIndicadorBeneficiarios(reg.getIndicadorBeneficiario());
				regFin.setIndicadorProveedoresRecursos(reg.getIndicadorProveedoresRecursos());
				regFin.setIndicadorAccionistas(reg.getIndicadorAccionistas());
				regFin.setFechaUltimaOperacion(reg.getFechaUltimaOperacion());
				regFin.setFechaUltimaModificacion(reg.getFechaUltimaModificacion());
				regFin.setDescripcionSubEstatus(reg.getDescripcionSubEstatus());
			
				localizadorContratoFin.add(regFin);
			}
		}
		
		return localizadorContratoFin;
	}
	
	/**
	 * Metódo que ejecutará el consumo de consultaDatosContrato y servicios de Personas : Detalle Contrato Inversion
	 * @param beanConsultaInversionRequest
	 * @param session
	 * @return BeanDetalleContratoInversionResultado
	 */
	@Override
	public BeanDetalleContratoInversionResultado detalleContratoInversion(BeanConsultaInversionRequest beanConsultaInversionRequest,ArchitechSessionBean session) throws BusinessException {
		BeanDetalleContratoInversionResultado response = new BeanDetalleContratoInversionResultado();
		//Campos obligatorios
		String[] camposObligatorios = {
				beanConsultaInversionRequest.getTipoConsulta()};
		//Longitudes de campos no obligatorios
		int[] longitudesObligatorias={2};
		//Campos no obligatorios
		String[] camposNoObligatorios = {
				beanConsultaInversionRequest.getMulticanalidad().getEmpresaConexion(),
				beanConsultaInversionRequest.getMulticanalidad().getCanalOperacion(),
				beanConsultaInversionRequest.getMulticanalidad().getCanalComercializacion(),
				beanConsultaInversionRequest.getEmpresaContrato(),
				beanConsultaInversionRequest.getCentroContrato(),beanConsultaInversionRequest.getNumeroContrato(),
				beanConsultaInversionRequest.getCodigoProducto(),beanConsultaInversionRequest.getCodigoSubProducto(),
				beanConsultaInversionRequest.getEmpresaContratoInversion(),beanConsultaInversionRequest.getAliasContrato(),
				beanConsultaInversionRequest.getIndicadorInformacionIntervinientes(),beanConsultaInversionRequest.getIndicadorInformacionContratosRelacionados(),
				beanConsultaInversionRequest.getIndicadorUsoDomicilios(),beanConsultaInversionRequest.getIndicadorInformacionContactos(),
				beanConsultaInversionRequest.getIndicadorInformacionReferenciaContrato(),beanConsultaInversionRequest.getFechaConsulta()};
		//Longitudes de campos no obligatorios
		int[] longitudesDeCamposNoObligatorios = {4,2,2,4,4,12,2,4,4,15,1,1,1,1,1,10};
			
		try {			
			//Se validan campos obligatorios
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			//Se validan campos no obligatorios
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesDeCamposNoObligatorios);
			//Se consulta la MI16
			BeanConsultaInversionResultado contrato = boGestionContratos.consultaDatosContrato(beanConsultaInversionRequest, session);
			//Se inicializan las listas
			List<BeanDetalleContratoInversionInterviniente> lstIntervinientes= new ArrayList<>();
			List<BeanDetalleContratoInversionContacto> lstContactos=new ArrayList<>();
			List<BeanDetalleContratoInversionUsosDomicilio> lstDomicilios= new ArrayList<>();
			
			//Si exsiten intervinientes
			if(contrato.getListaConsultaInversion().get(0).getListaIntervinientes().size()>0){
				lstIntervinientes= daoRelacionClienteContrato.consultaDetIntervinientes(contrato.getListaConsultaInversion().get(0),session);
			}
			//Si se cuentan con intervinientes
			if(contrato.getListaConsultaInversion().get(0).getListaContactos().size()>0){				
				for(int i =0;i<lstIntervinientes.size();i++){
					lstContactos.addAll(daoRelacionClienteContrato.consultaDetContactos(contrato.getListaConsultaInversion().get(0), lstIntervinientes.get(i).getNumeroPersona(), session));
				}
			}
			//Si se encuentran domicilios
			if(contrato.getListaConsultaInversion().get(0).getListaDatosDomicilio().size()>0){
				lstDomicilios = daoRelacionClienteContrato.listadoDomicilios(contrato.getListaConsultaInversion().get(0),session);
			}					
			
			BeanConsultaInversionResponse contResp = contrato.getListaConsultaInversion().get(0);
			BeanDatosContratos beanCont = contResp.getBeanDatosContratos();
			
			response= getBeanDetalleContratoInversionResultado(beanCont, contResp);
			response.setCodError(contrato.getCodError());
			response.setMsgError(contrato.getMsgError());
			response.setListContactos(lstContactos);
			response.setLstIntervinientes(lstIntervinientes);
			response.setLstUsosDomiclio(lstDomicilios);
			
			return response;
		}catch (MIContratosException e){
			error("", e);
			showException(e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
	
	/**
	 * Método que construirá el bean resultado de salida del servicio: Detalle de contrato de inversión
	 * @param beanCont
	 * @param contResp
	 * @return BeanDetalleContratoInversionResultado
	 */
	public BeanDetalleContratoInversionResultado getBeanDetalleContratoInversionResultado(BeanDatosContratos beanCont, BeanConsultaInversionResponse contResp ){
		BeanDetalleContratoInversionResultado response = new BeanDetalleContratoInversionResultado();
		
		response.setCodEmpresa(beanCont.getCodigoEmpresa());
		response.setCentroContrato(beanCont.getCentroContrato());
		response.setNumeroContrato(beanCont.getNumeroContrato());
		response.setCodProductoContrato(beanCont.getCodigoProducto());
		response.setCodSubProductoCto(beanCont.getCodigoSubProductoContrato());
		response.setCodMonedaCto(beanCont.getCodigoMonedaContrato());
		response.setFolioProvieneCto(beanCont.getNumeroFolio());
		response.setCodEmpContratoInversion(beanCont.getCodigoEmpresaContrato());
		response.setCodBancaContrato(beanCont.getCodigoBanca());
		response.setFechaAperturaContrato(beanCont.getFechaApertura());
		response.setAliasContrato(beanCont.getDescripcionAlias());
		response.setFechaVencimientoCto(beanCont.getFechaVencimiento());
		response.setCodEjecutivoCto(beanCont.getCodigoEjecutivo());
		response.setCentroCostosCto(beanCont.getCentroCostosContrato());
		response.setIndTipoCuenta(beanCont.getIndicadorTipoCuenta());
		response.setIntTipoContrato(beanCont.getIndicadorTipoContrato());
		response.setIndAutPosicionCortoCapitales(beanCont.getIndicadorAutorizacionPosicionCortoCapitales());
		response.setIndAutPosicionCortoFondos(beanCont.getIndicadorAutorizacionPosicionCortoFondos());
		response.setIndAutPosicionFondosInvers(beanCont.getIndicadorAutorizacionCruceFondosInversion());
		response.setIndDiscrecionalidad(beanCont.getIndicadorDiscrecionalidad());
		response.setIndTipoCustodiaDirecto(beanCont.getIndicadorTipoCustodaDirecto());
		response.setIndTipoCustodiaRepos(beanCont.getIndicadorTipoCustodiaRepos());
		response.setIndEstadoCuenta(beanCont.getIndicadorEstadoCuenta());
		response.setMontoMaxOperContrato(beanCont.getImporteMaximoOperacion());
		response.setDivisaMontoMaxContrato(beanCont.getDivisaMaximaOperacion());
		response.setIndEnvioCorreo(beanCont.getIndicadorEnvioCorreoCartaConfirmacionCapitales());
		response.setEstatusContrato(beanCont.getEstatusContrato());
		response.setDetalleEstatus(beanCont.getDetalleEstatus());
		response.setFechaEstadoContrato(beanCont.getFechaEstado());
		response.setIndContratoAgrupado(beanCont.getIndicadorContratoAgrupador());
		response.setIndTipoPosicion(beanCont.getIndicadorTipoPosicion());
		response.setIndContratoEspejo(beanCont.getIndicadorContratoEspejo());
		response.setIndCtoAmparadoChequera(beanCont.getIndicadorContratoChequera());
		response.setCodigoTipoServicio(beanCont.getCodigoTipoServicio());
		response.setCodigoAfi(beanCont.getCodigoAsesorAfinanciero());
		response.setIndBeneficiaros(beanCont.getIndicadorBeneficiarios());
		response.setIndProveedoresRecursos(beanCont.getIndicadorProveedoresRecursos());
		response.setIndAccionistas(beanCont.getIndicadorAccionistas());
		response.setFechaUltimaOperacion(beanCont.getFechaUltimaOperacion());
		response.setFechaUltimaModificacion(beanCont.getFechaUltimaModificacion());
		response.setDescripcionContrato(beanCont.getDescripcionContrato());
		response.setDescripcionFirma(beanCont.getDescripcionFirma());
		response.setDescripcionAreaReferencia(beanCont.getDescripcionAreaReferencia());
		response.setDescripcionRefExterna(beanCont.getDescripcionReferenciaExterna());
		response.setDescripcionSubestatus(beanCont.getDescripcionTextoLibreSubestatus());
		response.setEmpresaContratoAgrupador(beanCont.getEmpresaContratoAgrupador());
		response.setCentroContratoAgrupador(beanCont.getCentroContratoAgrupador());
		response.setNumeroConAgrupador(beanCont.getNumeroContratoAgrupador());
		response.setCodProdAgrupador(beanCont.getCodigoProductoAgrupador());
		response.setCodSubprdAgrupador(beanCont.getCodigoSubproductoAgrupador());
		response.setEmpresaContratoAgr(beanCont.getEmpresaContratoAgrupador());
		response.setAliasContratoAgrup(beanCont.getAliasContratoAgrupador());
		response.setEmpresaContratoEspejo(beanCont.getEmpresaContratoEspejo());
		response.setCentroContratoEspejo(beanCont.getCentroContratoEspejo());
		response.setNumeroContratoEspejo(beanCont.getNumeroContratoEspejo());
		response.setCodigoProdEspejo(beanCont.getCodigoProductoEspejo());
		response.setCodigoSubProdEspejo(beanCont.getCodigoSubProductoEspejo());
		response.setEmpresaContratoEsp(beanCont.getEmpresaContratoEspejo());
		response.setAliasContratoEspejo(beanCont.getAliasContratoEspej());
		response.setEmpresaContratoChequera(beanCont.getEmpresaContratoChequera());
		response.setCentroContratoChequera(beanCont.getCentroContratoChequera());
		response.setNumeroContratoChequera(beanCont.getNumeroContratoChequera());
		response.setCodigoProductoChequera(beanCont.getCodigoProductoChequera());
		response.setCodigoSubProductoChequera(beanCont.getCodigoSubProductoChequera());
		response.setNumeroIntervinientes(contResp.getNumeroIntervinientes());
		response.setNumeroContactos(contResp.getNumeroContacto());
		response.setNumeroUsosDomiclio(contResp.getNumeroUsosDomicilio());
		
		return response;
	}

}
