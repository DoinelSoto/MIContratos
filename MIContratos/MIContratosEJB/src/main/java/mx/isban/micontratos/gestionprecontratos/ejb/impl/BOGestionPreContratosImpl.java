package mx.isban.micontratos.gestionprecontratos.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanBajaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPreContratoRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPrecontratoResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanModificacionTablaAuxiliarRequest;
import mx.isban.micontratos.gestionprecontratos.dao.DAOGestionPreContratos;
import mx.isban.micontratos.gestionprecontratos.ejb.BOGestionPreContratos;
import mx.isban.micontratos.util.MIContratosConstantes;
import mx.isban.micontratos.util.MIContratosFuncionesGenericas;
import mx.isban.micontratos.validator.MIContratosException;
import mx.isban.micontratos.validator.MIContratosValidator;

/**
 * @author everis
 *
 */

@Stateless
@Remote(BOGestionPreContratos.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOGestionPreContratosImpl extends Architech implements BOGestionPreContratos{


	/**
	 * Serializacion
	 */
	private static final long serialVersionUID = 5374855289696253359L;
	
	@EJB
	private DAOGestionPreContratos daoGestionPreContratos;

	
	/**
	 * Método que ejecutará el consumo de la transacción MI10 : Alta en tabla auxiliar
	 */
	@Override
	public ResponseGenerico altaDatosAuxiliar(BeanAltaTablaAuxiliarRequest beanAltaTablaAuxiliarRequestPrueba,ArchitechSessionBean session) throws BusinessException {
	
		
		beanAltaTablaAuxiliarRequestPrueba.setPorcentajeIpab(MIContratosValidator.validarPunto(beanAltaTablaAuxiliarRequestPrueba.getPorcentajeIpab()));
		beanAltaTablaAuxiliarRequestPrueba.setPorcentajeParticipacion(MIContratosValidator.validarPunto(beanAltaTablaAuxiliarRequestPrueba.getPorcentajeParticipacion()));
		
		String[] camposObligatorios = {
				MIContratosValidator.validarEnteroString(beanAltaTablaAuxiliarRequestPrueba.getNumeroFolio()),									
				beanAltaTablaAuxiliarRequestPrueba.getCodigoTipoRegistroGuardado(),
						
		};
		int[] longitudesObligatorias = {9,2};
		
		String[] camposNoObligatorios = {
				beanAltaTablaAuxiliarRequestPrueba.getMulticanalidad().getEmpresaConexion(),
				beanAltaTablaAuxiliarRequestPrueba.getMulticanalidad().getCanalOperacion(),
				beanAltaTablaAuxiliarRequestPrueba.getMulticanalidad().getCanalComercializacion(),
				beanAltaTablaAuxiliarRequestPrueba.getIdentificadorTipoFirma(),
				beanAltaTablaAuxiliarRequestPrueba.getCodigoTipoPoder(),
				beanAltaTablaAuxiliarRequestPrueba.getFolioPoder(),
				beanAltaTablaAuxiliarRequestPrueba.getTipoContacto(),
				beanAltaTablaAuxiliarRequestPrueba.getPersonalidadJuridica(), 
				beanAltaTablaAuxiliarRequestPrueba.getNombreContacto(),
				beanAltaTablaAuxiliarRequestPrueba.getPrimerApellidoContacto(), 
				beanAltaTablaAuxiliarRequestPrueba.getSegundoApellidoContacto(),
				beanAltaTablaAuxiliarRequestPrueba.getFechaNacimiento(),
				beanAltaTablaAuxiliarRequestPrueba.getTipoDocumento(),				 		
				beanAltaTablaAuxiliarRequestPrueba.getNumeroDocumento(),			 		
				beanAltaTablaAuxiliarRequestPrueba.getNacionalidadContacto(),		 		
				beanAltaTablaAuxiliarRequestPrueba.getPaisNacimientoContacto(), 			
				beanAltaTablaAuxiliarRequestPrueba.getPaisContacto(), 						
				beanAltaTablaAuxiliarRequestPrueba.getNombreCalle(), 						
				beanAltaTablaAuxiliarRequestPrueba.getNumeroExteriorCalle(), 				
				beanAltaTablaAuxiliarRequestPrueba.getNumeroInteriorCalle(), 				
				beanAltaTablaAuxiliarRequestPrueba.getNumeroTelefonicoContacto(), 			
				beanAltaTablaAuxiliarRequestPrueba.getColonia(), 							
				beanAltaTablaAuxiliarRequestPrueba.getCodigoPostal(), 						
				beanAltaTablaAuxiliarRequestPrueba.getCiudadPoblacionLocalidad(), 			
				beanAltaTablaAuxiliarRequestPrueba.getCodigoProvincia(), 					
				beanAltaTablaAuxiliarRequestPrueba.getDelegacionMunicipio(), 				
				beanAltaTablaAuxiliarRequestPrueba.getEmailContacto(), 						
				beanAltaTablaAuxiliarRequestPrueba.getEmpresaContratoRelacionado(), 		
				beanAltaTablaAuxiliarRequestPrueba.getCentroContratoRelacionado(), 			
				beanAltaTablaAuxiliarRequestPrueba.getNumeroContratoRelacionado(), 			
				beanAltaTablaAuxiliarRequestPrueba.getCodigoProductoContratoRelacionado(), 	
				beanAltaTablaAuxiliarRequestPrueba.getCodigoSubproductoContratoRelacionado(),
				beanAltaTablaAuxiliarRequestPrueba.getDescripcionAsociadaReferencia(), 		
				beanAltaTablaAuxiliarRequestPrueba.getDescripcionAsociadaReferencia2(), 	
				beanAltaTablaAuxiliarRequestPrueba.getIndicadorValidacionFolio(), 
				beanAltaTablaAuxiliarRequestPrueba.getCalidadParticipacion(),
				beanAltaTablaAuxiliarRequestPrueba.getNumeroPersona(),
				beanAltaTablaAuxiliarRequestPrueba.getTipoDomicilio(),
				String.valueOf(beanAltaTablaAuxiliarRequestPrueba.getSecuenciaDomicilio()),
				beanAltaTablaAuxiliarRequestPrueba.getPorcentajeParticipacion(),
				beanAltaTablaAuxiliarRequestPrueba.getPorcentajeIpab()};
		int[] longitudesNoObligatorias = {4,2,2,2,2,30,2,1,40,20,20,10,2,20,3,3,3,50,15,15,10,30,8,7,2,5,50,4,4,12,2,4,50,50,1,2,8,3,3,5,5};
		
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			ResponseGenerico response=daoGestionPreContratos.altaDatosAuxiliar(beanAltaTablaAuxiliarRequestPrueba, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI11 : Alta de precontrato global
	 * @return 
	 */
	@Override
	public BeanAltaPreContratoGlobalResultado altaFolioPreContrato(BeanAltaPreContratoGlobalRequest beanAltaPreContratoGlobalRequestPrueba,ArchitechSessionBean session) throws BusinessException {
		
		beanAltaPreContratoGlobalRequestPrueba.setMontoMaximoOperarContrato(MIContratosValidator.validarPunto(beanAltaPreContratoGlobalRequestPrueba.getMontoMaximoOperarContrato()));
		
		
		String[] camposObligatorios = {
				beanAltaPreContratoGlobalRequestPrueba.getEmpresaContrato(),									
				beanAltaPreContratoGlobalRequestPrueba.getCodigoProductoContrato(),								
				beanAltaPreContratoGlobalRequestPrueba.getCodigoSubproductoContrato(),							
				beanAltaPreContratoGlobalRequestPrueba.getCodigoMonedaContrato(),								
				beanAltaPreContratoGlobalRequestPrueba.getCodigoCliente(),										
				beanAltaPreContratoGlobalRequestPrueba.getCodigoEjecutivoContrato(),							
				beanAltaPreContratoGlobalRequestPrueba.getCodigoBancaContratoInversion(),						
				beanAltaPreContratoGlobalRequestPrueba.getCodigoEmpresaContratoInversion(),						
				beanAltaPreContratoGlobalRequestPrueba.getCentroCostosContrato(),								
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorTipoCuenta(),								
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorTipoContrato(),								
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorAutorizacionPosicionCortoCapitales(),		
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorAutorizacionPosicionCortoFondos(),			
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorAutorizacionCruceFondosInversion(),			
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorDiscrecionalidad(),							
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorTipoCustodiaDirecto(),						
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorTipoCustodiaRepos(),							
				beanAltaPreContratoGlobalRequestPrueba.getMontoMaximoOperarContrato(), 							
				beanAltaPreContratoGlobalRequestPrueba.getDivisaMontoMaximoOperarContrato(),					
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorEnvioCorreoCartaConfirmacionCapitales(),		
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorContratoAgrupado(),	
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorContratoEspejo(),
				beanAltaPreContratoGlobalRequestPrueba.getIndicadorTipos(),										
				beanAltaPreContratoGlobalRequestPrueba.getCodigoTipoServicio(),									
				MIContratosValidator.validarEnteroString(beanAltaPreContratoGlobalRequestPrueba.getSecuenciaDomicilioPrincipal())};
		
		int[] longitudesObligatorias = {4,2,4,3,8,8,3,4,4,1,1,1,1,1,1,1,1,15,3,1,1,1,1,2,3};
		
		String[] camposNoObligatorios = {
				beanAltaPreContratoGlobalRequestPrueba.getMulticanalidad().getEmpresaConexion(),			
				beanAltaPreContratoGlobalRequestPrueba.getMulticanalidad().getCanalOperacion(),				
				beanAltaPreContratoGlobalRequestPrueba.getMulticanalidad().getCanalComercializacion(),		
				beanAltaPreContratoGlobalRequestPrueba.getCodigoAsesorFianciero(),								
				String.valueOf(beanAltaPreContratoGlobalRequestPrueba.getSecuenciaDomicilioEnvio()),						
				String.valueOf(beanAltaPreContratoGlobalRequestPrueba.getSecuenciaDomicilioFiscal()),						
				beanAltaPreContratoGlobalRequestPrueba.getDescripcionCont1(),								
				beanAltaPreContratoGlobalRequestPrueba.getDescripcionCont2(),								
				beanAltaPreContratoGlobalRequestPrueba.getDescripcionFirm1(),								
				beanAltaPreContratoGlobalRequestPrueba.getDescripcionFirm2(),								
				beanAltaPreContratoGlobalRequestPrueba.getDescripcionArre1(),								
				beanAltaPreContratoGlobalRequestPrueba.getDescripcionArre2(),								
				beanAltaPreContratoGlobalRequestPrueba.getDescripcionReex1(),								
				beanAltaPreContratoGlobalRequestPrueba.getDescripcionReex2(),
				beanAltaPreContratoGlobalRequestPrueba.getEmpresaContratoEspejo(),								
				beanAltaPreContratoGlobalRequestPrueba.getCentroContratoEspejo(),								
				beanAltaPreContratoGlobalRequestPrueba.getContratoEspejo(),										
				beanAltaPreContratoGlobalRequestPrueba.getProductoContratoEspejo(),								
				beanAltaPreContratoGlobalRequestPrueba.getSubProductoContratoEspejo(),
				beanAltaPreContratoGlobalRequestPrueba.getEmpresaContratoAgrupador(),							
				beanAltaPreContratoGlobalRequestPrueba.getCentroContratoAgrupador(),							
				beanAltaPreContratoGlobalRequestPrueba.getContratoAgrupador(),									
				beanAltaPreContratoGlobalRequestPrueba.getProductoContratoAgrupador(),							
				beanAltaPreContratoGlobalRequestPrueba.getSubproductoContratoAgrupador()};
		
		int[] longitudesNoObligatorias = {4,2,2,8,3,3,50,50,50,50,50,50,50,50,4,4,12,2,4,4,4,12,2,4};
		
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanAltaPreContratoGlobalResultado response=daoGestionPreContratos.altaFolioPreContrato(beanAltaPreContratoGlobalRequestPrueba, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI12 : Modificación de tabla auxiliar
	 * @return 
	 */
	@Override
	public ResponseGenerico modificacionDatosAuxiliar(BeanModificacionTablaAuxiliarRequest beanModificacionTablaAuxiliarRequest,ArchitechSessionBean session) throws BusinessException {
		
		beanModificacionTablaAuxiliarRequest.setPorcentajeIpab(MIContratosValidator.validarPunto(beanModificacionTablaAuxiliarRequest.getPorcentajeIpab()));		
		beanModificacionTablaAuxiliarRequest.setPorcentajeParticipacion(MIContratosValidator.validarPunto(beanModificacionTablaAuxiliarRequest.getPorcentajeParticipacion()));	
		
		String[] camposObligatorios = {
				MIContratosValidator.validarEnteroString(beanModificacionTablaAuxiliarRequest.getNumeroFolio()),
				beanModificacionTablaAuxiliarRequest.getCodigoTipoRegistroGuardado(),
				MIContratosValidator.validarEnteroString(beanModificacionTablaAuxiliarRequest.getSecuencialTipoRegistro()),};
		int[] longitudesObligatorias = {9,2,3};		
		
		String[] camposNoObligatorios = {
				beanModificacionTablaAuxiliarRequest.getMulticanalidad().getEmpresaConexion(),		
				beanModificacionTablaAuxiliarRequest.getMulticanalidad().getCanalOperacion(),		
				beanModificacionTablaAuxiliarRequest.getMulticanalidad().getCanalComercializacion(),
				beanModificacionTablaAuxiliarRequest.getCalidadParticipacion(),					
				beanModificacionTablaAuxiliarRequest.getNumeroPersona(),						
				beanModificacionTablaAuxiliarRequest.getTipoDomicilio(),						
				String.valueOf(beanModificacionTablaAuxiliarRequest.getSecuenciaDomicilio()),					
				beanModificacionTablaAuxiliarRequest.getIdentificadorTipoFirma(),				
				beanModificacionTablaAuxiliarRequest.getCodigoTipoPoder(),						
				beanModificacionTablaAuxiliarRequest.getFolioPoder(),							
				beanModificacionTablaAuxiliarRequest.getPorcentajeParticipacion(),				
				beanModificacionTablaAuxiliarRequest.getPorcentajeIpab(),						
				beanModificacionTablaAuxiliarRequest.getTipoContacto(),							
				beanModificacionTablaAuxiliarRequest.getPersonalidadJuridica(),					
				beanModificacionTablaAuxiliarRequest.getNombreContacto(),						
				beanModificacionTablaAuxiliarRequest.getPrimerApellidoContacto(),				
				beanModificacionTablaAuxiliarRequest.getSegundoApellidoContacto(),				
				beanModificacionTablaAuxiliarRequest.getFechaNacimiento(),						
				beanModificacionTablaAuxiliarRequest.getTipoDocumento(),						
				beanModificacionTablaAuxiliarRequest.getNumeroDocumento(),						
				beanModificacionTablaAuxiliarRequest.getNacionalidadContacto(),					
				beanModificacionTablaAuxiliarRequest.getPaisNacionalidad(),						
				beanModificacionTablaAuxiliarRequest.getPaisContacto(),							
				beanModificacionTablaAuxiliarRequest.getNombreCalle(),							
				beanModificacionTablaAuxiliarRequest.getNumeroExteriorCalle(),					
				beanModificacionTablaAuxiliarRequest.getNumeroInteriorCalle(),					
				beanModificacionTablaAuxiliarRequest.getNumeroTelefonicoContacto(),				
				beanModificacionTablaAuxiliarRequest.getColonia(),								
				beanModificacionTablaAuxiliarRequest.getCodigoPostal(),							
				beanModificacionTablaAuxiliarRequest.getLocalidad(),							
				beanModificacionTablaAuxiliarRequest.getCodigoProvincia(),						
				beanModificacionTablaAuxiliarRequest.getDelegacion(),							
				beanModificacionTablaAuxiliarRequest.getEmailContacto(),						
				beanModificacionTablaAuxiliarRequest.getEmpresaContratoRelacionado(),			
				beanModificacionTablaAuxiliarRequest.getCentroContratoRelacionado(),			
				beanModificacionTablaAuxiliarRequest.getNumeroContratoRelacionado(),			
				beanModificacionTablaAuxiliarRequest.getCodigoProductoContratoRelacionado(),	
				beanModificacionTablaAuxiliarRequest.getCodigoSubproductoContratoRelacionado(),	
				beanModificacionTablaAuxiliarRequest.getDescripcionAsociadaReferencia(),		
				beanModificacionTablaAuxiliarRequest.getDescripcionAsociadaReferencia2(),		
		};
		
		int[] longitudesNoObligatorias = {4,2,2,2,8,3,3,2,2,30,5,5,2,1,40,20,20,10,2,20,3,3,3,50,15,15,10,30,8,7,2,5,50,4,4,12,2,4,50,50};

		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			ResponseGenerico response=daoGestionPreContratos.modificacionDatosAuxiliar(beanModificacionTablaAuxiliarRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI13 : Baja de tabla auxiliar
	 * @return 
	 */
	@Override
	public ResponseGenerico bajaDatosAuxiliar(BeanBajaTablaAuxiliarRequest beanBajaTablaAuxiliarRequest,ArchitechSessionBean session) throws BusinessException {
		String[] camposObligatorios={MIContratosValidator.validarEnteroString(beanBajaTablaAuxiliarRequest.getNumeroFolio())};
		int[] longitudesObligatorias = { 9};
		String[] camposNoObligatorios = {
				String.valueOf(beanBajaTablaAuxiliarRequest.getMulticanalidad()
						.getEmpresaConexion()),
				beanBajaTablaAuxiliarRequest.getMulticanalidad()
						.getCanalComercializacion(),
				beanBajaTablaAuxiliarRequest.getMulticanalidad()
						.getCanalOperacion(),
				beanBajaTablaAuxiliarRequest.getSecuencialTipoRegistro(),
				beanBajaTablaAuxiliarRequest.getCodigoTipoRegistroGuardado() };
		int[] longitudesNoObligatorias={4,2,2, 3, 2 };
		try {
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			ResponseGenerico response=daoGestionPreContratos.bajaDatosAuxiliar(beanBajaTablaAuxiliarRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

	/**
	 * Método que ejecutará el consumo de la transacción MI14 : consulta lista precontrato
	 */
	@Override
	public BeanListaPrecontratoResultado consultaPreContratos(BeanListaPreContratoRequest beanListaPreContratoRequest,ArchitechSessionBean session) throws BusinessException {		
		String[] camposNoObligatorios = {
				beanListaPreContratoRequest.getMulticanalidad().getEmpresaConexion(),
				beanListaPreContratoRequest.getMulticanalidad().getCanalOperacion(),
				beanListaPreContratoRequest.getMulticanalidad().getCanalComercializacion(),
				MIContratosValidator.validarEnteroString(beanListaPreContratoRequest.getNumeroFolio()),
				beanListaPreContratoRequest.getCodigoEjecutivo(),
				beanListaPreContratoRequest.getNumeroCliente(),
				beanListaPreContratoRequest.getFechaAperturaFolio(),
				beanListaPreContratoRequest.getRellamada().getMasDatos(),
				beanListaPreContratoRequest.getRellamada().getMemento() };
		int[] longitudesNoObligatorias = {4,2,2,9,8,8,10,1,9};
		try {
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanListaPrecontratoResultado response=daoGestionPreContratos.consultaPreContratos(beanListaPreContratoRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}
	
	/**
	 * Método que ejecutará el consumo de la transacción MI15 : Consulta de tabla auxiliar
	 */
	@Override
	public BeanConsultaTablaAuxiliarResultado detallePreContratos(BeanConsultaTablaAuxiliarRequest beanConsultaTablaAuxiliarRequest,ArchitechSessionBean session) throws BusinessException {
		
		String [] camposObligatorios = {
				MIContratosValidator.validarEnteroString(beanConsultaTablaAuxiliarRequest.getNumeroFolio())};
		
		int[] longitudesObligatorias = {9};
		
		String[] camposNoObligatorios = {
				beanConsultaTablaAuxiliarRequest.getMulticanalidad().getEmpresaConexion(),	
				beanConsultaTablaAuxiliarRequest.getMulticanalidad().getCanalOperacion(),
				beanConsultaTablaAuxiliarRequest.getCodigoTipoRegistro(),							
				beanConsultaTablaAuxiliarRequest.getRellamada().getMasDatos(),		
				beanConsultaTablaAuxiliarRequest.getRellamada().getMemento(),
				beanConsultaTablaAuxiliarRequest.getMulticanalidad().getCanalComercializacion()};
		int[] longitudesNoObligatorias = {4,2,2,1,14,2};
		try {
			MIContratosValidator.getSingletonInstance().validarCamposObligatorios(camposObligatorios, longitudesObligatorias);
			MIContratosValidator.getSingletonInstance().validarCampos(camposNoObligatorios, longitudesNoObligatorias);
			BeanConsultaTablaAuxiliarResultado response=daoGestionPreContratos.detallePreContratos(beanConsultaTablaAuxiliarRequest, session);
			response.setCodError(MIContratosFuncionesGenericas.getSingletonInstance().validarCodError(response.getCodError(), response.getMsgError())); 
			return response; 
		} catch (ExceptionDataAccess e) {
			this.error(MIContratosConstantes.MENSAJE_ERROR_EJECUCION + MIContratosConstantes.MENSAJE_EJECUCION);
			this.error("",e); 
			throw new BusinessException(e.getCode(),e.getMessage()); 
		} catch (MIContratosException e){
			showException(e);
			error("", e);
			throw new BusinessException(e.getFaultInfo().getFaultCode(),e.getFaultInfo().getFaultString());
		}
	}

}
