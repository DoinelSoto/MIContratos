package mx.isban.micontratos.relacionclientecontrato.dao;

import java.util.List;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionResponse;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanCambioTitularRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionContacto;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionInterviniente;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionUsosDomicilio;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosRequest;
import mx.isban.miperfilamiento.niveldeacceso.beans.BeanValidacionNivelAccesoResponse;
import mx.isban.mipersonas.contratos.beans.ConsultaContratosClienteResponseBean;
import mx.isban.mipersonas.contratos.beans.ConsultaContratosClienteResponseRegBean;

/**
 * @author everis
 * Interface DAORelacionClienteContrato
 *
 */
public interface DAORelacionClienteContrato {
	
	/**
	 * Método que define la función que ejecutará el consumo de la transacción MI30 : Cambio de titular de contrato
	 * @param beanCambioTitularRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws ExceptionDataAccess 
	 */
	ResponseGenerico modificarTitularContrato(BeanCambioTitularRequest beanCambioTitularRequest,ArchitechSessionBean session) throws ExceptionDataAccess;

	/**
	 * Método que define la función que ejecutará el consumo de la transacción OD39
	 * @param beanLocalizadorContratosRequest
	 * @param sesion
	 * @return ConsultaContratosClienteResponseBean
	 * @throws BusinessException 
	 */
	ConsultaContratosClienteResponseBean localizadorContratos(BeanLocalizadorContratosRequest beanLocalizadorContratosRequest,ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Método que define la función que el EJB de VALIDACIONNIVELACESO DE PERFILAMIENTO
	 * @param ejecutivo
	 * @param tipoConsulta
	 * @param list
	 * @param session
	 * @return BeanValidacionNivelAccesoResponse
	 * @throws BusinessException 
	 */
	BeanValidacionNivelAccesoResponse validacioNivelAcceso(String ejecutivo, String tipoConsulta, List<ConsultaContratosClienteResponseRegBean> list, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Método que define la función que el EJB de PERSONAS -> INTERVINIENTES
	 * @param contrato
	 * @param session
	 * @return List<BeanDetalleContratoInversionInterviniente>
	 * @throws BusinessException 
	 */
	List<BeanDetalleContratoInversionInterviniente> consultaDetIntervinientes(BeanConsultaInversionResponse contrato, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Método que define la función que el EJB de PERSONAS -> CONTACTOS
	 * @param beanConsultaInversionResponse
	 * @param string 
	 * @param session
	 * @return List<BeanDetalleContratoInversionContacto>
	 * @throws BusinessException 
	 */
	List<BeanDetalleContratoInversionContacto> consultaDetContactos(BeanConsultaInversionResponse beanConsultaInversionResponse, String string, ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Método que define la función que el EJB de PERSONAS -> USOS DOMICILIO
	 * @param contrato
	 * @param session
	 * @return List<BeanDetalleContratoInversionUsosDomicilio>
	 * @throws BusinessException 
	 */
	List<BeanDetalleContratoInversionUsosDomicilio> listadoDomicilios(BeanConsultaInversionResponse contrato, ArchitechSessionBean session) throws BusinessException;
}
