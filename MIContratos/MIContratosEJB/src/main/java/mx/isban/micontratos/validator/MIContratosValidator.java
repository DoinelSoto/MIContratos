package mx.isban.micontratos.validator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.micontratos.util.MIContratosConstantes;

/**************************************************************
 * Querétaro, Qro Enero 2017
 * 
 * La redistribución y el uso en formas fuente y binario, son responsabilidad
 * del propietario.
 * 
 * Este software fue elaborado en @Everis por Jemima Del Ángel San Martín y
 * Daniel Hernández Soto
 * 
 * Para mas información, consulte <www.everis.com/mexico>
 ***************************************************************/
public final class MIContratosValidator extends Architech {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8261849670331082399L;
	/**
	 * Objeto singleton de la clase
	 */
	private static MIContratosValidator validator;


	/**
	 * getSingletonInstance
	 * 
	 * @return validator Validador
	 */
	public static MIContratosValidator getSingletonInstance() {
		if (validator == null) {
			validator = new MIContratosValidator();

		}
		return validator;
	}

	/**
	 * Valida campos obligatorios
	 * 
	 * @param camposObligatorios
	 *            Campos a validar
	 * @param longitudes
	 *            Longitudes de campos a validar
	 * @throws MIPerfilamientoException
	 *             MIPerfilamientoException
	 */
	public void validarCamposObligatorios(String[] camposObligatorios,
			int[] longitudes) throws MIContratosException {
		for (int i = 0; i < camposObligatorios.length; i++) {
			if (camposObligatorios[i] != null
					&& !camposObligatorios[i].isEmpty()) {
				validarCampo(camposObligatorios[i], longitudes[i]);

			} else {
				throw new MIContratosException(
						getProperties().getProperty(
								MIContratosConstantes.MIC0002_FAULT_MESSAGE),
						new MIContratosFault(
								getProperties()
										.getProperty(
												MIContratosConstantes.MIC0002_FAULT_CODE),
								getProperties()
										.getProperty(
												MIContratosConstantes.MIC0002_FAULT_MESSAGE)));
			}
		}
	}

	/**
	 * 
	 * @return Properties
	 */
	public Properties getProperties() {
		Properties prop = new Properties();
		try {
			info("Ruta Properties: " + this.getConfigDeCmpAplicacion(MIContratosConstantes.RUTA_PROPERTIES_ERRORES));
			InputStream inputStream = new FileInputStream(this.getConfigDeCmpAplicacion(MIContratosConstantes.RUTA_PROPERTIES_ERRORES));
			prop.load(inputStream);
		} catch (IOException e1) {
			error("", e1);
		}
		return prop;
	}

	/**
	 * Valida los campos no obligatorios
	 * 
	 * @param campos
	 *            Campos a validar
	 * @param longitudes
	 *            Longitudes de campos a validar
	 * @throws MIPerfilamientoException
	 *             MIPerfilamientoException
	 */
	public void validarCampos(String[] campos, int[] longitudes)
			throws MIContratosException {
		for (int i = 0; i < campos.length; i++) {
			if (campos[i] != null) {
				validarCampo(campos[i], longitudes[i]);
			}
		}
	}

	/**
	 * validarCampo
	 * 
	 * @param campo
	 *            Campo a validar
	 * @param longitud
	 *            Longitud de campo a validar
	 * @throws MIPerfilamientoException
	 *             MIPerfilamientoException
	 */

	public void validarCampo(String campo, int longitud)
			throws MIContratosException {
		if (campo.length() > longitud) {
			throw new MIContratosException(
					getProperties().getProperty(
							MIContratosConstantes.MIC0001_FAULT_MESSAGE),
					new MIContratosFault(
							getProperties().getProperty(
									MIContratosConstantes.MIC0001_FAULT_CODE),
							getProperties()
									.getProperty(
											MIContratosConstantes.MIC0001_FAULT_MESSAGE)));
		}
	}

	/**
	 * Se deberá recibir una cantidad enteramente númerica y a partir de ahí
	 * adicionarle el punto antes de las dos ultimas posiciones
	 * 
	 * @param campo
	 * @return String
	 */
	public static String validarCamposFlotante(String campo) {
		StringBuilder cadena2 = new StringBuilder(campo);
		if (!campo.isEmpty() && campo.length() >= 2) {
			cadena2.insert(campo.length() - 2, ".");
		}
		return cadena2.toString();
	} // finalización de validación para devolver el punto

	/**
	 * Se validará una entrada con punto decimal y se
	 * devolverá la entrada pero sin el punto
	 * @param campo
	 * @return String
	 */

	public static String validarPunto(String campo) {

		StringBuilder valor = new StringBuilder();
		String porcentaje = "";

		for (int i = 0; i < campo.length(); i++) {
			if (campo.charAt(i) != '.') {

				valor.append(campo.charAt(i));
			}// fin de if
			porcentaje = valor.toString();
		}// fin de for
		return porcentaje;
	}// validarPunto

	/**
	 * Método para validar enteros para beans de salida
	 * @param campo
	 * @return int
	 */
	public static int validarEnteros(String campo) {
		if(campo == null || campo.isEmpty() || "".equals(campo.trim())){
			return 0;
		}
		return Integer.valueOf(campo);
	}

	/**
	 * Métedo para validar enteros en string
	 * @param i
	 * @return String
	 */
	public static String validarEnteroString(int i) {
		if(i == 0){
			return "";
		}
		return String.valueOf(i);
	}

	/**
	 * Separa la salida
	 * @param tramaSalida
	 * @param size
	 * @return cadena
	 */
	public String[] separarSalida(String trama, int size) {
		String tramaSalida = trama;
		String[] cadena = new String[tramaSalida.length() / size];
		for (int i = 0; tramaSalida.length() > size; i++) {
			cadena[i] = StringUtils.substring(tramaSalida, 0, size);
			tramaSalida = tramaSalida.substring(size);
		}
		return cadena;
	}
}
