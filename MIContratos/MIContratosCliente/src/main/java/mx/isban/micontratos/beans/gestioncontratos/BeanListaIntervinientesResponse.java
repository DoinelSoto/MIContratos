/**
 * 
 */
package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

/**
 * @author everis
 * Bean con los campos pertenecientes a intervinientes devueltos por la TRX MI16
 */
public class BeanListaIntervinientesResponse implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 1534120385673703L;
	
	/**
	 * Calidad de participación del interviniente
	 */
	private String calidadParticipacionInterviniente;
	/**
	 * Orden de participación del interviniente	
	 */
	private int ordenParticipacionInterviniente;
	/**
	 * Número de personas 1	
	 */
	private String numeroPersona;
	/**
	 * @return the calidadParticipacionInterviniente
	 */
	public String getCalidadParticipacionInterviniente() {
		return calidadParticipacionInterviniente;
	}
	/**
	 * @param calidadParticipacionInterviniente the calidadParticipacionInterviniente to set
	 */
	public void setCalidadParticipacionInterviniente(
			String calidadParticipacionInterviniente) {
		this.calidadParticipacionInterviniente = calidadParticipacionInterviniente;
	}
	/**
	 * @return the ordenParticipacionInterviniente
	 */
	public int getOrdenParticipacionInterviniente() {
		return ordenParticipacionInterviniente;
	}
	/**
	 * @param ordenParticipacionInterviniente the ordenParticipacionInterviniente to set
	 */
	public void setOrdenParticipacionInterviniente(
			int ordenParticipacionInterviniente) {
		this.ordenParticipacionInterviniente = ordenParticipacionInterviniente;
	}
	/**
	 * @return the numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	/**
	 * @param numeroPersona the numeroPersona to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}

}
