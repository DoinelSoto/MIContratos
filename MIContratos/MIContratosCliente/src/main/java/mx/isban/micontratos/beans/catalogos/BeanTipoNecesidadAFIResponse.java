package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

/**
 * 
 * @author Luis Martínez - everis
 *	Bean de los campos de salida del servicio: Consulta de tipo de servicio de un cliente
 */
public class BeanTipoNecesidadAFIResponse  implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7898793320816915187L;
	
	/**
	 * Código de tipo de servicio 
	 */
	private String codigoTipoServicio;
	
	/**
	 * Descripción del tipo de servicio
	 */
	private String descTipoServicio;
	
	/**
	 * Indicador de asesoramiento
	 */
	private String indAsesoramiento;
	
	/**
	 * Indicador de Necesidad AFI
	 */
	private String indicadorNecesidadAfi;
	
	/**
	 * 
	 * @return the codigoTipoServicio
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}
	
	/**
	 * 
	 * @param codigoTipoServicio
	 */
	public void setCodigoTipoServicio(String codigoTipoServicio) {
		this.codigoTipoServicio = codigoTipoServicio;
	}
	
	/**
	 * 
	 * @return the descTipoServicio
	 */
	public String getDescTipoServicio() {
		return descTipoServicio;
	}
	
	/**
	 * 
	 * @param descTipoServicio
	 */
	public void setDescTipoServicio(String descTipoServicio) {
		this.descTipoServicio = descTipoServicio;
	}
	
	/**
	 * 
	 * @return the indAsesoramiento
	 */
	public String getIndAsesoramiento() {
		return indAsesoramiento;
	}
	
	/**
	 * 
	 * @param indAsesoramiento
	 */
	public void setIndAsesoramiento(String indAsesoramiento) {
		this.indAsesoramiento = indAsesoramiento;
	}
	
	/**
	 * 
	 * @return the indicadorNecesidadAfi
	 */
	public String getIndicadorNecesidadAfi() {
		return indicadorNecesidadAfi;
	}
	
	/**
	 * 
	 * @param indicadorNecesidadAfi
	 */
	public void setIndicadorNecesidadAfi(String indicadorNecesidadAfi) {
		this.indicadorNecesidadAfi = indicadorNecesidadAfi;
	}	
}