package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida e implementación de la interfaz BeanResultBO para la transacción : MI14
 *
 */
public class BeanListaPrecontratoResultado extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 3437789501302798543L;
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanListaPrecontratoResponse> listaPrecontrato;

	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Class BeanListaPrecontratoResultado
	 */
	public BeanListaPrecontratoResultado(){
		
		rellamada = new BeanPaginacion();
	}
	
	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

	/**
	 * @return the consulta
	 */
	public List<BeanListaPrecontratoResponse> getListaPrecontrato() {
		return new ArrayList<>(listaPrecontrato);
	}

	/**
	 * @param listaPrecontrato the consulta to set
	 */
	public void setListaPrecontrato(List<BeanListaPrecontratoResponse> listaPrecontrato) {
		this.listaPrecontrato = new ArrayList<>(listaPrecontrato);
	}
	

	
}
