package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI16 : Consulta de datos del contrato de inversión
 *
 */
public class BeanConsultaInversionRequest implements Serializable{

	/**
	 * serialización
	 */
	private static final long serialVersionUID = 6759179217090473222L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	
	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias	
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto	
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del contrato de Inversión	
	 */
	@Size(max=4)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato	
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Indicador de información de intervinientes	
	 */
	@Size(max=1)
	private String indicadorInformacionIntervinientes;
	/**
	 * Indicador de información de contactos	
	 */
	@Size(max=1)
	private String indicadorInformacionContactos;
	/**
	 * Indicador de información de usos de domicilios	
	 */
	@Size(max=1)
	private String indicadorUsoDomicilios;
	/**
	 * Indicador de información de contratos relacionados	
	 */
	@Size(max=1)
	private String indicadorInformacionContratosRelacionados;
	/**
	 * Indicador de información de referencias del contrato	
	 */
	@Size(max=1)
	private String indicadorInformacionReferenciaContrato;
	/**
	 * Fecha de la consulta	
	 */
	@Size(max=10)
	private String fechaConsulta;

	/**
	 * Constructor para la rellamada generica
	 */
	public BeanConsultaInversionRequest(){
		multicanalidad=new MulticanalidadGenerica();	
	}
	
	
	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipConsu the tipConsu to set
	 */
	public void setTipoConsulta(String tipConsu) {
		this.tipoConsulta = tipConsu;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idsProd
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idsProd to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}
	/**
	 * @param empresaContratoInversion the idEmprCo to set
	 */
	public void setEmpresaContratoInversion(String empresaContratoInversion) {
		this.empresaContratoInversion = empresaContratoInversion;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param aliasContrato the desAlias to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}
	/**
	 * @return the indInInt
	 */
	public String getIndicadorInformacionIntervinientes() {
		return indicadorInformacionIntervinientes;
	}
	/**
	 * @param indicadorInformacionIntervinientes the indInInt to set
	 */
	public void setIndicadorInformacionIntervinientes(String indicadorInformacionIntervinientes) {
		this.indicadorInformacionIntervinientes = indicadorInformacionIntervinientes;
	}
	/**
	 * @return the indInCon
	 */
	public String getIndicadorInformacionContactos() {
		return indicadorInformacionContactos;
	}
	/**
	 * @param indicadorInformacionContactos the indInCon to set
	 */
	public void setIndicadorInformacionContactos(String indicadorInformacionContactos) {
		this.indicadorInformacionContactos = indicadorInformacionContactos;
	}
	/**
	 * @return the indInUso
	 */
	public String getIndicadorUsoDomicilios() {
		return indicadorUsoDomicilios;
	}
	/**
	 * @param indicadorUsoDomicilios the indInUso to set
	 */
	public void setIndicadorUsoDomicilios(String indicadorUsoDomicilios) {
		this.indicadorUsoDomicilios = indicadorUsoDomicilios;
	}
	/**
	 * @return the indInRel
	 */
	public String getIndicadorInformacionContratosRelacionados() {
		return indicadorInformacionContratosRelacionados;
	}
	/**
	 * @param indicadorInformacionContratosRelacionados the indInRel to set
	 */
	public void setIndicadorInformacionContratosRelacionados(String indicadorInformacionContratosRelacionados) {
		this.indicadorInformacionContratosRelacionados = indicadorInformacionContratosRelacionados;
	}
	/**
	 * @return the indInRef
	 */
	public String getIndicadorInformacionReferenciaContrato() {
		return indicadorInformacionReferenciaContrato;
	}
	/**
	 * @param indicadorInformacionReferenciaContrato the indInRef to set
	 */
	public void setIndicadorInformacionReferenciaContrato(String indicadorInformacionReferenciaContrato) {
		this.indicadorInformacionReferenciaContrato = indicadorInformacionReferenciaContrato;
	}
	/**
	 * @return the fecConsu
	 */
	public String getFechaConsulta() {
		return fechaConsulta;
	}
	/**
	 * @param fechaConsulta the fecConsu to set
	 */
	public void setFechaConsulta(String fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
}
