package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI15 : Consulta de tabla auxiliar
 *
 */
public class BeanConsultaTablaAuxiliarResponse implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -4223655485964970995L;
	

	/**
	 * Número de folio	
	 */
	private int numeroFolio;
	/**
	 * Código de tipo de registro guardado	
	 */
	private String codigoTipoRegistro;
	/**
	 * Secuencial por tipo de registro	
	 */
	private int numeroSecuencial;
	/**
	 * Calidad de participación	
	 */
	private String calidadParticipacion;
	/**
	 * Orden de participación
	 */
	private String ordenParticipacion;
	/**
	 * Número de persona	
	 */
	private String numeroPersona;
	/**
	 * Tipo de domicilio	
	 */
	private String tipoDomicilio;
	/**
	 * Secuencia de domicilio
	 */
	private int secuenciaDomicilio;
	/**
	 * Identificador del tipo de firma	
	 */
	private String identificadorTipoFirma;
	/**
	 * Código de tipo de poder
	 */
	private String codigoTipoPoder;
	/**
	 * 	Folio del poder	
	 */
	private String folioPoder;
	/**
	 * Porcentaje de participación
	 */
	private String porcentajeParticipacion;
	/**
	 * Porcentaje de IPAB	
	 */
	private String porcentajeIpab;
	/**
	 * Tipo de contacto	
	 */
	private String tipoContacto;
	/**
	 * Personalidad jurídica	
	 */
	private String tipoPersonalidadJuridica;
	/**
	 * Nombre del contacto	
	 */
	private String nombreContacto;
	/**
	 * Primer apellido del contacto	
	 */
	private String primerApellidoContacto;
	/**
	 * Segundo apellido del contacto	
	 */
	private String segundoApellidoContacto;
	/**
	 * Fecha de nacimiento	
	 */
	private String fechaNacimiento;
	/**
	 * Tipo de documento	
	 */
	private String tipoDocumento;
	/**
	 * Número de documento	
	 */
	private String numeroDocumento;
	/**
	 * Nacionalidad del contacto
	 */
	private String nacionalidadContacto;
	/**
	 * País de nacimiento del contacto
	 */
	private String paisNacimientoContacto;
	/**
	 * País del contacto	
	 */
	private String paisContacto;
	/**
	 * Nombre de calle
	 */
	private String nombreCalle;
	/**
	 * Número exterior de la calle
	 */
	private String numeroExterior;
	/**
	 * Número interior de la calle	
	 */
	private String numeroInteriorCalle;
	/**
	 * Número telefónico del contacto	
	 */
	private String numeroTelefonicoContacto;
	/**
	 * Colonia
	 */
	private String nombreAsentamiento;
	/**
	 * 	Código postal	
	 */
	private String codigoPostal;
	/**
	 * Ciudad / Población / Localidad
	 */
	private String nombreLocalidad;
	/**
	 * Código provincia	
	 */
	private String nombreProvincia;
	/**
	 * Delegación o municipio
	 */
	private String nombreMunicipio;
	/**
	 * Email del contacto
	 */
	private String emailContacto;
	/**
	 * Empresa del contrato relacionado	
	 */
	private String empresaContratoRelacionado;
	/**
	 * Centro del contrato relacionado
	 */
	private String centroContratoRelacionado;
	/**
	 * Número de contrato relacionado
	 */
	private String numeroContratoRelacionado;
	/**
	 * Código de producto del contrato relacionado	
	 */
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato relacionado	
	 */
	private String codigoSubProducto;
	/**
	 * Descripción asociada a la referencia	
	 */
	private String descripcion;
	/**
	 * Descripción asociada a la referencia	
	 */
	private String descripcion2;
	/**
	 * @return the numeroFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}
	/**
	 * @param numeroFolio the numeroFolio to set
	 */
	public void setNumeroFolio(int numeroFolio) {
		this.numeroFolio = numeroFolio;
	}
	/**
	 * @return the codigoTipoRegistro
	 */
	public String getCodigoTipoRegistro() {
		return codigoTipoRegistro;
	}
	/**
	 * @param codigoTipoRegistro the codigoTipoRegistro to set
	 */
	public void setCodigoTipoRegistro(String codigoTipoRegistro) {
		this.codigoTipoRegistro = codigoTipoRegistro;
	}
	/**
	 * @return the numeroSecuencial
	 */
	public int getNumeroSecuencial() {
		return numeroSecuencial;
	}
	/**
	 * @param numeroSecuencial the numeroSecuencial to set
	 */
	public void setNumeroSecuencial(int numeroSecuencial) {
		this.numeroSecuencial = numeroSecuencial;
	}
	/**
	 * @return the calidadParticipacion
	 */
	public String getCalidadParticipacion() {
		return calidadParticipacion;
	}
	/**
	 * @param calidadParticipacion the calidadParticipacion to set
	 */
	public void setCalidadParticipacion(String calidadParticipacion) {
		this.calidadParticipacion = calidadParticipacion;
	}
	/**
	 * @return the ordenParticipacion
	 */
	public String getOrdenParticipacion() {
		return ordenParticipacion;
	}
	/**
	 * @param ordenParticipacion the ordenParticipacion to set
	 */
	public void setOrdenParticipacion(String ordenParticipacion) {
		this.ordenParticipacion = ordenParticipacion;
	}
	/**
	 * @return the numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	/**
	 * @param numeroPersona the numeroPersona to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	/**
	 * @return the tipoDomicilio
	 */
	public String getTipoDomicilio() {
		return tipoDomicilio;
	}
	/**
	 * @param tipoDomicilio the tipoDomicilio to set
	 */
	public void setTipoDomicilio(String tipoDomicilio) {
		this.tipoDomicilio = tipoDomicilio;
	}
	/**
	 * @return the secuenciaDomicilio
	 */
	public int getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}
	/**
	 * @param secuenciaDomicilio the secuenciaDomicilio to set
	 */
	public void setSecuenciaDomicilio(int secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}
	/**
	 * @return the identificadorTipoFirma
	 */
	public String getIdentificadorTipoFirma() {
		return identificadorTipoFirma;
	}
	/**
	 * @param identificadorTipoFirma the identificadorTipoFirma to set
	 */
	public void setIdentificadorTipoFirma(String identificadorTipoFirma) {
		this.identificadorTipoFirma = identificadorTipoFirma;
	}
	/**
	 * @return the codigoTipoPoder
	 */
	public String getCodigoTipoPoder() {
		return codigoTipoPoder;
	}
	/**
	 * @param codigoTipoPoder the codigoTipoPoder to set
	 */
	public void setCodigoTipoPoder(String codigoTipoPoder) {
		this.codigoTipoPoder = codigoTipoPoder;
	}
	/**
	 * @return the folioPoder
	 */
	public String getFolioPoder() {
		return folioPoder;
	}
	/**
	 * @param folioPoder the folioPoder to set
	 */
	public void setFolioPoder(String folioPoder) {
		this.folioPoder = folioPoder;
	}
	/**
	 * @return the porcentajeParticipacion
	 */
	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	/**
	 * @param porcentajeParticipacion the porcentajeParticipacion to set
	 */
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	/**
	 * @return the porcentajeIpab
	 */
	public String getPorcentajeIpab() {
		return porcentajeIpab;
	}
	/**
	 * @param porcentajeIpab the porcentajeIpab to set
	 */
	public void setPorcentajeIpab(String porcentajeIpab) {
		this.porcentajeIpab = porcentajeIpab;
	}
	/**
	 * @return the tipoContacto
	 */
	public String getTipoContacto() {
		return tipoContacto;
	}
	/**
	 * @param tipoContacto the tipoContacto to set
	 */
	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}
	/**
	 * @return the tipoPersonalidadJuridica
	 */
	public String getTipoPersonalidadJuridica() {
		return tipoPersonalidadJuridica;
	}
	/**
	 * @param tipoPersonalidadJuridica the tipoPersonalidadJuridica to set
	 */
	public void setTipoPersonalidadJuridica(String tipoPersonalidadJuridica) {
		this.tipoPersonalidadJuridica = tipoPersonalidadJuridica;
	}
	/**
	 * @return the nombreContacto
	 */
	public String getNombreContacto() {
		return nombreContacto;
	}
	/**
	 * @param nombreContacto the nombreContacto to set
	 */
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	/**
	 * @return the primerApellidoContacto
	 */
	public String getPrimerApellidoContacto() {
		return primerApellidoContacto;
	}
	/**
	 * @param primerApellidoContacto the primerApellidoContacto to set
	 */
	public void setPrimerApellidoContacto(String primerApellidoContacto) {
		this.primerApellidoContacto = primerApellidoContacto;
	}
	/**
	 * @return the segundoApellidoContacto
	 */
	public String getSegundoApellidoContacto() {
		return segundoApellidoContacto;
	}
	/**
	 * @param segundoApellidoContacto the segundoApellidoContacto to set
	 */
	public void setSegundoApellidoContacto(String segundoApellidoContacto) {
		this.segundoApellidoContacto = segundoApellidoContacto;
	}
	/**
	 * @return the fechaNacimiento
	 */
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/**
	 * @return the nacionalidadContacto
	 */
	public String getNacionalidadContacto() {
		return nacionalidadContacto;
	}
	/**
	 * @param nacionalidadContacto the nacionalidadContacto to set
	 */
	public void setNacionalidadContacto(String nacionalidadContacto) {
		this.nacionalidadContacto = nacionalidadContacto;
	}
	/**
	 * @return the paisNacimientoContacto
	 */
	public String getPaisNacimientoContacto() {
		return paisNacimientoContacto;
	}
	/**
	 * @param paisNacimientoContacto the paisNacimientoContacto to set
	 */
	public void setPaisNacimientoContacto(String paisNacimientoContacto) {
		this.paisNacimientoContacto = paisNacimientoContacto;
	}
	/**
	 * @return the codigoPaisContacto
	 */
	public String getPaisContacto() {
		return paisContacto;
	}
	/**
	 * @param codigoPaisContacto the codigoPaisContacto to set
	 */
	public void setPaisContacto(String paisContacto) {
		this.paisContacto = paisContacto;
	}
	/**
	 * @return the nombreCalle
	 */
	public String getNombreCalle() {
		return nombreCalle;
	}
	/**
	 * @param nombreCalle the nombreCalle to set
	 */
	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}
	/**
	 * @return the numeroExterior
	 */
	public String getNumeroExterior() {
		return numeroExterior;
	}
	/**
	 * @param numeroExterior the numeroExterior to set
	 */
	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}
	/**
	 * @return the numeroInteriorCalle
	 */
	public String getNumeroInteriorCalle() {
		return numeroInteriorCalle;
	}
	/**
	 * @param numeroInteriorCalle the numeroInteriorCalle to set
	 */
	public void setNumeroInteriorCalle(String numeroInteriorCalle) {
		this.numeroInteriorCalle = numeroInteriorCalle;
	}
	/**
	 * @return the numeroTelefonicoContacto
	 */
	public String getNumeroTelefonicoContacto() {
		return numeroTelefonicoContacto;
	}
	/**
	 * @param numeroTelefonicoContacto the numeroTelefonicoContacto to set
	 */
	public void setNumeroTelefonicoContacto(String numeroTelefonicoContacto) {
		this.numeroTelefonicoContacto = numeroTelefonicoContacto;
	}
	/**
	 * @return the nombreAsentamiento
	 */
	public String getNombreAsentamiento() {
		return nombreAsentamiento;
	}
	/**
	 * @param nombreAsentamiento the nombreAsentamiento to set
	 */
	public void setNombreAsentamiento(String nombreAsentamiento) {
		this.nombreAsentamiento = nombreAsentamiento;
	}
	/**
	 * @return the codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}
	/**
	 * @param codigoPostal the codigoPostal to set
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	/**
	 * @return the nombreLocalidad
	 */
	public String getNombreLocalidad() {
		return nombreLocalidad;
	}
	/**
	 * @param nombreLocalidad the nombreLocalidad to set
	 */
	public void setNombreLocalidad(String nombreLocalidad) {
		this.nombreLocalidad = nombreLocalidad;
	}
	/**
	 * @return the nombreProvincia
	 */
	public String getNombreProvincia() {
		return nombreProvincia;
	}
	/**
	 * @param nombreProvincia the nombreProvincia to set
	 */
	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}
	/**
	 * @return the nombreMunicipio
	 */
	public String getNombreMunicipio() {
		return nombreMunicipio;
	}
	/**
	 * @param nombreMunicipio the nombreMunicipio to set
	 */
	public void setNombreMunicipio(String nombreMunicipio) {
		this.nombreMunicipio = nombreMunicipio;
	}
	/**
	 * @return the emailContacto
	 */
	public String getEmailContacto() {
		return emailContacto;
	}
	/**
	 * @param emailContacto the emailContacto to set
	 */
	public void setEmailContacto(String emailContacto) {
		this.emailContacto = emailContacto;
	}
	/**
	 * @return the empresaContratoRelacionado
	 */
	public String getEmpresaContratoRelacionado() {
		return empresaContratoRelacionado;
	}
	/**
	 * @param empresaContratoRelacionado the empresaContratoRelacionado to set
	 */
	public void setEmpresaContratoRelacionado(String empresaContratoRelacionado) {
		this.empresaContratoRelacionado = empresaContratoRelacionado;
	}
	/**
	 * @return the centroContratoRelacionado
	 */
	public String getCentroContratoRelacionado() {
		return centroContratoRelacionado;
	}
	/**
	 * @param centroContratoRelacionado the centroContratoRelacionado to set
	 */
	public void setCentroContratoRelacionado(String centroContratoRelacionado) {
		this.centroContratoRelacionado = centroContratoRelacionado;
	}
	/**
	 * @return the numeroContratoRelacionado
	 */
	public String getNumeroContratoRelacionado() {
		return numeroContratoRelacionado;
	}
	/**
	 * @param numeroContratoRelacionado the numeroContratoRelacionado to set
	 */
	public void setNumeroContratoRelacionado(String numeroContratoRelacionado) {
		this.numeroContratoRelacionado = numeroContratoRelacionado;
	}
	/**
	 * @return the codigoProducto
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the codigoProducto to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the codigoSubProducto
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the codigoSubProducto to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the descripcion2
	 */
	public String getDescripcion2() {
		return descripcion2;
	}
	/**
	 * @param descripcion2 the descripcion2 to set
	 */
	public void setDescripcion2(String descripcion2) {
		this.descripcion2 = descripcion2;
	}

}
