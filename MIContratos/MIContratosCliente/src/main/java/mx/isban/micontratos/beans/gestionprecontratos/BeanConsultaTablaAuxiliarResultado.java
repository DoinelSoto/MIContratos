package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI15 : Consulta de tabla auxiliar
 *
 */
public class BeanConsultaTablaAuxiliarResultado extends ResponseGenerico implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -4223655485964970995L;

	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanConsultaTablaAuxiliarResponse> listaTablaAuxiliar;

	/**
	 * Bean con campos de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Constructor
	 * Se inicializa el bean de paginación
	 */
	public BeanConsultaTablaAuxiliarResultado() {
		rellamada= new BeanPaginacion();
	}
	/**
	 * @return BeanPaginacion
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	/**
	 * @param rellamada
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

	/**
	 * @return the consulta
	 */
	public List<BeanConsultaTablaAuxiliarResponse> getListaTablaAuxiliar() {
		return new ArrayList<>(listaTablaAuxiliar);
	}

	/**
	 * @param consulta the consulta to set
	 */
	public void setListaTablaAuxiliar(List<BeanConsultaTablaAuxiliarResponse> listaTablaAuxiliar) {
		this.listaTablaAuxiliar = new ArrayList<>(listaTablaAuxiliar);
	}
	
}
