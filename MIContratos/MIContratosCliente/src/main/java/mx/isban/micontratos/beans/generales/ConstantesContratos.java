package mx.isban.micontratos.beans.generales;

import java.io.Serializable;

/**
 * @author everis
 *
 */
public final class ConstantesContratos implements Serializable{
	
	/**
	 * Serializacion memento
	 */
	private static final long serialVersionUID = -7254615493744864032L;

	/**
	 * Separador de memento
	 */
	public static final String SEPARADOR_MEMENTO=";";
	
	/**
	 * Valor de S asignado a indicador de mas datos
	 */
	public static final char INDICADOR_MAS_DATOS_S='S';
	
	/**
	 * Valor de N asignado a indicador de mas datos
	 */
	public static final char INDICADOR_MAS_DATOS_N='N';
	
	private ConstantesContratos(){
		
	}

}
