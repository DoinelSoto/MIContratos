package mx.isban.micontratos.beans.gestordocumental;

import java.io.Serializable;

/**
 * @author Daniel hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de respuesta para la transacción : MI17 Consulta Tabla
 * Auxiliar
 * 
 */
public class BeanConsultaTablaAuxiliarGestorResponse implements Serializable {

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7105862110115705346L;

	/**
	 * Calidad de participación
	 */
	private String calidadParticipacion;
	/**
	 * Número de persona
	 */
	private String numeroPersona;
	/**
	 * Código de documento
	 */
	private String codigoDocumento;
	/**
	 * Fecha de alta del documento
	 */
	private String fechaAlta;
	/**
	 * Comentarios
	 */
	private String comentario;
	/**
	 * Código de estado del documento
	 */
	private String codigoEstadoDocumento;
	/**
	 * Fecha de estado del documento
	 */
	private String fechaEstado;
	/**
	 * Fecha de vencimiento del documento
	 */
	private String fechaVencimientoDocumento;
	/**
	 * Indicador de obligatoriedad
	 */
	private String indicadorObligatoriedad;

	/**
	 * @return the peCalPar
	 */
	public String getCalidadParticipacion() {
		return calidadParticipacion;
	}

	/**
	 * @param calidadParticipacion
	 *            the peCalPar to set
	 */
	public void setCalidadParticipacion(String calidadParticipacion) {
		this.calidadParticipacion = calidadParticipacion;
	}

	/**
	 * @return the peNumPer
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}

	/**
	 * @param numeroPersona
	 *            the peNumPer to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}

	/**
	 * @return the codDocum
	 */
	public String getCodigoDocumento() {
		return codigoDocumento;
	}

	/**
	 * @param codigoDocumento
	 *            the codDocum to set
	 */
	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	/**
	 * @return the fecAlta
	 */
	public String getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fecAlta to set
	 */
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the desComen
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario
	 *            the desComen to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the codEstat
	 */
	public String getCodigoEstadoDocumento() {
		return codigoEstadoDocumento;
	}

	/**
	 * @param codigoEstadoDocumento
	 *            the codEstat to set
	 */
	public void setCodigoEstadoDocumento(String codigoEstadoDocumento) {
		this.codigoEstadoDocumento = codigoEstadoDocumento;
	}

	/**
	 * @return fechaEstado
	 */
	public String getFechaEstado() {
		return fechaEstado;
	}

	/**
	 * @param fechaEstado
	 *            the fecEstad to set
	 */
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	/**
	 * @return fechaVencimientoDocumento
	 */
	public String getFechaVencimientoDocumento() {
		return fechaVencimientoDocumento;
	}

	/**
	 * @param fechaVencimientoDocumento
	 */
	public void setFechaVencimientoDocumento(String fechaVencimientoDocumento) {
		this.fechaVencimientoDocumento = fechaVencimientoDocumento;
	}

	/**
	 * @return indicadorObligatoriedad
	 */
	public String getIndicadorObligatoriedad() {
		return indicadorObligatoriedad;
	}

	/**
	 * @param indicadorObligatoriedad
	 */
	public void setIndicadorObligatoriedad(String indicadorObligatoriedad) {
		this.indicadorObligatoriedad = indicadorObligatoriedad;
	}
}
