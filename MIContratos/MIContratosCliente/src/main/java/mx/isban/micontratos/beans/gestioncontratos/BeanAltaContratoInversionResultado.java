package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de respuesta e implementación de la interfaz para la transaccion MI18 : Alta contrato Inversión
 *
 */
public class BeanAltaContratoInversionResultado extends ResponseGenerico implements Serializable{
	
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 6831111812918665915L;

	
	private List<BeanAltaContratoInversionResponse> listaAltaContratoInversionResponse;
	


	/**
	 * @return the consulta
	 */
	public List<BeanAltaContratoInversionResponse> getListaAltaContratoInversionResponse() {
		return new ArrayList<>(listaAltaContratoInversionResponse);
	}

	/**
	 * @param listaAltaContratoInversionResponse the consulta to set
	 */
	public void setListaAltaContratoInversionResponse(List<BeanAltaContratoInversionResponse> listaAltaContratoInversionResponse) {
		this.listaAltaContratoInversionResponse = new ArrayList<> (listaAltaContratoInversionResponse);
	}
}
