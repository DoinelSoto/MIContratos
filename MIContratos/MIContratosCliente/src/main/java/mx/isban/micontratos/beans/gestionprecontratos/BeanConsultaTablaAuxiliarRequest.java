package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI15 : Consulta de tabla auxiliar
 *
 */
public class BeanConsultaTablaAuxiliarRequest implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -6732889611314968093L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	

	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;

	
	/**
	 * Número de folio del pre contrato
	 */
	
	private int numeroFolio;
	/**
	 * Código de tipo de registro	
	 */
	@Size(max=2)
	private String codigoTipoRegistro;
	
	/**
	 * Constructor de tabla auxiliar
	 */
	public BeanConsultaTablaAuxiliarRequest(){		

		multicanalidad=new MulticanalidadGenerica();	
		rellamada = new BeanPaginacion();
	
	
	}


	/**
	 * @return the numFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}
	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumeroFolio(int numFolio) {
		this.numeroFolio = numFolio;
	}
	/**
	 * @return the codTipRe
	 */
	public String getCodigoTipoRegistro() {
		return codigoTipoRegistro;
	}
	/**
	 * @param codTipRe the codTipRe to set
	 */
	public void setCodigoTipoRegistro(String codTipRe) {
		this.codigoTipoRegistro = codTipRe;
	}	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	/**
	 * @return BeanPaginacion
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	/**
	 * @param rellamada
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

}
