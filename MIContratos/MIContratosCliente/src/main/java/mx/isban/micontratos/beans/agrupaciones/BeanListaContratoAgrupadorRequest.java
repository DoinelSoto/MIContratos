package mx.isban.micontratos.beans.agrupaciones;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI32 : Transaccion Lista contrato Agrupador
 *
 */
public class BeanListaContratoAgrupadorRequest  implements Serializable{
	
	/**
	 * Serialización
	 */
	
	private static final long serialVersionUID = 8979024243507639903L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;

	/**
	 * TipoConsulta
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa Contrato de inversión
	 */
	@Size(max=4)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Tipo de agrupación
	 */
	@Size(max=3)
	private String tipoAgrupacion;
	/**
	 * Empresa del Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato	
	 */
	@Size(max=2)
	private String codigoProductoContrato;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubProductoContrato;
	
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Constructor para la multi-canalidad
	 */
	public BeanListaContratoAgrupadorRequest(){
		
		multicanalidad=new MulticanalidadGenerica();

		rellamada = new BeanPaginacion();

		
	}
	/**
	 * @param aliasContrato the aliasContrato to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}
	/**
	 * @return the aliasContrato
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * Tipo de agrupación	
	 */
	/**
	 * @return the empresaContratoInversion
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}

	/**
	 * @param empresaContratoInversion the empresaContratoInversion to set
	 */
	public void setEmpresaContratoInversion(String empresaContratoInversion) {
		this.empresaContratoInversion = empresaContratoInversion;
	}
	/**
	 * @return the tipAgrup
	 */
	public String getTipoAgrupacion() {
		return tipoAgrupacion;
	}
	/**
	 * @param tipoAgrupacion the tipAgrup to set
	 */
	public void setTipoAgrupacion(String tipoAgrupacion) {
		this.tipoAgrupacion = tipoAgrupacion;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProductoContrato() {
		return codigoProductoContrato;
	}
	/**
	 * @param codigoProductoContrato the idProd to set
	 */
	public void setCodigoProductoContrato(String codigoProductoContrato) {
		this.codigoProductoContrato = codigoProductoContrato;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProductoContrato() {
		return codigoSubProductoContrato;
	}
	/**
	 * @param codigoSubProductoContrato the idStiPro to set
	 */
	public void setCodigoSubProductoContrato(String codigoSubProductoContrato) {
		this.codigoSubProductoContrato = codigoSubProductoContrato;
	}
	/**
	 * @return the tipoConsulta
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}

	/**
	 * @param tipoConsulta the tipoConsulta to set
	 */
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	
	/**
	 * 
	 * @return MulticanalidadGenerica regresa el objeto de multicanlidad
	 */

	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;

	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

}
