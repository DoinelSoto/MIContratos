package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;


/**
 * @author everis
 *
 */

/**  
 *	Bean con los datos de salida e implementación de la interfaz BeanResultBO para la transacción : MI39
 **/
public class BeanConsultarBranchRequest implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 1863578261148761371L;
	
	/**
	 * Código de branch
	 */
	private String codigoBranch;
	
	/**
	 * Código de empresa
	 */
	private String codigoEmpresa;
	
	/**
	 * Bean para la paginación
	 */
	private BeanPaginacion rellamada;

	/**
	 * Bean de multicanalidad
	 */
	private MulticanalidadGenerica multicanalidad;
	/**
	 * Constructor para las instancias
	 */
	public BeanConsultarBranchRequest(){
		rellamada = new BeanPaginacion();
		multicanalidad= new MulticanalidadGenerica();
	}
	
	/**
	 * @return the codigoBranch
	 */
	public String getCodigoBranch() {
		return codigoBranch;
	}

	/**
	 * @param codigoBranch the codigoBranch to set
	 */
	public void setCodigoBranch(String codigoBranch) {
		this.codigoBranch = codigoBranch;
	}

	/**
	 * @return the codigoEmpresa
	 */
	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}

	/**
	 * @param codigoEmpresa the codigoEmpresa to set
	 */
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
	
	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	
	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

	/**
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}

	/**
	 * @param multicanalidad the multicanalidad to set
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
}