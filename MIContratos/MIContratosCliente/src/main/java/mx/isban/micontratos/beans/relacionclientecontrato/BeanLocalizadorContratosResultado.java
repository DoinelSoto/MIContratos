package mx.isban.micontratos.beans.relacionclientecontrato;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada de la transaccion MI30 : Cambio titular del contrato
 *
 */
public class BeanLocalizadorContratosResultado extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 2207409665267331113L;
	
	/**
	 * Lista de objetos a regresar del response
	 */
	private List<BeanLocalizadorContratosResponse> localizadorContratos;
	
	/**
	 * @return String localizadorContratos
	 */
	public List<BeanLocalizadorContratosResponse> getLocalizadorContratos() {
		return new ArrayList<>(localizadorContratos);
	}
	
	/**
	 * @param localizadorContratos
	 */
	public void setLocalizadorContratos(List<BeanLocalizadorContratosResponse> localizadorContratos) {
		this.localizadorContratos = new ArrayList<>(localizadorContratos);
	}	
}