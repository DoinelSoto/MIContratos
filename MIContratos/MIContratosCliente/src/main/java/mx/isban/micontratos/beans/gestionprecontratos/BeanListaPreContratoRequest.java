package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción : MI14
 * 
 */
public class BeanListaPreContratoRequest implements Serializable {

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -760049826420872861L;

	@Valid
	private  MulticanalidadGenerica multicanalidad;



	/**
	 * Número de folio del que proviene el contrato
	 */
	private int numeroFolio;
	/**
	 * Código de ejecutivo del contrato
	 */
	@Size(max = 8)
	private String codigoEjecutivo;
	/**
	 * Número de cliente
	 */
	@Size(max = 8)
	private String numeroCliente;
	/**
	 * Fecha de apertura del folio
	 */
	@Size(max = 10)
	private String fechaAperturaFolio;

	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanListaPreContratoRequest() {
		multicanalidad = new MulticanalidadGenerica();
		rellamada = new BeanPaginacion();

	}

	/**
	 * @return the codEjecu
	 */
	public String getCodigoEjecutivo() {
		return codigoEjecutivo;
	}

	/**
	 * @return the numeroFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}



	/**
	 * @param numeroFolio the numeroFolio to set
	 */
	public void setNumeroFolio(int numeroFolio) {
		this.numeroFolio = numeroFolio;
	}



	/**
	 * @param codEjecu
	 *            the codEjecu to set
	 */
	public void setCodigoEjecutivo(String codEjecu) {
		this.codigoEjecutivo = codEjecu;
	}

	/**
	 * @return the peNumPer
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * @param peNumPer
	 *            the peNumPer to set
	 */
	public void setNumeroCliente(String peNumPer) {
		this.numeroCliente = peNumPer;
	}

	/**
	 * @return the fecApert
	 */
	public String getFechaAperturaFolio() {
		return fechaAperturaFolio;
	}

	/**
	 * @param fecApert
	 *            the fecApert to set
	 */
	public void setFechaAperturaFolio(String fecApert) {
		this.fechaAperturaFolio = fecApert;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}

     
     public BeanPaginacion getRellamada() {
 		return rellamada;
 	}
 	public void setRellamada(BeanPaginacion rellamada) {
 		this.rellamada = rellamada;
 	}
 	
}
