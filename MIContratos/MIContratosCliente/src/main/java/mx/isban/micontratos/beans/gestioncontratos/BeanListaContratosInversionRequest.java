package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * 
 * @author Luis Martínez - everis
 * Bean con los campos de entrada para la transacción : MI40 Listar contratos de inversion
 *
 */
public class BeanListaContratosInversionRequest implements Serializable {

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -3737585024938981282L;
	/**
	 * Indicador para traer todos los campos
	 */
	@Size(max=1)
	private String indicadorTodosCampos;
	/**
	 * Bean para la multicanalidad 
	 */
	private MulticanalidadGenerica multicanalidad;
	/**
	 * Código usuario de conexión
	 */
	@Size(max=8)
	private String codigoUsuarioConexion;
	/**
	 * Código ejecutivo del contrato
	 */
	@Size(max=8)
	private String codigoEjecutivo;
	/**
	 * Código de empresa del contrato de inversión
	 */
	@Size(max=4)
	private String codigoEmpresaContratoInversion;
	/**
	 * Alias del contrato
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Código de persona del titular
	 */
	@Size(max=8)
	private String codigoPersonaTitular;
	/**
	 * Bean para la paginación
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Constructor para inicializar instancias
	 */
	public BeanListaContratosInversionRequest() {
		multicanalidad= new MulticanalidadGenerica();
		rellamada = new BeanPaginacion();
	}

	/**
	 * @return the indicadorTodosCampos
	 */
	public String getIndicadorTodosCampos() {
		return indicadorTodosCampos;
	}

	/**
	 * @param indicadorTodosCampos the indicadorTodosCampos to set
	 */
	public void setIndicadorTodosCampos(String indicadorTodosCampos) {
		this.indicadorTodosCampos = indicadorTodosCampos;
	}

	/**
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}

	/**
	 * @param multicanalidad the multicanalidad to set
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}

	/**
	 * @return the codigoUsuarioConexion
	 */
	public String getCodigoUsuarioConexion() {
		return codigoUsuarioConexion;
	}

	/**
	 * @param codigoUsuarioConexion the codigoUsuarioConexion to set
	 */
	public void setCodigoUsuarioConexion(String codigoUsuarioConexion) {
		this.codigoUsuarioConexion = codigoUsuarioConexion;
	}
	
	/**
	 * @return the codigoEjecutivo
	 */
	public String getCodigoEjecutivo() {
		return codigoEjecutivo;
	}

	/**
	 * @param codigoEjecutivo the codigoEjecutivo to set
	 */
	public void setCodigoEjecutivo(String codigoEjecutivo) {
		this.codigoEjecutivo = codigoEjecutivo;
	}

	/**
	 * @return the codigoEmpresaContratoInversion
	 */
	public String getCodigoEmpresaContratoInversion() {
		return codigoEmpresaContratoInversion;
	}

	/**
	 * @param codigoEmpresaContratoInversion the codigoEmpresaContratoInversion to set
	 */
	public void setCodigoEmpresaContratoInversion(
			String codigoEmpresaContratoInversion) {
		this.codigoEmpresaContratoInversion = codigoEmpresaContratoInversion;
	}

	/**
	 * @return the aliasContrato
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}

	/**
	 * @param aliasContrato the aliasContrato to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}

	/**
	 * @return the codigoPersonaTitular
	 */
	public String getCodigoPersonaTitular() {
		return codigoPersonaTitular;
	}

	/**
	 * @param codigoPersonaTitular the codigoPersonaTitular to set
	 */
	public void setCodigoPersonaTitular(String codigoPersonaTitular) {
		this.codigoPersonaTitular = codigoPersonaTitular;
	}

	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
}
