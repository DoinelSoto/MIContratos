package mx.isban.micontratos.beans.gestordocumental;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de respuesta para la transacción : MI17 Consulta Tabla Auxiliar
 *
 */
public class BeanConsultaTablaAuxiliarGestorResultado extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 669478779721916671L;
	
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanConsultaTablaAuxiliarGestorResponse> listaTablaAuxiliarGestor;
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanConsultaTablaAuxiliarGestorResultado(){
		rellamada = new BeanPaginacion();
	}
	

	/**
	 * Devuelve una copia de listaTablaAuxiliarGestor
	 * @return listaTablaAuxiliarGestor
	 */
	public List<BeanConsultaTablaAuxiliarGestorResponse> getListaTablaAuxiliarGestor() {
		return new ArrayList<>(listaTablaAuxiliarGestor);
	}

	/**
	 * Guarda una copia del parametro
	 * @param listaTablaAuxiliarGestor the consulta to set
	 */
	public void setListaTablaAuxiliarGestor(List<BeanConsultaTablaAuxiliarGestorResponse> listaTablaAuxiliarGestor) {
		this.listaTablaAuxiliarGestor = new ArrayList<>(listaTablaAuxiliarGestor);
	}

	/**
	 * @return BeanPaginacion
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	
}
