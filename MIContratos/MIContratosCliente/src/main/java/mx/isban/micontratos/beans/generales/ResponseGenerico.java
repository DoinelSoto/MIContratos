package mx.isban.micontratos.beans.generales;

import java.io.Serializable;

import mx.isban.agave.commons.interfaces.BeanResultBO;

/**
 * @author everis
 *
 */
public class ResponseGenerico implements Serializable,BeanResultBO{

	/**
	 * Serializacion
	 */
	private static final long serialVersionUID = 5190717259695057661L;

	/**
	 * Código de error del response
	 */
	private String codeError;

	/**
	 * Mensaje de error del response
	 */
	private String msgErr;

	/**
	 * @return String codeError
	 */
	@Override
	public String getCodError() {
		return this.codeError;
	}

	/**
	 * @return String msgErr
	 */
	@Override
	public String getMsgError() {
		return this.msgErr;
	}

	/**
	 * @param codError
	 */
	@Override
	public void setCodError(String codError) {
		
		this.codeError = codError;
	}

	/**
	 * @param msgError
	 */
	@Override
	public void setMsgError(String msgError) {
		
		this.msgErr = msgError;
	}

}
