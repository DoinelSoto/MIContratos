package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

/**
 * 
 * Clase principal para el conversor de cuentas
 *
 */
public class BeanConversorCuentasResponse implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 2035437444698834440L;
	
	/**
	 * Código de empresa
	 */
	private String codigoEmpresa;
	
	/**
	 * Centro del contrato
	 */
	private String centroContrato;
	
	/**
	 * Número del contrato
	 */
	private String numeroContrato;
	
	/**
	 * Código de producto del contrato
	 */
	private String codigoProductoContrato;
	
	/**
	 * Código de sub producto del contrato
	 */
	private String codigoSubProductoContrato;
	
	/**
	 * Código de moneda del contrato
	 */
	private String codigoMonedaContrato;
	
	/**
	 * Folio proveniente del contrato
	 */
	private int folioProvieneContrato;
	
	/**
	 * Código de empresa del contrato de inversión
	 */
	private String codEmpresaContratoInversion;
	
	/**
	 * Código banca del contrato
	 */
	private String codigoBancaContrato;
	
	/**
	 * Fecha de apertura del contrato
	 */
	private String fechaAperturaContrato;
	
	/**
	 * Alias del contrato
	 */
	private String aliasContrato;
	
	/**
	 * Fecha de vencimiento del contrato
	 */
	private String fechaVencimientoContrato;
	
	/**
	 * Código de ejecutivo del contrato
	 */
	private String codigoEjecutivoContrato;
	
	/**
	 * Centro de costos del contrato
	 */
	private String centroCostosContrato;
	
	/**
	 * Indicador tipo cuenta
	 */
	private String indicadorTipoCuenta;

	/**
	 * Indicador tipo contrato
	 */
	private String indicadorTipoContrato;
	
	/**
	 * Indicador autorización posicion corto capitales
	 */
	private String indAutPosicionCortoCapitales;
	
	/**
	 * Indicador autorización posición corto fondos
	 */
	private String indAutPosicionCortoFondos;
	
	/**
	 * Indicador autorización posición fondos inversión
	 */
	private String indAutPosicionFondosInvers;
	
	/**
	 * Indicador de discrecionalidad
	 */
	private String indicadorDiscrecionalidad;
	
	/**
	 * indicador tipo custodia directo
	 */
	private String indicadorTipoCustodiaDirecto;
	
	/**
	 * Indicador tipo custodia Repos
	 */
	private String indicadorTipoCustodiaRepos;
	
	/**
	 * Indicador estado de cuenta
	 */
	private String indicadorEstadoCuenta;
	
	/**
	 * Monto máximo a operar en el contrato
	 */
	private String montoMaxOperarContrato;
	
	/**
	 *Divisa de monto máximo a operar en el contrato 
	 */
	private String divisaMontoMaxContrato;
	
	/**
	 * Indicador envío de correo
	 */
	private String indicadorEnvioCorreo;
	
	/**
	 * Estatus del contrato
	 */
	private String estatusContrato;
	
	/**
	 * Detalle del estatus
	 */
	private String detalleEstatus;
	
	/**
	 * Fecha de estado del contrato
	 */
	private String fechaEstadoContrato;
	
	/**
	 * Indicador contrato agrupado
	 */
	private String indicadorContratoAgrupado;
	
	/**
	 * Indicador tipo posición
	 */
	private String indicadorTipoPosicion;
	
	/**
	 * Indicador contrato espejo
	 */
	private String indicadorContratoEspejo;
	
	/**
	 * Indicador contrato amparado chequera
	 */
	private String indCtoAmparadoChequera;
	
	/**
	 * Código tipo de servicio
	 */
	private String codigoTipoServicio;
	
	/**
	 * Código de asesor financiero
	 */
	private String codigoAfi;
	
	/**
	 * Indicador de benerficiario
	 */
	private String indicadorBeneficiario;
	
	/**
	 * Indicador de proveedores de recursos
	 */
	private String indicadorProveedoresRecursos;
	
	/**
	 * Indicador de accionistas
	 */
	private String indicadorAccionistas;
	
	/**
	 * Fecha de última operación
	 */
	private String fechaUltimaOperacion;
	
	/**
	 * Fecha de última modificación
	 */
	private String fechaUltimaModificacion;
	
	/**
	 * Descripción de sub estatus
	 */
	private String descripcionSubEstatus;

	/**
	 * @return the codigoEmpresa
	 */
	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}

	/**
	 * @param codigoEmpresa the codigoEmpresa to set
	 */
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	/**
	 * @return the centroContrato
	 */
	public String getCentroContrato() {
		return centroContrato;
	}

	/**
	 * @param centroContrato the centroContrato to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}

	/**
	 * @return the numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * @param numeroContrato the numeroContrato to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * @return the codigoProductoContrato
	 */
	public String getCodigoProductoContrato() {
		return codigoProductoContrato;
	}

	/**
	 * @param codigoProductoContrato the codigoProductoContrato to set
	 */
	public void setCodigoProductoContrato(String codigoProductoContrato) {
		this.codigoProductoContrato = codigoProductoContrato;
	}

	/**
	 * @return the codigoSubproductoContrato
	 */
	public String getCodigoSubProductoContrato() {
		return codigoSubProductoContrato;
	}

	/**
	 * @param codigoSubproductoContrato the codigoSubproductoContrato to set
	 */
	public void setCodigoSubProductoContrato(String codigoSubProductoContrato) {
		this.codigoSubProductoContrato = codigoSubProductoContrato;
	}

	/**
	 * @return the codigoMonedaContrato
	 */
	public String getCodigoMonedaContrato() {
		return codigoMonedaContrato;
	}

	/**
	 * @param codigoMonedaContrato the codigoMonedaContrato to set
	 */
	public void setCodigoMonedaContrato(String codigoMonedaContrato) {
		this.codigoMonedaContrato = codigoMonedaContrato;
	}

	/**
	 * @return the folioProvieneContrato
	 */
	public int getFolioProvieneContrato() {
		return folioProvieneContrato;
	}

	/**
	 * @param folioProvieneContrato the folioProvieneContrato to set
	 */
	public void setFolioProvieneContrato(int folioProvieneContrato) {
		this.folioProvieneContrato = folioProvieneContrato;
	}

	/**
	 * @return the codEmpresaContratoInversion
	 */
	public String getCodEmpresaContratoInversion() {
		return codEmpresaContratoInversion;
	}

	/**
	 * @param codEmpresaContratoInversion the codEmpresaContratoInversion to set
	 */
	public void setCodEmpresaContratoInversion(String codEmpresaContratoInversion) {
		this.codEmpresaContratoInversion = codEmpresaContratoInversion;
	}

	/**
	 * @return the codigoBancaContrato
	 */
	public String getCodigoBancaContrato() {
		return codigoBancaContrato;
	}

	/**
	 * @param codigoBancaContrato the codigoBancaContrato to set
	 */
	public void setCodigoBancaContrato(String codigoBancaContrato) {
		this.codigoBancaContrato = codigoBancaContrato;
	}

	/**
	 * @return the fechaAperturaContrato
	 */
	public String getFechaAperturaContrato() {
		return fechaAperturaContrato;
	}

	/**
	 * @param fechaAperturaContrato the fechaAperturaContrato to set
	 */
	public void setFechaAperturaContrato(String fechaAperturaContrato) {
		this.fechaAperturaContrato = fechaAperturaContrato;
	}

	/**
	 * @return the aliasContrato
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}

	/**
	 * @param aliasContrato the aliasContrato to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}

	/**
	 * @return the fechaVencimientoContrato
	 */
	public String getFechaVencimientoContrato() {
		return fechaVencimientoContrato;
	}

	/**
	 * @param fechaVencimientoContrato the fechaVencimientoContrato to set
	 */
	public void setFechaVencimientoContrato(String fechaVencimientoContrato) {
		this.fechaVencimientoContrato = fechaVencimientoContrato;
	}

	/**
	 * @return the codigoEjecutivoContrato
	 */
	public String getCodigoEjecutivoContrato() {
		return codigoEjecutivoContrato;
	}

	/**
	 * @param codigoEjecutivoContrato the codigoEjecutivoContrato to set
	 */
	public void setCodigoEjecutivoContrato(String codigoEjecutivoContrato) {
		this.codigoEjecutivoContrato = codigoEjecutivoContrato;
	}

	/**
	 * @return the centroCostosContrato
	 */
	public String getCentroCostosContrato() {
		return centroCostosContrato;
	}

	/**
	 * @param centroCostosContrato the centroCostosContrato to set
	 */
	public void setCentroCostosContrato(String centroCostosContrato) {
		this.centroCostosContrato = centroCostosContrato;
	}

	/**
	 * @return the indicadorTipoCuenta
	 */
	public String getIndicadorTipoCuenta() {
		return indicadorTipoCuenta;
	}

	/**
	 * @param indicadorTipoCuenta the indicadorTipoCuenta to set
	 */
	public void setIndicadorTipoCuenta(String indicadorTipoCuenta) {
		this.indicadorTipoCuenta = indicadorTipoCuenta;
	}

	/**
	 * @return the indicadorTipoContrato
	 */
	public String getIndicadorTipoContrato() {
		return indicadorTipoContrato;
	}

	/**
	 * @param indicadorTipoContrato the indicadorTipoContrato to set
	 */
	public void setIndicadorTipoContrato(String indicadorTipoContrato) {
		this.indicadorTipoContrato = indicadorTipoContrato;
	}

	/**
	 * @return the indAutPosicionCortoCapitales
	 */
	public String getIndAutPosicionCortoCapitales() {
		return indAutPosicionCortoCapitales;
	}

	/**
	 * @param indAutPosicionCortoCapitales the indAutPosicionCortoCapitales to set
	 */
	public void setIndAutPosicionCortoCapitales(String indAutPosicionCortoCapitales) {
		this.indAutPosicionCortoCapitales = indAutPosicionCortoCapitales;
	}

	/**
	 * @return the indAutPosicionCortoFondos
	 */
	public String getIndAutPosicionCortoFondos() {
		return indAutPosicionCortoFondos;
	}

	/**
	 * @param indAutPosicionCortoFondos the indAutPosicionCortoFondos to set
	 */
	public void setIndAutPosicionCortoFondos(String indAutPosicionCortoFondos) {
		this.indAutPosicionCortoFondos = indAutPosicionCortoFondos;
	}

	/**
	 * @return the indAutPosicionFondosInvers
	 */
	public String getIndAutPosicionFondosInvers() {
		return indAutPosicionFondosInvers;
	}

	/**
	 * @param indAutPosicionFondosInvers the indAutPosicionFondosInvers to set
	 */
	public void setIndAutPosicionFondosInvers(String indAutPosicionFondosInvers) {
		this.indAutPosicionFondosInvers = indAutPosicionFondosInvers;
	}

	/**
	 * @return the indicadorDiscrecionalidad
	 */
	public String getIndicadorDiscrecionalidad() {
		return indicadorDiscrecionalidad;
	}

	/**
	 * @param indicadorDiscrecionalidad the indicadorDiscrecionalidad to set
	 */
	public void setIndicadorDiscrecionalidad(String indicadorDiscrecionalidad) {
		this.indicadorDiscrecionalidad = indicadorDiscrecionalidad;
	}

	/**
	 * @return the indicadorTipoCustodiaDirecto
	 */
	public String getIndicadorTipoCustodiaDirecto() {
		return indicadorTipoCustodiaDirecto;
	}

	/**
	 * @param indicadorTipoCustodiaDirecto the indicadorTipoCustodiaDirecto to set
	 */
	public void setIndicadorTipoCustodiaDirecto(String indicadorTipoCustodiaDirecto) {
		this.indicadorTipoCustodiaDirecto = indicadorTipoCustodiaDirecto;
	}

	/**
	 * @return the indicadorTipoCustodiaRepos
	 */
	public String getIndicadorTipoCustodiaRepos() {
		return indicadorTipoCustodiaRepos;
	}

	/**
	 * @param indicadorTipoCustodiaRepos the indicadorTipoCustodiaRepos to set
	 */
	public void setIndicadorTipoCustodiaRepos(String indicadorTipoCustodiaRepos) {
		this.indicadorTipoCustodiaRepos = indicadorTipoCustodiaRepos;
	}

	/**
	 * @return the indicadorEstadoCuenta
	 */
	public String getIndicadorEstadoCuenta() {
		return indicadorEstadoCuenta;
	}

	/**
	 * @param indicadorEstadoCuenta the indicadorEstadoCuenta to set
	 */
	public void setIndicadorEstadoCuenta(String indicadorEstadoCuenta) {
		this.indicadorEstadoCuenta = indicadorEstadoCuenta;
	}

	/**
	 * @return the montoMaxOperarContrato
	 */
	public String getMontoMaxOperarContrato() {
		return montoMaxOperarContrato;
	}

	/**
	 * @param montoMaxOperarContrato the montoMaxOperarContrato to set
	 */
	public void setMontoMaxOperarContrato(String montoMaxOperarContrato) {
		this.montoMaxOperarContrato = montoMaxOperarContrato;
	}

	/**
	 * @return the divisaMontoMaxContrato
	 */
	public String getDivisaMontoMaxContrato() {
		return divisaMontoMaxContrato;
	}

	/**
	 * @param divisaMontoMaxContrato the divisaMontoMaxContrato to set
	 */
	public void setDivisaMontoMaxContrato(String divisaMontoMaxContrato) {
		this.divisaMontoMaxContrato = divisaMontoMaxContrato;
	}

	/**
	 * @return the indicadorENvioCorreo
	 */
	public String getIndicadorEnvioCorreo() {
		return indicadorEnvioCorreo;
	}

	/**
	 * @param indicadorENvioCorreo the indicadorENvioCorreo to set
	 */
	public void setIndicadorEnvioCorreo(String indicadorEnvioCorreo) {
		this.indicadorEnvioCorreo = indicadorEnvioCorreo;
	}

	/**
	 * @return the esattusCOntrato
	 */
	public String getEstatusContrato() {
		return estatusContrato;
	}

	/**
	 * @param esattusCOntrato the esattusCOntrato to set
	 */
	public void setEstatusContrato(String estatusContrato) {
		this.estatusContrato = estatusContrato;
	}

	/**
	 * @return the detalleEstatus
	 */
	public String getDetalleEstatus() {
		return detalleEstatus;
	}

	/**
	 * @param detalleEstatus the detalleEstatus to set
	 */
	public void setDetalleEstatus(String detalleEstatus) {
		this.detalleEstatus = detalleEstatus;
	}

	/**
	 * @return the fechaEstadoContrato
	 */
	public String getFechaEstadoContrato() {
		return fechaEstadoContrato;
	}

	/**
	 * @param fechaEstadoContrato the fechaEstadoContrato to set
	 */
	public void setFechaEstadoContrato(String fechaEstadoContrato) {
		this.fechaEstadoContrato = fechaEstadoContrato;
	}

	/**
	 * @return the indicadorContratoAgrupado
	 */
	public String getIndicadorContratoAgrupado() {
		return indicadorContratoAgrupado;
	}

	/**
	 * @param indicadorContratoAgrupado the indicadorContratoAgrupado to set
	 */
	public void setIndicadorContratoAgrupado(String indicadorContratoAgrupado) {
		this.indicadorContratoAgrupado = indicadorContratoAgrupado;
	}

	/**
	 * @return the indicadorTipoPosicion
	 */
	public String getIndicadorTipoPosicion() {
		return indicadorTipoPosicion;
	}

	/**
	 * @param indicadorTipoPosicion the indicadorTipoPosicion to set
	 */
	public void setIndicadorTipoPosicion(String indicadorTipoPosicion) {
		this.indicadorTipoPosicion = indicadorTipoPosicion;
	}

	/**
	 * @return the indicadorContratoEspejo
	 */
	public String getIndicadorContratoEspejo() {
		return indicadorContratoEspejo;
	}

	/**
	 * @param indicadorContratoEspejo the indicadorContratoEspejo to set
	 */
	public void setIndicadorContratoEspejo(String indicadorContratoEspejo) {
		this.indicadorContratoEspejo = indicadorContratoEspejo;
	}

	/**
	 * @return the intCtoAmparadoChequera
	 */
	public String getIndCtoAmparadoChequera() {
		return indCtoAmparadoChequera;
	}

	/**
	 * @param intCtoAmparadoChequera the intCtoAmparadoChequera to set
	 */
	public void setIndCtoAmparadoChequera(String indCtoAmparadoChequera) {
		this.indCtoAmparadoChequera = indCtoAmparadoChequera;
	}

	/**
	 * @return the codigoTipoServicio
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}

	/**
	 * @param codigoTipoServicio the codigoTipoServicio to set
	 */
	public void setCodigoTipoServicio(String codigoTipoServicio) {
		this.codigoTipoServicio = codigoTipoServicio;
	}

	/**
	 * @return the codigoAfi
	 */
	public String getCodigoAfi() {
		return codigoAfi;
	}

	/**
	 * @param codigoAfi the codigoAfi to set
	 */
	public void setCodigoAfi(String codigoAfi) {
		this.codigoAfi = codigoAfi;
	}

	/**
	 * @return the indicadorBeneficiario
	 */
	public String getIndicadorBeneficiario() {
		return indicadorBeneficiario;
	}

	/**
	 * @param indicadorBeneficiario the indicadorBeneficiario to set
	 */
	public void setIndicadorBeneficiario(String indicadorBeneficiario) {
		this.indicadorBeneficiario = indicadorBeneficiario;
	}

	/**
	 * @return the indicadorProveedoresRecursos
	 */
	public String getIndicadorProveedoresRecursos() {
		return indicadorProveedoresRecursos;
	}

	/**
	 * @param indicadorProveedoresRecursos the indicadorProveedoresRecursos to set
	 */
	public void setIndicadorProveedoresRecursos(String indicadorProveedoresRecursos) {
		this.indicadorProveedoresRecursos = indicadorProveedoresRecursos;
	}

	/**
	 * @return the indicadorAccionistas
	 */
	public String getIndicadorAccionistas() {
		return indicadorAccionistas;
	}

	/**
	 * @param indicadorAccionistas the indicadorAccionistas to set
	 */
	public void setIndicadorAccionistas(String indicadorAccionistas) {
		this.indicadorAccionistas = indicadorAccionistas;
	}

	/**
	 * @return the fechaUltimaOperacion
	 */
	public String getFechaUltimaOperacion() {
		return fechaUltimaOperacion;
	}

	/**
	 * @param fechaUltimaOperacion the fechaUltimaOperacion to set
	 */
	public void setFechaUltimaOperacion(String fechaUltimaOperacion) {
		this.fechaUltimaOperacion = fechaUltimaOperacion;
	}

	/**
	 * @return the fechaUltimaModificacion
	 */
	public String getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	/**
	 * @param fechaUltimaModificacion the fechaUltimaModificacion to set
	 */
	public void setFechaUltimaModificacion(String fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	/**
	 * @return the descripcionSubestatus
	 */
	public String getDescripcionSubEstatus() {
		return descripcionSubEstatus;
	}

	/**
	 * @param descripcionSubestatus the descripcionSubestatus to set
	 */
	public void setDescripcionSubEstatus(String descripcionSubEstatus) {
		this.descripcionSubEstatus = descripcionSubEstatus;
	}	
}