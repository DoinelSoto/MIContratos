package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI38 : Indicador obligatoriedad
 *
 */
public class BeanIndicadorObligatoriedadRequest  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -5570071361754264636L;
	
	/**
	 * Bean con los campos de multicanalidad
	 */
	@Valid
	private transient MulticanalidadGenerica multicanalidad;
	
	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del Contrato de Inversión	
	 */
	@Size(max=4)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato	
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Fecha de proceso	
	 */
	@Size(max=10)
	private String fechaProceso;
	
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanIndicadorObligatoriedadRequest(){
	
		multicanalidad=new MulticanalidadGenerica();	
		
	}
	
	/**
	 * @return the tipConsulta
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipConsu the tipConsu to set
	 */
	public void setTipoConsulta(String tipConsu) {
		this.tipoConsulta = tipConsu;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param idEmpr the idEmpr to set
	 */
	public void setEmpresaContrato(String idEmpr) {
		this.empresaContrato = idEmpr;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param idCent the idCent to set
	 */
	public void setCentroContrato(String idCent) {
		this.centroContrato = idCent;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param idContr the idContr to set
	 */
	public void setNumeroContrato(String idContr) {
		this.numeroContrato = idContr;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param idProd the idProd to set
	 */
	public void setCodigoProducto(String idProd) {
		this.codigoProducto = idProd;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param idStiPro the idStiPro to set
	 */
	public void setCodigoSubProducto(String idStiPro) {
		this.codigoSubProducto = idStiPro;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}
	/**
	 * @param idEmprCo the idEmprCo to set
	 */
	public void setEmpresaContratoInversion(String idEmprCo) {
		this.empresaContratoInversion = idEmprCo;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param desAlias the desAlias to set
	 */
	public void setAliasContrato(String desAlias) {
		this.aliasContrato = desAlias;
	}
	/**
	 * @return the fechaPro
	 */
	public String getFechaProceso() {
		return fechaProceso;
	}
	/**
	 * @param fechaPro the fechaPro to set
	 */
	public void setFechaProceso(String fechaPro) {
		this.fechaProceso = fechaPro;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
}
