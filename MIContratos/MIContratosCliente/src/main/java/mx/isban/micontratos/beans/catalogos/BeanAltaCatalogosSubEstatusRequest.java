package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transaccion MI28 : Alta Sub Estatus
 *
 */
public class BeanAltaCatalogosSubEstatusRequest  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -8379255613591534602L;
	
	/**
	 * Código de estatus	
	 */
	@Size(max=2)
	private String codigoEstatus;
	/**
	 * Código de subestatus	
	 */
	@Size(max=2)
	private String codigoSubEstatus;
	/**
	 * Descripción subestatus	
	 */
	@Size(max=50)
	private String descripcionSubEstatus;
	/**
	 * @return the codEstat
	 */
	public String getCodigoEstatus() {
		return codigoEstatus;
	}
	/**
	 * @param codigoEstatus the codEstat to set
	 */
	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}
	/**
	 * @return the codSubes
	 */
	public String getCodigoSubEstatus() {
		return codigoSubEstatus;
	}
	/**
	 * @param codigoSubEstatus the codSubes to set
	 */
	public void setCodigoSubEstatus(String codigoSubEstatus) {
		this.codigoSubEstatus = codigoSubEstatus;
	}
	/**
	 * @return the desSubes
	 */
	public String getDescripcionSubEstatus() {
		return descripcionSubEstatus;
	}
	/**
	 * @param descripcionSubEstatus the desSubes to set
	 */
	public void setDescripcionSubEstatus(String descripcionSubEstatus) {
		this.descripcionSubEstatus = descripcionSubEstatus;
	}
	
	


}
