package mx.isban.micontratos.validator;

import javax.xml.ws.WebFault;

/**************************************************************
 * Querétaro, Qro Enero 2017
 *
 * La redistribución y el uso en formas fuente y binario, 
 * son responsabilidad del propietario.
 * 
 * Este software fue elaborado en @Everis
 * por Jemima Del Ángel San Martín 
 * 
 * Para mas información, consulte <www.everis.com/mexico>
 ***************************************************************/
@WebFault(name="MIContratosFault")
public class MIContratosException extends Exception {

	/**
	 * Número de versión de clase serializable
	 */
	private static final long serialVersionUID = 5623274485165697770L;
	
	private MIContratosFault fault;

	/**
	 * Constructor con causa
	 */
	public MIContratosException() {
		//Constructor con causa
	}

	/**
	 * @param fault
	 */
	protected MIContratosException(MIContratosFault fault) {
        super(fault.getFaultString()); 
        this.fault = fault;
     }
	/**
	 * 
	 * @param message
	 * @param faultInfo
	 */
	public MIContratosException(String message, MIContratosFault faultInfo){
		super(message);
        this.fault = faultInfo;
	}
	/**
	 * 
	 * @param message
	 * @param faultInfo
	 * @param cause
	 */
	public MIContratosException(String message, MIContratosFault faultInfo, Throwable cause){
		super(message,cause);
        this.fault = faultInfo;
	}
	
	/**
	 * @param message
	 */
	public MIContratosException(String message) {
		super(message);
	}
	
	/**
	 * @param message
	 * @param code
	 */
	public MIContratosException(String code, String message) {
		super(message);
		this.fault = new MIContratosFault();
	    this.fault.setFaultString(message);
	    this.fault.setFaultCode(code);
	}

	/**
	 * @param cause
	 */
	public MIContratosException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public MIContratosException(String message, Throwable cause) {
		super(message, cause);
	}
	
	/** 
	 * @return the fault
	 */
	public MIContratosFault getFaultInfo(){
		return fault;
	}

}
