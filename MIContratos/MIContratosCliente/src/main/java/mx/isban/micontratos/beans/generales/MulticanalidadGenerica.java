package mx.isban.micontratos.beans.generales;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * 
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Beans con los datos de multicanalidad 
 *
 */
public class MulticanalidadGenerica  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 1011532361847952169L;
	
	/**
	 * Empresa de conexión	
	 */
	@Size(max=4)
	private String empresaConexion;
	/**	
	 * Canal operación
	 */
	@Size(max=2)
	private String canalOperacion;
	/**
	 * Canal comercialización	
	 */
	@Size(max=2)
	private String canalComercializacion;
	
	
	/**
	 * @return the coIdEmpr
	 */
	public String getEmpresaConexion() {
		return empresaConexion;
	}
	/**
	 * @param empresaConexion the coIdEmpr to set
	 */
	public void setEmpresaConexion(String empresaConexion) {
		this.empresaConexion = empresaConexion;
	}
	/**
	 * @return the canalOpe
	 */
	public String getCanalOperacion() {
		return canalOperacion;
	}
	/**
	 * @param canalOperacion the canalOpe to set
	 */
	public void setCanalOperacion(String canalOperacion) {
		this.canalOperacion = canalOperacion;
	}
	/**
	 * @return the canalCom
	 */
	public String getCanalComercializacion() {
		return canalComercializacion;
	}
	/**
	 * @param canalComercializacion the canalCom to set
	 */
	public void setCanalComercializacion(String canalComercializacion) {
		this.canalComercializacion = canalComercializacion;
	}
	
	
}
