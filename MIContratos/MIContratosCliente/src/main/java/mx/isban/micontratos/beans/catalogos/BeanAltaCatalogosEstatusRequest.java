package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import javax.validation.constraints.Size;
/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transaccion MI25 : Alta de asesores financieros
 *
 */
public class BeanAltaCatalogosEstatusRequest  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7651473099129407931L;
	
	/**
	 * Código de estatus	
	 */
	@Size(max=2)
	private String codigoEstatus;
	/**
	 * Descripción de estatus	
	 */
	@Size(max=50)
	private String descripcionEstatus;
	/**
	 * Indicador operativo	
	 */
	@Size(max=1)
	private String indicadorOperativo;
	/**
	 * Indicador de bloqueo	
	 */
	@Size(max=1)
	private String indicadorBloqueo;
	/**
	 * Indicador estatus cancelado	
	 */
	@Size(max=1)
	private String indicadorEstatusCancelado;
	/**
	 * Indicador de bloqueo legal
	 */
	@Size(max=1)
	private String indicadorBloqueoLegal;
	/**
	 * Indicador inversiones	
	 */
	@Size(max=1)
	private String indicadorInversiones;
	/**
	 * Indicador de detalle	
	 */
	@Size(max=1)
	private String indicadorDetalle;
	/**
	 * Indicador bloqueable	
	 */
	@Size(max=1)
	private String indicadorBloqueable;
	/**
	 * Indicador de cancelable	
	 */
	@Size(max=1)
	private String indicadorCancelable;
	/**
	 * Indicador de validación de documentos	
	 */
	@Size(max=1)
	private String indicadorValidacionDocumentos;
	/**
	 * @return the codEstat
	 */
	@Size(max=1)
	public String getCodigoEstatus() {
		return codigoEstatus;
	}
	/**
	 * @param codigoEstatus the codEstat to set
	 */
	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}
	/**
	 * @return the desEstat
	 */
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}
	/**
	 * @param descripcionEstatus the desEstat to set
	 */
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	/**
	 * @return the indOpera
	 */
	public String getIndicadorOperativo() {
		return indicadorOperativo;
	}
	/**
	 * @param indicadorOperativo the indOpera to set
	 */
	public void setIndicadorOperativo(String indicadorOperativo) {
		this.indicadorOperativo = indicadorOperativo;
	}
	/**
	 * @return the indBloq
	 */
	public String getIndicadorBloqueo() {
		return indicadorBloqueo;
	}
	/**
	 * @param indicadorBloqueo the indBloq to set
	 */
	public void setIndicadorBloqueo(String indicadorBloqueo) {
		this.indicadorBloqueo = indicadorBloqueo;
	}
	/**
	 * @return the indCance
	 */
	public String getIndicadorEstatusCancelado() {
		return indicadorEstatusCancelado;
	}
	/**
	 * @param indicadorEstatusCancelado the indCance to set
	 */
	public void setIndicadorEstatusCancelado(String indicadorEstatusCancelado) {
		this.indicadorEstatusCancelado = indicadorEstatusCancelado;
	}
	/**
	 * @return the indBloLe
	 */
	public String getIndicadorBloqueoLegal() {
		return indicadorBloqueoLegal;
	}
	/**
	 * @param indicadorBloqueoLegal the indBloLe to set
	 */
	public void setIndicadorBloqueoLegal(String indicadorBloqueoLegal) {
		this.indicadorBloqueoLegal = indicadorBloqueoLegal;
	}
	/**
	 * @return the indInver
	 */
	public String getIndicadorInversiones() {
		return indicadorInversiones;
	}
	/**
	 * @param indicadorInversiones the indInver to set
	 */
	public void setIndicadorInversiones(String indicadorInversiones) {
		this.indicadorInversiones = indicadorInversiones;
	}
	/**
	 * @return the indDetal
	 */
	public String getIndicadorDetalle() {
		return indicadorDetalle;
	}
	/**
	 * @param indicadorDetalle the indDetal to set
	 */
	public void setIndicadorDetalle(String indicadorDetalle) {
		this.indicadorDetalle = indicadorDetalle;
	}
	/**
	 * @return the indBlqbl
	 */
	public String getIndicadorBloqueable() {
		return indicadorBloqueable;
	}
	/**
	 * @param indicadorBloqueable the indBlqbl to set
	 */
	public void setIndicadorBloqueable(String indicadorBloqueable) {
		this.indicadorBloqueable = indicadorBloqueable;
	}
	/**
	 * @return the indCanbl
	 */
	public String getIndicadorCancelable() {
		return indicadorCancelable;
	}
	/**
	 * @param indicadorCancelable the indCanbl to set
	 */
	public void setIndicadorCancelable(String indicadorCancelable) {
		this.indicadorCancelable = indicadorCancelable;
	}
	/**
	 * @return the indValDo
	 */
	public String getIndicadorValidacionDocumentos() {
		return indicadorValidacionDocumentos;
	}
	/**
	 * @param indicadorValidacionDocumentos the indValDo to set
	 */
	public void setIndicadorValidacionDocumentos(String indicadorValidacionDocumentos) {
		this.indicadorValidacionDocumentos = indicadorValidacionDocumentos;
	}

	

}
