package mx.isban.micontratos.beans.gestordocumental;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción : MI17 Consulta Tabla
 * Auxiliar
 * 
 */
public class BeanConsultaTablaAuxiliarGestorRequest implements Serializable {

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 159897842244095830L;

	@Valid
	private MulticanalidadGenerica multicanalidad;

	/**
	 * Tipo de consumidor
	 */
	@Size(max = 2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato
	 */
	@Size(max = 4)
	private String empresaContrato;
	/**
	 * Centro del contrato
	 */
	@Size(max = 4)
	private String centroContrato;
	/**
	 * Número contrato
	 */
	@Size(max = 12)
	private String numeroContrato;
	/**
	 * Número de producto
	 */
	@Size(max = 2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato
	 */
	@Size(max = 4)
	private String codigoSubProducto;
	/**
	 * Identificador de empresa del contrato
	 */
	@Size(max = 4)
	private String identificadorEmpresaContrato;
	/**
	 * Descripción del alias
	 */
	@Size(max = 15)
	private String descripcionAlias;
	/**
	 * Calidad de participación
	 */
	@Size(max = 2)
	private String calidadParticipacion;
	/**
	 * Número de persona
	 */
	@Size(max = 8)
	private String numeroPersona;
	/**
	 * Código de documento
	 */
	@Size(max = 8)
	private String codigoDocumento;
	/**
	 * Indicador de más datos
	 */
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;

	/**
	 * Constructor para la rellamada generica
	 */
	public BeanConsultaTablaAuxiliarGestorRequest() {
		multicanalidad = new MulticanalidadGenerica();
		rellamada = new BeanPaginacion();
	}

	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}

	/**
	 * @param tipConsu
	 *            the tipConsu to set
	 */
	public void setTipoConsulta(String tipConsu) {
		this.tipoConsulta = tipConsu;
	}

	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * @param empresaContrato
	 *            the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}

	/**
	 * @param centroContrato
	 *            the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}

	/**
	 * @return the numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * @param numeroContrato
	 *            the numeroContrato to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}

	/**
	 * @param idProd
	 *            the idProd to set
	 */
	public void setCodigoProducto(String idProd) {
		this.codigoProducto = idProd;
	}

	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}

	/**
	 * @param idStiPro
	 *            the idStiPro to set
	 */
	public void setCodigoSubProducto(String idStiPro) {
		this.codigoSubProducto = idStiPro;
	}

	/**
	 * @return the idEmprCo
	 */
	public String getIdentificadorEmpresaContrato() {
		return identificadorEmpresaContrato;
	}

	/**
	 * @param identificadorEmpresaContrato
	 *            the idEmprCo to set
	 */
	public void setIdentificadorEmpresaContrato(
			String identificadorEmpresaContrato) {
		this.identificadorEmpresaContrato = identificadorEmpresaContrato;
	}

	/**
	 * @return the desAlias
	 */
	public String getDescripcionAlias() {
		return descripcionAlias;
	}

	/**
	 * @param descripcionAlias
	 *            the desAlias to set
	 */
	public void setDescripcionAlias(String descripcionAlias) {
		this.descripcionAlias = descripcionAlias;
	}

	/**
	 * @return the peCalPar
	 */
	public String getCalidadParticipacion() {
		return calidadParticipacion;
	}

	/**
	 * @param peCalPar
	 *            the peCalPar to set
	 */
	public void setCalidadParticipacion(String peCalPar) {
		this.calidadParticipacion = peCalPar;
	}

	/**
	 * @return the peNumPer
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}

	/**
	 * @param numeroPersona
	 *            the peNumPer to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}

	/**
	 * @return the codDocum
	 */
	public String getCodigoDocumento() {
		return codigoDocumento;
	}

	/**
	 * @param codigoDocumento
	 *            the codDocum to set
	 */
	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}

	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}

	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	/**
	 * @return rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	/**
	 * @param rellamada
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

}
