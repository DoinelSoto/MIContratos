package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;


/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI08 : Consulta estado documentos
 */
public class BeanConsultaEstadoDocumentosResponse implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 6722510412956954620L;
	/**
	 * Número de ocurrencias
	 */
	private String numeroOcurrencias;
	/**
	 * Código estado documento	
	 */
	private String codigoEstadoDocumento;
	/**
	 * Descripción de estado documento
	 */
	private String descripcionEstadoDocumento;
	/**
	 * @return the numOccur
	 */
	public String getNumeroOcurrencias() {
		return numeroOcurrencias;
	}
	/**
	 * @param numOccur the numOccur to set
	 */
	public void setNumeroOcurrencias(String numOccur) {
		this.numeroOcurrencias = numOccur;
	}
	/**
	 * @return the codEsDoc
	 */
	public String getCodigoEstadoDocumento() {
		return codigoEstadoDocumento;
	}
	/**
	 * @param codEsDoc the codEsDoc to set
	 */
	public void setCodigoEstadoDocumento(String codEsDoc) {
		this.codigoEstadoDocumento = codEsDoc;
	}
	/**
	 * @return the desEsDoc
	 */
	public String getDescripcionEstadoDocumento() {
		return descripcionEstadoDocumento;
	}
	/**
	 * @param desEsDoc the desEsDoc to set
	 */
	public void setDescripcionEstadoDocumento(String desEsDoc) {
		this.descripcionEstadoDocumento = desEsDoc;
	}
}
