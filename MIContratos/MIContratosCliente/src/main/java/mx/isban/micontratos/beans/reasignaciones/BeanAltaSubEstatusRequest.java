package mx.isban.micontratos.beans.reasignaciones;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada de la transaccion MI22 : Alta de Sub Estatus
 *
 */
public class BeanAltaSubEstatusRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1823376916476496353L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	/**
	 * Tipo de consumidor
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa de Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro de Contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de Contrato
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Identificador de empresa del contrato
	 */
	@Size(max=4)
	private String identificadorEmpresaContrato;
	/**
	 * Descriptor del alias
	 */
	@Size(max=15)
	private String descripcionAlias;
	/**
	 * Código del Producto	
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código del Subproducto	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Id de cliente
	 */
	@Size(max=8)
	private String idCliente;
	/**
	 * Nuevo ejecutivo
	 */
	@Size(max=8)
	private String nuevoEjecutivo;
	/**
	 * Nuevo centro de costo del contrato
	 */
	@Size(max=4)
	private String nuevoCentroCostoContrato;
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanAltaSubEstatusRequest(){
		
		multicanalidad=new MulticanalidadGenerica();	
		
	}
	
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param idCent the idCent to set
	 */
	public void setCentroContrato(String idCent) {
		this.centroContrato = idCent;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idStiPro to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}

	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}

	/**
	 * @param tipoConsulta the tipConsu to set
	 */
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	/**
	 * @return the nueEjecu
	 */
	public String getNuevoEjecutivo() {
		return nuevoEjecutivo;
	}

	/**
	 * @param nuevoEjecutivo the nueEjecu to set
	 */
	public void setNuevoEjecutivo(String nuevoEjecutivo) {
		this.nuevoEjecutivo = nuevoEjecutivo;
	}

	/**
	 * @return the nueCenco
	 */
	public String getNuevoCentroCostoContrato() {
		return nuevoCentroCostoContrato;
	}

	/**
	 * @param nuevoCentroCostoContrato the nueCenco to set
	 */
	public void setNuevoCentroCostoContrato(String nuevoCentroCostoContrato) {
		this.nuevoCentroCostoContrato = nuevoCentroCostoContrato;
	}

	/**
	 * @return the idEmprCo
	 */
	public String getIdentificadorEmpresaContrato() {
		return identificadorEmpresaContrato;
	}

	/**
	 * @param identificadorEmpresaContrato the idEmprCo to set
	 */
	public void setIdentificadorEmpresaContrato(String identificadorEmpresaContrato) {
		this.identificadorEmpresaContrato = identificadorEmpresaContrato;
	}

	/**
	 * @return the desAlias
	 */
	public String getDescripcionAlias() {
		return descripcionAlias;
	}

	/**
	 * @param descripcionAlias the desAlias to set
	 */
	public void setDescripcionAlias(String descripcionAlias) {
		this.descripcionAlias = descripcionAlias;
	}

	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
}
