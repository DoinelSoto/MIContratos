package mx.isban.micontratos.gestionprecontratos.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaPreContratoGlobalResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanAltaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanBajaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanConsultaTablaAuxiliarResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPreContratoRequest;
import mx.isban.micontratos.beans.gestionprecontratos.BeanListaPrecontratoResultado;
import mx.isban.micontratos.beans.gestionprecontratos.BeanModificacionTablaAuxiliarRequest;

/**
 * @author everis
 *
 */

public interface BOGestionPreContratos {
	
	/**
	 * Método que hará el llamado al método que consumirá la transacción MI10 : Alta en tabla auxiliar
	 * @param beanAltaTablaAuxiliarRequestPrueba
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico altaDatosAuxiliar(BeanAltaTablaAuxiliarRequest beanAltaTablaAuxiliarRequestPrueba, ArchitechSessionBean session) throws BusinessException;


	/**
	 * Método que hará el llamado al método que consumirá la transacción MI11 : Alta de precontrato global
	 * @param beanAltaPreContratoGlobalRequestPrueba
	 * @param session
	 * @return BeanAltaPreContratoGlobalResultado
	 */
	BeanAltaPreContratoGlobalResultado altaFolioPreContrato(BeanAltaPreContratoGlobalRequest beanAltaPreContratoGlobalRequestPrueba,ArchitechSessionBean session) throws BusinessException;

	
	/**
	 * Método que hará el llamado al método que consumirá la transacción MI12 : Modificación de tabla auxiliar
	 * @param beanModificacionTablaAuxiliarRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico modificacionDatosAuxiliar(BeanModificacionTablaAuxiliarRequest beanModificacionTablaAuxiliarRequest,ArchitechSessionBean session) throws BusinessException;


	/**
	 * Método que hará el llamado al método que consumirá la transacción MI13 : Baja de tabla auxiliar
	 * @param beanBajaTablaAuxiliarRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico bajaDatosAuxiliar(BeanBajaTablaAuxiliarRequest beanBajaTablaAuxiliarRequest,ArchitechSessionBean session) throws BusinessException;


	/**
	 * Método que hará el llamado al método que consumirá la transacción MI14 : Consulta de lista de precontrato
	 * @param beanListaPreContratoRequest
	 * @param session
	 * @return BeanListaPrecontratoResultado
	 */
	BeanListaPrecontratoResultado consultaPreContratos(BeanListaPreContratoRequest beanListaPreContratoRequest, ArchitechSessionBean session) throws BusinessException;

	
	
	/**
	 * Método que hará el llamado al método que consumirá la transacción MI15 : Consulta de tabla auxiliar
	 * @param beanConsultaTablaAuxiliar
	 * @param session
	 * @return BeanConsultaTablaAuxiliarResultado
	 */
	BeanConsultaTablaAuxiliarResultado detallePreContratos(BeanConsultaTablaAuxiliarRequest beanConsultaTablaAuxiliar, ArchitechSessionBean session) throws BusinessException;

}
