package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transaccion MI18 : Alta contrato Inversión
 *
 */
public class BeanAltaContratoInversionRequest implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 4289133028558163477L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	/**
	 * Número de folio del que proviene el contrato
	 */
	private int numeroFolio;
	/**
	 * Fecha en la que se da de alta el contrato
	 */
	@Size(max=10)
	private String fechaAlta;
	
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanAltaContratoInversionRequest(){
		multicanalidad=new MulticanalidadGenerica();	
	}
	
	/**
	 * @return the numFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}
	/**
	 * @param numeroFolio the numFolio to set
	 */
	public void setNumeroFolio(int numeroFolio) {
		this.numeroFolio = numeroFolio;
	}
	/**
	 * @return the fecAlta
	 */
	public String getFechaAlta() {
		return fechaAlta;
	}
	/**
	 * @param fechaAlta the fecAlta to set
	 */
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
}
