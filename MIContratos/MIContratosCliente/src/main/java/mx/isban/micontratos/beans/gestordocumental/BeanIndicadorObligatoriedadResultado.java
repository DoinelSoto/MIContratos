package mx.isban.micontratos.beans.gestordocumental;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * Bean con los datos de respuesta para la transacción MI38 : Consulta del indicador de obligatoriedad
 * @author everis
 *
 */
public class BeanIndicadorObligatoriedadResultado extends ResponseGenerico implements Serializable{

	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 3794356375305461824L;
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanIndicadorObligatoriedadResponse> listaIndicadorObligatoriedad;
	
	/**
	 * Bean para la paginación
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Constructor para inicializar bean de paginación
	 */
	public BeanIndicadorObligatoriedadResultado() {
		rellamada= new BeanPaginacion();
	}	
	/**
	 * Devuelve una copia de listaIndicadorObligatoriedad
	 * @return the consulta
	 */
	public List<BeanIndicadorObligatoriedadResponse> getListaIndicadorObligatoriedad() {
		return new ArrayList<>(listaIndicadorObligatoriedad);
	}
	/**
	 * Guarda una copia del parametro
	 * @param listaIndicadorObligatoriedad the consulta to set
	 */
	public void setListaIndicadorObligatoriedad(List<BeanIndicadorObligatoriedadResponse> listaIndicadorObligatoriedad) {
		this.listaIndicadorObligatoriedad = new ArrayList<>(listaIndicadorObligatoriedad);
	}
	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	
}
