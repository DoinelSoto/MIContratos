package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;


/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI08 : Consulta estado documentos
 */
public class BeanConsultaEstadoDocumentosRequest implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -6561435346188273584L;
	
	/**
	 * Código estado documento
	 */
	@Size(max=2)
	private String codigoEstadoDocumento;

	 /**
	  * Bean para la paginación
	  */
	private BeanPaginacion rellamada;
	/**
	 * Constructor para inicializar instancias
	 */
	public BeanConsultaEstadoDocumentosRequest() {
		rellamada = new BeanPaginacion();
	}
	
	/**
	 * @return the codEsDoc
	 */
	public String getCodigoEstadoDocumento() {
		return codigoEstadoDocumento;
	}
	/**
	 * @param codEsDoc the codEsDoc to set
	 */
	public void setCodigoEstadoDocumento(String codEsDoc) {
		this.codigoEstadoDocumento = codEsDoc;
	}
	
	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
}
