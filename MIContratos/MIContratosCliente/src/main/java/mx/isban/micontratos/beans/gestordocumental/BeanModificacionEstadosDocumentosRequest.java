package mx.isban.micontratos.beans.gestordocumental;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI20 : MOdificación de estados de documentos
 *
 */
public class BeanModificacionEstadosDocumentosRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5566325921346059479L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	/**
	 * Tipo de consumidor
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa de Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro de Contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de Contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código del Producto	
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código del Subproducto	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Identificador de empresa del contrato
	 */
	@Size(max=4)
	private String identificadorEmpresaContrato;
	/**
	 * Descripción del alias
	 */
	@Size(max=15)
	private String descripcionAlias;
	/**
	 * Número de persona	
	 */
	@Size(max=8)
	private String numeroPersona;
	/**
	 * Calidad de Participación	
	 */
	@Size(max=2)
	private String calidadParticipacion;
	/**
	 * Código de Documento	
	 */
	@Size(max=8)
	private String codigoDocumento;
	/**
	 * Código de Estado Nuevo	
	 */
	@Size(max=2)
	private String codigoEstadoNuevo;
	/**
	 * Comentarios
	 */
	@Size(max=50)
	private String comentario;
	/**
	 * Fecha Estado de Dcoumento
	 */
	@Size(max=10)
	private String fechaEstadoDocumento;
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanModificacionEstadosDocumentosRequest(){
		
		multicanalidad=new MulticanalidadGenerica();	
		
	}
	
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idStiPro to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the peNumPer
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	/**
	 * @param numeroPersona the peNumPer to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	/**
	 * @return the peCalPar
	 */
	public String getCalidadParticipacion() {
		return calidadParticipacion;
	}
	/**
	 * @param calidadParticipacion the peCalPar to set
	 */
	public void setCalidadParticipacion(String calidadParticipacion) {
		this.calidadParticipacion = calidadParticipacion;
	}
	/**
	 * @return the codDocum
	 */
	public String getCodigoDocumento() {
		return codigoDocumento;
	}
	/**
	 * @param codigoDocumento the codDocum to set
	 */
	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}
	/**
	 * @return the codEstNu
	 */
	public String getCodigoEstadoNuevo() {
		return codigoEstadoNuevo;
	}
	/**
	 * @param codigoEstadoNuevo the codEstNu to set
	 */
	public void setCodigoEstadoNuevo(String codigoEstadoNuevo) {
		this.codigoEstadoNuevo = codigoEstadoNuevo;
	}
	/**
	 * @return the desComen
	 */
	public String getComentario() {
		return comentario;
	}
	/**
	 * @param comentario the desComen to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	/**
	 * @return the fecEstad
	 */
	public String getFechaEstadoDocumento() {
		return fechaEstadoDocumento;
	}
	/**
	 * @param fechaEstadoDocumento the fecEstad to set
	 */
	public void setFechaEstadoDocumento(String fechaEstadoDocumento) {
		this.fechaEstadoDocumento = fechaEstadoDocumento;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}

	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
	
	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}

	/**
	 * @param tipoConsulta the tipConsu to set
	 */
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	/**
	 * @return the desAlias
	 */
	public String getDescripcionAlias() {
		return descripcionAlias;
	}

	/**
	 * @param descripcionAlias the desAlias to set
	 */
	public void setDescripcionAlias(String descripcionAlias) {
		this.descripcionAlias = descripcionAlias;
	}

	/**
	 * @return the idEmprCo
	 */
	public String getIdentificadorEmpresaContrato() {
		return identificadorEmpresaContrato;
	}

	/**
	 * @param identificadorEmpresaContrato the idEmprCo to set
	 */
	public void setIdentificadorEmpresaContrato(String identificadorEmpresaContrato) {
		this.identificadorEmpresaContrato = identificadorEmpresaContrato;
	}


}
