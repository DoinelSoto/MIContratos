package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida e implementación de la interfaz BeanResultBO para la transaccion : MI04	Catálogo de Estatus Operativos
 *
 */
public class BeanCatalogoEstatusOperativoResultado  extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -566317604811269720L;
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanCatalogoEstatusOperativoResponse> listaCatalogoEstatusOperativo;
	
	
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanCatalogoEstatusOperativoResultado(){
		rellamada = new BeanPaginacion();
	}
	
	/**
	 * @return the consulta
	 */
	public List<BeanCatalogoEstatusOperativoResponse> getListaCatalogoEstatusOperativo() {
		return new ArrayList<>(listaCatalogoEstatusOperativo);
	}

	/**
	 * @param listaCatalogoEstatusOperativo the consulta to set
	 */
	public void setListaCatalogoEstatusOperativo(List<BeanCatalogoEstatusOperativoResponse> listaCatalogoEstatusOperativo) {
		this.listaCatalogoEstatusOperativo=new ArrayList<>(listaCatalogoEstatusOperativo);
	}

	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	

}
