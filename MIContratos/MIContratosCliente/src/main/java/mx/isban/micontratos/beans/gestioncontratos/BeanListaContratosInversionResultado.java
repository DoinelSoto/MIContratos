package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * 
 * @author Luis Alberto Martínez, Everis
 *
 */

public class BeanListaContratosInversionResultado extends ResponseGenerico implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7615905915009387998L;
	/**
	 * Bean para la paginación
	 */
	private BeanPaginacion rellamada;
	/**
	 * Lista de contratos de inversion
	 */
	private List<BeanListaContratosInversionResponse> listaContratoInversion;
	
	/**
	 * Constructor para inicializar bean de paginación
	 * 		y lista de contratos de inversión
	 */
	public BeanListaContratosInversionResultado() {
		rellamada= new BeanPaginacion();
		listaContratoInversion= new ArrayList<>();
	}

	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

	/**
	 * @return the listaContratoInversion
	 */
	public List<BeanListaContratosInversionResponse> getListaContratoInversion() {
		return new ArrayList<>(listaContratoInversion);
	}

	/**
	 * @param listaContratoInversion the listaContratoInversion to set
	 */
	public void setListaContratoInversion(
			List<BeanListaContratosInversionResponse> listaContratoInversion) {
		this.listaContratoInversion = new ArrayList<>(listaContratoInversion);
	}
	

}
