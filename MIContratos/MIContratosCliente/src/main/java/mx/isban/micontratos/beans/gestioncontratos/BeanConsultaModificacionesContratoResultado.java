package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI24 : Consulta modificaciones contrato
 *
 */
public class BeanConsultaModificacionesContratoResultado extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -3194002029732129892L;
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanConsultaModificacionesContratoResponse> listaModificacionesContrato;
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Se instancia la clase y se inicializa rellamada
	 */
	public BeanConsultaModificacionesContratoResultado() {
		rellamada= new BeanPaginacion();
	}
	/**
	 * @return the consulta
	 */
	public List<BeanConsultaModificacionesContratoResponse> getListaModificacionesContrato() {
		return new ArrayList<>(listaModificacionesContrato);
	}
	/**
	 * @param consulta the consulta to set
	 */
	public void setListaModificacionesContrato(List<BeanConsultaModificacionesContratoResponse> consulta) {
		this.listaModificacionesContrato = new ArrayList<>(consulta);
	}
	/**
	 * @return BeanPaginacion
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	/**
	 * @param rellamada
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
}
