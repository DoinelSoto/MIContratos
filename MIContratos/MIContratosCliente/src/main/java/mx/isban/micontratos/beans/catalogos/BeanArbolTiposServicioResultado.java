package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author everis
 *
 */

/**
 * Bean con los datos de respuesta e implementación de la interfaz BeanResultBO para la transaccion : 	MI07	Árbol Tipos de servicio
 * */
public class BeanArbolTiposServicioResultado extends ResponseGenerico implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 450390788559360019L;
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;

	/**
	 * Lista de beans BeanArbolTiposServicioResponse
	 */
	private List<BeanArbolTiposServicioResponse> listaArbolTiposServicio;
	/**
	 * Class BeanArbolTiposServicioResultado
	 */
	
	public BeanArbolTiposServicioResultado(){
		rellamada = new BeanPaginacion();
	}
	

	/**
	 * @return the consulta
	 */
	public List<BeanArbolTiposServicioResponse> getListaArbolTiposServicio() {
		return new ArrayList<> (listaArbolTiposServicio);
	}

	/**
	 * @param listaArbolTiposServicio the consulta to set
	 */
	public void setListaArbolTiposServicio(List<BeanArbolTiposServicioResponse> listaArbolTiposServicio) {
		this.listaArbolTiposServicio = new ArrayList<> (listaArbolTiposServicio);
	}

	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
}
