package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * 
 * @author Luis Martínez - everis
 * Clase con los campos de entrada del WS : Consulta del tipo de servicio de un cliente
 *
 */
public class BeanTipoNecesidadAFIRequest  implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7898793320816915187L;
	
	/**
	 * Código de estatus	
	 */
	@Size(max=8)
	private String numeroPersona;
	
	/**
	 * Bean de multicanalidada	
	 */
	private MulticanalidadGenerica multicanalidad;
		
	/**
	 * 
	 * @return the numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	
	/**
	 * 
	 * @param numeroPersona
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @param multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}

}