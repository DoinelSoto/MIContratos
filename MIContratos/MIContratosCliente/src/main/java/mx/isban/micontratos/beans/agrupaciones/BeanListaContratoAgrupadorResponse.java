package mx.isban.micontratos.beans.agrupaciones;

import java.io.Serializable;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de respuesta para la transacción MI32 : Transaccion Lista contrato Agrupador
 *
 */
public class BeanListaContratoAgrupadorResponse  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 5587484780588798282L;
	

	/**
	 * Empresa del Contrato	
	 */
	private String empresaContrato;
	/**
	 * Centro del Contrato	
	 */
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	private String numeroContrato;
	/**
	 * Código de producto del contrato	
	 */
	private String codigoProductoContrato;
	/**
	 * Código de subproducto del contrato	
	 */
	private String codigoSubProductoContrato;
	/**
	 * empresa contrato inversión
	 */
	private String empresaContratoInversion;
	/**
	 * alias de contrato
	 */
	private String aliasContrato;
	/**
	 * Código de ejecutivo de la agrupación	
	 */
	private String codigoEjecutivo;
	/**
	 * Número telefónico;
	 */
	private String numeroTelefonico;
	/**
	 * Moneda del grupo;	
	 */
	private String modedaGrupo;
	/**
	 * Nombre del grupo;	
	 */
	private String nombreGrupo;
	/**
	 * Descripción del grupo;	
	 */
	private String descripcionGrupo;
	/**
	 * Indicador de tipo de grupo;
	 */
	private String indicadorTipoGrupo;
	/**
	 * Código del grupo de usuarios;	
	 */
	private String identificadorGrupoUsuario;
	/**
	 * Fecha de comienzo de grupo;	
	 */
	private String fechaComienzoGrupo;
	/**
	 * Fecha de fin de grupo;	
	 */
	private String fechaFinGrupo;
	/**
	 * Indicador de uso del grupo;
	 */
	private String indicadorUsoGrupo;
	/**
	 * @return the codigoSubProductoContrato
	 */
	public String getCodigoSubProductoContrato() {
		return codigoSubProductoContrato;
	}
	/**
	 * @param codigoSubProductoContrato the codigoSubProductoContrato to set
	 */
	public void setCodigoSubProductoContrato(String codigoSubProductoContrato) {
		this.codigoSubProductoContrato = codigoSubProductoContrato;
	}
	/**
	 * @return the codigoProductoContrato
	 */
	public String getCodigoProductoContrato() {
		return codigoProductoContrato;
	}
	/**
	 * @param codigoProductoContrato the codigoProductoContrato to set
	 */
	public void setCodigoProductoContrato(String codigoProductoContrato) {
		this.codigoProductoContrato = codigoProductoContrato;
	}
	/**
	 * @return the numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the numeroContrato to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the empresaContrato to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the centroContrato
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the centroContrato to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the codEjecu
	 */
	public String getCodigoEjecutivo() {
		return codigoEjecutivo;
	}
	/**
	 * @param codigoEjecutivo the codEjecu to set
	 */
	public void setCodigoEjecutivo(String codigoEjecutivo) {
		this.codigoEjecutivo = codigoEjecutivo;
	}
	/**
	 * @return the peNumTel
	 */
	public String getNumeroTelefonico() {
		return numeroTelefonico;
	}
	/**
	 * @param numeroTelefonico the peNumTel to set
	 */
	public void setNumeroTelefonico(String numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}
	/**
	 * @return the codMonSw
	 */
	public String getModedaGrupo() {
		return modedaGrupo;
	}
	/**
	 * @param modedaGrupo the codMonSw to set
	 */
	public void setModedaGrupo(String modedaGrupo) {
		this.modedaGrupo = modedaGrupo;
	}
	/**
	 * @return the nomGrupo
	 */
	public String getNombreGrupo() {
		return nombreGrupo;
	}
	/**
	 * @param nombreGrupo the nomGrupo to set
	 */
	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}
	/**
	 * @return the desGrupo
	 */
	public String getDescripcionGrupo() {
		return descripcionGrupo;
	}
	/**
	 * @param descripcionGrupo the desGrupo to set
	 */
	public void setDescripcionGrupo(String descripcionGrupo) {
		this.descripcionGrupo = descripcionGrupo;
	}
	/**
	 * @return the indTiGru
	 */
	public String getIndicadorTipoGrupo() {
		return indicadorTipoGrupo;
	}
	/**
	 * @param indicadorTipoGrupo the indTiGru to set
	 */
	public void setIndicadorTipoGrupo(String indicadorTipoGrupo) {
		this.indicadorTipoGrupo = indicadorTipoGrupo;
	}
	/**
	 * @return the idGruUsu
	 */
	public String getIdentificadorGrupoUsuario() {
		return identificadorGrupoUsuario;
	}
	/**
	 * @param identificadorGrupoUsuario the idGruUsu to set
	 */
	public void setIdentificadorGrupoUsuario(String identificadorGrupoUsuario) {
		this.identificadorGrupoUsuario = identificadorGrupoUsuario;
	}
	/**
	 * @return the feComVal
	 */
	public String getFechaComienzoGrupo() {
		return fechaComienzoGrupo;
	}
	/**
	 * @param fechaComienzoGrupo the feComVal to set
	 */
	public void setFechaComienzoGrupo(String fechaComienzoGrupo) {
		this.fechaComienzoGrupo = fechaComienzoGrupo;
	}
	/**
	 * @return the feFinVal
	 */
	public String getFechaFinGrupo() {
		return fechaFinGrupo;
	}
	/**
	 * @param fechaFinGrupo the feFinVal to set
	 */
	public void setFechaFinGrupo(String fechaFinGrupo) {
		this.fechaFinGrupo = fechaFinGrupo;
	}
	/**
	 * @return the indUsoGr
	 */
	public String getIndicadorUsoGrupo() {
		return indicadorUsoGrupo;
	}
	/**
	 * @param indicadorUsoGrupo the indUsoGr to set
	 */
	public void setIndicadorUsoGrupo(String indicadorUsoGrupo) {
		this.indicadorUsoGrupo = indicadorUsoGrupo;
	}
	/**
	 * @return the empresaContratoInversion
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}
	/**
	 * @param empresaContratoInversion the empresaContratoInversion to set
	 */
	public void setEmpresaContratoInversion(String empresaContratoInversion) {
		this.empresaContratoInversion = empresaContratoInversion;
	}
	/**
	 * @return the aliasContrato
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param aliasContrato the aliasContrato to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;

	}
	
	
	

}
