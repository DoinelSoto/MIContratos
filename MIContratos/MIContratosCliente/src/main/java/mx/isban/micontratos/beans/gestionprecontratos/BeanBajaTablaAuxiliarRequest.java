package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI13 : Baja Tabla Auxiliar
 *
 */
public class BeanBajaTablaAuxiliarRequest implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 5308518258154008358L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	
	
	/**
	 * Número de folio del pre contrato	
	 */
	private int numeroFolio;
	/**
	 * Código de tipo de registro guardado	
	 */
	@Size(max=2)
	private String codigoTipoRegistroGuardado;
	/**
	 * Secuencial por tipo de registro	
	 */
	@Size(max=3)
	private String secuencialTipoRegistro;
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanBajaTablaAuxiliarRequest(){
		
		multicanalidad=new MulticanalidadGenerica();	
		
	}
	
	/**
	 * @return the numFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}
	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumeroFolio(int numFolio) {
		this.numeroFolio = numFolio;
	}
	/**
	 * @return the codTipRe
	 */
	public String getCodigoTipoRegistroGuardado() {
		return codigoTipoRegistroGuardado;
	}
	/**
	 * @param codTipRe the codTipRe to set
	 */
	public void setCodigoTipoRegistroGuardado(String codTipRe) {
		this.codigoTipoRegistroGuardado = codTipRe;
	}
	/**
	 * @return the nuSecuen
	 */
	public String getSecuencialTipoRegistro() {
		return secuencialTipoRegistro;
	}
	/**
	 * @param nuSecuen the nuSecuen to set
	 */
	public void setSecuencialTipoRegistro(String nuSecuen) {
		this.secuencialTipoRegistro = nuSecuen;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
}
