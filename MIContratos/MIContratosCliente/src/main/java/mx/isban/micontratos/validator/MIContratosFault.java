package mx.isban.micontratos.validator;

/**************************************************************
* Querétaro, Qro Marzo 2017
*
* La redistribución y el uso en formas fuente y binario, 
* son responsabilidad del propietario.
* 
* Este software fue elaborado en @Everis
* por Jemima Del Ángel San Martín 
* 
* Para mas información, consulte <www.everis.com/mexico>
***************************************************************/
public class MIContratosFault {

	/**
	 * Fault Code
	 */
	 private String faultCode;
	 /**
	  * Fault String
	  */
     private String faultString;
	/**
	 * @param faultCode
	 * @param faultString
	 */
	public MIContratosFault(String faultCode, String faultString) {
		super();
		this.faultCode = faultCode;
		this.faultString = faultString;
	}
	/**
	 * Constructor vacío
	 */
	public MIContratosFault() {
		//Constructor vacío
	}
	/**
	 * @return the faultCode
	 */
	public String getFaultCode() {
		return faultCode;
	}
	/**
	 * @param faultCode the faultCode to set
	 */
	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}
	/**
	 * @return the faultString
	 */
	public String getFaultString() {
		return faultString;
	}
	/**
	 * @param faultString the faultString to set
	 */
	public void setFaultString(String faultString) {
		this.faultString = faultString;
	}
}
