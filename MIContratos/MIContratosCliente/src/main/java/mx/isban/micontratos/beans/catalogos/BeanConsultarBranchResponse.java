package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

/**
 * @author everis
 *
 */

/**  
 *	Bean con los datos de salida e implementación de la interfaz BeanResultBO para la transacción : MI39
 **/
public class BeanConsultarBranchResponse implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 1853558264142761391L;
	
	
	private String codigoBranch;
	
	private String codigoEmpresa;
	
	private String descripcionEmpresa;
	
	/**
	 * @return the codigoBranch
	 */
	public String getCodigoBranch() {
		return codigoBranch;
	}

	/**
	 * @param codigoBranch the codigoBranch to set
	 */
	public void setCodigoBranch(String codigoBranch) {
		this.codigoBranch = codigoBranch;
	}

	/**
	 * @return the codigoEmpresa
	 */
	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}

	/**
	 * @param codigoEmpresa the codigoEmpresa to set
	 */
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	/**
	 * @return the descripcionEmpresa
	 */
	public String getDescripcionEmpresa() {
		return descripcionEmpresa;
	}

	/**
	 * @param descripcionEmpresa the descripcionEmpresa to set
	 */
	public void setDescripcionEmpresa(String descripcionEmpresa) {
		this.descripcionEmpresa = descripcionEmpresa;
	}
}