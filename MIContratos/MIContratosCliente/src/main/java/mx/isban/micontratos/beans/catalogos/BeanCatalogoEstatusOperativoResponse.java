package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;


/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salidapara la transaccion : MI04	Catálogo de Estatus Operativos
 *
 */
public class BeanCatalogoEstatusOperativoResponse  implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -566317604811269720L;
	

	/**
	 * Código de estatus
	 */
	private String codigoEstatus;
	/**
	 * Descripción de estatus
	 */
	private String descripcionEstatus;
	/**
	 * Indicador operativo
	 */
	private String indicadorOperativo;
	/**
	 * Indicador de bloqueo
	 */
	private String indicadorBloqueo;
	/**
	 * Indicador estatus cancelado
	 */
	private String indicadorCancelacion;
	/**
	 * Indicador de bloqueo legal
	 */
	private String indicadorBloqueoLegal;
	/**
	 * Indicador inversiones
	 */
	private String indicadorInversion;
	/**
	 * Indicador de detalle
	 */
	private String indicadorDetalle;
	/**
	 * Indicador bloqueable
	 */
	private String indicadorBloqueable;
	/**
	 * Indicador de cancelable
	 */
	private String indicadorCancelable;
	/**
	 * Indicador de validación de documentos
	 */
	private String indicadorValidacion;
	/**
	 * @return the codEstat
	 */
	public String getCodigoEstatus() {
		return codigoEstatus;
	}
	/**
	 * @param codigoEstatus the codEstat to set
	 */
	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}
	/**
	 * @return the desEstat
	 */
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}
	/**
	 * @param descripcionEstatus the desEstat to set
	 */
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	/**
	 * @return the indOpera
	 */
	public String getIndicadorOperativo() {
		return indicadorOperativo;
	}
	/**
	 * @param indicadorOperativo the indOpera to set
	 */
	public void setIndicadorOperativo(String indicadorOperativo) {
		this.indicadorOperativo = indicadorOperativo;
	}
	/**
	 * @return the indBloq
	 */
	public String getIndicadorBloqueo() {
		return indicadorBloqueo;
	}
	/**
	 * @param indicadorBloqueo the indBloq to set
	 */
	public void setIndicadorBloqueo(String indicadorBloqueo) {
		this.indicadorBloqueo = indicadorBloqueo;
	}
	/**
	 * @return the indCance
	 */
	public String getIndicadorCancelacion() {
		return indicadorCancelacion;
	}
	/**
	 * @param indicadorCancelacion the indCance to set
	 */
	public void setIndicadorCancelacion(String indicadorCancelacion) {
		this.indicadorCancelacion = indicadorCancelacion;
	}
	/**
	 * @return the indBloLe
	 */
	public String getIndicadorBloqueoLegal() {
		return indicadorBloqueoLegal;
	}
	/**
	 * @param indicadorBloqueoLegal the indBloLe to set
	 */
	public void setIndicadorBloqueoLegal(String indicadorBloqueoLegal) {
		this.indicadorBloqueoLegal = indicadorBloqueoLegal;
	}
	/**
	 * @return the indInver
	 */
	public String getIndicadorInversion() {
		return indicadorInversion;
	}
	/**
	 * @param indicadorInversion the indInver to set
	 */
	public void setIndicadorInversion(String indicadorInversion) {
		this.indicadorInversion = indicadorInversion;
	}
	/**
	 * @return the indDetal
	 */
	public String getIndicadorDetalle() {
		return indicadorDetalle;
	}
	/**
	 * @param indicadorDetalle the indDetal to set
	 */
	public void setIndicadorDetalle(String indicadorDetalle) {
		this.indicadorDetalle = indicadorDetalle;
	}
	/**
	 * @return the indBlqbl
	 */
	public String getIndicadorBloqueable() {
		return indicadorBloqueable;
	}
	/**
	 * @param indicadorBloqueable the indBlqbl to set
	 */
	public void setIndicadorBloqueable(String indicadorBloqueable) {
		this.indicadorBloqueable = indicadorBloqueable;
	}
	/**
	 * @return the indCanbl
	 */
	public String getIndicadorCancelable() {
		return indicadorCancelable;
	}
	/**
	 * @param indicadorCancelable the indCanbl to set
	 */
	public void setIndicadorCancelable(String indicadorCancelable) {
		this.indicadorCancelable = indicadorCancelable;
	}
	/**
	 * @return the indValDo
	 */
	public String getIndicadorValidacion() {
		return indicadorValidacion;
	}
	/**
	 * @param indicadorValidacion the indValDo to set
	 */
	public void setIndicadorValidacion(String indicadorValidacion) {
		this.indicadorValidacion = indicadorValidacion;
	}

	
}
