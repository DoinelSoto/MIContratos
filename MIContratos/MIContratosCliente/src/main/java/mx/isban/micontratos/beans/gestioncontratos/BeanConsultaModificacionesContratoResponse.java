package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI24 : Consulta modificaciones contrato
 *
 */
public class BeanConsultaModificacionesContratoResponse implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 5781134793655198716L;
	
	/**
	 * Fecha comienzo valor	
	 */
	private String fechaComienzoValor;
	/**
	 * Fecha fin valor
	 */
	private String fechaFinValor;
	/**
	 * Código de banca del contrato antigua	
	 */
	private String codigoBancaContratoAntiguo;
	/**
	 * Código de banca del contrato nueva	
	 */
	private String codigoBancaContratoNuevo;
	/**
	 * Alias del contrato antiguo
	 */
	private String aliasContratoAntiguo;
	/**
	 * Alias del contrato nuevo
	 */
	private String aliasContratoNuevo;
	/**
	 * Fecha de vencimiento del contrato antiguo	
	 */
	private String fechaVencimientoContratoAntiguo;
	/**
	 * Fecha de vencimiento del contrato nuevo	
	 */
	private String fechaVencimientoContratoNuevo;
	/**
	 * Código de ejecutivo del contrato antiguo
	 */
	private String codigoEjecutivoAntiguo;
	/**
	 * Código de ejecutivo del contrato nuevo
	 */
	private String codigoEjecutivoNuevo;
	/**
	 * Centro de costos del contrato antiguo	
	 */
	private String centroCostosContratoAntiguo;
	/**
	 * Centro de costos del contrato nuevo
	 */
	private String centroCostosContratoNuevo;
	/**
	 * Indicador de tipo de cuenta antigua	
	 */
	private String indicadorTipoCuentaAntigua;
	/**
	 * Indicador de tipo de cuenta nueva	
	 */
	private String indicadorTipoCuentaNueva;
	/**
	 * Indicador de tipo de Contrato antiguo	
	 */
	private String indicadorTipoContratoAntiguo;
	/**
	 * Indicador de tipo de Contrato nuevo	
	 */
	private String indicadorTipoContratoNuevo;
	/**
	 * Indicador autorización posición en corto en capitales antiguo	
	 */
	private String indicadorAutorizacionPosicionCapitalesAntiguo;
	/**
	 * Indicador autorización posición en corto en capitales nuevo	
	 */
	private String indicadorAutorizacionPosicionCapitalesNuevo;
	/**
	 * Indicador autorización posición en corto en fondos antiguo	
	 */
	private String indicadorAutorizacionPosicionFondosAntiguo;
	/**
	 * Indicador autorización posición en corto en fondos nuevo	
	 */
	private String indicadorAutorizacionPosicionFondosNuevo;
	/**
	 * Indicador autorización cruce en fondos de inversión antiguo	
	 */
	private String indicadorCruceFondosInversionAntiguo;
	/**
	 * Indicador autorización cruce en fondos de inversión nuevo	
	 */	
	private String indicadorCruceFondosInversionNuevo;
	/**
	 * Indicador de discrecionalidad antiguo
	 */
	private String indicadorDiscrecionalidadAntiguo;
	/**
	 * Indicador de discrecionalidad nuevo	
	 */
	private String indicadorDiscrecionalidadNuevo;
	/**
	 * Indicador tipo de custodia en directo antiguo	
	 */
	private String indicadorTipoCustodiaDirectoAntiguo;
	/**
	 * Indicador tipo de custodia en directo nuevo
	 */
	private String indicadorTipoCustodiaDirectoNuevo;
	/**
	 * Indicador tipo de custodia en Repos antiguo	
	 */
	private String indicadorTipoCustodiaReposAntiguo;
	/**
	 * Indicador tipo de custodia en Repos nuevo	
	 */
	private String indicadorTipoCustodiaReposNuevo;
	/**
	 * Indicador de estado de cuenta antigua	
	 */
	private String indicadorEstadoCuentaAntigua;
	/**
	 * Indicador de estado de cuenta nueva
	 */
	private String indicadorEstadoCuentaNueva;
	/**
	 * Monto Máximo a operar en el contrato antiguo
	 */
	private String importeMaximoOperacionAntiguo;
	/**
	 * Divisa del Monto Máximo a operar en el contrato antiguo
	 */
	private String divisaMontoMaximoOperacionContratoAntiguo;
	/**
	 * Monto Máximo a operar en el contrato nuevo	
	 */
	private String importeMaximoOperacionNuevo;
	/**
	 * Divisa del Monto Máximo a operar en el contrato nuevo	
	 */
	private String 	divisaMontoMaximoOperacionContratoNuevo;
	/**
	 * Indicador envío por correo carta confirmación capitales antiguo	
	 */
	private String indicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo;
	/**
	 * Indicador envío por correo carta confirmación capitales nuevo	
	 */
	private String indicadorEnvioCorreoCartaConfirmacionCapitalesNuevo;
	/**
	 * Código de estatus antiguo
	 */
	private String codigoEstatusAntiguo;
	/**
	 * Código de estatus nuevo
	 */
	private String codigoEstatusNuevo;
	/**
	 * Detalle del estatus antiguo
	 */
	private String detalleEstatusAntiguo;
	/**
	 * Detalle del estatus nuevo	
	 */
	private String detalleEstatusNuevo;
	/**
	 * Indicador de Contrato Agrupado antiguo
	 */
	private String indicadorContratoAgrupadoAntiguo;
	/**
	 * Indicador de Contrato Agrupado nuevo	
	 */
	private String indicadorContratoAgrupadoNuevo;
	/**
	 * Indicador de Tipo de Posición antiguo	
	 */
	private String indicadorTipoPosicionAntiguo;
	/**
	 * Indicador de Tipo de Posición nuevo	
	 */
	private String indicadorTipoPosicionNuevo;
	/**
	 * Indicador Contrato “Espejo” antiguo	
	 */
	private String indicadorContratoEspejoAntiguo;
	/**
	 * Indicador Contrato “Espejo” nuevo	
	 */
	private String indicadorContratoEspejoNuevo;
	/**
	 * Indicador contrato amparado por una chequera antiguo	
	 */
	private String indicadorContratoAmparadoChequeraAntiguo;
	/**
	 * Indicador contrato amparado por una chequera nuevo	
	 */
	private String indicadorContratoAmparadoChequeraNuevo;
	/**
	 * Código de tipo de servicio antiguo	
	 */
	private String codigoTipoServicioAntiguo;
	/**
	 * Código de tipo de servicio nuevo	
	 */
	private String codigoTipoServicioNuevo;
	/**
	 * Código AFI antiguo	
	 */
	private String codigoAsesorFinancieroAntiguo;
	/**
	 * Código AFI nuevo	
	 */
	private String codigoAsesorFinancieroNuevo;
	/**
	 * @return the fechaComienzoValor
	 */
	public String getFechaComienzoValor() {
		return fechaComienzoValor;
	}
	/**
	 * @param fechaComienzoValor the fechaComienzoValor to set
	 */
	public void setFechaComienzoValor(String fechaComienzoValor) {
		this.fechaComienzoValor = fechaComienzoValor;
	}
	/**
	 * @return the fechaFinValor
	 */
	public String getFechaFinValor() {
		return fechaFinValor;
	}
	/**
	 * @param fechaFinValor the fechaFinValor to set
	 */
	public void setFechaFinValor(String fechaFinValor) {
		this.fechaFinValor = fechaFinValor;
	}
	/**
	 * @return the codigoBancaContratoAntiguo
	 */
	public String getCodigoBancaContratoAntiguo() {
		return codigoBancaContratoAntiguo;
	}
	/**
	 * @param codigoBancaContratoAntiguo the codigoBancaContratoAntiguo to set
	 */
	public void setCodigoBancaContratoAntiguo(String codigoBancaContratoAntiguo) {
		this.codigoBancaContratoAntiguo = codigoBancaContratoAntiguo;
	}
	/**
	 * @return the codigoBancaContratoNuevo
	 */
	public String getCodigoBancaContratoNuevo() {
		return codigoBancaContratoNuevo;
	}
	/**
	 * @param codigoBancaContratoNuevo the codigoBancaContratoNuevo to set
	 */
	public void setCodigoBancaContratoNuevo(String codigoBancaContratoNuevo) {
		this.codigoBancaContratoNuevo = codigoBancaContratoNuevo;
	}
	/**
	 * @return the descripcionAlias
	 */
	public String getAliasContratoAntiguo() {
		return aliasContratoAntiguo;
	}
	/**
	 * @param descripcionAlias the descripcionAlias to set
	 */
	public void setAliasContratoAntiguo(String aliasContratoAntiguo) {
		this.aliasContratoAntiguo = aliasContratoAntiguo;
	}
	/**
	 * @return the aliasContratoNuevo
	 */
	public String getAliasContratoNuevo() {
		return aliasContratoNuevo;
	}
	/**
	 * @param aliasContratoNuevo the aliasContratoNuevo to set
	 */
	public void setAliasContratoNuevo(String aliasContratoNuevo) {
		this.aliasContratoNuevo = aliasContratoNuevo;
	}
	/**
	 * @return the fechaVencimientoContratoAntiguo
	 */
	public String getFechaVencimientoContratoAntiguo() {
		return fechaVencimientoContratoAntiguo;
	}
	/**
	 * @param fechaVencimientoContratoAntiguo the fechaVencimientoContratoAntiguo to set
	 */
	public void setFechaVencimientoContratoAntiguo(
			String fechaVencimientoContratoAntiguo) {
		this.fechaVencimientoContratoAntiguo = fechaVencimientoContratoAntiguo;
	}
	/**
	 * @return the fechaVencimientoContratoNuevo
	 */
	public String getFechaVencimientoContratoNuevo() {
		return fechaVencimientoContratoNuevo;
	}
	/**
	 * @param fechaVencimientoContratoNuevo the fechaVencimientoContratoNuevo to set
	 */
	public void setFechaVencimientoContratoNuevo(
			String fechaVencimientoContratoNuevo) {
		this.fechaVencimientoContratoNuevo = fechaVencimientoContratoNuevo;
	}
	/**
	 * @return the codigoEjecutivoAntiguo
	 */
	public String getCodigoEjecutivoAntiguo() {
		return codigoEjecutivoAntiguo;
	}
	/**
	 * @param codigoEjecutivoAntiguo the codigoEjecutivoAntiguo to set
	 */
	public void setCodigoEjecutivoAntiguo(String codigoEjecutivoAntiguo) {
		this.codigoEjecutivoAntiguo = codigoEjecutivoAntiguo;
	}
	/**
	 * @return the codigoEjecutivoNuevo
	 */
	public String getCodigoEjecutivoNuevo() {
		return codigoEjecutivoNuevo;
	}
	/**
	 * @param codigoEjecutivoNuevo the codigoEjecutivoNuevo to set
	 */
	public void setCodigoEjecutivoNuevo(String codigoEjecutivoNuevo) {
		this.codigoEjecutivoNuevo = codigoEjecutivoNuevo;
	}
	/**
	 * @return the centroCostosContratoAntiguo
	 */
	public String getCentroCostosContratoAntiguo() {
		return centroCostosContratoAntiguo;
	}
	/**
	 * @param centroCostosContratoAntiguo the centroCostosContratoAntiguo to set
	 */
	public void setCentroCostosContratoAntiguo(String centroCostosContratoAntiguo) {
		this.centroCostosContratoAntiguo = centroCostosContratoAntiguo;
	}
	/**
	 * @return the centroCostosContactoNuevo
	 */
	public String getCentroCostosContratoNuevo() {
		return centroCostosContratoNuevo;
	}
	/**
	 * @param centroCostosContactoNuevo the centroCostosContactoNuevo to set
	 */
	public void setCentroCostosContratoNuevo(String centroCostosContactoNuevo) {
		this.centroCostosContratoNuevo = centroCostosContactoNuevo;
	}
	/**
	 * @return the indicadorTipoCuentaAntigua
	 */
	public String getIndicadorTipoCuentaAntigua() {
		return indicadorTipoCuentaAntigua;
	}
	/**
	 * @param indicadorTipoCuentaAntigua the indicadorTipoCuentaAntigua to set
	 */
	public void setIndicadorTipoCuentaAntigua(String indicadorTipoCuentaAntigua) {
		this.indicadorTipoCuentaAntigua = indicadorTipoCuentaAntigua;
	}
	/**
	 * @return the indicadorTipoCuentaNueva
	 */
	public String getIndicadorTipoCuentaNueva() {
		return indicadorTipoCuentaNueva;
	}
	/**
	 * @param indicadorTipoCuentaNueva the indicadorTipoCuentaNueva to set
	 */
	public void setIndicadorTipoCuentaNueva(String indicadorTipoCuentaNueva) {
		this.indicadorTipoCuentaNueva = indicadorTipoCuentaNueva;
	}
	/**
	 * @return the indicadorTipoContratoAntiguo
	 */
	public String getIndicadorTipoContratoAntiguo() {
		return indicadorTipoContratoAntiguo;
	}
	/**
	 * @param indicadorTipoContratoAntiguo the indicadorTipoContratoAntiguo to set
	 */
	public void setIndicadorTipoContratoAntiguo(String indicadorTipoContratoAntiguo) {
		this.indicadorTipoContratoAntiguo = indicadorTipoContratoAntiguo;
	}
	/**
	 * @return the indicadorTipoContratoNuevo
	 */
	public String getIndicadorTipoContratoNuevo() {
		return indicadorTipoContratoNuevo;
	}
	/**
	 * @param indicadorTipoContratoNuevo the indicadorTipoContratoNuevo to set
	 */
	public void setIndicadorTipoContratoNuevo(String indicadorTipoContratoNuevo) {
		this.indicadorTipoContratoNuevo = indicadorTipoContratoNuevo;
	}
	/**
	 * @return the indicadorAutorizacionPosicionCapitalesAntiguo
	 */
	public String getIndicadorAutorizacionPosicionCapitalesAntiguo() {
		return indicadorAutorizacionPosicionCapitalesAntiguo;
	}
	/**
	 * @param indicadorAutorizacionPosicionCapitalesAntiguo the indicadorAutorizacionPosicionCapitalesAntiguo to set
	 */
	public void setIndicadorAutorizacionPosicionCapitalesAntiguo(
			String indicadorAutorizacionPosicionCapitalesAntiguo) {
		this.indicadorAutorizacionPosicionCapitalesAntiguo = indicadorAutorizacionPosicionCapitalesAntiguo;
	}
	/**
	 * @return the indicadorAutorizacionPosicionCapitalesNuevo
	 */
	public String getIndicadorAutorizacionPosicionCapitalesNuevo() {
		return indicadorAutorizacionPosicionCapitalesNuevo;
	}
	/**
	 * @param indicadorAutorizacionPosicionCapitalesNuevo the indicadorAutorizacionPosicionCapitalesNuevo to set
	 */
	public void setIndicadorAutorizacionPosicionCapitalesNuevo(
			String indicadorAutorizacionPosicionCapitalesNuevo) {
		this.indicadorAutorizacionPosicionCapitalesNuevo = indicadorAutorizacionPosicionCapitalesNuevo;
	}
	/**
	 * @return the indicadorAutorizacionPosicionFondosAntiguo
	 */
	public String getIndicadorAutorizacionPosicionFondosAntiguo() {
		return indicadorAutorizacionPosicionFondosAntiguo;
	}
	/**
	 * @param indicadorAutorizacionPosicionFondosAntiguo the indicadorAutorizacionPosicionFondosAntiguo to set
	 */
	public void setIndicadorAutorizacionPosicionFondosAntiguo(
			String indicadorAutorizacionPosicionFondosAntiguo) {
		this.indicadorAutorizacionPosicionFondosAntiguo = indicadorAutorizacionPosicionFondosAntiguo;
	}
	/**
	 * @return the indicadorAutorizacionPosicionFondosNuevo
	 */
	public String getIndicadorAutorizacionPosicionFondosNuevo() {
		return indicadorAutorizacionPosicionFondosNuevo;
	}
	/**
	 * @param indicadorAutorizacionPosicionFondosNuevo the indicadorAutorizacionPosicionFondosNuevo to set
	 */
	public void setIndicadorAutorizacionPosicionFondosNuevo(
			String indicadorAutorizacionPosicionFondosNuevo) {
		this.indicadorAutorizacionPosicionFondosNuevo = indicadorAutorizacionPosicionFondosNuevo;
	}
	/**
	 * @return the indicadorCruceFondosInversionAntiguo
	 */
	public String getIndicadorCruceFondosInversionAntiguo() {
		return indicadorCruceFondosInversionAntiguo;
	}
	/**
	 * @param indicadorCruceFondosInversionAntiguo the indicadorCruceFondosInversionAntiguo to set
	 */
	public void setIndicadorCruceFondosInversionAntiguo(
			String indicadorCruceFondosInversionAntiguo) {
		this.indicadorCruceFondosInversionAntiguo = indicadorCruceFondosInversionAntiguo;
	}
	/**
	 * @return the indicadorCruceFondosInversionNuevo
	 */
	public String getIndicadorCruceFondosInversionNuevo() {
		return indicadorCruceFondosInversionNuevo;
	}
	/**
	 * @param indicadorCruceFondosInversionNuevo the indicadorCruceFondosInversionNuevo to set
	 */
	public void setIndicadorCruceFondosInversionNuevo(
			String indicadorCruceFondosInversionNuevo) {
		this.indicadorCruceFondosInversionNuevo = indicadorCruceFondosInversionNuevo;
	}
	/**
	 * @return the indicadorDiscrecionalidadAntiguo
	 */
	public String getIndicadorDiscrecionalidadAntiguo() {
		return indicadorDiscrecionalidadAntiguo;
	}
	/**
	 * @param indicadorDiscrecionalidadAntiguo the indicadorDiscrecionalidadAntiguo to set
	 */
	public void setIndicadorDiscrecionalidadAntiguo(
			String indicadorDiscrecionalidadAntiguo) {
		this.indicadorDiscrecionalidadAntiguo = indicadorDiscrecionalidadAntiguo;
	}
	/**
	 * @return the indicadorDiscrecionalidadNuevo
	 */
	public String getIndicadorDiscrecionalidadNuevo() {
		return indicadorDiscrecionalidadNuevo;
	}
	/**
	 * @param indicadorDiscrecionalidadNuevo the indicadorDiscrecionalidadNuevo to set
	 */
	public void setIndicadorDiscrecionalidadNuevo(
			String indicadorDiscrecionalidadNuevo) {
		this.indicadorDiscrecionalidadNuevo = indicadorDiscrecionalidadNuevo;
	}
	/**
	 * @return the indicadorTipoCustodiaDirectoAntiguo
	 */
	public String getIndicadorTipoCustodiaDirectoAntiguo() {
		return indicadorTipoCustodiaDirectoAntiguo;
	}
	/**
	 * @param indicadorTipoCustodiaDirectoAntiguo the indicadorTipoCustodiaDirectoAntiguo to set
	 */
	public void setIndicadorTipoCustodiaDirectoAntiguo(
			String indicadorTipoCustodiaDirectoAntiguo) {
		this.indicadorTipoCustodiaDirectoAntiguo = indicadorTipoCustodiaDirectoAntiguo;
	}
	/**
	 * @return the indicadorTipoCustodiaDirectoNuevo
	 */
	public String getIndicadorTipoCustodiaDirectoNuevo() {
		return indicadorTipoCustodiaDirectoNuevo;
	}
	/**
	 * @param indicadorTipoCustodiaDirectoNuevo the indicadorTipoCustodiaDirectoNuevo to set
	 */
	public void setIndicadorTipoCustodiaDirectoNuevo(
			String indicadorTipoCustodiaDirectoNuevo) {
		this.indicadorTipoCustodiaDirectoNuevo = indicadorTipoCustodiaDirectoNuevo;
	}
	/**
	 * @return the indicadorTipoCustodiaReposAntiguo
	 */
	public String getIndicadorTipoCustodiaReposAntiguo() {
		return indicadorTipoCustodiaReposAntiguo;
	}
	/**
	 * @param indicadorTipoCustodiaReposAntiguo the indicadorTipoCustodiaReposAntiguo to set
	 */
	public void setIndicadorTipoCustodiaReposAntiguo(
			String indicadorTipoCustodiaReposAntiguo) {
		this.indicadorTipoCustodiaReposAntiguo = indicadorTipoCustodiaReposAntiguo;
	}
	/**
	 * @return the indicadorTipoCustodiaReposNuevo
	 */
	public String getIndicadorTipoCustodiaReposNuevo() {
		return indicadorTipoCustodiaReposNuevo;
	}
	/**
	 * @param indicadorTipoCustodiaReposNuevo the indicadorTipoCustodiaReposNuevo to set
	 */
	public void setIndicadorTipoCustodiaReposNuevo(
			String indicadorTipoCustodiaReposNuevo) {
		this.indicadorTipoCustodiaReposNuevo = indicadorTipoCustodiaReposNuevo;
	}
	/**
	 * @return the indicadorEstadoCuentaAntigua
	 */
	public String getIndicadorEstadoCuentaAntigua() {
		return indicadorEstadoCuentaAntigua;
	}
	/**
	 * @param indicadorEstadoCuentaAntigua the indicadorEstadoCuentaAntigua to set
	 */
	public void setIndicadorEstadoCuentaAntigua(String indicadorEstadoCuentaAntigua) {
		this.indicadorEstadoCuentaAntigua = indicadorEstadoCuentaAntigua;
	}
	/**
	 * @return the indicadorEstadoCuentaNueva
	 */
	public String getIndicadorEstadoCuentaNueva() {
		return indicadorEstadoCuentaNueva;
	}
	/**
	 * @param indicadorEstadoCuentaNueva the indicadorEstadoCuentaNueva to set
	 */
	public void setIndicadorEstadoCuentaNueva(String indicadorEstadoCuentaNueva) {
		this.indicadorEstadoCuentaNueva = indicadorEstadoCuentaNueva;
	}
	/**
	 * @return the importeMaximoOperacionAntiguo
	 */
	public String getImporteMaximoOperacionAntiguo() {
		return importeMaximoOperacionAntiguo;
	}
	/**
	 * @param importeMaximoOperacionAntiguo the importeMaximoOperacionAntiguo to set
	 */
	public void setImporteMaximoOperacionAntiguo(
			String importeMaximoOperacionAntiguo) {
		this.importeMaximoOperacionAntiguo = importeMaximoOperacionAntiguo;
	}
	/**
	 * @return the divisaMontoMaximoOperacionContratoAntiguo
	 */
	public String getDivisaMontoMaximoOperacionContratoAntiguo() {
		return divisaMontoMaximoOperacionContratoAntiguo;
	}
	/**
	 * @param divisaMontoMaximoOperacionContratoAntiguo the divisaMontoMaximoOperacionContratoAntiguo to set
	 */
	public void setDivisaMontoMaximoOperacionContratoAntiguo(
			String divisaMontoMaximoOperacionContratoAntiguo) {
		this.divisaMontoMaximoOperacionContratoAntiguo = divisaMontoMaximoOperacionContratoAntiguo;
	}
	/**
	 * @return the importeMaximoOperacionnuevo
	 */
	public String getImporteMaximoOperacionNuevo() {
		return importeMaximoOperacionNuevo;
	}
	/**
	 * @param importeMaximoOperacionnuevo the importeMaximoOperacionnuevo to set
	 */
	public void setImporteMaximoOperacionNuevo(String importeMaximoOperacionNuevo) {
		this.importeMaximoOperacionNuevo = importeMaximoOperacionNuevo;
	}
	/**
	 * @return the divisaMontoMaximoOperacionContratoNuevo
	 */
	public String getDivisaMontoMaximoOperacionContratoNuevo() {
		return divisaMontoMaximoOperacionContratoNuevo;
	}
	/**
	 * @param divisaMontoMaximoOperacionContratoNuevo the divisaMontoMaximoOperacionContratoNuevo to set
	 */
	public void setDivisaMontoMaximoOperacionContratoNuevo(
			String divisaMontoMaximoOperacionContratoNuevo) {
		this.divisaMontoMaximoOperacionContratoNuevo = divisaMontoMaximoOperacionContratoNuevo;
	}
	/**
	 * @return the indicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo
	 */
	public String getIndicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo() {
		return indicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo;
	}
	/**
	 * @param indicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo the indicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo to set
	 */
	public void setIndicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo(
			String indicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo) {
		this.indicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo = indicadorEnvioCorreoCartaConfirmacionCapitalesAntiguo;
	}
	/**
	 * @return the indicadorEnvioCorreoCartaConfirmacionCapitalesNuevo
	 */
	public String getIndicadorEnvioCorreoCartaConfirmacionCapitalesNuevo() {
		return indicadorEnvioCorreoCartaConfirmacionCapitalesNuevo;
	}
	/**
	 * @param indicadorEnvioCorreoCartaConfirmacionCapitalesNuevo the indicadorEnvioCorreoCartaConfirmacionCapitalesNuevo to set
	 */
	public void setIndicadorEnvioCorreoCartaConfirmacionCapitalesNuevo(
			String indicadorEnvioCorreoCartaConfirmacionCapitalesNuevo) {
		this.indicadorEnvioCorreoCartaConfirmacionCapitalesNuevo = indicadorEnvioCorreoCartaConfirmacionCapitalesNuevo;
	}
	/**
	 * @return the codigoEstatusAntiguo
	 */
	public String getCodigoEstatusAntiguo() {
		return codigoEstatusAntiguo;
	}
	/**
	 * @param codigoEstatusAntiguo the codigoEstatusAntiguo to set
	 */
	public void setCodigoEstatusAntiguo(String codigoEstatusAntiguo) {
		this.codigoEstatusAntiguo = codigoEstatusAntiguo;
	}
	/**
	 * @return the codigoEstatusNuevo
	 */
	public String getCodigoEstatusNuevo() {
		return codigoEstatusNuevo;
	}
	/**
	 * @param codigoEstatusNuevo the codigoEstatusNuevo to set
	 */
	public void setCodigoEstatusNuevo(String codigoEstatusNuevo) {
		this.codigoEstatusNuevo = codigoEstatusNuevo;
	}
	/**
	 * @return the detalleEstatusAntiguo
	 */
	public String getDetalleEstatusAntiguo() {
		return detalleEstatusAntiguo;
	}
	/**
	 * @param detalleEstatusAntiguo the detalleEstatusAntiguo to set
	 */
	public void setDetalleEstatusAntiguo(String detalleEstatusAntiguo) {
		this.detalleEstatusAntiguo = detalleEstatusAntiguo;
	}
	/**
	 * @return the detalleEstatusNuevo
	 */
	public String getDetalleEstatusNuevo() {
		return detalleEstatusNuevo;
	}
	/**
	 * @param detalleEstatusNuevo the detalleEstatusNuevo to set
	 */
	public void setDetalleEstatusNuevo(String detalleEstatusNuevo) {
		this.detalleEstatusNuevo = detalleEstatusNuevo;
	}
	/**
	 * @return the indicadorContratoAgrupadoAntiguo
	 */
	public String getIndicadorContratoAgrupadoAntiguo() {
		return indicadorContratoAgrupadoAntiguo;
	}
	/**
	 * @param indicadorContratoAgrupadoAntiguo the indicadorContratoAgrupadoAntiguo to set
	 */
	public void setIndicadorContratoAgrupadoAntiguo(
			String indicadorContratoAgrupadoAntiguo) {
		this.indicadorContratoAgrupadoAntiguo = indicadorContratoAgrupadoAntiguo;
	}
	/**
	 * @return the indicadorContratoAgrupadoNuevo
	 */
	public String getIndicadorContratoAgrupadoNuevo() {
		return indicadorContratoAgrupadoNuevo;
	}
	/**
	 * @param indicadorContratoAgrupadoNuevo the indicadorContratoAgrupadoNuevo to set
	 */
	public void setIndicadorContratoAgrupadoNuevo(
			String indicadorContratoAgrupadoNuevo) {
		this.indicadorContratoAgrupadoNuevo = indicadorContratoAgrupadoNuevo;
	}
	/**
	 * @return the indicadorTipoPosicionAntiguo
	 */
	public String getIndicadorTipoPosicionAntiguo() {
		return indicadorTipoPosicionAntiguo;
	}
	/**
	 * @param indicadorTipoPosicionAntiguo the indicadorTipoPosicionAntiguo to set
	 */
	public void setIndicadorTipoPosicionAntiguo(String indicadorTipoPosicionAntiguo) {
		this.indicadorTipoPosicionAntiguo = indicadorTipoPosicionAntiguo;
	}
	/**
	 * @return the indicadorTipoPosicionNuevo
	 */
	public String getIndicadorTipoPosicionNuevo() {
		return indicadorTipoPosicionNuevo;
	}
	/**
	 * @param indicadorTipoPosicionNuevo the indicadorTipoPosicionNuevo to set
	 */
	public void setIndicadorTipoPosicionNuevo(String indicadorTipoPosicionNuevo) {
		this.indicadorTipoPosicionNuevo = indicadorTipoPosicionNuevo;
	}
	/**
	 * @return the indicadorContratoEspejoAntiguo
	 */
	public String getIndicadorContratoEspejoAntiguo() {
		return indicadorContratoEspejoAntiguo;
	}
	/**
	 * @param indicadorContratoEspejoAntiguo the indicadorContratoEspejoAntiguo to set
	 */
	public void setIndicadorContratoEspejoAntiguo(
			String indicadorContratoEspejoAntiguo) {
		this.indicadorContratoEspejoAntiguo = indicadorContratoEspejoAntiguo;
	}
	/**
	 * @return the indicadorContratoEspejoNuevo
	 */
	public String getIndicadorContratoEspejoNuevo() {
		return indicadorContratoEspejoNuevo;
	}
	/**
	 * @param indicadorContratoEspejoNuevo the indicadorContratoEspejoNuevo to set
	 */
	public void setIndicadorContratoEspejoNuevo(String indicadorContratoEspejoNuevo) {
		this.indicadorContratoEspejoNuevo = indicadorContratoEspejoNuevo;
	}
	/**
	 * @return the indicadorContratoAmparadoChequeraAntiguo
	 */
	public String getIndicadorContratoAmparadoChequeraAntiguo() {
		return indicadorContratoAmparadoChequeraAntiguo;
	}
	/**
	 * @param indicadorContratoAmparadoChequeraAntiguo the indicadorContratoAmparadoChequeraAntiguo to set
	 */
	public void setIndicadorContratoAmparadoChequeraAntiguo(
			String indicadorContratoAmparadoChequeraAntiguo) {
		this.indicadorContratoAmparadoChequeraAntiguo = indicadorContratoAmparadoChequeraAntiguo;
	}
	/**
	 * @return the indicadorContratoAmparadoChequeraNuevo
	 */
	public String getIndicadorContratoAmparadoChequeraNuevo() {
		return indicadorContratoAmparadoChequeraNuevo;
	}
	/**
	 * @param indicadorContratoAmparadoChequeraNuevo the indicadorContratoAmparadoChequeraNuevo to set
	 */
	public void setIndicadorContratoAmparadoChequeraNuevo(
			String indicadorContratoAmparadoChequeraNuevo) {
		this.indicadorContratoAmparadoChequeraNuevo = indicadorContratoAmparadoChequeraNuevo;
	}
	/**
	 * @return the codigoTipoServicioAntiguo
	 */
	public String getCodigoTipoServicioAntiguo() {
		return codigoTipoServicioAntiguo;
	}
	/**
	 * @param codigoTipoServicioAntiguo the codigoTipoServicioAntiguo to set
	 */
	public void setCodigoTipoServicioAntiguo(String codigoTipoServicioAntiguo) {
		this.codigoTipoServicioAntiguo = codigoTipoServicioAntiguo;
	}
	/**
	 * @return the codigoTipoServicioNuevo
	 */
	public String getCodigoTipoServicioNuevo() {
		return codigoTipoServicioNuevo;
	}
	/**
	 * @param codigoTipoServicioNuevo the codigoTipoServicioNuevo to set
	 */
	public void setCodigoTipoServicioNuevo(String codigoTipoServicioNuevo) {
		this.codigoTipoServicioNuevo = codigoTipoServicioNuevo;
	}
	/**
	 * @return the codigoAsesorFinancieroAntiguo
	 */
	public String getCodigoAsesorFinancieroAntiguo() {
		return codigoAsesorFinancieroAntiguo;
	}
	/**
	 * @param codigoAsesorFinancieroAntiguo the codigoAsesorFinancieroAntiguo to set
	 */
	public void setCodigoAsesorFinancieroAntiguo(
			String codigoAsesorFinancieroAntiguo) {
		this.codigoAsesorFinancieroAntiguo = codigoAsesorFinancieroAntiguo;
	}
	/**
	 * @return the codigoAsesorFinancieroNuevo
	 */
	public String getCodigoAsesorFinancieroNuevo() {
		return codigoAsesorFinancieroNuevo;
	}
	/**
	 * @param codigoAsesorFinancieroNuevo the codigoAsesorFinancieroNuevo to set
	 */
	public void setCodigoAsesorFinancieroNuevo(String codigoAsesorFinancieroNuevo) {
		this.codigoAsesorFinancieroNuevo = codigoAsesorFinancieroNuevo;
	}
}
