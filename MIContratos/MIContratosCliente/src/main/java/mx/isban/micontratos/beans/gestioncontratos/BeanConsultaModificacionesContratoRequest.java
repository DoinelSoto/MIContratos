package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI24 : Consulta modificaciones contrato
 *
 */
public class BeanConsultaModificacionesContratoRequest implements Serializable{
	
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -6986592794289955219L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Tipo consulta, "01" por contrato, "02" por código empresa contrato de iversión + alias	
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de Contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato	
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del Contrato de Inversion	
	 */
	@Size(max=15)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato;	
	 */
	@Size(max=15)
	private String aliasContrato;
	
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanConsultaModificacionesContratoRequest(){
		multicanalidad=new MulticanalidadGenerica();
		rellamada= new BeanPaginacion();
	}
	
	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipConsu the tipConsu to set
	 */
	public void setTipoConsulta(String tipConsu) {
		this.tipoConsulta = tipConsu;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param idEmpr the idEmpr to set
	 */
	public void setEmpresaContrato(String idEmpr) {
		this.empresaContrato = idEmpr;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param idCent the idCent to set
	 */
	public void setCentroContrato(String idCent) {
		this.centroContrato = idCent;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param idContr the idContr to set
	 */
	public void setNumeroContrato(String idContr) {
		this.numeroContrato = idContr;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param idProd the idProd to set
	 */
	public void setCodigoProducto(String idProd) {
		this.codigoProducto = idProd;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param idStiPro the idStiPro to set
	 */
	public void setCodigoSubProducto(String idStiPro) {
		this.codigoSubProducto = idStiPro;
	}
	/**
	 * @return empresaContratoInversion
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}
	/**
	 * @param empresaContratoInversion
	 */
	public void setEmpresaContratoInversion(String empresaContratoInversion) {
		this.empresaContratoInversion = empresaContratoInversion;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param desAlias the desAlias to set
	 */
	public void setAliasContrato(String desAlias) {
		this.aliasContrato = desAlias;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	/**
	 * @return BeanPaginacion
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	/**
	 * @param rellamada
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	

}
