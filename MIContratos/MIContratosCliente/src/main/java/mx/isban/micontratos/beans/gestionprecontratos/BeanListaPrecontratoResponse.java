package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción : MI14
 *
 */
public class BeanListaPrecontratoResponse implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -5593475651079914814L;
	

	/**
	 * Número de folio del que proviene el contrato	
	 */
	private int numeroFolio;
	/**
	 * Código de empresa del producto	
	 */
	private String codigoEmpresa;
	/**
	 * Código de producto del contrato
	 */
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato
	 */
	private String codigoSubProductoContrato;
	/**
	 * Código de moneda del contrato	
	 */
	private String codigoMonedaContrato;
	/**
	 * Número de persona	
	 */
	private String 	numeroPersona;
	/**
	 * Código de empresa del contrato de Inversión	
	 */
	private String codigoEmpresaContrato;
	/**
	 * Código de banca del contrato de Inversión	
	 */
	private String codigoBanca;
	/**
	 * Fecha de apertura del folio
	 */
	private String fechaApertura;
	/**
	 * Código de ejecutivo del contrato	
	 */
	private String codigoEjecutivo;
	/**
	 * Centro de costos del contrato
	 */
	private String centroCostosContrato;
	/**
	 * Indicador de tipo de cuenta	
	 */
	private String tipoCuenta;
	/**
	 * Indicador de tipo de Contrato
	 */
	private String indicadorTipoContrato;
	/**
	 * Indicador autorización posición en corto en capitales	
	 */
	private String indicadorAutorizacionPosicionCortoCapitales;
	/**
	 * Indicador autorización posición en corto en fondos	
	 */
	private String indicadorAutorizacionPosicionCortoFondos;
	/**
	 * Indicador autorización cruce en fondos de inversión	
	 */
	private String indicadorAutorizacionCruceFondosInversion;
	/**
	 * Indicador de discrecionalidad	
	 */
	private String indicadorDiscrecionalidad;
	/**
	 * Indicador tipo de custodia en directo
	 */
	private String indicadorTipoCustodiaDirecto;
	/**
	 * Indicador tipo de custodia en Repos
	 */
	private String indicadorTipoCustodiaRepos;
	/**
	 * Monto Máximo a operar en el contrato	
	 */
	private String importeMaximoOperacion;
	/**
	 * Divisa del Monto Máximo a operar en el contrato
	 */
	private String divisaMaximaOperacion;
	/**
	 * Indicador envío por correo carta confirmación capitales
	 */
	private String indicadorEnvioCorrreoCartaConfirmacionCapitales;
	/**
	 * Indicador de Contrato Agrupado	
	 */
	private String indicadorContratoAgrupado;
	/**
	 * Indicador de Tipo de Posición	
	 */
	private String indicadorTipoPosicion;
	/**
	 * Indicador Contrato “Espejo”
	 */
	private String indicadorContratoEspejo;
	/**
	 * Código tipo de servicio	
	 */
	private String codigoTipoServicio;
	/**
	 * Código AFI	
	 */
	private String codigoAsesorFinanciero;
	/**
	 * @return the numFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}
	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumeroFolio(int numFolio) {
		this.numeroFolio = numFolio;
	}
	/**
	 * @return the idEmpr
	 */
	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}
	/**
	 * @param idEmpr the idEmpr to set
	 */
	public void setCodigoEmpresa(String idEmpr) {
		this.codigoEmpresa = idEmpr;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param idProd the idProd to set
	 */
	public void setCodigoProducto(String idProd) {
		this.codigoProducto = idProd;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProductoContrato() {
		return codigoSubProductoContrato;
	}
	/**
	 * @param idStiPro the idStiPro to set
	 */
	public void setCodigoSubProductoContrato(String idStiPro) {
		this.codigoSubProductoContrato = idStiPro;
	}
	/**
	 * @return the codMonSw
	 */
	public String getCodigoMonedaContrato() {
		return codigoMonedaContrato;
	}
	/**
	 * @param codMonSw the codMonSw to set
	 */
	public void setCodigoMonedaContrato(String codMonSw) {
		this.codigoMonedaContrato = codMonSw;
	}
	/**
	 * @return the peNumPer
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	/**
	 * @param peNumPer the peNumPer to set
	 */
	public void setNumeroPersona(String peNumPer) {
		this.numeroPersona = peNumPer;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getCodigoEmpresaContrato() {
		return codigoEmpresaContrato;
	}
	/**
	 * @param idEmprCo the idEmprCo to set
	 */
	public void setCodigoEmpresaContrato(String idEmprCo) {
		this.codigoEmpresaContrato = idEmprCo;
	}
	/**
	 * @return the idBanca
	 */
	public String getCodigoBanca() {
		return codigoBanca;
	}
	/**
	 * @param idBanca the idBanca to set
	 */
	public void setCodigoBanca(String idBanca) {
		this.codigoBanca = idBanca;
	}
	/**
	 * @return the fecApert
	 */
	public String getFechaApertura() {
		return fechaApertura;
	}
	/**
	 * @param fecApert the fecApert to set
	 */
	public void setFechaApertura(String fecApert) {
		this.fechaApertura = fecApert;
	}
	/**
	 * @return the codEjecu
	 */
	public String getCodigoEjecutivo() {
		return codigoEjecutivo;
	}
	/**
	 * @param codEjecu the codEjecu to set
	 */
	public void setCodigoEjecutivo(String codEjecu) {
		this.codigoEjecutivo = codEjecu;
	}
	/**
	 * @return the codCenCo
	 */
	public String getCentroCostosContrato() {
		return centroCostosContrato;
	}
	/**
	 * @param codCenCo the codCenCo to set
	 */
	public void setCentroCostosContrato(String codCenCo) {
		this.centroCostosContrato = codCenCo;
	}
	/**
	 * @return the idTipCue
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	/**
	 * @param idTipCue the idTipCue to set
	 */
	public void setTipoCuenta(String idTipCue) {
		this.tipoCuenta = idTipCue;
	}
	/**
	 * @return the idTipCon
	 */
	public String getIndicadorTipoContrato() {
		return indicadorTipoContrato;
	}
	/**
	 * @param idTipCon the idTipCon to set
	 */
	public void setIndicadorTipoContrato(String idTipCon) {
		this.indicadorTipoContrato = idTipCon;
	}
	/**
	 * @return the indAuPCC
	 */
	public String getIndicadorAutorizacionPosicionCortoCapitales() {
		return indicadorAutorizacionPosicionCortoCapitales;
	}
	/**
	 * @param indAuPCC the indAuPCC to set
	 */
	public void setIndicadorAutorizacionPosicionCortoCapitales(String indAuPCC) {
		this.indicadorAutorizacionPosicionCortoCapitales = indAuPCC;
	}
	/**
	 * @return the indAuPCF
	 */
	public String getIndicadorAutorizacionPosicionCortoFondos() {
		return indicadorAutorizacionPosicionCortoFondos;
	}
	/**
	 * @param indAuPCF the indAuPCF to set
	 */
	public void setIndicadorAutorizacionPosicionCortoFondos(String indAuPCF) {
		this.indicadorAutorizacionPosicionCortoFondos = indAuPCF;
	}
	/**
	 * @return the indAuCFI
	 */
	public String getIndicadorAutorizacionCruceFondosInversion() {
		return indicadorAutorizacionCruceFondosInversion;
	}
	/**
	 * @param indAuCFI the indAuCFI to set
	 */
	public void setIndicadorAutorizacionCruceFondosInversion(String indAuCFI) {
		this.indicadorAutorizacionCruceFondosInversion = indAuCFI;
	}
	/**
	 * @return the indDiscr
	 */
	public String getIndicadorDiscrecionalidad() {
		return indicadorDiscrecionalidad;
	}
	/**
	 * @param indDiscr the indDiscr to set
	 */
	public void setIndicadorDiscrecionalidad(String indDiscr) {
		this.indicadorDiscrecionalidad = indDiscr;
	}
	/**
	 * @return the indTiCuD
	 */
	public String getIndicadorTipoCustodiaDirecto() {
		return indicadorTipoCustodiaDirecto;
	}
	/**
	 * @param indTiCuD the indTiCuD to set
	 */
	public void setIndicadorTipoCustodiaDirecto(String indTiCuD) {
		this.indicadorTipoCustodiaDirecto = indTiCuD;
	}
	/**
	 * @return the indTiCuR
	 */
	public String getIndicadorTipoCustodiaRepos() {
		return indicadorTipoCustodiaRepos;
	}
	/**
	 * @param indTiCuR the indTiCuR to set
	 */
	public void setIndicadorTipoCustodiaRepos(String indTiCuR) {
		this.indicadorTipoCustodiaRepos = indTiCuR;
	}
	/**
	 * @return the impMaxOp
	 */
	public String getImporteMaximoOperacion() {
		return importeMaximoOperacion;
	}
	/**
	 * @param impMaxOp the impMaxOp to set
	 */
	public void setImporteMaximoOperacion(String impMaxOp) {
		this.importeMaximoOperacion = impMaxOp;
	}
	
	/**
	 * @return the divisaMaximaOperacion
	 */
	public String getDivisaMaximaOperacion() {
		return divisaMaximaOperacion;
	}
	/**
	 * @param divMaxOp the divMaxOp to set
	 */
	public void setDivisaMaximaOperacion(String divMaxOp) {
		this.divisaMaximaOperacion = divMaxOp;
	}
	/**
	 * @return the indEnCoC
	 */
	public String getIndicadorEnvioCorrreoCartaConfirmacionCapitales() {
		return indicadorEnvioCorrreoCartaConfirmacionCapitales;
	}
	/**
	 * @param indEnCoC the indEnCoC to set
	 */
	public void setIndicadorEnvioCorrreoCartaConfirmacionCapitales(String indEnCoC) {
		this.indicadorEnvioCorrreoCartaConfirmacionCapitales = indEnCoC;
	}
	/**
	 * @return the indCoAgr
	 */
	public String getIndicadorContratoAgrupado() {
		return indicadorContratoAgrupado;
	}
	/**
	 * @param indCoAgr the indCoAgr to set
	 */
	public void setIndicadorContratoAgrupado(String indCoAgr) {
		this.indicadorContratoAgrupado = indCoAgr;
	}
	/**
	 * @return the indTiPos
	 */
	public String getIndicadorTipoPosicion() {
		return indicadorTipoPosicion;
	}
	/**
	 * @param indTiPos the indTiPos to set
	 */
	public void setIndicadorTipoPosicion(String indTiPos) {
		this.indicadorTipoPosicion = indTiPos;
	}
	/**
	 * @return the indCoEsp
	 */
	public String getIndicadorContratoEspejo() {
		return indicadorContratoEspejo;
	}
	/**
	 * @param indCoEsp the indCoEsp to set
	 */
	public void setIndicadorContratoEspejo(String indCoEsp) {
		this.indicadorContratoEspejo = indCoEsp;
	}
	/**
	 * @return the codTipSe
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}
	/**
	 * @param codTipSe the codTipSe to set
	 */
	public void setCodigoTipoServicio(String codTipSe) {
		this.codigoTipoServicio = codTipSe;
	}
	/**
	 * @return the codAFi
	 */
	public String getCodigoAsesorFinanciero() {
		return codigoAsesorFinanciero;
	}
	/**
	 * @param codAFi the codAFi to set
	 */
	public void setCodigoAsesorFinanciero(String codAFi) {
		this.codigoAsesorFinanciero = codAFi;
	}

}
