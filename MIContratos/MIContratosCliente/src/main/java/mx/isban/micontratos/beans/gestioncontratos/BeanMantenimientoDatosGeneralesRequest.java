package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI31 : Mantenimientos datos generales request
 *
 */
public class BeanMantenimientoDatosGeneralesRequest implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -1918479646231035312L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias	
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato	
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del Contrato de Inversión
	 */
	@Size(max=4)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato	
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Código de banca del contrato	
	 */
	@Size(max=3)
	private String codigoBanca;
	/**
	 * Indicador de tipo de cuenta	
	 */
	@Size(max=1)
	private String indicadorTipoCuenta;
	/**
	 * Indicador de tipo de Contrato	
	 */
	@Size(max=1)
	private String indicadorTipoContrato;
	/**
	 * Indicador autorización posición en corto en capitales	
	 */
	@Size(max=1)
	private String indicadorAutorizacionPosicionCortoCapitales;
	/**
	 * Indicador autorización posición en corto en fondos	
	 */
	@Size(max=1)
	private String indicadorAutorizacionPosicionCortoFondos;
	/**
	 * Indicador autorización cruce en fondos de inversión
	 */
	@Size(max=1)
	private String indicadorAutorizacionCruceFondosInversion;
	/**
	 * Indicador de discrecionalidad	
	 */
	@Size(max=1)
	private String indicadorDiscrecionalidad;
	/**
	 * Indicador tipo de custodia en directo	
	 */
	@Size(max=1)
	private String indicadorTipoCustodiaDirecto;
	/**
	 * Indicador tipo de custodia en Repos
	 */
	@Size(max=1)
	private String indicadorTipoCustodiaRepos;
	/**
	 * Indicador de estado de cuenta	
	 */
	@Size(max=1)
	private String indicadorEstadoCuenta;
	/**
	 * Monto Máximo a operar en el contrato	
	 */
	@Size(max=16)
	private String montoMaximoOperacionContrato;
	/**
	 * Divisa del Monto Máximo a operar en el contrato
	 */
	@Size(max=3)
	private String divisamaximoOperacionContrato;
	/**
	 * Indicador envío por correo carta confirmación capitales
	 */
	@Size(max=1)
	private String indicadorEnvioCorreoCartaConfirmacionCapitales;
	/**
	 * Indicador de Contrato Agrupado	
	 */
	@Size(max=1)
	private String indicadorContratoAgrupado;
	/**
	 * Empresa del Contrato Agrupador	
	 */
	@Size(max=4)
	private String empresaContratoAgrupador;
	/**
	 * Centro del Contrato Agrupador	
	 */
	@Size(max=4)
	private String centroContratoAgrupador;
	/**
	 * Número de contrato agrupador	
	 */
	@Size(max=12)
	private String numeroContratoAgrupador;
	/**
	 * Código de producto del contrato agrupador	
	 */
	@Size(max=2)
	private String codigoProductoContratoAgrupador;
	/**
	 * Código de subproducto del contrato agrupador	
	 */
	@Size(max=4)
	private String codigoSubProductoContratoAgrupador;
	/**
	 * Indicador de Contrato Espejo	
	 */
	@Size(max=1)
	private String indicadorContratoEspejo;
	/**
	 * Empresa del Contrato Espejo	
	 */
	@Size(max=4)
	private String empresaContratoEspejo;
	/**
	 * Centro del Contrato Espejo	
	 */
	@Size(max=4)
	private String centroContratoEspejo;
	/**
	 * Número de contrato Espejo	
	 */
	@Size(max=12)
	private String numeroContratoEspejo;
	/**
	 * Código de producto del contrato Espejo	
	 */
	@Size(max=2)
	private String codigoProductoContratoEspejo;
	/**
	 * Código de subproducto del contrato Espejo
	 */
	@Size(max=4)
	private String codigoSubProductoContratoEspejo;
	/**
	 * Código de tipo de servicio
	 */
	@Size(max=2)
	private String codigoTipoServicio;
	/**
	 * Código AFI	
	 */
	@Size(max=6)
	private String codigoAfiliacion;
	/**
	 * Descripción del contrato	
	 */
	@Size(max=100)
	private String descripcionContrato;
	/**
	 * Descripción de la firma	
	 */
	@Size(max=100)
	private String descripcionFirma;
	/**
	 * Área de referencia	
	 */
	@Size(max=100)
	private String descripcionAreaReferencia;
	/**
	 * Área de referencia externa	
	 */
	@Size(max=100)
	private String descripcionAreaReferenciaExterna;
	
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanMantenimientoDatosGeneralesRequest(){
		
		multicanalidad=new MulticanalidadGenerica();	
		
	}
	/**
	 * @return the tipConsu
	 */
		public String getTipoConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipConsu the tipConsu to set
	 */
	public void setTipoConsulta(String tipConsu) {
		this.tipoConsulta = tipConsu;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param idEmpr the idEmpr to set
	 */
	public void setEmpresaContrato(String idEmpr) {
		this.empresaContrato = idEmpr;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param idCent the idCent to set
	 */
	public void setCentroContrato(String idCent) {
		this.centroContrato = idCent;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param idContr the idContr to set
	 */
	public void setNumeroContrato(String idContr) {
		this.numeroContrato = idContr;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param idProd the idProd to set
	 */
	public void setCodigoProducto(String idProd) {
		this.codigoProducto = idProd;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param idStiPro the idStiPro to set
	 */
	public void setCodigoSubProducto(String idStiPro) {
		this.codigoSubProducto = idStiPro;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}
	/**
	 * @param idEmprCo the idEmprCo to set
	 */
	public void setEmpresaContratoInversion(String idEmprCo) {
		this.empresaContratoInversion = idEmprCo;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param desAlias the desAlias to set
	 */
	public void setAliasContrato(String desAlias) {
		this.aliasContrato = desAlias;
	}
	/**
	 * @return the idBanca
	 */
	public String getCodigoBanca() {
		return codigoBanca;
	}
	/**
	 * @param idBanca the idBanca to set
	 */
	public void setCodigoBanca(String idBanca) {
		this.codigoBanca = idBanca;
	}
	/**
	 * @return the idTipCue
	 */
	public String getIndicadorTipoCuenta() {
		return indicadorTipoCuenta;
	}
	/**
	 * @param idTipCue the idTipCue to set
	 */
	public void setIndicadorTipoCuenta(String idTipCue) {
		this.indicadorTipoCuenta = idTipCue;
	}
	/**
	 * @return the idTipCon
	 */
	public String getIndicadorTipoContrato() {
		return indicadorTipoContrato;
	}
	/**
	 * @param indicadorTipoContacto the idTipCon to set
	 */
	public void setIndicadorTipoContrato(String indicadorTipoContacto) {
		this.indicadorTipoContrato = indicadorTipoContacto;
	}
	/**
	 * @return the indAuPCC
	 */
	public String getIndicadorAutorizacionPosicionCortoCapitales() {
		return indicadorAutorizacionPosicionCortoCapitales;
	}
	/**
	 * @param indAuPCC the indAuPCC to set
	 */
	public void setIndicadorAutorizacionPosicionCortoCapitales(String indAuPCC) {
		this.indicadorAutorizacionPosicionCortoCapitales = indAuPCC;
	}
	/**
	 * @return the indAuPCF
	 */
	public String getIndicadorAutorizacionPosicionCortoFondos() {
		return indicadorAutorizacionPosicionCortoFondos;
	}
	/**
	 * @param indAuPCF the indAuPCF to set
	 */
	public void setIndicadorAutorizacionPosicionCortoFondos(String indAuPCF) {
		this.indicadorAutorizacionPosicionCortoFondos = indAuPCF;
	}
	/**
	 * @return the indAuCFI
	 */
	public String getIndicadorAutorizacionCruceFondosInversion() {
		return indicadorAutorizacionCruceFondosInversion;
	}
	/**
	 * @param indAuCFI the indAuCFI to set
	 */
	public void setIndicadorAutorizacionCruceFondosInversion(String indAuCFI) {
		this.indicadorAutorizacionCruceFondosInversion = indAuCFI;
	}
	/**
	 * @return the indDiscr
	 */
	public String getIndicadorDiscrecionalidad() {
		return indicadorDiscrecionalidad;
	}
	/**
	 * @param indDiscr the indDiscr to set
	 */
	public void setIndicadorDiscrecionalidad(String indDiscr) {
		this.indicadorDiscrecionalidad = indDiscr;
	}
	/**
	 * @return the indTiCuD
	 */
	public String getIndicadorTipoCustodiaDirecto() {
		return indicadorTipoCustodiaDirecto;
	}
	/**
	 * @param indTiCuD the indTiCuD to set
	 */
	public void setIndicadorTipoCustodiaDirecto(String indTiCuD) {
		this.indicadorTipoCustodiaDirecto = indTiCuD;
	}
	/**
	 * @return the indTiCuR
	 */
	public String getIndicadorTipoCustodiaRepos() {
		return indicadorTipoCustodiaRepos;
	}
	/**
	 * @param indTiCuR the indTiCuR to set
	 */
	public void setIndicadorTipoCustodiaRepos(String indTiCuR) {
		this.indicadorTipoCustodiaRepos = indTiCuR;
	}
	/**
	 * @return the indEstCu
	 */
	public String getIndicadorEstadoCuenta() {
		return indicadorEstadoCuenta;
	}
	/**
	 * @param indicadorEstadoCuenta the indEstCu to set
	 */
	public void setIndicadorEstadoCuenta(String indicadorEstadoCuenta) {
		this.indicadorEstadoCuenta = indicadorEstadoCuenta;
	}
	/**
	 * @return the impMaxOp
	 */
	public String getMontoMaximoOperacionContrato() {
		return montoMaximoOperacionContrato;
	}
	/**
	 * @param impMaxOp the impMaxOp to set
	 */
	public void setMontoMaximoOperacionContrato(String impMaxOp) {
		this.montoMaximoOperacionContrato = impMaxOp;
	}
	/**
	 * @return the divMaxOp
	 */
	public String getDivisamaximoOperacionContrato() {
		return divisamaximoOperacionContrato;
	}
	/**
	 * @param divMaxOp the divMaxOp to set
	 */
	public void setDivisamaximoOperacionContrato(String divMaxOp) {
		this.divisamaximoOperacionContrato = divMaxOp;
	}
	/**
	 * @return the indEnCoC
	 */
	public String getIndicadorEnvioCorreoCartaConfirmacionCapitales() {
		return indicadorEnvioCorreoCartaConfirmacionCapitales;
	}
	/**
	 * @param indEnCoC the indEnCoC to set
	 */
	public void setIndicadorEnvioCorreoCartaConfirmacionCapitales(String indEnCoC) {
		this.indicadorEnvioCorreoCartaConfirmacionCapitales = indEnCoC;
	}
	/**
	 * @return the indCoAgr
	 */
	public String getIndicadorContratoAgrupado() {
		return indicadorContratoAgrupado;
	}
	/**
	 * @param indCoAgr the indCoAgr to set
	 */
	public void setIndicadorContratoAgrupado(String indCoAgr) {
		this.indicadorContratoAgrupado = indCoAgr;
	}
	/**
	 * @return the idEmprAg
	 */
	public String getEmpresaContratoAgrupador() {
		return empresaContratoAgrupador;
	}
	/**
	 * @param idEmprAg the idEmprAg to set
	 */
	public void setEmpresaContratoAgrupador(String idEmprAg) {
		this.empresaContratoAgrupador = idEmprAg;
	}
	/**
	 * @return the idCentAg
	 */
	public String getCentroContratoAgrupador() {
		return centroContratoAgrupador;
	}
	/**
	 * @param idCentAg the idCentAg to set
	 */
	public void setCentroContratoAgrupador(String idCentAg) {
		this.centroContratoAgrupador = idCentAg;
	}
	/**
	 * @return the idContAg
	 */
	public String getNumeroContratoAgrupador() {
		return numeroContratoAgrupador;
	}
	/**
	 * @param idContAg the idContAg to set
	 */
	public void setNumeroContratoAgrupador(String idContAg) {
		this.numeroContratoAgrupador = idContAg;
	}
	/**
	 * @return the idProAg
	 */
	public String getCodigoProductoContratoAgrupador() {
		return codigoProductoContratoAgrupador;
	}
	/**
	 * @param idProAg the idProAg to set
	 */
	public void setCodigoProductoContratoAgrupador(String idProAg) {
		this.codigoProductoContratoAgrupador = idProAg;
	}
	/**
	 * @return the idStipAg
	 */
	public String getCodigoSubProductoContratoAgrupador() {
		return codigoSubProductoContratoAgrupador;
	}
	/**
	 * @param idStipAg the idStipAg to set
	 */
	public void setCodigoSubProductoContratoAgrupador(String idStipAg) {
		this.codigoSubProductoContratoAgrupador = idStipAg;
	}
	/**
	 * @return the indCoEsp
	 */
	public String getIndicadorContratoEspejo() {
		return indicadorContratoEspejo;
	}
	/**
	 * @param indicadorContratoEspejo the indCoEsp to set
	 */
	public void setIndicadorContratoEspejo(String indicadorContratoEspejo) {
		this.indicadorContratoEspejo = indicadorContratoEspejo;
	}
	/**
	 * @return the idEmprEs
	 */
	public String getEmpresaContratoEspejo() {
		return empresaContratoEspejo;
	}
	/**
	 * @param idEmprEs the idEmprEs to set
	 */
	public void setEmpresaContratoEspejo(String idEmprEs) {
		this.empresaContratoEspejo = idEmprEs;
	}
	/**
	 * @return the idCentEs
	 */
	public String getCentroContratoEspejo() {
		return centroContratoEspejo;
	}
	/**
	 * @param idCentEs the idCentEs to set
	 */
	public void setCentroContratoEspejo(String idCentEs) {
		this.centroContratoEspejo = idCentEs;
	}
	/**
	 * @return the idContEs
	 */
	public String getNumeroContratoEspejo() {
		return numeroContratoEspejo;
	}
	/**
	 * @param numeroContratoEspejo the idContEs to set
	 */
	public void setNumeroContratoEspejo(String numeroContratoEspejo) {
		this.numeroContratoEspejo = numeroContratoEspejo;
	}
	/**
	 * @return the idProdEs
	 */
	public String getCodigoProductoContratoEspejo() {
		return codigoProductoContratoEspejo;
	}
	/**
	 * @param idProdEs the idProdEs to set
	 */
	public void setCodigoProductoContratoEspejo(String idProdEs) {
		this.codigoProductoContratoEspejo = idProdEs;
	}
	
	/**
	 * @return the codTipSe
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}
	/**
	 * @param codTipSe the codTipSe to set
	 */
	public void setCodigoTipoServicio(String codTipSe) {
		this.codigoTipoServicio = codTipSe;
	}
	/**
	 * @return the codAFi
	 */
	public String getCodigoAfiliacion() {
		return codigoAfiliacion;
	}
	/**
	 * @param codAFi the codAFi to set
	 */
	public void setCodigoAfiliacion(String codAFi) {
		this.codigoAfiliacion = codAFi;
	}
	/**
	 * @return the desContr
	 */
	public String getDescripcionContrato() {
		return descripcionContrato;
	}
	/**
	 * @param desContr the desContr to set
	 */
	public void setDescripcionContrato(String desContr) {
		this.descripcionContrato = desContr;
	}
	/**
	 * @return the desFirma
	 */
	public String getDescripcionFirma() {
		return descripcionFirma;
	}
	/**
	 * @param desFirma the desFirma to set
	 */
	public void setDescripcionFirma(String desFirma) {
		this.descripcionFirma = desFirma;
	}
	/**
	 * @return the desAreaR
	 */
	public String getDescripcionAreaReferencia() {
		return descripcionAreaReferencia;
	}
	/**
	 * @param desAreaR the desAreaR to set
	 */
	public void setDescripcionAreaReferencia(String desAreaR) {
		this.descripcionAreaReferencia = desAreaR;
	}
	/**
	 * @return the desRefEx
	 */
	public String getDescripcionAreaReferenciaExterna() {
		return descripcionAreaReferenciaExterna;
	}
	/**
	 * @param desRefEx the desRefEx to set
	 */
	public void setDescripcionAreaReferenciaExterna(String desRefEx) {
		this.descripcionAreaReferenciaExterna = desRefEx;
	}

	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
	/**
	 * @return the idsTipEs
	 */
	public String getCodigoSubProductoContratoEspejo() {
		return codigoSubProductoContratoEspejo;
	}

	/**
	 * @param idsTipEs the idsTipEs to set
	 */
	public void setCodigoSubProductoContratoEspejo(String idsTipEs) {
		this.codigoSubProductoContratoEspejo = idsTipEs;
	}

	
}
