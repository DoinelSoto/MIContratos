package mx.isban.micontratos.beans.agrupaciones;

import java.io.Serializable;
import javax.validation.Valid;

import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI33 : Alta contrato agrupador
 *
 */
public class BeanAltaContratoAgrupadorRequest implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 7657160024207125154L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	
	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del Contrato de Inversión
	 */
	@Size(max=4)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato	
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Tipo de agrupación	
	 */
	@Size(max=3)
	private String tipoAgrupacion;
	/**
	 * Código de ejecutivo de la agrupación	
	 */
	@Size(max=8)
	private String codigoEjecutivo;
	/**
	 * Número telefónico
	 */
	@Size(max=10)
	private String numeroTelefonico;
	/**
	 * Moneda del grupo	
	 */
	@Size(max=3)
	private String monedaGrupo;
	/**
	 * Nombre del grupo	
	 */
	@Size(max=30)
	private String nombreGrupo;
	/**
	 * Descripción del grupo
	 */
	@Size(max=50)
	private String descripcionGrupo;
	/**
	 * Indicador de tipo de grupo	
	 */
	@Size(max=1)
	private String indicadorTipoGrupo;
	/**
	 * Código del grupo de usuarios	
	 */
	@Size(max=8)
	private String codigoGrupoUsuario;
	/**
	 * Fecha de comienzo de grupo
	 */
	@Size(max=10)
	private String fechaComienzoGrupo;
	/**
	 * Fecha de fin de grupo	
	 */
	@Size(max=10)
	private String fechaFinGrupo;
	/**
	 * Indicador de uso del grupo	
	 */
	@Size(max=1)
	private String indicadorUsoGrupo;
	
	/**
	 * Constructor para la multi-canalidad
	 */
	public BeanAltaContratoAgrupadorRequest(){
		multicanalidad=new MulticanalidadGenerica();
	}
	
	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipoConsulta the tipConsu to set
	 */
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param idEmpr the idEmpr to set
	 */
	public void setEmpresaContrato(String idEmpr) {
		this.empresaContrato = idEmpr;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param idProd the idProd to set
	 */
	public void setCodigoProducto(String idProd) {
		this.codigoProducto = idProd;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idStiPro to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}
	/**
	 * @param empresaContratoInversion the idEmprCo to set
	 */
	public void setEmpresaContratoInversion(String empresaContratoInversion) {
		this.empresaContratoInversion = empresaContratoInversion;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param aliasContrato the desAlias to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}
	/**
	 * @return the tipAgrup
	 */
	public String getTipoAgrupacion() {
		return tipoAgrupacion;
	}
	/**
	 * @param tipoAgrupacion the tipAgrup to set
	 */
	public void setTipoAgrupacion(String tipoAgrupacion) {
		this.tipoAgrupacion = tipoAgrupacion;
	}
	/**
	 * @return the codEjecu
	 */
	public String getCodigoEjecutivo() {
		return codigoEjecutivo;
	}
	/**
	 * @param codigoEjecutivo the codEjecu to set
	 */
	public void setCodigoEjecutivo(String codigoEjecutivo) {
		this.codigoEjecutivo = codigoEjecutivo;
	}
	/**
	 * @return the peNumTel
	 */
	public String getNumeroTelefonico() {
		return numeroTelefonico;
	}
	/**
	 * @param numeroTelefonico the peNumTel to set
	 */
	public void setNumeroTelefonico(String numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}
	/**
	 * @return the codMonSw
	 */
	public String getMonedaGrupo() {
		return monedaGrupo;
	}
	/**
	 * @param monedaGrupo the codMonSw to set
	 */
	public void setMonedaGrupo(String monedaGrupo) {
		this.monedaGrupo = monedaGrupo;
	}
	/**
	 * @return the nomGrupo
	 */
	public String getNombreGrupo() {
		return nombreGrupo;
	}
	/**
	 * @param nombreGrupo the nomGrupo to set
	 */
	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}
	/**
	 * @return the desGrupo
	 */
	public String getDescripcionGrupo() {
		return descripcionGrupo;
	}
	/**
	 * @param descripcionGrupo the desGrupo to set
	 */
	public void setDescripcionGrupo(String descripcionGrupo) {
		this.descripcionGrupo = descripcionGrupo;
	}
	/**
	 * @return the indTiGru
	 */
	public String getIndicadorTipoGrupo() {
		return indicadorTipoGrupo;
	}
	/**
	 * @param indicadorTipoGrupo the indTiGru to set
	 */
	public void setIndicadorTipoGrupo(String indicadorTipoGrupo) {
		this.indicadorTipoGrupo = indicadorTipoGrupo;
	}
	/**
	 * @return the idGruUsu
	 */
	public String getCodigoGrupoUsuario() {
		return codigoGrupoUsuario;
	}
	/**
	 * @param codigoGrupoUsuario the idGruUsu to set
	 */
	public void setCodigoGrupoUsuario(String codigoGrupoUsuario) {
		this.codigoGrupoUsuario = codigoGrupoUsuario;
	}
	/**
	 * @return the feComVal
	 */
	public String getFechaComienzoGrupo() {
		return fechaComienzoGrupo;
	}
	/**
	 * @param fechaComienzoGrupo the feComVal to set
	 */
	public void setFechaComienzoGrupo(String fechaComienzoGrupo) {
		this.fechaComienzoGrupo = fechaComienzoGrupo;
	}
	/**
	 * @return the feFinVal
	 */
	public String getFechaFinGrupo() {
		return fechaFinGrupo;
	}
	/**
	 * @param fechaFinGrupo the feFinVal to set
	 */
	public void setFechaFinGrupo(String fechaFinGrupo) {
		this.fechaFinGrupo = fechaFinGrupo;
	}
	/**
	 * @return the indUsoGr
	 */
	public String getIndicadorUsoGrupo() {
		return indicadorUsoGrupo;
	}
	/**
	 * @param indicadorUsoGrupo the indUsoGr to set
	 */
	public void setIndicadorUsoGrupo(String indicadorUsoGrupo) {
		this.indicadorUsoGrupo = indicadorUsoGrupo;
	}

	/**
	 * 
	 * @return MulticanalidadGenerica regresa el objeto de multicanlidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
	
}
