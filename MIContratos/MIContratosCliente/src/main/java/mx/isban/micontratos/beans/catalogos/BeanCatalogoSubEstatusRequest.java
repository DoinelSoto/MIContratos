package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;


/**
 * @author everis
 *
 */

/** Bean con los datos de entrada para la transaccion :
 * 
	MI05	Catálogo de sub-Estatus o detalle de Estatus

 * */
public class BeanCatalogoSubEstatusRequest implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 1863678361048771971L;
	
	/**
	 * Código de estatus
	 */
	@Size(max=2)
	private String codigoEstatus;
	/**
	 * Código de subestatus
	 */
	@Size(max=2)
	private String codigoSubEstatus;

	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Constructor para la instancia
	 */
	public BeanCatalogoSubEstatusRequest(){
		
		rellamada = new BeanPaginacion();
		
	}

	/**
	 * @return the codEstat
	 */
	public String getCodigoEstatus() {
		return codigoEstatus;
	}
	/**
	 * @param codigoEstatus the codEstat to set
	 */
	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}

	/**
	 * @return the codSubes
	 */
	public String getCodigoSubEstatus() {
		return codigoSubEstatus;
	}
	/**
	 * @param codigoSubEstatus the codSubes to set
	 */
	public void setCodigoSubEstatus(String codigoSubEstatus) {
		this.codigoSubEstatus = codigoSubEstatus;
	}

	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	

}
