/**
 * 
 */
package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

/**
 * @author everis
 *
 */
public class BeanDatosDomicilioResponse implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -4833322163209966262L;
	
	/**
	 * num de persona de uso de domicilio 1	
	 */
	private String numeroPersonaUsoDomicilio;
	
	/**
	 * tipo de uso domicilio 1	
	 */
	private String tipoUsoDomicilio;
	
	/**
	 * sec de domicilio 1	
	 */
	private int secuenciaDomicilio;

	/**
	 * 
	 * @return the numeroPersonaUsoDomicilio
	 */
	public String getNumeroPersonaUsoDomicilio() {
		return numeroPersonaUsoDomicilio;
	}
	
	/**
	 * 
	 * @param numeroPersonaUsoDomicilio
	 */
	public void setNumeroPersonaUsoDomicilio(String numeroPersonaUsoDomicilio) {
		this.numeroPersonaUsoDomicilio = numeroPersonaUsoDomicilio;
	}
	
	/**
	 * 
	 * @return the tipoUsoDomicilio
	 */
	public String getTipoUsoDomicilio() {
		return tipoUsoDomicilio;
	}
	
	/**
	 * 
	 * @param tipoUsoDomicilio
	 */
	public void setTipoUsoDomicilio(String tipoUsoDomicilio) {
		this.tipoUsoDomicilio = tipoUsoDomicilio;
	}
	
	/**
	 * 
	 * @return the secuenciaDomicilio
	 */
	public int getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}
	
	/**
	 * 
	 * @param secuenciaDomicilio
	 */
	public void setSecuenciaDomicilio(int secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}
}