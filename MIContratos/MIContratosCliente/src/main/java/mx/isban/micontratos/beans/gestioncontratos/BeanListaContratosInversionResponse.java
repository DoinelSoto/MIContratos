package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

import javax.validation.constraints.Size;
/**
 * 
 * @author Luis Martínez - everis
 * Bean con los campos de salida de transacción MI40
 *
 */

public class BeanListaContratosInversionResponse implements Serializable{

	/**
	 *Serialización 
	 */
	private static final long serialVersionUID = 2908825245619129777L;
	
	/**
	 *Código de persona del titular 
	 */
	@Size(max=8)
	private String codigoPersonaTitular;
	/**
	 *Descripción del Contrato
	 */
	@Size(max=100)
	private String descripcionContrato;
	/**
	 * Empresa del contrato
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del contrato
	 */
	@Size(max=4)
	private String centroContrato ;
	/**
	 * Número del contrato
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato
	 */
	@Size(max=2)
	private String codigoProductoContrato;
	/**
	 * Código de subproducto del contrato
	 */
	@Size(max=4)
	private String codigoSubProductoContrato;
	/**
	 * Código de monda del contrato
	 */
	@Size(max=3)
	private String codigoMonedaContrato;
	/**
	 * Número de folio del que proviene el contrato
	 */
	private int numeroFolioContrato;
	/**
	 * Código de empresa del contrato de inversión
	 */
	@Size(max=4)
	private String codigoEmpresaContratoInversion;
	/**
	 * Alias del contrato
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Código de banca del contrato de inversión
	 */
	@Size(max=3)
	private String codigoBancaContratoInversion;
	/**
	 * Fecha de apertura del contrato
	 */
	@Size(max=10)
	private String fechaAperturaContrato;
	/**
	 * Fecha de vencimiento del contrato
	 */
	@Size(max=10)
	private String fechaVencimientoContrato;
	/**
	 * Código de ejecutivo del contrato
	 */
	@Size(max=8)
	private String codigoEjecutivoContrato;
	/**
	 * Centro de costos del contrato
	 */
	@Size(max=4)
	private String centroCostosContrato;
	/**
	 * Indicador de tipo de cuenta
	 */
	@Size(max=1)
	private String indicadorTipoCuenta;
	/**
	 * Indicador de tipo de contrato
	 */
	@Size(max=1)
	private String indicadorTipoContrato;
	/**
	 * Indicador autorización posición en corto en capitales
	 */
	@Size(max=1)
	private String inidicadorAutorizacionPosicionCortoCapitales;
	/**
	 * Indicador autorización posición en corto en fondos
	 */
	@Size(max=1)
	private String indicadorAutorizacionPosicionCortoFondos;
	/**
	 * Indicador autorización cruce en fondos de inversión
	 */
	@Size(max=1)
	private String indicadorAutorizacionCruceFondosInversion;
	/**
	 * Indicador de discrecionalidad
	 */
	@Size(max=1)
	private String indicadorDiscrecionalidad;
	/**
	 * Indicador tipo de custodia en directo
	 */
	@Size(max=1)
	private String indicadorTipoCustodiaDirecto;
	/**
	 * Indicador tipo de custodia en Repos
	 */
	@Size(max=1)
	private String indicadorTipoCustodiaRepos;
	/**
	 * Indicador de estado de cuenta
	 */
	@Size(max=1)
	private String indicadorEstatoCuenta;
	/**
	 * Monto máximo a operar en el contrato
	 */
	@Size(max=15)
	private String montoMaximoOperarContrato;
	/**
	 * Divisa del monto máximo a operar en el contrato
	 */
	@Size(max=3)
	private String divisaMontoMaximoOperarContrato;
	/**
	 * Indicador envío por correo carta confirmación capitales
	 */
	@Size(max=1)
	private String indicadorEnvioCorreoCartaConfirmacionCapitales;
	/**
	 * Estatus del contrato
	 */
	@Size(max=2)
	private String estatusContrato;
	/**
	 * Detalle del estatus
	 */
	@Size(max=2)
	private String detalleEstatus;
	/**
	 * Fecha de estado del contrato
	 */
	@Size(max=10)
	private String fechaEstadoContrato;
	/**
	 * Indicador de contrato agrupado
	 */
	@Size(max=1)
	private String indicadorContratoAgrupado;
	/**
	 * Indicador de tipo de posición
	 */
	@Size(max=1)
	private String indicadorTipoPosicion;
	/**
	 * Indicador contrato espejo
	 */
	@Size(max=1)
	private String indicadorContratoEspejo;
	/**
	 * Indicador contrato amparado por una chequera
	 */
	@Size(max=1)
	private String indicadorContratoAmparadoChequera;
	/**
	 * Código de tipo de servicio
	 */
	@Size(max=2)
	private String codigoTipoServicio;
	/**
	 * Código AFI
	 */
	@Size(max=8)
	private String codigoAfiliacion;
	/**
	 * Indicador beneficiarios
	 */
	@Size(max=1)
	private String indicadorBeneficiarios;
	/**
	 * Indicador Proveedores de Recursos
	 */
	@Size(max=1)
	private String indicadorProveedoresRecursos;
	/**
	 * Indicador accionistas
	 */
	@Size(max=1)
	private String indicadorAccionistas;
	/**
	 * Fecha de última opración
	 */
	@Size(max=10)
	private String fechaUlitmaOperacion;
	/**
	 * Fecha de última modificación
	 */
	@Size(max=10)
	private String fechaUltimaModificacion;
	/**
	 * Indicador de operación
	 */
	@Size(max=1)
	private String indicadorOperacion;
	/**
	 * Indicador de consulta
	 */
	@Size(max=1)
	private String indicadorConsulta;
	
	/**
	 * Indicador de autorización posición en corto capitales
	 */
	@Size(max=1)
	private String indicadorAutorizacionPosicionCortoCapitales;
	/**
	 * Indicador de autorización para operar
	 */
	@Size(max=1)
	private String indicadorAutorizacionOperar;
	/**
	 * Indicador de autorización para consultar
	 */
	@Size(max=1)
	private String indicadorAutorizacionConsultar;
	/**
	 * Branch del Contrato
	 */
	@Size(max=2)
	private String branchContrato;

	/**
	 * @return the codigoPersonaTitular
	 */
	public String getCodigoPersonaTitular() {
		return codigoPersonaTitular;
	}

	/**
	 * @param codigoPersonaTitular the codigoPersonaTitular to set
	 */
	public void setCodigoPersonaTitular(String codigoPersonaTitular) {
		this.codigoPersonaTitular = codigoPersonaTitular;
	}

	/**
	 * @return the empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * @param empresaContrato the empresaContrato to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * @return the centroContrato
	 */
	public String getCentroContrato() {
		return centroContrato;
	}

	/**
	 * @param centroContrato the centroContrato to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}

	/**
	 * @return the numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * @param numeroContrato the numeroContrato to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * @return the codigoProductoContrato
	 */
	public String getCodigoProductoContrato() {
		return codigoProductoContrato;
	}

	/**
	 * @param codigoProductoContrato the codigoProductoContrato to set
	 */
	public void setCodigoProductoContrato(String codigoProductoContrato) {
		this.codigoProductoContrato = codigoProductoContrato;
	}

	/**
	 * @return the codigoSubProductoContrato
	 */
	public String getCodigoSubProductoContrato() {
		return codigoSubProductoContrato;
	}

	/**
	 * @param codigoSubProductoContrato the codigoSubProductoContrato to set
	 */
	public void setCodigoSubProductoContrato(String codigoSubProductoContrato) {
		this.codigoSubProductoContrato = codigoSubProductoContrato;
	}

	/**
	 * @return the codigoMonedaContrato
	 */
	public String getCodigoMonedaContrato() {
		return codigoMonedaContrato;
	}

	/**
	 * @param codigoMonedaContrato the codigoMonedaContrato to set
	 */
	public void setCodigoMonedaContrato(String codigoMonedaContrato) {
		this.codigoMonedaContrato = codigoMonedaContrato;
	}

	/**
	 * @return the numeroFolioContrato
	 */
	public int getNumeroFolioContrato() {
		return numeroFolioContrato;
	}

	/**
	 * @param numeroFolioContrato the numeroFolioContrato to set
	 */
	public void setNumeroFolioContrato(int numeroFolioContrato) {
		this.numeroFolioContrato = numeroFolioContrato;
	}

	/**
	 * @return the codigoEmpresaContratoInversion
	 */
	public String getCodigoEmpresaContratoInversion() {
		return codigoEmpresaContratoInversion;
	}

	/**
	 * @param codigoEmpresaContratoInversion the codigoEmpresaContratoInversion to set
	 */
	public void setCodigoEmpresaContratoInversion(
			String codigoEmpresaContratoInversion) {
		this.codigoEmpresaContratoInversion = codigoEmpresaContratoInversion;
	}

	/**
	 * @return the aliasContrato
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}

	/**
	 * @param aliasContrato the aliasContrato to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}

	/**
	 * @return the codigoBancaContratoInversion
	 */
	public String getCodigoBancaContratoInversion() {
		return codigoBancaContratoInversion;
	}

	/**
	 * @param codigoBancaContratoInversion the codigoBancaContratoInversion to set
	 */
	public void setCodigoBancaContratoInversion(String codigoBancaContratoInversion) {
		this.codigoBancaContratoInversion = codigoBancaContratoInversion;
	}

	/**
	 * @return the fechaAperturaContrato
	 */
	public String getFechaAperturaContrato() {
		return fechaAperturaContrato;
	}

	/**
	 * @param fechaAperturaContrato the fechaAperturaContrato to set
	 */
	public void setFechaAperturaContrato(String fechaAperturaContrato) {
		this.fechaAperturaContrato = fechaAperturaContrato;
	}

	/**
	 * @return the fechaVencimientoContrato
	 */
	public String getFechaVencimientoContrato() {
		return fechaVencimientoContrato;
	}

	/**
	 * @param fechaVencimientoContrato the fechaVencimientoContrato to set
	 */
	public void setFechaVencimientoContrato(String fechaVencimientoContrato) {
		this.fechaVencimientoContrato = fechaVencimientoContrato;
	}

	/**
	 * @return the codigoEjecutivoContrato
	 */
	public String getCodigoEjecutivoContrato() {
		return codigoEjecutivoContrato;
	}

	/**
	 * @param codigoEjecutivoContrato the codigoEjecutivoContrato to set
	 */
	public void setCodigoEjecutivoContrato(String codigoEjecutivoContrato) {
		this.codigoEjecutivoContrato = codigoEjecutivoContrato;
	}

	/**
	 * @return the centroCostosContrato
	 */
	public String getCentroCostosContrato() {
		return centroCostosContrato;
	}

	/**
	 * @param centroCostosContrato the centroCostosContrato to set
	 */
	public void setCentroCostosContrato(String centroCostosContrato) {
		this.centroCostosContrato = centroCostosContrato;
	}

	/**
	 * @return the indicadorTipoCuenta
	 */
	public String getIndicadorTipoCuenta() {
		return indicadorTipoCuenta;
	}

	/**
	 * @param indicadorTipoCuenta the indicadorTipoCuenta to set
	 */
	public void setIndicadorTipoCuenta(String indicadorTipoCuenta) {
		this.indicadorTipoCuenta = indicadorTipoCuenta;
	}

	/**
	 * @return the indicadorTipoContrato
	 */
	public String getIndicadorTipoContrato() {
		return indicadorTipoContrato;
	}

	/**
	 * @param indicadorTipoContrato the indicadorTipoContrato to set
	 */
	public void setIndicadorTipoContrato(String indicadorTipoContrato) {
		this.indicadorTipoContrato = indicadorTipoContrato;
	}

	/**
	 * @return the inidicadorAutorizacionPosicionCortoCapitales
	 */
	public String getInidicadorAutorizacionPosicionCortoCapitales() {
		return inidicadorAutorizacionPosicionCortoCapitales;
	}

	/**
	 * @param inidicadorAutorizacionPosicionCortoCapitales the inidicadorAutorizacionPosicionCortoCapitales to set
	 */
	public void setInidicadorAutorizacionPosicionCortoCapitales(
			String inidicadorAutorizacionPosicionCortoCapitales) {
		this.inidicadorAutorizacionPosicionCortoCapitales = inidicadorAutorizacionPosicionCortoCapitales;
	}

	/**
	 * @return the indicadorAutorizacionPosicionCortoFondos
	 */
	public String getIndicadorAutorizacionPosicionCortoFondos() {
		return indicadorAutorizacionPosicionCortoFondos;
	}

	/**
	 * @param indicadorAutorizacionPosicionCortoFondos the indicadorAutorizacionPosicionCortoFondos to set
	 */
	public void setIndicadorAutorizacionPosicionCortoFondos(
			String indicadorAutorizacionPosicionCortoFondos) {
		this.indicadorAutorizacionPosicionCortoFondos = indicadorAutorizacionPosicionCortoFondos;
	}

	/**
	 * @return the indicadorAutorizacionCruceFondosInversion
	 */
	public String getIndicadorAutorizacionCruceFondosInversion() {
		return indicadorAutorizacionCruceFondosInversion;
	}

	/**
	 * @param indicadorAutorizacionCruceFondosInversion the indicadorAutorizacionCruceFondosInversion to set
	 */
	public void setIndicadorAutorizacionCruceFondosInversion(
			String indicadorAutorizacionCruceFondosInversion) {
		this.indicadorAutorizacionCruceFondosInversion = indicadorAutorizacionCruceFondosInversion;
	}

	/**
	 * @return the indicadorDiscrecionalidad
	 */
	public String getIndicadorDiscrecionalidad() {
		return indicadorDiscrecionalidad;
	}

	/**
	 * @param indicadorDiscrecionalidad the indicadorDiscrecionalidad to set
	 */
	public void setIndicadorDiscrecionalidad(String indicadorDiscrecionalidad) {
		this.indicadorDiscrecionalidad = indicadorDiscrecionalidad;
	}

	/**
	 * @return the indicadorTipoCustodiaDirecto
	 */
	public String getIndicadorTipoCustodiaDirecto() {
		return indicadorTipoCustodiaDirecto;
	}

	/**
	 * @param indicadorTipoCustodiaDirecto the indicadorTipoCustodiaDirecto to set
	 */
	public void setIndicadorTipoCustodiaDirecto(String indicadorTipoCustodiaDirecto) {
		this.indicadorTipoCustodiaDirecto = indicadorTipoCustodiaDirecto;
	}

	/**
	 * @return the indicadorTipoCustodiaRepos
	 */
	public String getIndicadorTipoCustodiaRepos() {
		return indicadorTipoCustodiaRepos;
	}

	/**
	 * @param indicadorTipoCustodiaRepos the indicadorTipoCustodiaRepos to set
	 */
	public void setIndicadorTipoCustodiaRepos(String indicadorTipoCustodiaRepos) {
		this.indicadorTipoCustodiaRepos = indicadorTipoCustodiaRepos;
	}

	/**
	 * @return the indicadorEstatoCuenta
	 */
	public String getIndicadorEstatoCuenta() {
		return indicadorEstatoCuenta;
	}

	/**
	 * @param indicadorEstatoCuenta the indicadorEstatoCuenta to set
	 */
	public void setIndicadorEstatoCuenta(String indicadorEstatoCuenta) {
		this.indicadorEstatoCuenta = indicadorEstatoCuenta;
	}

	/**
	 * @return the montoMaximoOperarContrato
	 */
	public String getMontoMaximoOperarContrato() {
		return montoMaximoOperarContrato;
	}

	/**
	 * @param montoMaximoOperarContrato the montoMaximoOperarContrato to set
	 */
	public void setMontoMaximoOperarContrato(String montoMaximoOperarContrato) {
		this.montoMaximoOperarContrato = montoMaximoOperarContrato;
	}

	/**
	 * @return the divisaMontoMaximoOperarContrato
	 */
	public String getDivisaMontoMaximoOperarContrato() {
		return divisaMontoMaximoOperarContrato;
	}

	/**
	 * @param divisaMontoMaximoOperarContrato the divisaMontoMaximoOperarContrato to set
	 */
	public void setDivisaMontoMaximoOperarContrato(
			String divisaMontoMaximoOperarContrato) {
		this.divisaMontoMaximoOperarContrato = divisaMontoMaximoOperarContrato;
	}

	/**
	 * @return the indicadorEnvioCorreoCartaConfirmacionCapitales
	 */
	public String getIndicadorEnvioCorreoCartaConfirmacionCapitales() {
		return indicadorEnvioCorreoCartaConfirmacionCapitales;
	}

	/**
	 * @param indicadorEnvioCorreoCartaConfirmacionCapitales the indicadorEnvioCorreoCartaConfirmacionCapitales to set
	 */
	public void setIndicadorEnvioCorreoCartaConfirmacionCapitales(
			String indicadorEnvioCorreoCartaConfirmacionCapitales) {
		this.indicadorEnvioCorreoCartaConfirmacionCapitales = indicadorEnvioCorreoCartaConfirmacionCapitales;
	}

	/**
	 * @return the estatusContrato
	 */
	public String getEstatusContrato() {
		return estatusContrato;
	}

	/**
	 * @param estatusContrato the estatusContrato to set
	 */
	public void setEstatusContrato(String estatusContrato) {
		this.estatusContrato = estatusContrato;
	}

	/**
	 * @return the detalleEstatus
	 */
	public String getDetalleEstatus() {
		return detalleEstatus;
	}

	/**
	 * @param detalleEstatus the detalleEstatus to set
	 */
	public void setDetalleEstatus(String detalleEstatus) {
		this.detalleEstatus = detalleEstatus;
	}

	/**
	 * @return the fechaEstadoContrato
	 */
	public String getFechaEstadoContrato() {
		return fechaEstadoContrato;
	}

	/**
	 * @param fechaEstadoContrato the fechaEstadoContrato to set
	 */
	public void setFechaEstadoContrato(String fechaEstadoContrato) {
		this.fechaEstadoContrato = fechaEstadoContrato;
	}

	/**
	 * @return the indicadorContratoAgrupado
	 */
	public String getIndicadorContratoAgrupado() {
		return indicadorContratoAgrupado;
	}

	/**
	 * @param indicadorContratoAgrupado the indicadorContratoAgrupado to set
	 */
	public void setIndicadorContratoAgrupado(String indicadorContratoAgrupado) {
		this.indicadorContratoAgrupado = indicadorContratoAgrupado;
	}

	/**
	 * @return the indicadorTipoPosicion
	 */
	public String getIndicadorTipoPosicion() {
		return indicadorTipoPosicion;
	}

	/**
	 * @param indicadorTipoPosicion the indicadorTipoPosicion to set
	 */
	public void setIndicadorTipoPosicion(String indicadorTipoPosicion) {
		this.indicadorTipoPosicion = indicadorTipoPosicion;
	}

	/**
	 * @return the indicadorContratoEspejo
	 */
	public String getIndicadorContratoEspejo() {
		return indicadorContratoEspejo;
	}

	/**
	 * @param indicadorContratoEspejo the indicadorContratoEspejo to set
	 */
	public void setIndicadorContratoEspejo(String indicadorContratoEspejo) {
		this.indicadorContratoEspejo = indicadorContratoEspejo;
	}

	/**
	 * @return the indicadorContratoAmparadoChequera
	 */
	public String getIndicadorContratoAmparadoChequera() {
		return indicadorContratoAmparadoChequera;
	}

	/**
	 * @param indicadorContratoAmparadoChequera the indicadorContratoAmparadoChequera to set
	 */
	public void setIndicadorContratoAmparadoChequera(
			String indicadorContratoAmparadoChequera) {
		this.indicadorContratoAmparadoChequera = indicadorContratoAmparadoChequera;
	}

	/**
	 * @return the codigoTipoServicio
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}

	/**
	 * @param codigoTipoServicio the codigoTipoServicio to set
	 */
	public void setCodigoTipoServicio(String codigoTipoServicio) {
		this.codigoTipoServicio = codigoTipoServicio;
	}

	/**
	 * @return the codigoAfiliacion
	 */
	public String getCodigoAfiliacion() {
		return codigoAfiliacion;
	}

	/**
	 * @param codigoAfiliacion the codigoAfiliacion to set
	 */
	public void setCodigoAfiliacion(String codigoAfiliacion) {
		this.codigoAfiliacion = codigoAfiliacion;
	}

	/**
	 * @return the indicadorBeneficiarios
	 */
	public String getIndicadorBeneficiarios() {
		return indicadorBeneficiarios;
	}

	/**
	 * @param indicadorBeneficiarios the indicadorBeneficiarios to set
	 */
	public void setIndicadorBeneficiarios(String indicadorBeneficiarios) {
		this.indicadorBeneficiarios = indicadorBeneficiarios;
	}

	/**
	 * @return the indicadorProveedoresRecursos
	 */
	public String getIndicadorProveedoresRecursos() {
		return indicadorProveedoresRecursos;
	}

	/**
	 * @param indicadorProveedoresRecursos the indicadorProveedoresRecursos to set
	 */
	public void setIndicadorProveedoresRecursos(String indicadorProveedoresRecursos) {
		this.indicadorProveedoresRecursos = indicadorProveedoresRecursos;
	}

	/**
	 * @return the indicadorAccionistas
	 */
	public String getIndicadorAccionistas() {
		return indicadorAccionistas;
	}

	/**
	 * @param indicadorAccionistas the indicadorAccionistas to set
	 */
	public void setIndicadorAccionistas(String indicadorAccionistas) {
		this.indicadorAccionistas = indicadorAccionistas;
	}

	/**
	 * @return the fechaUlitmaOperacion
	 */
	public String getFechaUlitmaOperacion() {
		return fechaUlitmaOperacion;
	}

	/**
	 * @param fechaUlitmaOperacion the fechaUlitmaOperacion to set
	 */
	public void setFechaUlitmaOperacion(String fechaUlitmaOperacion) {
		this.fechaUlitmaOperacion = fechaUlitmaOperacion;
	}

	/**
	 * @return the fechaUltimaModificacion
	 */
	public String getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	/**
	 * @param fechaUltimaModificacion the fechaUltimaModificacion to set
	 */
	public void setFechaUltimaModificacion(String fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	/**
	 * @return the indicadorOperacion
	 */
	public String getIndicadorOperacion() {
		return indicadorOperacion;
	}

	/**
	 * @param indicadorOperacion the indicadorOperacion to set
	 */
	public void setIndicadorOperacion(String indicadorOperacion) {
		this.indicadorOperacion = indicadorOperacion;
	}

	/**
	 * @return the indicadorConsulta
	 */
	public String getIndicadorConsulta() {
		return indicadorConsulta;
	}

	/**
	 * @param indicadorConsulta the indicadorConsulta to set
	 */
	public void setIndicadorConsulta(String indicadorConsulta) {
		this.indicadorConsulta = indicadorConsulta;
	}

	/**
	 * @return the indicadorAutorizacionPosicionCortoCapitales
	 */
	public String getIndicadorAutorizacionPosicionCortoCapitales() {
		return indicadorAutorizacionPosicionCortoCapitales;
	}

	/**
	 * @param indicadorAutorizacionPosicionCortoCapitales the indicadorAutorizacionPosicionCortoCapitales to set
	 */
	public void setIndicadorAutorizacionPosicionCortoCapitales(String indicadorAutorizacionPosicionCortoCapitales) {
		this.indicadorAutorizacionPosicionCortoCapitales = indicadorAutorizacionPosicionCortoCapitales;
	}

	/**
	 * @return the indicadorAutorizacionOperar
	 */
	public String getIndicadorAutorizacionOperar() {
		return indicadorAutorizacionOperar;
	}

	/**
	 * @param indicadorAutorizacionOperar the indicadorAutorizacionOperar to set
	 */
	public void setIndicadorAutorizacionOperar(String indicadorAutorizacionOperar) {
		this.indicadorAutorizacionOperar = indicadorAutorizacionOperar;
	}

	/**
	 * @return the indicadorAutorizacionConsultar
	 */
	public String getIndicadorAutorizacionConsultar() {
		return indicadorAutorizacionConsultar;
	}

	/**
	 * @param indicadorAutorizacionConsultar the indicadorAutorizacionConsultar to set
	 */
	public void setIndicadorAutorizacionConsultar(String indicadorAutorizacionConsultar) {
		this.indicadorAutorizacionConsultar = indicadorAutorizacionConsultar;
	}
	
	/**
	 * @return the indicadorAutorizacionConsultar
	 */
	public String getDescripcionContrato() {
		return descripcionContrato;
	}

	/**
	 * @param descripcionContrato the descripcionContrato to set
	 */
	public void setDescripcionContrato(String descripcionContrato) {
		this.descripcionContrato = descripcionContrato;
	}
	
	/**
	 * @return the branchContrato
	 */
	public String getBranchContrato() {
		return branchContrato;
	}

	/**
	 * @param branchContrato the branchContrato to set
	 */
	public void setBranchContrato(String branchContrato) {
		this.branchContrato = branchContrato;
	}
	
}


