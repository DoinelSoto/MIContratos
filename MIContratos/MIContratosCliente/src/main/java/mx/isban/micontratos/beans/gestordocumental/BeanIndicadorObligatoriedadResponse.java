package mx.isban.micontratos.beans.gestordocumental;

import java.io.Serializable;

/**
 * 
 * Bean con los datos de respuesta para la transacción : MI Indicador obligatoriedad
 *
 */
public class BeanIndicadorObligatoriedadResponse implements Serializable{

	
	/**
	 * Serializacion DTO
	 */
	private static final long serialVersionUID = -781267146533760335L;
	
	/**
	 * Calidad de participación
	 */
	private String calidadParticipacion;

	/**
	 * Número de persona
	 */
	private String numeroPersona;
	/**
	 * Código de documento
	 */
	private String codigoDocumento;
	/**
	 * Indicador de obligatoriedad		
	 */
	private String indicadorObligatoriedad;
	/**
	 * Comentarios
	 */
	private String comentarios;
	/**
	 * Código estado documentos
	 */
	private String codigoEstadoDocumentos;
	/**
	 * @return the indOblig
	 */
	public String getIndicadorObligatoriedad() {
		return indicadorObligatoriedad;
	}
	/**
	 * @param indicadorObligatoriedad the indOblig to set
	 */
	public void setIndicadorObligatoriedad(String indicadorObligatoriedad) {
		this.indicadorObligatoriedad = indicadorObligatoriedad;
	}
	/**
	 * @return the calidadParticipacion
	 */
	public String getCalidadParticipacion() {
		return calidadParticipacion;
	}
	/**
	 * @param calidadParticipacion the calidadParticipacion to set
	 */
	public void setCalidadParticipacion(String calidadParticipacion) {
		this.calidadParticipacion = calidadParticipacion;
	}
	/**
	 * @return the numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	/**
	 * @param numeroPersona the numeroPersona to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	/**
	 * @return the codigoDocumento
	 */
	public String getCodigoDocumento() {
		return codigoDocumento;
	}
	/**
	 * @param codigoDocumento the codigoDocumento to set
	 */
	public void setCodigoDocumento(String codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}
	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}
	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	/**
	 * @return the codigoEstadoDocumentos
	 */
	public String getCodigoEstadoDocumentos() {
		return codigoEstadoDocumentos;
	}
	/**
	 * @param codigoEstadoDocumentos the codigoEstadoDocumentos to set
	 */
	public void setCodigoEstadoDocumentos(String codigoEstadoDocumentos) {
		this.codigoEstadoDocumentos = codigoEstadoDocumentos;
	}
}
