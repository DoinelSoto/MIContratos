package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transaccion MI26 : baja Estatus Contratos
 *
 */
public class BeanBajaEstatusContratosRequest  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 7372768171117321835L;
	/**
	 * Código de estatus	
	 */
	@Size(max=2)
	private String codigoEstatus;

	/**
	 * @return the codEstat
	 */
	public String getCodigoEstatus() {
		return codigoEstatus;
	}

	/**
	 * @param codigoEstatus the codEstat to set
	 */
	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}
	
}
