package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;


/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI16 : Consulta de datos del contrato de inversión
 *
 */
public class BeanListaContactosResponse implements Serializable{

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 8903453546468727125L;
	
	/**
	 * Tipo de contrato
	 */
	private String tipoContacto;
	
	/**
	 * Secuencia contrato
	 */
	private int secuenciaContacto;

	/**
	 * @return the tipoContacto
	 */
	public String getTipoContacto() {
		return tipoContacto;
	}

	/**
	 * @param tipoContacto the tipoContacto to set
	 */
	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	/**
	 * @return the secuenciaContacto
	 */
	public int getSecuenciaContacto() {
		return secuenciaContacto;
	}

	/**
	 * @param secuenciaContacto the secuenciaContacto to set
	 */
	public void setSecuenciaContacto(int secuenciaContacto) {
		this.secuenciaContacto = secuenciaContacto;
	}
}
