package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

/**
 * 
 * Bean que contiene los datos de contratos
 *
 */
public class BeanDatosContratos implements Serializable{

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -4271421930625090119L;
	
	
	/**
	 * Código de empresa del producto	
	 */
	private String codigoEmpresa;
	/**
	 * Centro del contrato	
	 */
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	private String numeroContrato;
	/**
	 * Código de producto del contrato	
	 */
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato	
	 */
	private String codigoSubProductoContrato;
	/**
	 * Código de moneda del contrato	
	 */
	private String codigoMonedaContrato;
	/**
	 * Número de folio del que proviene el contrato
	 */
	private int numeroFolio;
	/**
	 * Código de empresa del contrato de Inversión
	 */
	private String codigoEmpresaContrato;
	/**
	 * Código de banca del contrato de Inversión	
	 */
	private String codigoBanca;
	/**
	 * Fecha de apertura del contrato	
	 */
	private String fechaApertura;
	/**
	 * Alias del contrato
	 */
	private String descripcionAlias;
	/**
	 * Fecha de vencimiento del contrato	
	 */
	private String fechaVencimiento;
	/**
	 * Código de ejecutivo del contrato	
	 */
	private String codigoEjecutivo;
	/**
	 * Centro de costos del contrato	
	 */
	private String centroCostosContrato;
	/**
	 * Indicador de tipo de cuenta	
	 */
	private String indicadorTipoCuenta;
	/**
	 * Indicador de tipo de Contrato	
	 */
	private String indicadorTipoContrato;
	/**
	 * Indicador autorización posición en corto en capitales	
	 */
	private String indicadorAutorizacionPosicionCortoCapitales;
	/**
	 * Indicador autorización posición en corto en fondos	
	 */
	private String indicadorAutorizacionPosicionCortoFondos;
	/**
	 * Indicador autorización cruce en fondos de inversión
	 */
	private String indicadorAutorizacionCruceFondosInversion;
	/**
	 * Indicador de discrecionalidad	
	 */
	private String indicadorDiscrecionalidad;
	/**
	 * Indicador tipo de custodia en directo	
	 */
	private String indicadorTipoCustodaDirecto;
	/**
	 * Indicador tipo de custodia en Repos
	 */
	private String indicadorTipoCustodiaRepos;
	/**
	 * Indicador de estado de cuenta	
	 */
	private String indicadorEstadoCuenta;
	/**
	 * Monto Máximo a operar en el contrato	
	 */
	private String importeMaximoOperacion;
	/**
	 * Divisa del Monto Máximo a operar en el contrato	
	 */
	private String divisaMaximaOperacion;
	/**
	 * Indicador envío por correo carta confirmación capitales
	 */
	private String indicadorEnvioCorreoCartaConfirmacionCapitales;
	/**
	 * Estatus del contrato	
	 */
	private String estatusContrato;
	/**
	 * Detalle del estatus	
	 */
	private String detalleEstatus;
	/**
	 * Fecha de estado del contrato	
	 */
	private String fechaEstado;
	/**
	 * Indicador de Contrato Agrupado	
	 */
	private String indicadorContratoAgrupador;
	/**
	 * Indicador de Tipo de Posición
	 */
	private String indicadorTipoPosicion;
	/**
	 * Indicador de Contrato Espejo
	 */
	private String indicadorContratoEspejo;
	/**
	 * Indicador contrato amparado por una chequera	
	 */
	private String indicadorContratoChequera;
	/**
	 * Código de tipo de servicio	
	 */
	private String codigoTipoServicio;
	/**
	 * Código AFI
	 */
	private String codigoAsesorAfinanciero;
	/**
	 * Indicador Beneficiarios	
	 */
	private String indicadorBeneficiarios;
	/**
	 * Indicador Proveedores de Recursos	
	 */
	private String indicadorProveedoresRecursos;
	/**
	 * Indicador Accionistas	
	 */
	private String indicadorAccionistas;
	/**
	 * Fecha de última operación	
	 */
	private String fechaUltimaOperacion;
	/**
	 * Fecha de última modificación	
	 */
	private String fechaUltimaModificacion;
	/**
	 * Descripción del Contrato	
	 */
	private String descripcionContrato;
	/**
	 * Descripción de la firma
	 */
	private String descripcionFirma;
	/**
	 * Descripción del área de referencia	
	 */
	private String descripcionAreaReferencia;
	/**
	 * Descripción de la referencia externa	
	 */
	private String descripcionReferenciaExterna;
	/**
	 * Descripción de texto libre del subestatus
	 */
	private String descripcionTextoLibreSubestatus;
	/**
	 * Empresa del contrato agrupador	
	 */
	private String empresaContratoAgrupador;
	/**
	 * Centro del contrato agrupador	
	 */
	private String centroContratoAgrupador;
	/**
	 * Número de contrato agrupador	
	 */
	private String numeroContratoAgrupador;
	/**
	 * Código de producto agrupador	
	 */
	private String codigoProductoAgrupador;
	/**
	 * Código de subproducto agrupador	
	 */
	private String codigoSubproductoAgrupador;
	/**
	 * Empresa de contrato agrupador	
	 */
	private String empresaContratoAgrupado;
	/**
	 * Alias de contrato agrupador
	 */
	private String aliasContratoAgrupador;
	/**
	 * Empresa del contrato espejo	
	 */
	private String empresaContratoEspejo;
	/**
	 * Centro del contrato espejo	
	 */
	private String centroContratoEspejo;
	/**
	 * Número de contrato espejo	
	 */
	private String numeroContratoEspejo;
	/**
	 * Código de producto espejo	
	 */
	private String codigoProductoEspejo;
	/**
	 * Código de subproducto espejo	
	 */
	private String codigoSubProductoEspejo;
	/**
	 * Empresa del contrato chequera
	 */
	private String empresaContratoChequera;
	/**
	 * Alias del contrato chequera	
	 */
	private String aliasContratoEspej;
	/**
	 * Fecha de contrato de chequera	
	 */
	private String empresaContratoAlias;
	/**
	 * Centro de contrato de chequera	
	 */
	private String centroContratoChequera;
	/**
	 * Número de contrato chequera	
	 */
	private String numeroContratoChequera;
	/**
	 * Código de producto chequera	
	 */
	private String codigoProductoChequera;
	/**
	 * Código de subproducto chequera	
	 */
	private String codigoSubProductoChequera;
	
	/**
	 * @return the idEmpr
	 */
	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}
	/**
	 * @param codigoEmpresa the idEmpr to set
	 */
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProductoContrato() {
		return codigoSubProductoContrato;
	}
	/**
	 * @param codigoSubProductoContrato the idStiPro to set
	 */
	public void setCodigoSubProductoContrato(String codigoSubProductoContrato) {
		this.codigoSubProductoContrato = codigoSubProductoContrato;
	}
	/**
	 * @return the codMonSw
	 */
	public String getCodigoMonedaContrato() {
		return codigoMonedaContrato;
	}
	/**
	 * @param codigoMonedaContrato the codMonSw to set
	 */
	public void setCodigoMonedaContrato(String codigoMonedaContrato) {
		this.codigoMonedaContrato = codigoMonedaContrato;
	}
	/**
	 * @return the numFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}
	/**
	 * @param numeroFolio the numFolio to set
	 */
	public void setNumeroFolio(int numeroFolio) {
		this.numeroFolio = numeroFolio;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getCodigoEmpresaContrato() {
		return codigoEmpresaContrato;
	}
	/**
	 * @param codigoEmpresaContrato the idEmprCo to set
	 */
	public void setCodigoEmpresaContrato(String codigoEmpresaContrato) {
		this.codigoEmpresaContrato = codigoEmpresaContrato;
	}
	/**
	 * @return the idBanca
	 */
	public String getCodigoBanca() {
		return codigoBanca;
	}
	/**
	 * @param codigoBanca the idBanca to set
	 */
	public void setCodigoBanca(String codigoBanca) {
		this.codigoBanca = codigoBanca;
	}
	/**
	 * @return the fecApert
	 */
	public String getFechaApertura() {
		return fechaApertura;
	}
	/**
	 * @param fechaApertura the fecApert to set
	 */
	public void setFechaApertura(String fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	/**
	 * @return the desAlias
	 */
	public String getDescripcionAlias() {
		return descripcionAlias;
	}
	/**
	 * @param descripcionAlias the desAlias to set
	 */
	public void setDescripcionAlias(String descripcionAlias) {
		this.descripcionAlias = descripcionAlias;
	}
	/**
	 * @return the fecVencim
	 */
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	/**
	 * @param fechaVencimiento the fecVencim to set
	 */
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	/**
	 * @return the codEjecu
	 */
	public String getCodigoEjecutivo() {
		return codigoEjecutivo;
	}
	/**
	 * @param codigoEjecutivo the codEjecu to set
	 */
	public void setCodigoEjecutivo(String codigoEjecutivo) {
		this.codigoEjecutivo = codigoEjecutivo;
	}
	/**
	 * @return the codCenCo
	 */
	public String getCentroCostosContrato() {
		return centroCostosContrato;
	}
	/**
	 * @param centroCostosContrato the codCenCo to set
	 */
	public void setCentroCostosContrato(String centroCostosContrato) {
		this.centroCostosContrato = centroCostosContrato;
	}
	/**
	 * @return the idTipCue
	 */
	public String getIndicadorTipoCuenta() {
		return indicadorTipoCuenta;
	}
	/**
	 * @param indicadorTipoCuenta the idTipCue to set
	 */
	public void setIndicadorTipoCuenta(String indicadorTipoCuenta) {
		this.indicadorTipoCuenta = indicadorTipoCuenta;
	}
	/**
	 * @return the idTipCon
	 */
	public String getIndicadorTipoContrato() {
		return indicadorTipoContrato;
	}
	/**
	 * @param indicadorTipoContrato the idTipCon to set
	 */
	public void setIndicadorTipoContrato(String indicadorTipoContrato) {
		this.indicadorTipoContrato = indicadorTipoContrato;
	}
	/**
	 * @return the indAuPCC
	 */
	public String getIndicadorAutorizacionPosicionCortoCapitales() {
		return indicadorAutorizacionPosicionCortoCapitales;
	}
	/**
	 * @param indicadorAutorizacionPosicionCortoCapitales the indAuPCC to set
	 */
	public void setIndicadorAutorizacionPosicionCortoCapitales(String indicadorAutorizacionPosicionCortoCapitales) {
		this.indicadorAutorizacionPosicionCortoCapitales = indicadorAutorizacionPosicionCortoCapitales;
	}
	/**
	 * @return the indAuPCF
	 */
	public String getIndicadorAutorizacionPosicionCortoFondos() {
		return indicadorAutorizacionPosicionCortoFondos;
	}
	/**
	 * @param indicadorAutorizacionPosicionCortoFondos the indAuPCF to set
	 */
	public void setIndicadorAutorizacionPosicionCortoFondos(String indicadorAutorizacionPosicionCortoFondos) {
		this.indicadorAutorizacionPosicionCortoFondos = indicadorAutorizacionPosicionCortoFondos;
	}
	/**
	 * @return the indAuCFI
	 */
	public String getIndicadorAutorizacionCruceFondosInversion() {
		return indicadorAutorizacionCruceFondosInversion;
	}
	/**
	 * @param indicadorAutorizacionCruceFondosInversion the indAuCFI to set
	 */
	public void setIndicadorAutorizacionCruceFondosInversion(String indicadorAutorizacionCruceFondosInversion) {
		this.indicadorAutorizacionCruceFondosInversion = indicadorAutorizacionCruceFondosInversion;
	}
	/**
	 * @return the indDiscr
	 */
	public String getIndicadorDiscrecionalidad() {
		return indicadorDiscrecionalidad;
	}
	/**
	 * @param indicadorDiscrecionalidad the indDiscr to set
	 */
	public void setIndicadorDiscrecionalidad(String indicadorDiscrecionalidad) {
		this.indicadorDiscrecionalidad = indicadorDiscrecionalidad;
	}
	/**
	 * @return the indTiCuD
	 */
	public String getIndicadorTipoCustodaDirecto() {
		return indicadorTipoCustodaDirecto;
	}
	/**
	 * @param indicadorTipoCustodaDirecto the indTiCuD to set
	 */
	public void setIndicadorTipoCustodaDirecto(String indicadorTipoCustodaDirecto) {
		this.indicadorTipoCustodaDirecto = indicadorTipoCustodaDirecto;
	}
	/**
	 * @return the indTiCuR
	 */
	public String getIndicadorTipoCustodiaRepos() {
		return indicadorTipoCustodiaRepos;
	}
	/**
	 * @param indicadorTipoCustodiaRepos the indTiCuR to set
	 */
	public void setIndicadorTipoCustodiaRepos(String indicadorTipoCustodiaRepos) {
		this.indicadorTipoCustodiaRepos = indicadorTipoCustodiaRepos;
	}
	/**
	 * @return the indEstCu
	 */
	public String getIndicadorEstadoCuenta() {
		return indicadorEstadoCuenta;
	}
	/**
	 * @param indicadorEstadoCuenta the indEstCu to set
	 */
	public void setIndicadorEstadoCuenta(String indicadorEstadoCuenta) {
		this.indicadorEstadoCuenta = indicadorEstadoCuenta;
	}
	/**
	 * @return the impMaxOp
	 */
	public String getImporteMaximoOperacion() {
		return importeMaximoOperacion;
	}
	/**
	 * @param impMaxOp the impMaxOp to set
	 */
	public void setImporteMaximoOperacion(String impMaxOp) {
		this.importeMaximoOperacion = impMaxOp;
	}
	/**
	 * @return the divMaxOp
	 */
	public String getDivisaMaximaOperacion() {
		return divisaMaximaOperacion;
	}
	/**
	 * @param divisaMaximaOperacion the divMaxOp to set
	 */
	public void setDivisaMaximaOperacion(String divisaMaximaOperacion) {
		this.divisaMaximaOperacion = divisaMaximaOperacion;
	}
	/**
	 * @return the indEnCoC
	 */
	public String getIndicadorEnvioCorreoCartaConfirmacionCapitales() {
		return indicadorEnvioCorreoCartaConfirmacionCapitales;
	}
	/**
	 * @param indicadorEnvioCorreoCartaConfirmacionCapitales the indEnCoC to set
	 */
	public void setIndicadorEnvioCorreoCartaConfirmacionCapitales(String indicadorEnvioCorreoCartaConfirmacionCapitales) {
		this.indicadorEnvioCorreoCartaConfirmacionCapitales = indicadorEnvioCorreoCartaConfirmacionCapitales;
	}
	/**
	 * @return the codEstad
	 */
	public String getEstatusContrato() {
		return estatusContrato;
	}
	/**
	 * @param estatusContrato the codEstad to set
	 */
	public void setEstatusContrato(String estatusContrato) {
		this.estatusContrato = estatusContrato;
	}
	/**
	 * @return the codDetEs
	 */
	public String getDetalleEstatus() {
		return detalleEstatus;
	}
	/**
	 * @param detalleEstatus the codDetEs to set
	 */
	public void setDetalleEstatus(String detalleEstatus) {
		this.detalleEstatus = detalleEstatus;
	}
	/**
	 * @return the fecEstad
	 */
	public String getFechaEstado() {
		return fechaEstado;
	}
	/**
	 * @param fechaEstado the fecEstad to set
	 */
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}


	/**
	 * @return the indCoAgr
	 */
	public String getIndicadorContratoAgrupador() {
		return indicadorContratoAgrupador;
	}
	/**
	 * @param indicadorContratoAgrupador the indCoAgr to set
	 */
	public void setIndicadorContratoAgrupador(String indicadorContratoAgrupador) {
		this.indicadorContratoAgrupador = indicadorContratoAgrupador;
	}
	/**
	 * @return the indTiPos
	 */
	public String getIndicadorTipoPosicion() {
		return indicadorTipoPosicion;
	}
	/**
	 * @param indicadorTipoPosicion the indTiPos to set
	 */
	public void setIndicadorTipoPosicion(String indicadorTipoPosicion) {
		this.indicadorTipoPosicion = indicadorTipoPosicion;
	}
	/**
	 * @return the indCoEsp
	 */
	public String getIndicadorContratoEspejo() {
		return indicadorContratoEspejo;
	}
	/**
	 * @param indicadorContratoEspejo the indCoEsp to set
	 */
	public void setIndicadorContratoEspejo(String indicadorContratoEspejo) {
		this.indicadorContratoEspejo = indicadorContratoEspejo;
	}
	/**
	 * @return the indCoChe
	 */
	public String getIndicadorContratoChequera() {
		return indicadorContratoChequera;
	}
	/**
	 * @param indicadorContratoChequera the indCoChe to set
	 */
	public void setIndicadorContratoChequera(String indicadorContratoChequera) {
		this.indicadorContratoChequera = indicadorContratoChequera;
	}
	/**
	 * @return the codTipSe
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}
	/**
	 * @param codigoTipoServicio the codTipSe to set
	 */
	public void setCodigoTipoServicio(String codigoTipoServicio) {
		this.codigoTipoServicio = codigoTipoServicio;
	}
	/**
	 * @return the codAFi
	 */
	public String getCodigoAsesorAfinanciero() {
		return codigoAsesorAfinanciero;
	}
	/**
	 * @param codigoAsesorAfinanciero the codAFi to set
	 */
	public void setCodigoAsesorAfinanciero(String codigoAsesorAfinanciero) {
		this.codigoAsesorAfinanciero = codigoAsesorAfinanciero;
	}
	/**
	 * @return the indCoBen
	 */
	public String getIndicadorBeneficiarios() {
		return indicadorBeneficiarios;
	}
	/**
	 * @param indicadorBeneficiarios the indCoBen to set
	 */
	public void setIndicadorBeneficiarios(String indicadorBeneficiarios) {
		this.indicadorBeneficiarios = indicadorBeneficiarios;
	}
	/**
	 * @return the indCoPro
	 */
	public String getIndicadorProveedoresRecursos() {
		return indicadorProveedoresRecursos;
	}
	/**
	 * @param indicadorProveedoresRecursos the indCoPro to set
	 */
	public void setIndicadorProveedoresRecursos(String indicadorProveedoresRecursos) {
		this.indicadorProveedoresRecursos = indicadorProveedoresRecursos;
	}
	/**
	 * @return the indCoAcc
	 */
	public String getIndicadorAccionistas() {
		return indicadorAccionistas;
	}
	/**
	 * @param indicadorAccionistas the indCoAcc to set
	 */
	public void setIndicadorAccionistas(String indicadorAccionistas) {
		this.indicadorAccionistas = indicadorAccionistas;
	}
	/**
	 * @return the fecUltOp
	 */
	public String getFechaUltimaOperacion() {
		return fechaUltimaOperacion;
	}
	/**
	 * @param fechaUltimaOperacion the fecUltOp to set
	 */
	public void setFechaUltimaOperacion(String fechaUltimaOperacion) {
		this.fechaUltimaOperacion = fechaUltimaOperacion;
	}
	/**
	 * @return the fecUlCam
	 */
	public String getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}
	/**
	 * @param fecUlCam the fecUlCam to set
	 */
	public void setFechaUltimaModificacion(String fecUlCam) {
		this.fechaUltimaModificacion = fecUlCam;
	}
	/**
	 * @return the desRefCo
	 */
	public String getDescripcionContrato() {
		return descripcionContrato;
	}
	/**
	 * @param descripcionContrato the desRefCo to set
	 */
	public void setDescripcionContrato(String descripcionContrato) {
		this.descripcionContrato = descripcionContrato;
	}
	/**
	 * @return the desRefFi
	 */
	public String getDescripcionFirma() {
		return descripcionFirma;
	}
	/**
	 * @param descripcionFirma the desRefFi to set
	 */
	public void setDescripcionFirma(String descripcionFirma) {
		this.descripcionFirma = descripcionFirma;
	}
	/**
	 * @return the desRefAr
	 */
	public String getDescripcionAreaReferencia() {
		return descripcionAreaReferencia;
	}
	/**
	 * @param descripcionAreaReferencia the desRefAr to set
	 */
	public void setDescripcionAreaReferencia(String descripcionAreaReferencia) {
		this.descripcionAreaReferencia = descripcionAreaReferencia;
	}
	/**
	 * @return the desRefEx
	 */
	public String getDescripcionReferenciaExterna() {
		return descripcionReferenciaExterna;
	}
	/**
	 * @param descripcionReferenciaExterna the desRefEx to set
	 */
	public void setDescripcionReferenciaExterna(String descripcionReferenciaExterna) {
		this.descripcionReferenciaExterna = descripcionReferenciaExterna;
	}
	/**
	 * @return the desDetSe
	 */
	public String getDescripcionTextoLibreSubestatus() {
		return descripcionTextoLibreSubestatus;
	}
	/**
	 * @param descripcionTextoLibreSubestatus the desDetSe to set
	 */
	public void setDescripcionTextoLibreSubestatus(String descripcionTextoLibreSubestatus) {
		this.descripcionTextoLibreSubestatus = descripcionTextoLibreSubestatus;
	}
	/**
	 * @return the idEmprAg
	 */
	public String getEmpresaContratoAgrupador() {
		return empresaContratoAgrupador;
	}
	/**
	 * @param empresaContratoAgrupador the idEmprAg to set
	 */
	public void setEmpresaContratoAgrupador(String empresaContratoAgrupador) {
		this.empresaContratoAgrupador = empresaContratoAgrupador;
	}
	/**
	 * @return the idCentAg
	 */
	public String getCentroContratoAgrupador() {
		return centroContratoAgrupador;
	}
	/**
	 * @param centroContratoAgrupador the idCentAg to set
	 */
	public void setCentroContratoAgrupador(String centroContratoAgrupador) {
		this.centroContratoAgrupador = centroContratoAgrupador;
	}
	/**
	 * @return the idContAg
	 */
	public String getNumeroContratoAgrupador() {
		return numeroContratoAgrupador;
	}
	/**
	 * @param numeroContratoAgrupador the idContAg to set
	 */
	public void setNumeroContratoAgrupador(String numeroContratoAgrupador) {
		this.numeroContratoAgrupador = numeroContratoAgrupador;
	}
	/**
	 * @return the idProAg
	 */
	public String getCodigoProductoAgrupador() {
		return codigoProductoAgrupador;
	}
	/**
	 * @param codigoProductoAgrupador the idProAg to set
	 */
	public void setCodigoProductoAgrupador(String codigoProductoAgrupador) {
		this.codigoProductoAgrupador = codigoProductoAgrupador;
	}
	/**
	 * @return the idSProAg
	 */
	public String getCodigoSubproductoAgrupador() {
		return codigoSubproductoAgrupador;
	}
	/**
	 * @param codigoSubproductoAgrupador the idSProAg to set
	 */
	public void setCodigoSubproductoAgrupador(String codigoSubproductoAgrupador) {
		this.codigoSubproductoAgrupador = codigoSubproductoAgrupador;
	}
	/**
	 * @return the idEmprAgr
	 */
	public String getEmpresaContratoAgrupado() {
		return empresaContratoAgrupado;
	}
	/**
	 * @param empresaContratoAgrupado the idEmprAgr to set
	 */
	public void setEmpresaContratoAgrupado(String empresaContratoAgrupado) {
		this.empresaContratoAgrupado = empresaContratoAgrupado;
	}
	/**
	 * @return the desAliasAgr
	 */
	public String getAliasContratoAgrupador() {
		return aliasContratoAgrupador;
	}
	/**
	 * @param aliasContratoAgrupador the desAliasAgr to set
	 */
	public void setAliasContratoAgrupador(String aliasContratoAgrupador) {
		this.aliasContratoAgrupador = aliasContratoAgrupador;
	}
	/**
	 * @return the idContEs
	 */
	public String getEmpresaContratoEspejo() {
		return empresaContratoEspejo;
	}
	/**
	 * @param empresaContratoEspejo the idContEs to set
	 */
	public void setEmpresaContratoEspejo(String empresaContratoEspejo) {
		this.empresaContratoEspejo = empresaContratoEspejo;
	}
	/**
	 * @return the cenCoEs
	 */
	public String getCentroContratoEspejo() {
		return centroContratoEspejo;
	}
	/**
	 * @param centroContratoEspejo the cenCoEs to set
	 */
	public void setCentroContratoEspejo(String centroContratoEspejo) {
		this.centroContratoEspejo = centroContratoEspejo;
	}
	/**
	 * @return the numConEs
	 */
	public String getNumeroContratoEspejo() {
		return numeroContratoEspejo;
	}
	/**
	 * @param numeroContratoEspejo the numConEs to set
	 */
	public void setNumeroContratoEspejo(String numeroContratoEspejo) {
		this.numeroContratoEspejo = numeroContratoEspejo;
	}
	/**
	 * @return the coProEs
	 */
	public String getCodigoProductoEspejo() {
		return codigoProductoEspejo;
	}
	/**
	 * @param codigoProductoEspejo the coProEs to set
	 */
	public void setCodigoProductoEspejo(String codigoProductoEspejo) {
		this.codigoProductoEspejo = codigoProductoEspejo;
	}
	/**
	 * @return the coSubProEs
	 */
	public String getCodigoSubProductoEspejo() {
		return codigoSubProductoEspejo;
	}
	/**
	 * @param codigoSubProductoEspejo the coSubProEs to set
	 */
	public void setCodigoSubProductoEspejo(String codigoSubProductoEspejo) {
		this.codigoSubProductoEspejo = codigoSubProductoEspejo;
	}
	/**
	 * @return the emprConChe
	 */
	public String getEmpresaContratoChequera() {
		return empresaContratoChequera;
	}
	/**
	 * @param empresaContratoChequera the emprConChe to set
	 */
	public void setEmpresaContratoChequera(String empresaContratoChequera) {
		this.empresaContratoChequera = empresaContratoChequera;
	}
	/**
	 * @return the aliasConEs
	 */
	public String getAliasContratoEspej() {
		return aliasContratoEspej;
	}
	/**
	 * @param aliasContratoChequera the aliasConEs to set
	 */
	public void setAliasContratoEspej(String aliasContratoChequera) {
		this.aliasContratoEspej = aliasContratoChequera;
	}
	/**
	 * @return the fecConChe
	 */
	public String getEmpresaContratoAlias() {
		return empresaContratoAlias;
	}
	/**
	 * @param fechaContratoChequera the fecConChe to set
	 */
	public void setEmpresaContratoAlias(String fechaContratoChequera) {
		this.empresaContratoAlias = fechaContratoChequera;
	}
	/**
	 * @return the cenConChe
	 */
	public String getCentroContratoChequera() {
		return centroContratoChequera;
	}
	/**
	 * @param centroContratoChequera the cenConChe to set
	 */
	public void setCentroContratoChequera(String centroContratoChequera) {
		this.centroContratoChequera = centroContratoChequera;
	}
	/**
	 * @return the numConChe
	 */
	public String getNumeroContratoChequera() {
		return numeroContratoChequera;
	}
	/**
	 * @param numeroContratoChequera the numConChe to set
	 */
	public void setNumeroContratoChequera(String numeroContratoChequera) {
		this.numeroContratoChequera = numeroContratoChequera;
	}
	/**
	 * @return the codProChe
	 */
	public String getCodigoProductoChequera() {
		return codigoProductoChequera;
	}
	/**
	 * @param codProChe the codProChe to set
	 */
	public void setCodigoProductoChequera(String codProChe) {
		this.codigoProductoChequera = codProChe;
	}
	/**
	 * @return the codStiProChe
	 */
	public String getCodigoSubProductoChequera() {
		return codigoSubProductoChequera;
	}
	/**
	 * @param codigoSubProductoChequera the codStiProChe to set
	 */
	public void setCodigoSubProductoChequera(String codigoSubProductoChequera) {
		this.codigoSubProductoChequera = codigoSubProductoChequera;
	}

}
