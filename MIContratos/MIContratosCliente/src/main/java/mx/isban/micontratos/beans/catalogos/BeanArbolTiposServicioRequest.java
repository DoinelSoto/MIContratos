package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;

/**
 * @author Daniel Hernández Soto
 *
 */

/** Bean con los datos de entradas para las transacciones :
 
	MI07	Árbol Tipos de servicio

 * */
public class BeanArbolTiposServicioRequest implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -1762399303271909120L;
	

	
	/**
	 * Código de tipo de cliente
	 */
	@Size(max=2)
	private String codigoTipoCliente;
	/**
	 * Código de perfil de inversión
	 */
	@Size(max=2)
	private String codigoPerfilInversion;
	/**
	 * Código de tipo de servicio
	 */
	@Size(max=2)
	private String codigoTipoServicio;
	
	/**
	 * memento
	 */
	@Size(max=8)
	private String memento;
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;

	/**
	 * Constructor para la rellamada generica
	 */
	public BeanArbolTiposServicioRequest(){

		rellamada = new BeanPaginacion();

	}
	
	
	/**
	 * @return the codTipSe
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}
	/**
	 * @param codigoTipoServicio the codTipSe to set
	 */
	public void setCodigoTipoServicio(String codigoTipoServicio) {
		this.codigoTipoServicio = codigoTipoServicio;
	}
	/**
	 * @return the codTiCli
	 */
	public String getCodigoTipoCliente() {
		return codigoTipoCliente;
	}
	/**
	 * @param codigoTipoCliente the codTiCli to set
	 */
	public void setCodigoTipoCliente(String codigoTipoCliente) {
		this.codigoTipoCliente = codigoTipoCliente;
	}
	/**
	 * @return the codPerIn
	 */
	public String getCodigoPerfilInversion() {
		return codigoPerfilInversion;
	}
	/**
	 * @param codigoPerfilInversion the codPerIn to set
	 */
	public void setCodigoPerfilInversion(String codigoPerfilInversion) {
		this.codigoPerfilInversion = codigoPerfilInversion;
	}
		
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
}
