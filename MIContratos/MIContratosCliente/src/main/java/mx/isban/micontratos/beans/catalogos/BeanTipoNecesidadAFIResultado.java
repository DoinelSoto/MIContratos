package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.ResponseGenerico;
/**
 * 
 * @author Luis Martínez - everis
 * Bean para los campos de salida del WS : Consulta del tipo de servicio para un cliente
 */
public class BeanTipoNecesidadAFIResultado extends ResponseGenerico  implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7898793320816915187L;
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanTipoNecesidadAFIResponse> listaCatalogoTipoNecesidadAfi;

	/**
	 * 
	 * @return the listaCatalogoTipoNecesidadAfi
	 */
	public List<BeanTipoNecesidadAFIResponse> getListaCatalogoTipoNecesidadAfi() {
		return new ArrayList<> (listaCatalogoTipoNecesidadAfi);
	}

	/**
	 * 
	 * @param listaCatalogoTipoNecesidadAfi
	 */
	public void setListaCatalogoTipoNecesidadAfi(List<BeanTipoNecesidadAFIResponse> listaCatalogoTipoNecesidadAfi) {
		this.listaCatalogoTipoNecesidadAfi = new ArrayList<>(listaCatalogoTipoNecesidadAfi);
	}	
}