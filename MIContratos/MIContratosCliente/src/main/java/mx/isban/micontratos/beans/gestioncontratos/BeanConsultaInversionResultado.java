package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI16 : Consulta de datos del contrato de inversión
 *
 */
public class BeanConsultaInversionResultado extends ResponseGenerico implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 2557023240541404709L;
	
	
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanConsultaInversionResponse> listaConsultaInversion;
	
	/**
	 * @return the consulta
	 */
	public List<BeanConsultaInversionResponse> getListaConsultaInversion() {
		return new ArrayList<> (listaConsultaInversion);

	}
	/**
	 * @param listaConsultaInversion the consulta to set
	 */
	public void setListaConsultaInversion(List<BeanConsultaInversionResponse> listaConsultaInversion) {
		this.listaConsultaInversion = new ArrayList<> (listaConsultaInversion);

	}	
}
