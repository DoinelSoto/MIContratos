/**
 * 
 */
package mx.isban.micontratos.beans.generales;

import java.io.Serializable;

/**
 * @author everis
 *
 */
public class BeanPaginacion implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 6786026530358954706L;
	
	
	/**
	 * memento
	 */
	private String memento;
	/**
	 * Indicador de mas datos
	 */
	private String masDatos;
	
	/**
	 * @param masDatos the masDatos to set
	 */
	public void setMasDatos(String masDatos) {
		this.masDatos = masDatos;
	}
	/**
	 * Obtiene el memento
	 * @return
	 */
	public String getMemento() {
		return memento;
	}
	/**
	 * Setea el memento
	 * @param memento
	 */
	public void setMemento(String memento) {
		this.memento = memento;
	}
	/**
	 * 
	 * @return
	 */
	public String getMasDatos() {
		return masDatos;
	}
	
	
	

}
