package mx.isban.micontratos.beans.relacionclientecontrato;

import java.io.Serializable;

/**
 * 
 * @author everis
 * Bean con los campos de los intervinientes (MI16)
 */
public class BeanDetalleContratoInversionInterviniente implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 2205609175238335213L;
	
	/**
	 * Calidad  de participación
	 */
	private String calidadParticipacion;
	
	/**
	 * Orden de participación
	 */
	private int ordenParticipacion;
	
	/**
	 * Número de persona
	 */
	private String numeroPersona;
	
	/**
	 * Porcentaje de participación
	 */
	private String porcentajeParticipacion;
	
	/**
	 * Identificación tipo firma
	 */
	private String identificacionTipoFirma;
	
	/**
	 * Folio poder
	 */
	private String folioPoder;
	
	/**
	 * Código poder
	 */
	private String codigoPoder;
	
	/**
	 * Porcentaje IPAB
	 */
	private String porcentajeIpab;
	
	/**
	 * Secuencia domicilio
	 */
	private String secuenciaDomicilio;
	
	/**
	 * Domicilio vinculador
	 */
	private String domicilioVinculador;

	/**
	 * @return the calidadParticipacion
	 */
	public String getCalidadParticipacion() {
		return calidadParticipacion;
	}

	/**
	 * @param calidadParticipacion the calidadParticipacion to set
	 */
	public void setCalidadParticipacion(String calidadParticipacion) {
		this.calidadParticipacion = calidadParticipacion;
	}

	/**
	 * @return the ordenParticipacion
	 */
	public int getOrdenParticipacion() {
		return ordenParticipacion;
	}

	/**
	 * @param i the ordenParticipacion to set
	 */
	public void setOrdenParticipacion(int i) {
		this.ordenParticipacion = i;
	}

	/**
	 * @return the numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}

	/**
	 * @param numeroPersona the numeroPersona to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}

	/**
	 * @return the porcentajeParticipacion
	 */
	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	/**
	 * @param porcentajeParticipacion the porcentajeParticipacion to set
	 */
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	/**
	 * @return the identificacionTipoFirma
	 */
	public String getIdentificacionTipoFirma() {
		return identificacionTipoFirma;
	}

	/**
	 * @param identificacionTipoFirma the identificacionTipoFirma to set
	 */
	public void setIdentificacionTipoFirma(String identificacionTipoFirma) {
		this.identificacionTipoFirma = identificacionTipoFirma;
	}

	/**
	 * @return the folioPoder
	 */
	public String getFolioPoder() {
		return folioPoder;
	}

	/**
	 * @param folioPoder the folioPoder to set
	 */
	public void setFolioPoder(String folioPoder) {
		this.folioPoder = folioPoder;
	}

	/**
	 * @return the codigoPoder
	 */
	public String getCodigoPoder() {
		return codigoPoder;
	}

	/**
	 * @param codigoPoder the codigoPoder to set
	 */
	public void setCodigoPoder(String codigoPoder) {
		this.codigoPoder = codigoPoder;
	}

	/**
	 * @return the porcentajeIpab
	 */
	public String getPorcentajeIpab() {
		return porcentajeIpab;
	}

	/**
	 * @param porcentajeIpab the porcentajeIpab to set
	 */
	public void setPorcentajeIpab(String porcentajeIpab) {
		this.porcentajeIpab = porcentajeIpab;
	}

	/**
	 * @return the secuenciaDomicilio
	 */
	public String getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}

	/**
	 * @param secuenciaDomicilio the secuenciaDomicilio to set
	 */
	public void setSecuenciaDomicilio(String secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}

	/**
	 * @return the domicilioVinculador
	 */
	public String getDomicilioVinculador() {
		return domicilioVinculador;
	}

	/**
	 * @param domicilioVinculador the domicilioVinculador to set
	 */
	public void setDomicilioVinculador(String domicilioVinculador) {
		this.domicilioVinculador = domicilioVinculador;
	}	
}