package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;
import mx.isban.micontratos.beans.generales.BeanPaginacion;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI09 : Catálogos estructurales
 *
 */
public class BeanCatalogosEstructuralesRequest implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 6329202083756257771L;
	
	/**
	 * Código de catálogo	
	 */
	 @Size(max=2)
	 private String codigoCatalogo;
	/**
	 * Código catálogo estructural de inversión	
	 */
	 @Size(max=2)
	private String codigoEstructuralInversion;
	 
	 /**
	 * Bean para la paginación
	 */
	 private BeanPaginacion rellamada;
	 
		/**
		 * Constructor para inicializar instancias
		 */
		public BeanCatalogosEstructuralesRequest() {
			rellamada = new BeanPaginacion();
		}
	
	/**
	 * @return the codCatal
	 */
	public String getCodigoCatalogo() {
		return codigoCatalogo;
	}
	/**
	 * @param codCatal the codCatal to set
	 */
	public void setCodigoCatalogo(String codCatal) {
		this.codigoCatalogo = codCatal;
	}
	/**
	 * @return the codValor
	 */
	public String getCodigoEstructuralInversion() {
		return codigoEstructuralInversion;
	}
	/**
	 * @param codValor the codValor to set
	 */
	public void setCodigoEstructuralInversion(String codigoEstructuralInversion) {
		this.codigoEstructuralInversion = codigoEstructuralInversion;
	}

	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
}
