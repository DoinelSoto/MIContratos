package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI08 : Consulta estado documentos
 */
public class BeanConsultaEstadoDocumentosResultado extends ResponseGenerico implements Serializable{
		/**
		 * serialización DTO
		 */
		private static final long serialVersionUID = 6722510412956954620L;
		
		/**
		 * Instancia a bean de rellamada
		 */
		private BeanPaginacion rellamada;
		
		/**
		 * Lista de beans BeanArbolTiposServicioResponse
		 */
		private List<BeanConsultaEstadoDocumentosResponse> listaConsultaEstadoDocumentoResponse;
		/**
		 * Class BeanConsultaEstadoDocumentosResultado
		 */
		
		public BeanConsultaEstadoDocumentosResultado(){
			rellamada = new BeanPaginacion();
		}
		
		/**
		 * @return the listaConsultaEstadoDocumento
		 */
		public List<BeanConsultaEstadoDocumentosResponse> getListaConsultaEstadoDocumento(){
			return new ArrayList<> (listaConsultaEstadoDocumentoResponse);
		}

		/**
		 * @param listaConsultaEstadoDocumento the listaConsultaEstadoDocumento to set
		 */
		public void setListaConsultaEstadoDocumento(List<BeanConsultaEstadoDocumentosResponse> listaConsultaEstadoDocumentoResponse) {
			this.listaConsultaEstadoDocumentoResponse = new ArrayList<> (listaConsultaEstadoDocumentoResponse);
		}

		/**
		 * @return the rellamada
		 */
		public BeanPaginacion getRellamada() {
			return rellamada;
		}

		/**
		 * @param rellamada the rellamada to set
		 */
		public void setRellamada(BeanPaginacion rellamada) {
			this.rellamada = rellamada;
		}
		
}
		