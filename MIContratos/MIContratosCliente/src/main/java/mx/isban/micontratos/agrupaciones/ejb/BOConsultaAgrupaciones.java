/**
 * 
 */
package mx.isban.micontratos.agrupaciones.ejb;


import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanAltaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanBajaContratosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratoAgrupadorResultado;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosRequest;
import mx.isban.micontratos.beans.agrupaciones.BeanListaContratosHijosResultado;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author everis
 *
 */

public interface BOConsultaAgrupaciones {

	/**
	 * Método que ejecutará el consumo de la transacción MI32 : consulta de la lista de contrato agrupador
	 * @param beanListaContratoAgrupadorRequest
	 * @param session
	 * @return BeanListaContratoAgrupadorResultado
	 */

	BeanListaContratoAgrupadorResultado listaContratosAgrupadores(BeanListaContratoAgrupadorRequest beanListaContratoAgrupadorRequest,ArchitechSessionBean session) throws BusinessException;

	
	/**
	 * Método que ejecutará el consumo de la transacción MI33 : alta de contrato agrupador
	 * @param beanAltaContratoAgrupadorRequest
	 * @param session
	 * @return ResponseGenerico
	 */

	ResponseGenerico altaContratoAgrupador(BeanAltaContratoAgrupadorRequest beanAltaContratoAgrupadorRequest,ArchitechSessionBean session) throws BusinessException;

	
	/**
	 * Método que ejecutará el consumo de la transacción MI34 : consulta de la lista de contratos hijos
	 * @param beanListaContratosHijosRequest
	 * @param session
	 * @return BeanListaContratosHijosResultado
	 */

	BeanListaContratosHijosResultado listarContratosAgrupados(BeanListaContratosHijosRequest beanListaContratosHijosRequest,ArchitechSessionBean session) throws BusinessException;

	
	/**
	 * Método que ejecutará el consumo de la transacción MI35 : Alta contratos hijos
	 * @param beanAltaContratosHijosRequest
	 * @param session
	 * @return ResponseGenerico
	 */

	ResponseGenerico altaContratoAgrupado(BeanAltaContratosHijosRequest beanAltaContratosHijosRequest,ArchitechSessionBean session) throws BusinessException;

	
	
	/**
	 * Método que ejecutará el consumo de la transacción MI36 : Baja de contratos hijos
	 * @param beanBajaContratosRequest
	 * @param session
	 * @return ResponseGenerico 
	 */

	ResponseGenerico bajaContratoAgrupado(BeanBajaContratosRequest beanBajaContratosRequest,ArchitechSessionBean session) throws BusinessException;
	

	
}
