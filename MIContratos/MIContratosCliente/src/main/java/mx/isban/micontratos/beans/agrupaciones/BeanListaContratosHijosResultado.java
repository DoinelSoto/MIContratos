/**
 * 
 */
package mx.isban.micontratos.beans.agrupaciones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI34 :  Lista Contratos Hijos
 */
public class BeanListaContratosHijosResultado extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7252081146615687507L;
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Consulta de la lista
	 */
	private List<BeanListaContratosHijosResponse> listaContratosHijos;
	
	/**
	 * Constructor
	 * Se inicializa el bean de paginación
	 */
	public BeanListaContratosHijosResultado(){
		
		rellamada = new BeanPaginacion();
		
	}
	/**
	 * @return ArrayList
	 */
	public List<BeanListaContratosHijosResponse> getListaContratosHijos() {

		return new ArrayList<>(listaContratosHijos);	
	}

	/**BeanListaContratosHijosResponse
	 * @param listaContratosHijos the consulta to set
	 */
	public void setListaContratosHijos(List<BeanListaContratosHijosResponse> listaContratosHijos) {
		this.listaContratosHijos = new ArrayList<>(listaContratosHijos);
	}
	
	/**
	 * @return BeanPaginacion
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	
	/**
	 * @param rellamada
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

}
