package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI09 : Catálogos estructurales
 *
 */
public class BeanCatalogosEstructuralesResponse implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -3761293603391708931L;
	/**
	 * Código estructural de inversión 	
	 */
	private String codigoEstructuralInversion;
	/**
	 * Descripción catálogo estructural de inversión	
	 */
	private String descripcionEstructuralInversion;
	
	/**
	 * @return the numOccur
	 */
	public String getCodigoEstructuralInversion() {
		return codigoEstructuralInversion;
	}
	/**
	 * @param codigoEstructuralInversion the codigoEstructuralInversion to set
	 */
	public void setCodigoEstructuralInversion(String codigoEstructuralInversion) {
		this.codigoEstructuralInversion = codigoEstructuralInversion;
	}
	/**
	 * @return the desValor
	 */
	public String getDescripcionEstructuralInversion() {
		return descripcionEstructuralInversion;
	}
	/**
	 * @param desValor the desValor to set
	 */
	public void setDescripcionEstructuralInversion(String descripcionEstructuralInversion) {
		this.descripcionEstructuralInversion = descripcionEstructuralInversion;
	}
	
}
