package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

/**
 * @author Daniel Hernández Soto
 *
 */

/** Bean con los datos de respuesta para las transacciones :
 *  MI06	Tipos de servicio
 *	
 * */
public class BeanListaTiposServicioResponse  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -5802641147356806656L;
	


	/**
	 * Código de tipo de servicio
	 */
	private String codigoTipoServicio;
	/**
	 * Descripción Código de tipo de servicio
	 */
	private String descripcionTipoServicio;
	/**
	 * Indicador de asesoramiento
	 */
	private String indicadorAsesoramiento;
	/**
	 * Indicador necesidad AFI
	 */
	private String indicadorNecesidadAsesor;
	/**
	 * @return the codTipSe
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}
	/**
	 * @param codigoTipoServicio the codTipSe to set
	 */
	public void setCodigoTipoServicio(String codigoTipoServicio) {
		this.codigoTipoServicio = codigoTipoServicio;
	}
	/**
	 * @return the desTipSe
	 */
	public String getDescripcionTipoServicio() {
		return descripcionTipoServicio;
	}
	/**
	 * @param descripcionTipoServicio the desTipSe to set
	 */
	public void setDescripcionTipoServicio(String descripcionTipoServicio) {
		this.descripcionTipoServicio = descripcionTipoServicio;
	}
	/**
	 * @return the indAseso
	 */
	public String getIndicadorAsesoramiento() {
		return indicadorAsesoramiento;
	}
	/**
	 * @param indicadorAsesoramiento the indAseso to set
	 */
	public void setIndicadorAsesoramiento(String indicadorAsesoramiento) {
		this.indicadorAsesoramiento = indicadorAsesoramiento;
	}
	/**
	 * @return the indCoAFi
	 */
	public String getIndicadorNecesidadAsesor() {
		return indicadorNecesidadAsesor;
	}
	/**
	 * @param indicadorCodigoAfiliacion the indCoAFi to set
	 */
	public void setIndicadorNecesidadAsesor(String indicadorCodigoAfiliacion) {
		this.indicadorNecesidadAsesor = indicadorCodigoAfiliacion;
	}

}
