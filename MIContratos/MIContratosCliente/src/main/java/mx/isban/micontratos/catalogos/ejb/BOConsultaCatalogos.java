package mx.isban.micontratos.catalogos.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanAltaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaCatalogosSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanBajaEstatusContratosRequest;
import mx.isban.micontratos.beans.catalogos.BeanModificacionEstatusTablaCatalogoRequest;
import mx.isban.micontratos.beans.generales.ResponseGenerico;


/**
 * @author everis
 *
 */

public interface BOConsultaCatalogos {
	

	
	/**
	 * Metódo que hará el llamado a el consumo de la transacción MI25 : Alta de catalogos de estatus
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return ResponseGenerico
	 */
	ResponseGenerico altaEstatusOperativo(BeanAltaCatalogosEstatusRequest beanAltaCatalogosEstatusRequest,ArchitechSessionBean sesion) throws BusinessException;

	
	/**
	 *  Metódo que hará el llamado a el consumo de la transacción MI26 : Baja de estatus de contratos
	 * @param beanBajaEstatusContratosRequest
	 * @param session
	 * @return ResponseGenerico 
	 */
	ResponseGenerico bajaEstatusOperativo(BeanBajaEstatusContratosRequest beanBajaEstatusContratosRequest,ArchitechSessionBean session) throws BusinessException;

	
	/**
	 * Metódo que hará el llamado a el consumo de la transacción MI27 : Modificación de estatus de la tabla catálogo
	 * @param beanModificacionEstatusTablaCatalogoRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico modificarEstatusOperativo(BeanModificacionEstatusTablaCatalogoRequest beanModificacionEstatusTablaCatalogoRequest,ArchitechSessionBean session) throws BusinessException;

	
	/**
	 * Metódo que hará el llamado a el consumo de la transacción MI28 : Alta de catálogos de subEstatus
	 * @param beanAltaCatalogosSubEstatusRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico altaSubEstatusOperativo(BeanAltaCatalogosSubEstatusRequest beanAltaCatalogosSubEstatusRequest,ArchitechSessionBean session) throws BusinessException;

	
	/**
	 * Metódo que hará el llamado a el consumo de la transacción MI29 : Baja de subestatus de la tabla de catálogos
	 * @param beanBajaCatalogosSubEstatusRequest
	 * @param session
	 * @return ResponseGenerico 
	 */
	ResponseGenerico bajaSubEstatusOperativo(BeanBajaCatalogosSubEstatusRequest beanBajaCatalogosSubEstatusRequest,ArchitechSessionBean session) throws BusinessException;
}
