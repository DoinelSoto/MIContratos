package mx.isban.micontratos.beans.agrupaciones;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI35 :  Alta Contratos Hijos Request
 */
public class BeanAltaContratosHijosRequest  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7225474335621669742L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias	
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del Contrato de Inversión	
	 */
	@Size(max=4)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato	
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Tipo de agrupación	
	 */
	@Size(max=3)
	private String tipoAgrupacion;
	/**
	 * Empresa del Contrato agrupado	
	 */
	@Size(max=4)
	private String empresaContratoAgrupador;
	/**
	 * Centro del Contrato agrupado	
	 */
	@Size(max=4)
	private String centroContratoAgrupado;
	/**
	 * Número de contrato agrupado	
	 */
	@Size(max=12)
	private String numeroContratoAgrupado;
	/**
	 * Código de producto del contrato agrupado	
	 */
	@Size(max=2)
	private String codigoProductoAgrupado;
	/**
	 * Código de subproducto del contrato agrupado
	 */
	@Size(max=4)
	private String codigoSubProductoAgrupado;
	/**
	 * Alias del contrato agrupado	
	 */
	@Size(max=15)
	private String aliasContratoAgrupado;
	/**
	 * identificador de empresa agrupador
	 */
	@Size(max=4)
	private String empresaContratoInversionAgrupado;	
	
	/**
	 * Constructor para la multi-canalidad
	 */
	public BeanAltaContratosHijosRequest(){
		multicanalidad=new MulticanalidadGenerica();
	}
	
	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	
	/**
	 * @param tipoConsulta the tipConsu to set
	 */
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idStiPro to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}
	/**
	 * @param empresaContratoInversion the idEmprCo to set
	 */
	public void setEmpresaContratoInversion(String empresaContratoInversion) {
		this.empresaContratoInversion = empresaContratoInversion;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param aliasContrato the desAlias to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}
	/**
	 * @return the tipAgrup
	 */
	public String getTipoAgrupacion() {
		return tipoAgrupacion;
	}
	/**
	 * @param tipoAgrupacion the tipAgrup to set
	 */
	public void setTipoAgrupacion(String tipoAgrupacion) {
		this.tipoAgrupacion = tipoAgrupacion;
	}
	/**
	 * @return the idEmprAg
	 */
	public String getEmpresaContratoAgrupador() {
		return empresaContratoAgrupador;
	}
	/**
	 * @param empresaContratoAgrupador the idEmprAg to set
	 */
	public void setEmpresaContratoAgrupador(String empresaContratoAgrupador) {
		this.empresaContratoAgrupador = empresaContratoAgrupador;
	}
	/**
	 * @return the idCentAg
	 */
	public String getCentroContratoAgrupado() {
		return centroContratoAgrupado;
	}
	/**
	 * @param centroContratoAgrupado the idCentAg to set
	 */
	public void setCentroContratoAgrupado(String centroContratoAgrupado) {
		this.centroContratoAgrupado = centroContratoAgrupado;
	}
	/**
	 * @return the idContrAg
	 */
	public String getNumeroContratoAgrupado() {
		return numeroContratoAgrupado;
	}
	/**
	 * @param numeroContratoAgrupado the idContrAg to set
	 */
	public void setNumeroContratoAgrupado(String numeroContratoAgrupado) {
		this.numeroContratoAgrupado = numeroContratoAgrupado;
	}
	/**
	 * @return the idProAg
	 */
	public String getCodigoProductoAgrupado() {
		return codigoProductoAgrupado;
	}
	/**
	 * @param codigoProductoAgrupado the idProAg to set
	 */
	public void setCodigoProductoAgrupado(String codigoProductoAgrupado) {
		this.codigoProductoAgrupado = codigoProductoAgrupado;
	}
	/**
	 * @return the idStiPrAg
	 */
	public String getCodigoSubProductoAgrupado() {
		return codigoSubProductoAgrupado;
	}
	/**
	 * @param codigoSubProductoAgrupado the idStiPrAg to set
	 */
	public void setCodigoSubProductoAgrupado(String codigoSubProductoAgrupado) {
		this.codigoSubProductoAgrupado = codigoSubProductoAgrupado;
	}
	/**
	 * @return the desAliaG
	 */
	public String getAliasContratoAgrupado() {
		return aliasContratoAgrupado;
	}
	/**
	 * @param aliasContratoAgrupado the desAliaG to set
	 */
	public void setAliasContratoAgrupado(String aliasContratoAgrupado) {
		this.aliasContratoAgrupado = aliasContratoAgrupado;
	}
	/**
	 * @return the desAliaG
	 */
	public String getEmpresaContratoInversionAgrupado() {
		return empresaContratoInversionAgrupado;
	}
	/**
	 * @param identidicadorEmpresaContratoAgrupador the idEmprInAg to set
	 */
	public void setEmpresaContratoInversionAgrupado(String empresaContratoInversionAgrupado) {
		this.empresaContratoInversionAgrupado = empresaContratoInversionAgrupado;
	}
	

	/**
	 * 
	 * @return MulticanalidadGenerica regresa el objeto de multicanlidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
	
}