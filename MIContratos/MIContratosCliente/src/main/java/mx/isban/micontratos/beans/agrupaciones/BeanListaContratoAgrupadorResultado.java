package mx.isban.micontratos.beans.agrupaciones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de respuesta e implementacion BeanResultBO para la transacción MI32 : Transaccion Lista contrato Agrupador
 *
 */
public class BeanListaContratoAgrupadorResultado  extends ResponseGenerico implements Serializable{
	
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -9166101579676669576L;
	
	
	
	private List<BeanListaContratoAgrupadorResponse> listaContratoAgrupador;
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Constructor para el rellamado
	 */
	public BeanListaContratoAgrupadorResultado(){
		rellamada=new BeanPaginacion();
	}
	
	/**
	 * @return the consulta
	 */
	public List<BeanListaContratoAgrupadorResponse> getListaContratoAgrupador() {
		return new ArrayList<>(listaContratoAgrupador);
	}

	/**
	 * @param listaContratoAgrupador the consulta to set
	 */
	public void setListaContratoAgrupador(List<BeanListaContratoAgrupadorResponse> listaContratoAgrupador) {
		this.listaContratoAgrupador = new ArrayList<>(listaContratoAgrupador);
	}



	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}



	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	
	
}
