package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI23 : Alta contratos amparados por chequera
 *
 */
public class BeanAltaContratosAmparadosChequeraRequest implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -9174310399746487485L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	
	
	/**
	 * Empresa del Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de ejecutivo del contrato	
	 */
	@Size(max=8)
	private String codigoEjecutivo;
	/**
	 * Numero de la persona titular	
	 */
	@Size(max=8)
	private String numeroPersonaTitular;
	/**
	 * Centro de costos del contrato	
	 */
	@Size(max=4)
	private String centroCostosContrato;
	/**
	 * Código de banca del contrato	
	 */
	@Size(max=3)
	private String codigoBanca;
	/**
	 * Fecha de alta	
	 */
	@Size(max=10)
	private String fechaAlta;
	/**
	 * @return the idEmpr
	 */
	
	public BeanAltaContratosAmparadosChequeraRequest(){
		multicanalidad=new MulticanalidadGenerica();	
	}

	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the codEjecu
	 */
	public String getCodigoEjecutivo() {
		return codigoEjecutivo;
	}
	/**
	 * @param codigoEjecutivo the codEjecu to set
	 */
	public void setCodigoEjecutivo(String codigoEjecutivo) {
		this.codigoEjecutivo = codigoEjecutivo;
	}
	/**
	 * @return the peNumPer
	 */
	public String getNumeroPersonaTitular() {
		return numeroPersonaTitular;
	}
	/**
	 * @param numeroPersonaTitular the peNumPer to set
	 */
	public void setNumeroPersonaTitular(String numeroPersonaTitular) {
		this.numeroPersonaTitular = numeroPersonaTitular;
	}
	/**
	 * @return the codCenCo
	 */
	public String getCentroCostosContrato() {
		return centroCostosContrato;
	}
	/**
	 * @param centroCostosContrato the codCenCo to set
	 */
	public void setCentroCostosContrato(String centroCostosContrato) {
		this.centroCostosContrato = centroCostosContrato;
	}
	/**
	 * @return the idBanca
	 */
	public String getCodigoBanca() {
		return codigoBanca;
	}
	/**
	 * @param codigoBanca the idBanca to set
	 */
	public void setCodigoBanca(String codigoBanca) {
		this.codigoBanca = codigoBanca;
	}
	/**
	 * @return the fecAlta
	 */
	public String getFechaAlta() {
		return fechaAlta;
	}
	/**
	 * @param fechaAlta the fecAlta to set
	 */
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}

	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}

	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}

	/**
	 * @param codigoSubProducto the idStiPro to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}

}
