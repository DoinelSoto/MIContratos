package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;

import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * 
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los parametros de entrada para la transacción MI11
 *
 */
public class BeanAltaPreContratoGlobalRequest implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -4831112139762605421L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	/**
	 * Empresa del contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Código de producto del contrato	
	 */
	@Size(max=2)
	private String codigoProductoContrato;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubproductoContrato;
	/**
	 * Código de moneda del contrato	
	 */
	@Size(max=3)
	private String codigoMonedaContrato;
	/**
	 * Código de cliente	
	 */
	@Size(max=8)
	private String codigoCliente;
	/**
	 * Código de ejecutivo del contrato
	 */
	@Size(max=8)
	private String codigoEjecutivoContrato;
	/**
	 * Código de banca del contrato de Inversión	
	 */
	@Size(max=3)
	private String codigoBancaContratoInversion;
	/**
	 * Código de empresa del contrato de Inversión
	 */
	@Size(max=4)
	private String codigoEmpresaContratoInversion;
	/**
	 * Centro de costos del contrato
	 */
	@Size(max=4)
	private String centroCostosContrato;
	/**
	 * Indicador de tipo de cuenta	
	 */
	@Size(max=1)
	private String indicadorTipoCuenta;
	/**
	 * Indicador de tipo de Contrato	
	 */
	@Size(max=1)
	private String indicadorTipoContrato;
	/**
	 * Indicador autorización posición en corto en capitales	
	 */
	@Size(max=1)
	private String indicadorAutorizacionPosicionCortoCapitales;
	/**
	 * Indicador autorización posición en corto en fondos	
	 */
	@Size(max=1)
	private String indicadorAutorizacionPosicionCortoFondos;
	/**
	 * Indicador autorización cruce en fondos de inversión
	 */
	@Size(max=1)
	private String indicadorAutorizacionCruceFondosInversion;
	/**
	 * 	Indicador de discrecionalidad	
	 */
	@Size(max=1)
	private String indicadorDiscrecionalidad;
	/**
	 * Indicador tipo de custodia en directo	
	 */
	@Size(max=1)
	private String indicadorTipoCustodiaDirecto;
	/**
	 * Indicador tipo de custodia en Repos
	 */
	@Size(max=1)
	private String indicadorTipoCustodiaRepos;
	/**
	 * Monto Máximo a operar en el contrato	
	 */
	@Size(max=15)
	private String montoMaximoOperarContrato;
	/**
	 * Divisa del Monto Máximo a operar en el contrato
	 */
	@Size(max=3)
	private String divisaMontoMaximoOperarContrato;
	/**
	 * Indicador envío por correo carta confirmación capitales
	 */
	@Size(max=1)
	private String indicadorEnvioCorreoCartaConfirmacionCapitales;
	/**
	 * Indicador de Contrato Agrupado
	 */
	@Size(max=1)
	private String indicadorContratoAgrupado;
	/**
	 * Empresa contrato agrupador	
	 */
	@Size(max=4)
	private String empresaContratoAgrupador;
	/**
	 * Centro contrato agrupador	
	 */
	@Size(max=2)
	private String centroContratoAgrupador;
	/**
	 * Contrato agrupador	
	 */
	@Size(max=9)
	private String contratoAgrupador;
	/**
	 * Producto  contrato agrupador	
	 */
	@Size(max=2)
	private String productoContratoAgrupador;
	/**
	 * Subproducto contrato agrupador	
	 */
	@Size(max=4)
	private String subproductoContratoAgrupador;
	/**
	 * Indicador de contrato espejo	
	 */
	@Size(max=1)
	private String indicadorContratoEspejo;
	/**
	 * Empresa contrato espejo
	 */
	@Size(max=4)
	private String empresaContratoEspejo;
	/**
	 * Centro contrato espejo	
	 */
	@Size(max=2)
	private String centroContratoEspejo;
	/**
	 * Contrato espejo	
	 */
	@Size(max=12)
	private String contratoEspejo;
	/**
	 * Producto  contrato espejo	
	 */
	@Size(max=2)
	private String productoContratoEspejo;
	/**
	 * Identificador de subproducto de contrato espejo	
	 */
	@Size(max=4)
	private String subProductoContratoEspejo;
	/**
	 * Indicador de tipos	
	 */
	@Size(max=1)
	private String indicadorTipos;
	/**
	 * Código tipo de servicio	
	 */
	@Size(max=2)
	private String codigoTipoServicio;
	/**
	 * Código de afiliación	
	 */
	@Size(max=8)
	private String codigoAsesorFianciero;
	/**
	 * Secuencia domicilio principal	
	 */
	private int secuenciaDomicilioPrincipal;
	/**
	 * Secuencia domicilio envío	
	 */
	private int secuenciaDomicilioEnvio;
	/**
	 * Secuencia domicilio fiscal	
	 */
	private int secuenciaDomicilioFiscal;
	/**
	 * Descripción cont 1	
	 */
	@Size(max=50)
	private String descripcionCont1;
	/**
	 * Descripción cont 2	
	 */
	@Size(max=50)
	private String descripcionCont2;
	/**
	 * Descripción firm 1	
	 */
	@Size(max=50)
	private String descripcionFirm1;
	/**
	 * Descripción firm 2	
	 */
	@Size(max=50)
	private String descripcionFirm2;
	/**
	 * Descripción arre 1	
	 */
	@Size(max=50)
	private String descripcionArre1;
	/**
	 * Descripción arre 2
	 */
	@Size(max=50)
	private String descripcionArre2;
	/**
	 * Descripción Reex 1
	 */
	@Size(max=50)
	private String descripcionReex1;
	/**
	 * Descripción Reex 2	
	 */
	@Size(max=50)
	private String descripcionReex2;


	/**
	 * Constructor
	 */
	public BeanAltaPreContratoGlobalRequest(){
	
		multicanalidad=new MulticanalidadGenerica();	
		
	}

	/**
	 * @return the empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}



	/**
	 * @param idEmpr the empresaContrato to set
	 */
	public void setEmpresaContrato(String idEmpr) {
		this.empresaContrato = idEmpr;
	}



	/**
	 * @return the codigoProductoContrato
	 */
	public String getCodigoProductoContrato() {
		return codigoProductoContrato;
	}



	/**
	 * @param idProd the codigoProductoContrato to set
	 */
	public void setCodigoProductoContrato(String idProd) {
		this.codigoProductoContrato = idProd;
	}



	/**
	 * @return the codigoSubproductoContrato
	 */
	public String getCodigoSubproductoContrato() {
		return codigoSubproductoContrato;
	}



	/**
	 * @param idStiPro the codigoSubproductoContrato to set
	 */
	public void setCodigoSubproductoContrato(String idStiPro) {
		this.codigoSubproductoContrato = idStiPro;
	}



	/**
	 * @return the codigoMonedaContrato
	 */
	public String getCodigoMonedaContrato() {
		return codigoMonedaContrato;
	}



	/**
	 * @param codMonSw the codigoMonedaContrato to set
	 */
	public void setCodigoMonedaContrato(String codMonSw) {
		this.codigoMonedaContrato = codMonSw;
	}



	/**
	 * @return the codigoCliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}



	/**
	 * @param buC the codigoCliente to set
	 */
	public void setCodigoCliente(String buC) {
		this.codigoCliente = buC;
	}



	/**
	 * @return the codEjecu
	 */
	public String getCodigoEjecutivoContrato() {
		return codigoEjecutivoContrato;
	}



	/**
	 * @param codEjecu the codEjecu to set
	 */
	public void setCodigoEjecutivoContrato(String codEjecu) {
		this.codigoEjecutivoContrato = codEjecu;
	}



	/**
	 * @return the idBanca
	 */
	public String getCodigoBancaContratoInversion() {
		return codigoBancaContratoInversion;
	}



	/**
	 * @param idBanca the idBanca to set
	 */
	public void setCodigoBancaContratoInversion(String idBanca) {
		this.codigoBancaContratoInversion = idBanca;
	}



	/**
	 * @return the idEmprCo
	 */
	public String getCodigoEmpresaContratoInversion() {
		return codigoEmpresaContratoInversion;
	}



	/**
	 * @param idEmprCo the idEmprCo to set
	 */
	public void setCodigoEmpresaContratoInversion(String idEmprCo) {
		this.codigoEmpresaContratoInversion = idEmprCo;
	}



	/**
	 * @return the codCenCo
	 */
	public String getCentroCostosContrato() {
		return centroCostosContrato;
	}



	/**
	 * @param codCenCo the codCenCo to set
	 */
	public void setCentroCostosContrato(String codCenCo) {
		this.centroCostosContrato = codCenCo;
	}



	/**
	 * @return the idTipCue
	 */
	public String getIndicadorTipoCuenta() {
		return indicadorTipoCuenta;
	}



	/**
	 * @param idTipCue the idTipCue to set
	 */
	public void setIndicadorTipoCuenta(String idTipCue) {
		this.indicadorTipoCuenta = idTipCue;
	}



	/**
	 * @return the idTipCon
	 */
	public String getIndicadorTipoContrato() {
		return indicadorTipoContrato;
	}



	/**
	 * @param idTipCon the idTipCon to set
	 */
	public void setIndicadorTipoContrato(String idTipCon) {
		this.indicadorTipoContrato = idTipCon;
	}



	/**
	 * @return the indAuPCC
	 */
	public String getIndicadorAutorizacionPosicionCortoCapitales() {
		return indicadorAutorizacionPosicionCortoCapitales;
	}



	/**
	 * @param indAuPCC the indAuPCC to set
	 */
	public void setIndicadorAutorizacionPosicionCortoCapitales(String indAuPCC) {
		this.indicadorAutorizacionPosicionCortoCapitales = indAuPCC;
	}



	/**
	 * @return the indAuPCF
	 */
	public String getIndicadorAutorizacionPosicionCortoFondos() {
		return indicadorAutorizacionPosicionCortoFondos;
	}



	/**
	 * @param indAuPCF the indAuPCF to set
	 */
	public void setIndicadorAutorizacionPosicionCortoFondos(String indAuPCF) {
		this.indicadorAutorizacionPosicionCortoFondos = indAuPCF;
	}



	/**
	 * @return the indAuCFI
	 */
	public String getIndicadorAutorizacionCruceFondosInversion() {
		return indicadorAutorizacionCruceFondosInversion;
	}



	/**
	 * @param indAuCFI the indAuCFI to set
	 */
	public void setIndicadorAutorizacionCruceFondosInversion(String indAuCFI) {
		this.indicadorAutorizacionCruceFondosInversion = indAuCFI;
	}



	/**
	 * @return the indDiscr
	 */
	public String getIndicadorDiscrecionalidad() {
		return indicadorDiscrecionalidad;
	}



	/**
	 * @param indDiscr the indDiscr to set
	 */
	public void setIndicadorDiscrecionalidad(String indDiscr) {
		this.indicadorDiscrecionalidad = indDiscr;
	}



	/**
	 * @return the indTiCuD
	 */
	public String getIndicadorTipoCustodiaDirecto() {
		return indicadorTipoCustodiaDirecto;
	}



	/**
	 * @param indTiCuD the indTiCuD to set
	 */
	public void setIndicadorTipoCustodiaDirecto(String indTiCuD) {
		this.indicadorTipoCustodiaDirecto = indTiCuD;
	}



	/**
	 * @return the indTiCuR
	 */
	public String getIndicadorTipoCustodiaRepos() {
		return indicadorTipoCustodiaRepos;
	}



	/**
	 * @param indTiCuR the indTiCuR to set
	 */
	public void setIndicadorTipoCustodiaRepos(String indTiCuR) {
		this.indicadorTipoCustodiaRepos = indTiCuR;
	}



	/**
	 * @return the impMaxOp
	 */
	public String getMontoMaximoOperarContrato() {
		return montoMaximoOperarContrato;
	}

	/**
	 * @param montoMaximoOperarContrato the montoMaximoOperarContrato to set
	 */
	public void setMontoMaximoOperarContrato(String montoMaximoOperarContrato) {
		this.montoMaximoOperarContrato = montoMaximoOperarContrato;
	}


	/**
	 * @return the divMaxOp
	 */
	public String getDivisaMontoMaximoOperarContrato() {
		return divisaMontoMaximoOperarContrato;
	}



	/**
	 * @param divMaxOp the divMaxOp to set
	 */
	public void setDivisaMontoMaximoOperarContrato(String divMaxOp) {
		this.divisaMontoMaximoOperarContrato = divMaxOp;
	}



	/**
	 * @return the indEnCoC
	 */
	public String getIndicadorEnvioCorreoCartaConfirmacionCapitales() {
		return indicadorEnvioCorreoCartaConfirmacionCapitales;
	}



	/**
	 * @param indEnCoC the indEnCoC to set
	 */
	public void setIndicadorEnvioCorreoCartaConfirmacionCapitales(String indEnCoC) {
		this.indicadorEnvioCorreoCartaConfirmacionCapitales = indEnCoC;
	}



	/**
	 * @return the indCoAgr
	 */
	public String getIndicadorContratoAgrupado() {
		return indicadorContratoAgrupado;
	}



	/**
	 * @param indCoAgr the indCoAgr to set
	 */
	public void setIndicadorContratoAgrupado(String indCoAgr) {
		this.indicadorContratoAgrupado = indCoAgr;
	}



	/**
	 * @return the idEmprGr
	 */
	public String getEmpresaContratoAgrupador() {
		return empresaContratoAgrupador;
	}



	/**
	 * @param idEmprGr the idEmprGr to set
	 */
	public void setEmpresaContratoAgrupador(String idEmprGr) {
		this.empresaContratoAgrupador = idEmprGr;
	}



	/**
	 * @return the idCentGr
	 */
	public String getCentroContratoAgrupador() {
		return centroContratoAgrupador;
	}



	/**
	 * @param idCentGr the idCentGr to set
	 */
	public void setCentroContratoAgrupador(String idCentGr) {
		this.centroContratoAgrupador = idCentGr;
	}



	/**
	 * @return the idContGr
	 */
	public String getContratoAgrupador() {
		return contratoAgrupador;
	}



	/**
	 * @param idContGr the idContGr to set
	 */
	public void setContratoAgrupador(String idContGr) {
		this.contratoAgrupador = idContGr;
	}



	/**
	 * @return the idProdGr
	 */
	public String getProductoContratoAgrupador() {
		return productoContratoAgrupador;
	}



	/**
	 * @param idProdGr the idProdGr to set
	 */
	public void setProductoContratoAgrupador(String idProdGr) {
		this.productoContratoAgrupador = idProdGr;
	}



	/**
	 * @return the idsTipGr
	 */
	public String getSubproductoContratoAgrupador() {
		return subproductoContratoAgrupador;
	}



	/**
	 * @param idsTipGr the idsTipGr to set
	 */
	public void setSubproductoContratoAgrupador(String idsTipGr) {
		this.subproductoContratoAgrupador = idsTipGr;
	}



	/**
	 * @return the indCoEsp
	 */
	public String getIndicadorContratoEspejo() {
		return indicadorContratoEspejo;
	}



	/**
	 * @param indCoEsp the indCoEsp to set
	 */
	public void setIndicadorContratoEspejo(String indCoEsp) {
		this.indicadorContratoEspejo = indCoEsp;
	}



	/**
	 * @return the idEmprEs
	 */
	public String getEmpresaContratoEspejo() {
		return empresaContratoEspejo;
	}



	/**
	 * @param idEmprEs the idEmprEs to set
	 */
	public void setEmpresaContratoEspejo(String idEmprEs) {
		this.empresaContratoEspejo = idEmprEs;
	}



	/**
	 * @return the idCentEs
	 */
	public String getCentroContratoEspejo() {
		return centroContratoEspejo;
	}



	/**
	 * @param idCentEs the idCentEs to set
	 */
	public void setCentroContratoEspejo(String idCentEs) {
		this.centroContratoEspejo = idCentEs;
	}



	/**
	 * @return the idContEs
	 */
	public String getContratoEspejo() {
		return contratoEspejo;
	}



	/**
	 * @param idContEs the idContEs to set
	 */
	public void setContratoEspejo(String idContEs) {
		this.contratoEspejo = idContEs;
	}



	/**
	 * @return the idProdEs
	 */
	public String getProductoContratoEspejo() {
		return productoContratoEspejo;
	}



	/**
	 * @param idProdEs the idProdEs to set
	 */
	public void setProductoContratoEspejo(String idProdEs) {
		this.productoContratoEspejo = idProdEs;
	}

	/**
	 * @return the subProductoContratoEspejo
	 */
	public String getSubProductoContratoEspejo() {
		return subProductoContratoEspejo;
	}

	/**
	 * @param subProductoContratoEspejo the subProductoContratoEspejo to set
	 */
	public void setSubProductoContratoEspejo(String subProductoContratoEspejo) {
		this.subProductoContratoEspejo = subProductoContratoEspejo;
	}

	/**
	 * @return the indTipos
	 */
	public String getIndicadorTipos() {
		return indicadorTipos;
	}



	/**
	 * @param indTipos the indTipos to set
	 */
	public void setIndicadorTipos(String indTipos) {
		this.indicadorTipos = indTipos;
	}



	/**
	 * @return the codTipSe
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}



	/**
	 * @param codTipSe the codTipSe to set
	 */
	public void setCodigoTipoServicio(String codTipSe) {
		this.codigoTipoServicio = codTipSe;
	}



	/**
	 * @return the codAFi
	 */
	public String getCodigoAsesorFianciero() {
		return codigoAsesorFianciero;
	}



	/**
	 * @param codAFi the codAFi to set
	 */
	public void setCodigoAsesorFianciero(String codAFi) {
		this.codigoAsesorFianciero = codAFi;
	}



	/**
	 * @return the secDomPr
	 */
	public int getSecuenciaDomicilioPrincipal() {
		return secuenciaDomicilioPrincipal;
	}



	/**
	 * @param secDomPr the secDomPr to set
	 */
	public void setSecuenciaDomicilioPrincipal(int secDomPr) {
		this.secuenciaDomicilioPrincipal = secDomPr;
	}



	/**
	 * @return the secDomEn
	 */
	public int getSecuenciaDomicilioEnvio() {
		return secuenciaDomicilioEnvio;
	}



	/**
	 * @param secDomEn the secDomEn to set
	 */
	public void setSecuenciaDomicilioEnvio(int secDomEn) {
		this.secuenciaDomicilioEnvio = secDomEn;
	}



	/**
	 * @return the secDomFi
	 */
	public int getSecuenciaDomicilioFiscal() {
		return secuenciaDomicilioFiscal;
	}



	/**
	 * @param secDomFi the secDomFi to set
	 */
	public void setSecuenciaDomicilioFiscal(int secDomFi) {
		this.secuenciaDomicilioFiscal = secDomFi;
	}



	/**
	 * @return the desCont1
	 */
	public String getDescripcionCont1() {
		return descripcionCont1;
	}



	/**
	 * @param desCont1 the desCont1 to set
	 */
	public void setDescripcionCont1(String desCont1) {
		this.descripcionCont1 = desCont1;
	}



	/**
	 * @return the desCont2
	 */
	public String getDescripcionCont2() {
		return descripcionCont2;
	}



	/**
	 * @param desCont2 the desCont2 to set
	 */
	public void setDescripcionCont2(String desCont2) {
		this.descripcionCont2 = desCont2;
	}



	/**
	 * @return the desFirm1
	 */
	public String getDescripcionFirm1() {
		return descripcionFirm1;
	}



	/**
	 * @param desFirm1 the desFirm1 to set
	 */
	public void setDescripcionFirm1(String desFirm1) {
		this.descripcionFirm1 = desFirm1;
	}



	/**
	 * @return the desFirm2
	 */
	public String getDescripcionFirm2() {
		return descripcionFirm2;
	}



	/**
	 * @param desFirm2 the desFirm2 to set
	 */
	public void setDescripcionFirm2(String desFirm2) {
		this.descripcionFirm2 = desFirm2;
	}



	/**
	 * @return the desArre1
	 */
	public String getDescripcionArre1() {
		return descripcionArre1;
	}



	/**
	 * @param desArre1 the desArre1 to set
	 */
	public void setDescripcionArre1(String desArre1) {
		this.descripcionArre1 = desArre1;
	}



	/**
	 * @return the desArre2
	 */
	public String getDescripcionArre2() {
		return descripcionArre2;
	}



	/**
	 * @param desArre2 the desArre2 to set
	 */
	public void setDescripcionArre2(String desArre2) {
		this.descripcionArre2 = desArre2;
	}



	/**
	 * @return the desReex1
	 */
	public String getDescripcionReex1() {
		return descripcionReex1;
	}



	/**
	 * @param desReex1 the desReex1 to set
	 */
	public void setDescripcionReex1(String desReex1) {
		this.descripcionReex1 = desReex1;
	}



	/**
	 * @return the desReex2
	 */
	public String getDescripcionReex2() {
		return descripcionReex2;
	}

	/**
	 * @param desReex2 the desReex2 to set
	 */
	public void setDescripcionReex2(String desReex2) {
		this.descripcionReex2 = desReex2;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
}
