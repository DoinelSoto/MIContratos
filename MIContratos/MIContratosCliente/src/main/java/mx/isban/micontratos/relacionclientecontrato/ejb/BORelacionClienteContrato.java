package mx.isban.micontratos.relacionclientecontrato.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestioncontratos.BeanConsultaInversionRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanCambioTitularRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanDetalleContratoInversionResultado;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosRequest;
import mx.isban.micontratos.beans.relacionclientecontrato.BeanLocalizadorContratosResultado;
/**
 * @author Daniel Hernández Soto
 * Interfaz que declara a los métodos que accederán al DAO que ejecuará el consumo de las transacciones de la relación clinete contrato
 *
 */

public interface BORelacionClienteContrato {

	/**
	 * Metódo que hará el llamado a el consumo de la transacción MI30 : Cambio de titular de contrato
	 * @param beanCambioTitularRequest
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico modificarTitularContrato(BeanCambioTitularRequest beanCambioTitularRequest,ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Metódo que ejecutará el consumo de la transacción OD39 y WS perfilamiento: Localizador de contratos por cliente 
	 * @param request
	 * @param session
	 * @return BeanLocalizadorContratosResultado
	 */
	BeanLocalizadorContratosResultado localizadorContratos(BeanLocalizadorContratosRequest request,ArchitechSessionBean session) throws BusinessException;

	/**
	 * Metódo que ejecutará el consumo de consultaDatosContrato y servicios de Personas : Detalle Contrato Inversion
	 * @param request
	 * @param session
	 * @return BeanDetalleContratoInversionResultado
	 */
	BeanDetalleContratoInversionResultado detalleContratoInversion(BeanConsultaInversionRequest request,ArchitechSessionBean session) throws BusinessException;
	
}
