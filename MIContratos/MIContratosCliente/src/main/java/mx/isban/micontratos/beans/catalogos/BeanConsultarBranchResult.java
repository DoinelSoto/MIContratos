package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;


/**
 * @author everis
 *
 */

/**  
 *	Bean con los datos de salida e implementación de la interfaz BeanResultBO para la transacción : MI39
 **/
public class BeanConsultarBranchResult extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 1863568241148361371L;

	/**
	 * Lista para beans branch de salida
	 */
	private List<BeanConsultarBranchResponse> lstBranchEntidad;

	/**
	 * Bean para la paginación
	 */
	private BeanPaginacion rellamada;

	/**
	 * Class BeanConsultarBranchResult
	 */
	public BeanConsultarBranchResult(){
		rellamada = new BeanPaginacion();
	}
	
	/**
	 * @return the lstBranchEntidad
	 */
	public List<BeanConsultarBranchResponse> getLstBranchEntidad() {
		return new ArrayList<> (lstBranchEntidad);
	}

	/**
	 * @param lstBranchEntidad the lstBranchEntidad to set
	 */
	public void setLstBranchEntidad(List<BeanConsultarBranchResponse> lstBranchEntidad) {
		this.lstBranchEntidad =  new ArrayList<>(lstBranchEntidad);
	}
	
	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	
	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	
}