package mx.isban.micontratos.beans.relacionclientecontrato;

import java.io.Serializable;

/**
 * 
 * @author everis
 *	Bean con los campos detallados de usos domicilios (devueltos por MI16): detalle de contrato de inversión
 */
public class BeanDetalleContratoInversionUsosDomicilio implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 2505601173238535253L;
	
	/**
	 * Número de persona
	 */
	private String numeroPersona;
	
	/**
	 * Tipo uso domicilio
	 */
	private String tipoUsoDomicilio;
	
	/**
	 * Secuencia domicilio
	 */
	private String secuenciaDomicilio;
	
	/**
	 * Tipo domicilio
	 */
	private String tipoDomicilio;
	
	/**
	 * Nombre domicilio
	 */
	private String nombreDomicilio;

	/**
	 * @return the numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}

	/**
	 * @param numeroPersona the numeroPersona to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}

	/**
	 * @return the tipoUsoDomicilio
	 */
	public String getTipoUsoDomicilio() {
		return tipoUsoDomicilio;
	}

	/**
	 * @param tipoUsoDomicilio the tipoUsoDomicilio to set
	 */
	public void setTipoUsoDomicilio(String tipoUsoDomicilio) {
		this.tipoUsoDomicilio = tipoUsoDomicilio;
	}

	/**
	 * @return the secuenciaDomicilio
	 */
	public String getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}

	/**
	 * @param secuenciaDomicilio the secuenciaDomicilio to set
	 */
	public void setSecuenciaDomicilio(String secuenciaDomicilio) {
		this.secuenciaDomicilio = secuenciaDomicilio;
	}

	/**
	 * @return the tipoDomicilio
	 */
	public String getTipoDomicilio() {
		return tipoDomicilio;
	}

	/**
	 * @param tipoDomicilio the tipoDomicilio to set
	 */
	public void setTipoDomicilio(String tipoDomicilio) {
		this.tipoDomicilio = tipoDomicilio;
	}

	/**
	 * @return the nombreDomicilio
	 */
	public String getNombreDomicilio() {
		return nombreDomicilio;
	}

	/**
	 * @param nombreDomicilio the nombreDomicilio to set
	 */
	public void setNombreDomicilio(String nombreDomicilio) {
		this.nombreDomicilio = nombreDomicilio;
	}	
}