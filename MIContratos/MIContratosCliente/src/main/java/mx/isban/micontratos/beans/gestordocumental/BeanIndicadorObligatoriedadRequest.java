package mx.isban.micontratos.beans.gestordocumental;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * 
 * Bean con los datos de entrada para la transacción MI : Indicador obligatoriedad
 *
 */
public class BeanIndicadorObligatoriedadRequest implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -8127015291052945511L;
	
	/**
	 * Bean con los campos de multicanalidad
	 */
	@Valid
	private MulticanalidadGenerica multicanalidad;
	

	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias	
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato		
	 */
	@Size(max=4)
	private String empresaContrato;

	/**
	 * Centro del Contrato		
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato		
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato	
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato		
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del Contrato de Inversión		
	 */
	@Size(max=4)
	private String codigoEmpresaContratoInversion;
	/**
	 * Alias del contrato	
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Fecha de Proceso
	 */
	@Size(max=10)
	private String fechaProceso;
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanIndicadorObligatoriedadRequest(){

		
		multicanalidad=new MulticanalidadGenerica();
		rellamada = new BeanPaginacion();

	}
	
	/**
	 * @return the tipConsu
	 */
	
	public String getTipoConsulta() {
		return tipoConsulta;
	}

	/**
	 * @param tipConsu the tipConsu to set
	 */
	public void setTipoConsulta(String tipConsu) {
		this.tipoConsulta = tipConsu;
	}

	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}

	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}

	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}

	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}

	/**
	 * @param idStiPro the idStiPro to set
	 */
	public void setCodigoSubProducto(String idStiPro) {
		this.codigoSubProducto = idStiPro;
	}

	/**
	 * @return the idEmprCo
	 */
	public String getCodigoEmpresaContratoInversion() {
		return codigoEmpresaContratoInversion;
	}

	/**
	 * @param codigoEmpresaContratoInversion the idEmprCo to set
	 */
	public void setCodigoEmpresaContratoInversion(String codigoEmpresaContratoInversion) {
		this.codigoEmpresaContratoInversion = codigoEmpresaContratoInversion;
	}

	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}

	/**
	 * @param aliasContrato the desAlias to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}


	/**
	 * @return multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad(){
		return multicanalidad;
	}
	
	/**
	 * @param multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad){
		this.multicanalidad=multicanalidad;
	}

	/**
	 * @return the fechaProceso
	 */
	public String getFechaProceso() {
		return fechaProceso;
	}

	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	
	/**
	 * 
	 * @return rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	
	/**
	 * 
	 * @param rellamada
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
}