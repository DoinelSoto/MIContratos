package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;

/**
 * @author everis
 *
 */

/** 
 *  
 *  Bean con los datos de entradas para la transaccion :
 * 
 	MI06	Tipos de servicio

 * */

public class BeanListaTiposServicioRequest implements Serializable{


	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -4503026259449295605L;


	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	


	/**
	 * Código de tipo de servicio
	 */
	@Size(max=2)
	private String codigoTipoServicio;
	

	/**
	 * Indicador de rellamada
	 */
	@Size(max=1)
	private String indicadorRellamada;
	/**
	 * memento
	 */
	@Size(max=3)
	private String memento;
	
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanListaTiposServicioRequest(){

	
		rellamada = new BeanPaginacion();
		

	}
	
	
	/**
	 * @return the codTipSe
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}
	/**
	 * @param codigoTipoServicio the codTipSe to set
	 */
	public void setCodigoTipoServicio(String codigoTipoServicio) {
		this.codigoTipoServicio = codigoTipoServicio;
	}
	
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

}
