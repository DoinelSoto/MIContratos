package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI10 Alta Tabla Auxiliar
 *
 */
public class BeanAltaTablaAuxiliarRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3371843083335644001L;

	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	
	/**
	 * Número de folio del que proviene el contrato	
	 */
	private int numeroFolio;
	/**
	 * Código de tipo de registro guardado	
	 */
	@Size(max=2)
	private String codigoTipoRegistroGuardado;
	/**
	 * Calidad de participación
	 */
	@Size(max=2)
	private String calidadParticipacion;
	/**
	 * Número de persona	
	 */
	@Size(max=8)
	private String numeroPersona;
	/**
	 * Tipo de domicilio
	 */
	@Size(max=3)
	private String tipoDomicilio;
	/**
	 * Secuencia de domicilio
	 */
	private int secuenciaDomicilio;
	/**
	 * Identificador del tipo de firma	
	 */
	@Size(max=2)
	private String identificadorTipoFirma;
	/**
	 * Código de tipo de poder	
	 */
	@Size(max=2)
	private String codigoTipoPoder;
	/**
	 * Folio del poder	
	 */
	@Size(max=30)
	private String folioPoder;
	/**
	 * Porcentaje de participación	
	 */
	@Size(max=5)
	private String porcentajeParticipacion;
	/**
	 * Porcentaje de IPAB	
	 */
	@Size(max=5)
	private String porcentajeIpab;
	/**
	 * Tipo de contacto	
	 */
	@Size(max=2)
	private String tipoContacto;
	/**
	 * Personalidad jurídica	
	 */
	@Size(max=1)
	private String personalidadJuridica;
	/**
	 * Nombre del contacto	
	 */
	@Size(max=40)
	private String nombreContacto;
	/**
	 * 	Primer apellido del contacto	
	 */
	@Size(max=20)
	private String primerApellidoContacto;
	/**
	 * Segundo apellido del contacto
	 */
	@Size(max=20)
	private String segundoApellidoContacto;
	/**
	 * Fecha de nacimiento
	 */
	@Size(max=10)
	private String fechaNacimiento;
	/**
	 * Tipo de documento	
	 */
	@Size(max=2)
	private String tipoDocumento;
	/**
	 * Número de documento
	 */
	@Size(max=20)
	private String numeroDocumento;
	/**
	 * Nacionalidad del contacto	
	 */
	@Size(max=3)
	private String nacionalidadContacto;
	/**
	 * País de nacimiento del contacto	
	 */
	@Size(max=3)
	private String paisNacimientoContacto;
	/**
	 * País del contacto	
	 */
	@Size(max=3)
	private String paisContacto;
	/**
	 * Nombre de calle
	 */
	@Size(max=50)
	private String nombreCalle;
	/**
	 * Número exterior de la calle
	 */
	@Size(max=15)
	private String numeroExteriorCalle;
	/**
	 * Número interior de la calle	
	 */
	@Size(max=15)
	private String numeroInteriorCalle;
	/**
	 * Número telefónico del contacto	
	 */
	@Size(max=10)
	private String numeroTelefonicoContacto;
	/**
	 * Colonia
	 */
	@Size(max=30)
	private String colonia;
	/**
	 * Código postal	
	 */
	@Size(max=8)
	private String codigoPostal;
	/**
	 * Ciudad / Población / Localidad	
	 */
	@Size(max=7)
	private String ciudadPoblacionLocalidad;
	/**
	 * Código provincia	
	 */
	@Size(max=2)
	private String codigoProvincia;
	/**
	 * Delegación o municipio	
	 */
	@Size(max=5)
	private String delegacionMunicipio;
	/**
	 * Email del contacto	
	 */
	@Size(max=50)
	private String emailContacto;
	/**
	 * Empresa del contrato relacionado	
	 */
	@Size(max=4)
	private String empresaContratoRelacionado;
	/**
	 * Centro del contrato relacionado
	 */
	@Size(max=4)
	private String centroContratoRelacionado;
	/**
	 * Número de contrato relacionado	
	 */
	@Size(max=12)
	private String numeroContratoRelacionado;
	/**
	 * Código de producto del contrato relacionado
	 */
	@Size(max=2)
	private String codigoProductoContratoRelacionado;
	/**
	 * Código de subproducto del contrato relacionado	
	 */
	@Size(max=4)
	private String codigoSubproductoContratoRelacionado;
	/**
	 * Descripción asociada a la referencia	
	 */
	@Size(max=50)
	private String descripcionAsociadaReferencia;
	/**
	 * Descripción asociada a la referencia	número dos
	 */
	@Size(max=50)
	private String descripcionAsociadaReferencia2;
	/**
	 * Indicador de validación de folio	
	 */
	@Size(max=1)
	private String indicadorValidacionFolio;

	/**
	 * Constructor para la rellamada generica
	 */
	public BeanAltaTablaAuxiliarRequest(){		
		multicanalidad=new MulticanalidadGenerica();			
	}

	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
	/**
	 * @return the numFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}



	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumeroFolio(int numFolio) {
		this.numeroFolio = numFolio;
	}



	/**
	 * @return the codTipRe
	 */
	public String getCodigoTipoRegistroGuardado() {
		return codigoTipoRegistroGuardado;
	}



	/**
	 * @param codTipRe the codTipRe to set
	 */
	public void setCodigoTipoRegistroGuardado(String codTipRe) {
		this.codigoTipoRegistroGuardado = codTipRe;
	}


	/**
	 * @return the peCalPar
	 */
	public String getCalidadParticipacion() {
		return calidadParticipacion;
	}



	/**
	 * @param peCalPar the peCalPar to set
	 */
	public void setCalidadParticipacion(String peCalPar) {
		this.calidadParticipacion = peCalPar;
	}



	/**
	 * @return the peNumPer
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}



	/**
	 * @param peNumPer the peNumPer to set
	 */
	public void setNumeroPersona(String peNumPer) {
		this.numeroPersona = peNumPer;
	}



	/**
	 * @return the peTipDom
	 */
	public String getTipoDomicilio() {
		return tipoDomicilio;
	}



	/**
	 * @param peTipDom the peTipDom to set
	 */
	public void setTipoDomicilio(String peTipDom) {
		this.tipoDomicilio = peTipDom;
	}



	/**
	 * @return the peSecDom
	 */
	public int getSecuenciaDomicilio() {
		return secuenciaDomicilio;
	}



	/**
	 * @param peSecDom the peSecDom to set
	 */
	public void setSecuenciaDomicilio(int peSecDom) {
		this.secuenciaDomicilio = peSecDom;
	}



	/**
	 * @return the idTipFir
	 */
	public String getIdentificadorTipoFirma() {
		return identificadorTipoFirma;
	}



	/**
	 * @param idTipFir the idTipFir to set
	 */
	public void setIdentificadorTipoFirma(String idTipFir) {
		this.identificadorTipoFirma = idTipFir;
	}



	/**
	 * @return the codTipPo
	 */
	public String getCodigoTipoPoder() {
		return codigoTipoPoder;
	}



	/**
	 * @param codTipPo the codTipPo to set
	 */
	public void setCodigoTipoPoder(String codTipPo) {
		this.codigoTipoPoder = codTipPo;
	}



	/**
	 * @return the folioPod
	 */
	public String getFolioPoder() {
		return folioPoder;
	}



	/**
	 * @param folioPod the folioPod to set
	 */
	public void setFolioPoder(String folioPod) {
		this.folioPoder = folioPod;
	}



	/**
	 * @return the porcPart
	 */
	public String      getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}



	/**
	 * @param porcPart the porcPart to set
	 */
	public void setPorcentajeParticipacion(String porcPart) {
		this.porcentajeParticipacion = porcPart;
	}



	/**
	 * @return the porcIpab
	 */
	public String getPorcentajeIpab() {
		return porcentajeIpab;
	}



	/**
	 * @param porcIpab the porcIpab to set
	 */
	public void setPorcentajeIpab(String porcIpab) {
		this.porcentajeIpab = porcIpab;
	}



	/**
	 * @return the peTipCon
	 */
	public String getTipoContacto() {
		return tipoContacto;
	}



	/**
	 * @param peTipCon the peTipCon to set
	 */
	public void setTipoContacto(String peTipCon) {
		this.tipoContacto = peTipCon;
	}



	/**
	 * @return the peTipPer
	 */
	public String getPersonalidadJuridica() {
		return personalidadJuridica;
	}



	/**
	 * @param peTipPer the peTipPer to set
	 */
	public void setPersonalidadJuridica(String peTipPer) {
		this.personalidadJuridica = peTipPer;
	}



	/**
	 * @return the peNomCon
	 */
	public String getNombreContacto() {
		return nombreContacto;
	}



	/**
	 * @param peNomCon the peNomCon to set
	 */
	public void setNombreContacto(String peNomCon) {
		this.nombreContacto = peNomCon;
	}



	/**
	 * @return the pePrApeC
	 */
	public String getPrimerApellidoContacto() {
		return primerApellidoContacto;
	}



	/**
	 * @param pePrApeC the pePrApeC to set
	 */
	public void setPrimerApellidoContacto(String pePrApeC) {
		this.primerApellidoContacto = pePrApeC;
	}



	/**
	 * @return the peSgApeC
	 */
	public String getSegundoApellidoContacto() {
		return segundoApellidoContacto;
	}



	/**
	 * @param peSgApeC the peSgApeC to set
	 */
	public void setSegundoApellidoContacto(String peSgApeC) {
		this.segundoApellidoContacto = peSgApeC;
	}



	/**
	 * @return the peFecNac
	 */
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}



	/**
	 * @param peFecNac the peFecNac to set
	 */
	public void setFechaNacimiento(String peFecNac) {
		this.fechaNacimiento = peFecNac;
	}



	/**
	 * @return the peTipDoc
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}



	/**
	 * @param peTipDoc the peTipDoc to set
	 */
	public void setTipoDocumento(String peTipDoc) {
		this.tipoDocumento = peTipDoc;
	}



	/**
	 * @return the peNumDoc
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}



	/**
	 * @param peNumDoc the peNumDoc to set
	 */
	public void setNumeroDocumento(String peNumDoc) {
		this.numeroDocumento = peNumDoc;
	}



	/**
	 * @return the peNacCon
	 */
	public String getNacionalidadContacto() {
		return nacionalidadContacto;
	}



	/**
	 * @param peNacCon the peNacCon to set
	 */
	public void setNacionalidadContacto(String peNacCon) {
		this.nacionalidadContacto = peNacCon;
	}



	/**
	 * @return the pePaiNac
	 */
	public String getPaisNacimientoContacto() {
		return paisNacimientoContacto;
	}



	/**
	 * @param pePaiNac the pePaiNac to set
	 */
	public void setPaisNacimientoContacto(String pePaiNac) {
		this.paisNacimientoContacto = pePaiNac;
	}



	/**
	 * @return the peCodPai
	 */
	public String getPaisContacto() {
		return paisContacto;
	}



	/**
	 * @param peCodPai the peCodPai to set
	 */
	public void setPaisContacto(String peCodPai) {
		this.paisContacto = peCodPai;
	}



	/**
	 * @return the peNomCal
	 */
	public String getNombreCalle() {
		return nombreCalle;
	}



	/**
	 * @param peNomCal the peNomCal to set
	 */
	public void setNombreCalle(String peNomCal) {
		this.nombreCalle = peNomCal;
	}



	/**
	 * @return the peNumExt
	 */
	public String getNumeroExteriorCalle() {
		return numeroExteriorCalle;
	}



	/**
	 * @param peNumExt the peNumExt to set
	 */
	public void setNumeroExteriorCalle(String peNumExt) {
		this.numeroExteriorCalle = peNumExt;
	}



	/**
	 * @return the peNuInte
	 */
	public String getNumeroInteriorCalle() {
		return numeroInteriorCalle;
	}



	/**
	 * @param peNuInte the peNuInte to set
	 */
	public void setNumeroInteriorCalle(String peNuInte) {
		this.numeroInteriorCalle = peNuInte;
	}



	/**
	 * @return the peNumTel
	 */
	public String getNumeroTelefonicoContacto() {
		return numeroTelefonicoContacto;
	}



	/**
	 * @param peNumTel the peNumTel to set
	 */
	public void setNumeroTelefonicoContacto(String peNumTel) {
		this.numeroTelefonicoContacto = peNumTel;
	}



	/**
	 * @return the peNomAse
	 */
	public String getColonia() {
		return colonia;
	}



	/**
	 * @param peNomAse the peNomAse to set
	 */
	public void setColonia(String peNomAse) {
		this.colonia = peNomAse;
	}



	/**
	 * @return the peCodPos
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}



	/**
	 * @param peCodPos the peCodPos to set
	 */
	public void setCodigoPostal(String peCodPos) {
		this.codigoPostal = peCodPos;
	}



	/**
	 * @return the peNomLoc
	 */
	public String getCiudadPoblacionLocalidad() {
		return ciudadPoblacionLocalidad;
	}



	/**
	 * @param peNomLoc the peNomLoc to set
	 */
	public void setCiudadPoblacionLocalidad(String peNomLoc) {
		this.ciudadPoblacionLocalidad = peNomLoc;
	}



	/**
	 * @return the peCodPrv
	 */
	public String getCodigoProvincia() {
		return codigoProvincia;
	}



	/**
	 * @param peCodPrv the peCodPrv to set
	 */
	public void setCodigoProvincia(String peCodPrv) {
		this.codigoProvincia = peCodPrv;
	}



	/**
	 * @return the peNomCom
	 */
	public String getDelegacionMunicipio() {
		return delegacionMunicipio;
	}



	/**
	 * @param peNomCom the peNomCom to set
	 */
	public void setDelegacionMunicipio(String peNomCom) {
		this.delegacionMunicipio = peNomCom;
	}



	/**
	 * @return the peEmailC
	 */
	public String getEmailContacto() {
		return emailContacto;
	}



	/**
	 * @param peEmailC the peEmailC to set
	 */
	public void setEmailContacto(String peEmailC) {
		this.emailContacto = peEmailC;
	}



	/**
	 * @return the idEmprRe
	 */
	public String getEmpresaContratoRelacionado() {
		return empresaContratoRelacionado;
	}



	/**
	 * @param idEmprRe the idEmprRe to set
	 */
	public void setEmpresaContratoRelacionado(String idEmprRe) {
		this.empresaContratoRelacionado = idEmprRe;
	}



	/**
	 * @return the idCentRe
	 */
	public String getCentroContratoRelacionado() {
		return centroContratoRelacionado;
	}



	/**
	 * @param idCentRe the idCentRe to set
	 */
	public void setCentroContratoRelacionado(String idCentRe) {
		this.centroContratoRelacionado = idCentRe;
	}



	/**
	 * @return the idContRe
	 */
	public String getNumeroContratoRelacionado() {
		return numeroContratoRelacionado;
	}



	/**
	 * @param idContRe the idContRe to set
	 */
	public void setNumeroContratoRelacionado(String idContRe) {
		this.numeroContratoRelacionado = idContRe;
	}



	/**
	 * @return the idProd
	 */
	public String getCodigoProductoContratoRelacionado() {
		return codigoProductoContratoRelacionado;
	}



	/**
	 * @param idProd the idProd to set
	 */
	public void setCodigoProductoContratoRelacionado(String idProd) {
		this.codigoProductoContratoRelacionado = idProd;
	}



	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubproductoContratoRelacionado() {
		return codigoSubproductoContratoRelacionado;
	}



	/**
	 * @param idStiPro the idStiPro to set
	 */
	public void setCodigoSubproductoContratoRelacionado(String idStiPro) {
		this.codigoSubproductoContratoRelacionado = idStiPro;
	}



	/**
	 * @return the descripc
	 */
	public String getDescripcionAsociadaReferencia() {
		return descripcionAsociadaReferencia;
	}



	/**
	 * @param descripc the descripc to set
	 */
	public void setDescripcionAsociadaReferencia(String descripc) {
		this.descripcionAsociadaReferencia = descripc;
	}

	/**
	 * @return the descripcionAsociadaReferencia2
	 */
	public String getDescripcionAsociadaReferencia2() {
		return descripcionAsociadaReferencia2;
	}

	/**
	 * @param descripcionAsociadaReferencia2 the descripcionAsociadaReferencia2 to set
	 */
	public void setDescripcionAsociadaReferencia2(String descripcionAsociadaReferencia2) {
		this.descripcionAsociadaReferencia2 = descripcionAsociadaReferencia2;
	}


	/**
	 * @return the idFolio
	 */
	public String getIndicadorValidacionFolio() {
		return indicadorValidacionFolio;
	}



	/**
	 * @param idFolio the idFolio to set
	 */
	public void setIndicadorValidacionFolio(String idFolio) {
		this.indicadorValidacionFolio = idFolio;
	}


}
