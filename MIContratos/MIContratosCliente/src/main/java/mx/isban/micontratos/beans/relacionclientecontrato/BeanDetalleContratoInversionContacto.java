package mx.isban.micontratos.beans.relacionclientecontrato;

import java.io.Serializable;

/**
 * 
 * @author everis
 *	Bean con los datos de contacto del bean de salida del servicio detalle de contrato de inversion
 */
public class BeanDetalleContratoInversionContacto implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 2205608173235331213L;
	
	/**
	 * Tipo de contacto
	 */
	private String tipoContacto;
	
	/**
	 * Secuencia del contacto
	 */
	private int secunciaContacto;
	
	/**
	 * Tipo de personalidad juridica del contacto
	 */
	private String tipPersJuridConta;
	
	/**
	 * Apellido paterno del contacto
	 */
	private String apePaterContact;
	
	/**
	 * Apellido materno del contacto
	 */
	private String apeMaternoContact;
	
	/**
	 * nombre del contacto
	 */
	private String nombreContact;
	
	/**
	 * Denominación o razón social
	 */
	private String denomRazonSocial;
	
	/**
	 * Fecha de nacimiento del contacto
	 */
	private String fechaNacimientoContacto;
	
	/**
	 * NUmero de identificación fiscal
	 */
	private String numeroIdentificacionFiscal;
	
	/**
	 * RFC
	 */
	private String rfc;
	
	/**
	 * CURP
	 */
	private String curp;
	
	/**
	 * País de nacimiento del contacto
	 */
	private String paisNacimientoContacto;
	
	/**
	 * Nacionalidad del contacto
	 */
	private String nacionalidadContacto;
	
	/**
	 * Email del contacto
	 */
	private String emailContacto;
	
	/**
	 * LADA
	 */
	private String prefijoTelefono;
	
	/**
	 * telefono del contacto
	 */
	private String telefonoContacto;
	
	/**
	 * Porcentaje de participación
	 */
	private String porcentajeParticpacion;
	
	/**
	 * Calle, avenida, via
	 */
	private String calleAvenidaVia;
	
	/**
	 * Número exterior
	 */
	private String numeroExterior;
	
	/**
	 * Número interior
	 */
	private String numeroInterior;
	
	/**
	 * Asentamiento - colonia
	 */
	private String asentamientoColonia;
	
	/**
	 * Código postal
	 */
	private String codigoPostal;
	
	/**
	 * Ciudad - población
	 */
	private String ciudadPoblacion;
	
	/**
	 * Descripción ciudad - población
	 */
	private String descripcionCiudadPoblacion;
	
	/**
	 * Delegación - municipio del contacto
	 */
	private String delegacionMunicipioContact;
	
	/**
	 * Descripción delegación - municipio del contacto
	 */
	private String descDelegacionMunicipioContacto;
	
	/**
	 * Entidad federativa domicilio del contacto
	 */
	private String entidadFederatDomicContacto;
	
	/**
	 * Descripción entidad federativa del contacto
	 */
	private String descEntidadFederatContacto;
	
	/**
	 * País del domicilio
	 */
	private String paisDomiclio;

	/**
	 * @return the tipoContacto
	 */
	public String getTipoContacto() {
		return tipoContacto;
	}

	/**
	 * @param tipoContacto the tipoContacto to set
	 */
	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	/**
	 * @return the secunciaContacto
	 */
	public int getSecunciaContacto() {
		return secunciaContacto;
	}

	/**
	 * @param secunciaContacto the secunciaContacto to set
	 */
	public void setSecunciaContacto(int secunciaContacto) {
		this.secunciaContacto = secunciaContacto;
	}

	/**
	 * @return the tipPersJuridConta
	 */
	public String getTipPersJuridConta() {
		return tipPersJuridConta;
	}

	/**
	 * @param tipPersJuridConta the tipPersJuridConta to set
	 */
	public void setTipPersJuridConta(String tipPersJuridConta) {
		this.tipPersJuridConta = tipPersJuridConta;
	}

	/**
	 * @return the apePaterContact
	 */
	public String getApePaterContact() {
		return apePaterContact;
	}

	/**
	 * @param apePaterContact the apePaterContact to set
	 */
	public void setApePaterContact(String apePaterContact) {
		this.apePaterContact = apePaterContact;
	}

	/**
	 * @return the apeMaternoContact
	 */
	public String getApeMaternoContact() {
		return apeMaternoContact;
	}

	/**
	 * @param apeMaternoContact the apeMaternoContact to set
	 */
	public void setApeMaternoContact(String apeMaternoContact) {
		this.apeMaternoContact = apeMaternoContact;
	}

	/**
	 * @return the nombreContact
	 */
	public String getNombreContact() {
		return nombreContact;
	}

	/**
	 * @param nombreContact the nombreContact to set
	 */
	public void setNombreContact(String nombreContact) {
		this.nombreContact = nombreContact;
	}

	/**
	 * @return the denomRazonSocial
	 */
	public String getDenomRazonSocial() {
		return denomRazonSocial;
	}

	/**
	 * @param denomRazonSocial the denomRazonSocial to set
	 */
	public void setDenomRazonSocial(String denomRazonSocial) {
		this.denomRazonSocial = denomRazonSocial;
	}

	/**
	 * @return the fechaNacimientoContacto
	 */
	public String getFechaNacimientoContacto() {
		return fechaNacimientoContacto;
	}

	/**
	 * @param fechaNacimientoContacto the fechaNacimientoContacto to set
	 */
	public void setFechaNacimientoContacto(String fechaNacimientoContacto) {
		this.fechaNacimientoContacto = fechaNacimientoContacto;
	}

	/**
	 * @return the numeroIdentificacionFiscal
	 */
	public String getNumeroIdentificacionFiscal() {
		return numeroIdentificacionFiscal;
	}

	/**
	 * @param numeroIdentificacionFiscal the numeroIdentificacionFiscal to set
	 */
	public void setNumeroIdentificacionFiscal(String numeroIdentificacionFiscal) {
		this.numeroIdentificacionFiscal = numeroIdentificacionFiscal;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the curp
	 */
	public String getCurp() {
		return curp;
	}

	/**
	 * @param curp the curp to set
	 */
	public void setCurp(String curp) {
		this.curp = curp;
	}

	/**
	 * @return the paisNacimientoContacto
	 */
	public String getPaisNacimientoContacto() {
		return paisNacimientoContacto;
	}

	/**
	 * @param paisNacimientoContacto the paisNacimientoContacto to set
	 */
	public void setPaisNacimientoContacto(String paisNacimientoContacto) {
		this.paisNacimientoContacto = paisNacimientoContacto;
	}

	/**
	 * @return the nacionalidadContacto
	 */
	public String getNacionalidadContacto() {
		return nacionalidadContacto;
	}

	/**
	 * @param nacionalidadContacto the nacionalidadContacto to set
	 */
	public void setNacionalidadContacto(String nacionalidadContacto) {
		this.nacionalidadContacto = nacionalidadContacto;
	}

	/**
	 * @return the emailContrato
	 */
	public String getEmailContacto() {
		return emailContacto;
	}

	/**
	 * @param emailContrato the emailContrato to set
	 */
	public void setEmailContacto(String emailContacto) {
		this.emailContacto = emailContacto;
	}

	/**
	 * @return the prefijoTelefono
	 */
	public String getPrefijoTelefono() {
		return prefijoTelefono;
	}

	/**
	 * @param prefijoTelefono the prefijoTelefono to set
	 */
	public void setPrefijoTelefono(String prefijoTelefono) {
		this.prefijoTelefono = prefijoTelefono;
	}

	/**
	 * @return the relefonoContacto
	 */
	public String getTelefonoContacto() {
		return telefonoContacto;
	}

	/**
	 * @param relefonoContacto the relefonoContacto to set
	 */
	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}

	/**
	 * @return the porcentajeParticpacion
	 */
	public String getPorcentajeParticpacion() {
		return porcentajeParticpacion;
	}

	/**
	 * @param porcentajeParticpacion the porcentajeParticpacion to set
	 */
	public void setPorcentajeParticpacion(String porcentajeParticpacion) {
		this.porcentajeParticpacion = porcentajeParticpacion;
	}

	/**
	 * @return the calleAvenidaVia
	 */
	public String getCalleAvenidaVia() {
		return calleAvenidaVia;
	}

	/**
	 * @param calleAvenidaVia the calleAvenidaVia to set
	 */
	public void setCalleAvenidaVia(String calleAvenidaVia) {
		this.calleAvenidaVia = calleAvenidaVia;
	}

	/**
	 * @return the numeroExterior
	 */
	public String getNumeroExterior() {
		return numeroExterior;
	}

	/**
	 * @param numeroExterior the numeroExterior to set
	 */
	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}

	/**
	 * @return the numeroInterior
	 */
	public String getNumeroInterior() {
		return numeroInterior;
	}

	/**
	 * @param numeroInterior the numeroInterior to set
	 */
	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

	/**
	 * @return the asentamientoColonia
	 */
	public String getAsentamientoColonia() {
		return asentamientoColonia;
	}

	/**
	 * @param asentamientoColonia the asentamientoColonia to set
	 */
	public void setAsentamientoColonia(String asentamientoColonia) {
		this.asentamientoColonia = asentamientoColonia;
	}

	/**
	 * @return the codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}

	/**
	 * @param codigoPostal the codigoPostal to set
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/**
	 * @return the ciudadPoblacion
	 */
	public String getCiudadPoblacion() {
		return ciudadPoblacion;
	}

	/**
	 * @param ciudadPoblacion the ciudadPoblacion to set
	 */
	public void setCiudadPoblacion(String ciudadPoblacion) {
		this.ciudadPoblacion = ciudadPoblacion;
	}

	/**
	 * @return the descripcionCiudadPoblacion
	 */
	public String getDescripcionCiudadPoblacion() {
		return descripcionCiudadPoblacion;
	}

	/**
	 * @param descripcionCiudadPoblacion the descripcionCiudadPoblacion to set
	 */
	public void setDescripcionCiudadPoblacion(String descripcionCiudadPoblacion) {
		this.descripcionCiudadPoblacion = descripcionCiudadPoblacion;
	}

	/**
	 * @return the delegacionMunicipioContact
	 */
	public String getDelegacionMunicipioContact() {
		return delegacionMunicipioContact;
	}

	/**
	 * @param delegacionMunicipioContact the delegacionMunicipioContact to set
	 */
	public void setDelegacionMunicipioContact(String delegacionMunicipioContact) {
		this.delegacionMunicipioContact = delegacionMunicipioContact;
	}

	/**
	 * @return the descDelegacionMunicipioContacto
	 */
	public String getDescDelegacionMunicipioContacto() {
		return descDelegacionMunicipioContacto;
	}

	/**
	 * @param descDelegacionMunicipioContacto the descDelegacionMunicipioContacto to set
	 */
	public void setDescDelegacionMunicipioContacto(String descDelegacionMunicipioContacto) {
		this.descDelegacionMunicipioContacto = descDelegacionMunicipioContacto;
	}

	/**
	 * @return the entidadFederatDomicContacto
	 */
	public String getEntidadFederatDomicContacto() {
		return entidadFederatDomicContacto;
	}

	/**
	 * @param entidadFederatDomicContacto the entidadFederatDomicContacto to set
	 */
	public void setEntidadFederatDomicContacto(String entidadFederatDomicContacto) {
		this.entidadFederatDomicContacto = entidadFederatDomicContacto;
	}

	/**
	 * @return the descEntidadFederatContacto
	 */
	public String getDescEntidadFederatContacto() {
		return descEntidadFederatContacto;
	}

	/**
	 * @param descEntidadFederatContacto the descEntidadFederatContacto to set
	 */
	public void setDescEntidadFederatContacto(String descEntidadFederatContacto) {
		this.descEntidadFederatContacto = descEntidadFederatContacto;
	}

	/**
	 * @return the paisDomiclio
	 */
	public String getPaisDomiclio() {
		return paisDomiclio;
	}

	/**
	 * @param paisDomiclio the paisDomiclio to set
	 */
	public void setPaisDomiclio(String paisDomiclio) {
		this.paisDomiclio = paisDomiclio;
	}	
}