package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

/**
 * 
 * @author everis
 * Bean con campos de salida de la transacción : MI16
 */
public class BeanConsultaInversionResponse implements Serializable{

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -3075745545761463105L;
	
	/**
	 * Bean de datos
	 */
	@Valid
	private BeanDatosContratos beanDatosContratos;
	
	/**
	 * Número de contactos devueltos	
	 */
	private String numeroContacto;
	
	/**
	 * Occurs de 30 para devolver los intervinientes del contrato	
	 */
	private String numeroIntervinientes;
	
	/**
	 * Número de usos de domicilio 
	 */
	private String numeroUsosDomicilio;
	
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanListaContactosResponse> listaContactos;
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanDatosDomicilioResponse> listaDatosDomicilio;
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanListaIntervinientesResponse> listaIntervinientes;
	
	/**
	 * Constructor
	 */
	public BeanConsultaInversionResponse() {
		beanDatosContratos= new BeanDatosContratos();
		listaContactos= new ArrayList<>();
		listaDatosDomicilio= new ArrayList<>();
		listaIntervinientes=new ArrayList<>();
	}

	/**
	 * 
	 * @return the beanDatosContratos
	 */
	public BeanDatosContratos getBeanDatosContratos() {
		return beanDatosContratos;
	}

	/**
	 * 
	 * @param beanDatosContratos
	 */
	public void setBeanDatosContratos(BeanDatosContratos beanDatosContratos) {
		this.beanDatosContratos = beanDatosContratos;
	}
	
	/**
	 * 
	 * @return the numeroContacto
	 */
	public String getNumeroContacto() {
		return numeroContacto;
	}
	
	/**
	 * @param numeroContacto the numCon to set
	 */
	public void setNumeroContacto(String numeroContacto) {
		this.numeroContacto = numeroContacto;
	}


	/**
	 * @return String numeroIntervinientes
	 */
	public String getNumeroIntervinientes() {
		return numeroIntervinientes;
	}

	/**
	 * @param numeroIntervinientes
	 */
	public void setNumeroIntervinientes(String numeroIntervinientes) {
		this.numeroIntervinientes = numeroIntervinientes;
	}
	
	/**
	 * 
	 * @return the numeroUsosDomicilio
	 */
	public String getNumeroUsosDomicilio() {
		return numeroUsosDomicilio;
	}
	/**
	 * 
	 * @param numeroUsosDomicilio
	 */
	public void setNumeroUsosDomicilio(String numeroUsosDomicilio) {
		this.numeroUsosDomicilio = numeroUsosDomicilio;
	}

	/**
	 * 
	 * @return the listaContactos
	 */
	public List<BeanListaContactosResponse> getListaContactos() {
		return new ArrayList<>(listaContactos);
	}
	
	/**
	 * 
	 * @param listaContactos
	 */
	public void setListaContactos(
			List<BeanListaContactosResponse> listaContactos) {
		this.listaContactos = new ArrayList<>(listaContactos);
	}
	
	/**
	 * @return the listaDatosDomicilio
	 */
	public List<BeanDatosDomicilioResponse> getListaDatosDomicilio() {
		return new ArrayList<>(listaDatosDomicilio);
	}

	/**
	 * @param listaDatosDomicilio the listaDatosDomicilio to set
	 */
	public void setListaDatosDomicilio(
			List<BeanDatosDomicilioResponse> listaDatosDomicilio) {
		this.listaDatosDomicilio = new ArrayList<>(listaDatosDomicilio);
	}

	/**
	 * @return  ArrayList
	 */
	public List<BeanListaIntervinientesResponse> getListaIntervinientes() {
		return new ArrayList<>(listaIntervinientes);
	}

	/**
	 * @param listaIntervinientes
	 */
	public void setListaIntervinientes(
			List<BeanListaIntervinientesResponse> listaIntervinientes) {
		this.listaIntervinientes = new ArrayList<>(listaIntervinientes);
	}
	
}
