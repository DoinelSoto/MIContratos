package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI23 : Alta contratos amparados por chequera
 *
 */
public class BeanAltaContratosAmparadosChequeraResultado extends ResponseGenerico implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 8737534830974989750L;
	
	
	/**
	 * Empresa del contrato	
	 */
	private String empresaContrato;
	/**
	 * Centro del contrato	
	 */
	private String centroContrato;
	/**
	 * Numero de contrato
	 */
	private String numeroContrato;
	/**
	 * Codigo de producto del contrato
	 */
	private String codigoProducto;
	/**
	 * Codigo de subproducto del contrato
	 */
	private String codigoSubProducto;
	/**
	 * Codigo de empresa del contrato de inversion
	 */
	private String codigoEmpresaContrato;
	/**
	 * Alias del contrato	
	 */
	private String aliasContrato;
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param idEmpr the idEmpr to set
	 */
	public void setEmpresaContrato(String idEmpr) {
		this.empresaContrato = idEmpr;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idStiPro to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getCodigoEmpresaContrato() {
		return codigoEmpresaContrato;
	}
	/**
	 * @param codigoEmpresaContrato the idEmprCo to set
	 */
	public void setCodigoEmpresaContrato(String codigoEmpresaContrato) {
		this.codigoEmpresaContrato = codigoEmpresaContrato;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param aliasContrato the desAlias to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}
	
	
}
