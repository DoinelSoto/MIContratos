package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * 
 * Clase principal para el conversor de cuentas
 *
 */
public class BeanConversorCuentasResultado extends ResponseGenerico implements Serializable{

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 203443244449882410L;
	
	/**
	 * Lista de objetos a regresar del response
	 */
	private List<BeanConversorCuentasResponse> beanconversorCuentas;

	/**
	 * @return the beanconversorCuentas
	 */
	public List<BeanConversorCuentasResponse> getBeanconversorCuentas() {
		return new ArrayList<>(beanconversorCuentas);
	}

	/**
	 * @param beanconversorCuentas the beanconversorCuentas to set
	 */
	public void setBeanconversorCuentas(List<BeanConversorCuentasResponse> beanconversorCuentas) {
		this.beanconversorCuentas = new ArrayList<>(beanconversorCuentas);
	}
	
	
}