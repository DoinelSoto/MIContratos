package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de respuesta para la transaccion MI18 : Alta contrato Inversión
 *
 */
public class BeanAltaContratoInversionResponse implements Serializable{


	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 374893621032559766L;
	/**
	 * Empresa del Contrato	
	 */
	private String empresaContrato;
	/**
	 * Centro del contrato	
	 */
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	private String numeroContrato;
	/**
	 * Código de producto 
	 */
	private String codigoProducto;
	/**
	 * Código de subproducto
	 */
	private String codigoSubProducto;
	/**
	 * Código de moneda del contrato
	 */
	private String codigoMonedaContrato;
	/**
	 * Código de empresa del contrato de Inversión	
	 */
	private String codigoEmpresaContratoInversion;
	/**
	 * Alias del contrato	
	 */
	private String aliasContrato;
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idsProd
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idsProd to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the codMonSw
	 */
	public String getCodigoMonedaContrato() {
		return codigoMonedaContrato;
	}
	/**
	 * @param codigoMonedaContrato the codMonSw to set
	 */
	public void setCodigoMonedaContrato(String codigoMonedaContrato) {
		this.codigoMonedaContrato = codigoMonedaContrato;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getCodigoEmpresaContratoInversion() {
		return codigoEmpresaContratoInversion;
	}
	/**
	 * @param codigoEmpresaContratoInversion the idEmprCo to set
	 */
	public void setCodigoEmpresaContratoInversion(String codigoEmpresaContratoInversion) {
		this.codigoEmpresaContratoInversion = codigoEmpresaContratoInversion;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param aliasContrato the desAlias to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}

	
}
