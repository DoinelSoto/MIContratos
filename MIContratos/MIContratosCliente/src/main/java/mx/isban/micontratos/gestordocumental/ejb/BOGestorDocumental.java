package mx.isban.micontratos.gestordocumental.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanConsultaTablaAuxiliarGestorResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadRequest;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadResultado;
import mx.isban.micontratos.beans.gestordocumental.BeanModificacionEstadosDocumentosRequest;
/**
 * @author Daniel Hernández Soto
 *
 */

public interface BOGestorDocumental {
	

	/**
	 * Método que hará el llamado al método que consumirá la transacción MI17 : consulta de tabla auxiliar de gestor de documentos
	 * @param beanConsultaTablaAuxiliarGestorRequest
	 * @param session
	 * @return BeanConsultaTablaAuxiliarGestorResultado
	 */
	BeanConsultaTablaAuxiliarGestorResultado listarDocumentosContrato(BeanConsultaTablaAuxiliarGestorRequest beanConsultaTablaAuxiliarGestorRequest,ArchitechSessionBean session) throws BusinessException;

	
	/**
	 * Método que hará el llamado al método que consumirá la transacción MI38 : consulta de indicador de obligatoriedad
	 * @param beanIndicadorObligatoriedadRequest
	 * @param session
	 * @return BeanIndicadorObligatoriedadResultado
	 */
	BeanIndicadorObligatoriedadResultado consultarVencimientoContrato(BeanIndicadorObligatoriedadRequest beanIndicadorObligatoriedadRequest,ArchitechSessionBean session) throws BusinessException;


	/**
	 * Método que hará el llamado al método que consumirá la transacción MI20 : Modificacion de estado de documentos 
	 * @param beanModificacionEstadosDocumentosRequest
	 * @param session
	 * @return ResponseGenerico
	 * @throws BusinessException 
	 */
	ResponseGenerico modificarEstatusDocumento( BeanModificacionEstadosDocumentosRequest beanModificacionEstadosDocumentosRequest,ArchitechSessionBean session) throws BusinessException;

	
}
