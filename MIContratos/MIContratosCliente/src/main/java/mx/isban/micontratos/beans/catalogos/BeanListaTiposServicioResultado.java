package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author everis
 *
 */

/**
 *  Bean con los datos de salida e implementación de interfaz BeanResultBO para la transaccion :   MI06	Tipos de servicio
 * 
 * */
public class BeanListaTiposServicioResultado extends ResponseGenerico implements Serializable{
	

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -5802641147356806656L;
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanListaTiposServicioResponse> listaTiposServicio;


	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Class BeanListaTiposServicioResultado
	 */
	public BeanListaTiposServicioResultado(){
		rellamada = new BeanPaginacion();
	}
	
	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}

	/**
	 * @return the consulta
	 */
	public List<BeanListaTiposServicioResponse> getListaTiposServicio() {
		return new ArrayList<>(listaTiposServicio);
	}

	/**
	 * @param consulta the consulta to set
	 */
	public void setListaTiposServicio(List<BeanListaTiposServicioResponse> listaTiposServicio) {
		this.listaTiposServicio = new ArrayList<>(listaTiposServicio);
	}
	
}
