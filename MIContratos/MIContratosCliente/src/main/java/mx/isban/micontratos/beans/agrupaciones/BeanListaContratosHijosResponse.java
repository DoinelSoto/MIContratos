package mx.isban.micontratos.beans.agrupaciones;

import java.io.Serializable;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida para la transacción MI34 :  Lista Contratos Hijos
 */
public class BeanListaContratosHijosResponse  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7252081146615687507L;
	

	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias	
	 */
	private String tipoConsulta;
	/**
	 * Empresa del Contrato	
	 */
	private String identificadorEmpresa;
	/**
	 * Centro del Contrato	
	 */
	private String identificadorCentroContrato;
	/**
	 * Número de contrato	
	 */
	private String identificadorContrato;
	/**
	 * Código de producto del contrato
	 */
	private String codigoProductoContrato;
	/**
	 * Código de subproducto del contrato
	 */
	private String codigoSubproductoContrato;
	/**
	 * Código de empresa del Contrato de Inversión	
	 */
	private String identificadorEmpresaContrato;
	/**
	 * Alias del contrato
	 */
	private String descripcionAlias;
	
	/**
	 * Class BeanListaContratosHijosResponse
	 */
	public String getTipConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipoConsulta the tipConsu to set
	 */
	public void getTipConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	/**
	 * @return the idEmpr
	 */
	public String getIdentificadorEmpresa() {
		return identificadorEmpresa;
	}
	/**
	 * @param identificadorEmpresa the idEmpr to set
	 */
	public void setIdentificadorEmpresa(String identificadorEmpresa) {
		this.identificadorEmpresa = identificadorEmpresa;
	}
	/**
	 * @return the idCent
	 */
	public String getIdentificadorCentroContrato() {
		return identificadorCentroContrato;
	}
	/**
	 * @param identificadorCentroContrato the idCent to set
	 */
	public void setIdentificadorCentroContrato(String identificadorCentroContrato) {
		this.identificadorCentroContrato = identificadorCentroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getIdentificadorContrato() {
		return identificadorContrato;
	}
	/**
	 * @param identificadorContrato the idContr to set
	 */
	public void setIdentificadorContrato(String identificadorContrato) {
		this.identificadorContrato = identificadorContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProductoContrato() {
		return codigoProductoContrato;
	}
	/**
	 * @param codigoProductoContrato the idProd to set
	 */
	public void setCodigoProductoContrato(String codigoProductoContrato) {
		this.codigoProductoContrato = codigoProductoContrato;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubproductoContrato() {
		return codigoSubproductoContrato;
	}
	/**
	 * @param codigoSubproductoContrato the idStiPro to set
	 */
	public void setCodigoSubproductoContrato(String codigoSubproductoContrato) {
		this.codigoSubproductoContrato = codigoSubproductoContrato;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getIdentificadorEmpresaContrato() {
		return identificadorEmpresaContrato;
	}
	/**
	 * @param identificadorEmpresaContrato the idEmprCo to set
	 */
	public void setIdentificadorEmpresaContrato(String identificadorEmpresaContrato) {
		this.identificadorEmpresaContrato = identificadorEmpresaContrato;
	}
	/**
	 * @return the desAlias
	 */
	public String getDescripcionAlias() {
		return descripcionAlias;
	}
	/**
	 * @param descripcionAlias the desAlias to set
	 */
	public void setDescripcionAlias(String descripcionAlias) {
		this.descripcionAlias = descripcionAlias;
	}

}
