package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

/**
 * @author everis
 *
 */

/** Bean con los datos de respuesta para la transaccion :
 * 
	MI05	Catálogo de sub-Estatus o detalle de Estatus

 **/
public class BeanCatalogoSubEstatusResponse  implements Serializable{
	

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -1735606032294734658L;

	
	/**
	 * Código de estatus
	 */
	private String codigoEstatus;
	/**
	 * Código de subestatus
	 */
	private String codigoSubEstatus;
	/**
	 * Descripción subestatus
	 */
	private String descripcionSubEstatus;
	/**
	 * @return the codEstat
	 */
	public String getCodigoEstatus() {
		return codigoEstatus;
	}
	/**
	 * @param codigoEstatus the codEstat to set
	 */
	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}

	/**
	 * @return the codSuBes
	 */
	public String getCodigoSubEstatus() {
		return codigoSubEstatus;
	}
	/**
	 * @param codigoSubEstatus the codSuBes to set
	 */
	public void setCodigoSubEstatus(String codigoSubEstatus) {
		this.codigoSubEstatus = codigoSubEstatus;
	}
	/**
	 * @return the desSubes
	 */
	public String getDescripcionSubEstatus() {
		return descripcionSubEstatus;
	}
	/**
	 * @param descripcionSubEstatus the desSubes to set
	 */
	public void setDescripcionSubEstatus(String descripcionSubEstatus) {
		this.descripcionSubEstatus = descripcionSubEstatus;
	}


}
