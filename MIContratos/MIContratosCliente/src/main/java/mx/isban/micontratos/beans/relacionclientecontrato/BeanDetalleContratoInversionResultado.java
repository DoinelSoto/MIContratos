package mx.isban.micontratos.beans.relacionclientecontrato;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * 
 * Bean con los datos de salida del servicio : Detalle de contrato de inversión
 *
 */
public class BeanDetalleContratoInversionResultado extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 2206608675168231413L;
	
	/**
	 * Código de empresa
	 */
	private String codEmpresa;
	
	/**
	 * Centro del contrato
	 */
	private String centroContrato;
	
	/**
	 * Número del contrato
	 */
	private String numeroContrato;
	
	/**
	 * Código del producto del contrato
	 */
	private String codProductoContrato;
	
	/**
	 * Código de subproducto de contrato
	 */
	private String codSubProductoCto;
	
	/**
	 * Código de moneda del producto
	 */
	private String codMonedaCto;
	
	/**
	 * Folio proveniente del contrato
	 */
	private int folioProvieneCto;
	
	/**
	 * Código de empresa del contrato de inversión
	 */
	private String codEmpContratoInversion;
	
	/**
	 * Código de banca del contrato
	 */
	private String codBancaContrato;
	
	
	/**
	 * Fecha de apertura del contrato
	 */
	private String fechaAperturaContrato;

	/**
	 * Alias del contrato
	 */
	private String aliasContrato;
	
	/**
	 * Fecha de vencimiento del contrato
	 */
	private String fechaVencimientoCto;
	
	/**
	 *Código ejecutivo del contrato 
	 */
	private String codEjecutivoCto;
	
	/**
	 * Centro de costos del contrato
	 */
	private String centroCostosCto;
	
	/**
	 * Indicador del tipo de cuenta
	 */
	private String indTipoCuenta;
	
	/**
	 * Indicador de tipo de contrato
	 */
	private String intTipoContrato;
	
	/**
	 * Indicador autorización posición corto capitales
	 */
	private String indAutPosicionCortoCapitales;
	
	/**
	 * Indicador autorización posisción corto fondos
	 */
	private String indAutPosicionCortoFondos;
	
	/**
	 * Indicador autorización posición fondos inversión
	 */
	private String indAutPosicionFondosInvers;
	
	/**
	 * Indicador discrecionalidad
	 */
	private String indDiscrecionalidad;
	
	/**
	 * Indicador tipo custodia directo
	 */
	private String indTipoCustodiaDirecto;
	
	/**
	 * Indicador tipo custodia repos
	 */
	private String indTipoCustodiaRepos;
	
	/**
	 * Indicador estado cuenta
	 */
	private String indEstadoCuenta;
	
	/**
	 * Monto máximo a operar en el contrato
	 */
	private String montoMaxOperContrato;
	
	/**
	 * Divisa del monto máximo a operar en el contrato
	 */
	private String divisaMontoMaxContrato;
	
	/**
	 * Indicador envío corre
	 */
	private String indEnvioCorreo;
	
	/**
	 * Estatus del contrato
	 */
	private String estatusContrato;
	
	/**
	 * Detalle de estatus
	 */
	private String detalleEstatus;
	
	/**
	 * Fecha de estado del contrato 
	 */
	private String fechaEstadoContrato;
	
	/**
	 * Indicador contrato agrupado
	 */
	private String indContratoAgrupado;
	
	/**
	 * Indicador tipo posición
	 */
	private String indTipoPosicion;
	
	/**
	 * Indicador de contrato espejo
	 */
	private String indContratoEspejo;
	
	/**
	 * Indicador de contrato amparado chequera
	 */
	private String indCtoAmparadoChequera;
	
	/**
	 * Código de tipo de servicio
	 */
	private String codigoTipoServicio;
	
	/**
	 * Código de asesor financiero
	 */
	private String codigoAfi;
	
	/**
	 * Indicador beneficiarios
	 */
	private String indBeneficiaros;
	
	/**
	 * Indicador proveedores recursos
	 */
	private String indProveedoresRecursos;
	
	/**
	 * Indicador accionistas
	 */
	private String indAccionistas;
	
	/**
	 * Fecha de ultima operación
	 */
	private String fechaUltimaOperacion;
	
	/**
	 * Fecha de la ultima modificación
	 */
	private String fechaUltimaModificacion;
	
	/**
	 * Descripción del contrato
	 */
	private String descripcionContrato;
	
	/**
	 * Descripción firma
	 */
	private String descripcionFirma;
	
	/**
	 * Descripción area referencia
	 */
	private String descripcionAreaReferencia;
	
	/**
	 * Descripción referencia externa
	 */
	private String descripcionRefExterna;
	
	/**
	 * Descripción de subestatus
	 */
	private String descripcionSubestatus;
	
	/**
	 * Empresa contrato agrupador
	 */
	private String empresaContratoAgrupador;
	
	/**
	 * Centro del contrato agrupador
	 */
	private String centroContratoAgrupador;
	
	/**
	 * Número del contrato agrupador
	 */
	private String numeroConAgrupador;
	
	/**
	 * Código de producto agrupador
	 */
	private String codProdAgrupador;
	
	/**
	 * Código de subproduco agrupador
	 */
	private String codSubprdAgrupador;
	
	/**
	 * Empresa del contrato agrupador
	 */
	private String empresaContratoAgr;
	
	/**
	 * Alias  del contrato agrupador
	 */
	private String aliasContratoAgrup;
	
	/**
	 * Empresa del contrato espejo
	 */
	private String empresaContratoEspejo;
	
	/**
	 * Centro del contrato espejo
	 */
	private String centroContratoEspejo;
	
	
	/**
	 * Número del contrato espejo
	 */
	private String numeroContratoEspejo;
	
	/**
	 * Código de producto espejo
	 */
	private String codigoProdEspejo;
	
	/**
	 * Código de subproducto espejo
	 */
	private String codigoSubProdEspejo;
	
	/**
	 * Empresa del contrato esp
	 */
	private String empresaContratoEsp;
	
	/**
	 * Alias del contrato espejo
	 */
	private String aliasContratoEspejo;
	
	/**
	 * Empresa del contrato chequera
	 */
	private String empresaContratoChequera;
	
	/**
	 * Centro del contrato chequera
	 */
	private String centroContratoChequera;
	
	/**
	 * Número del contrato chequera
	 */
	private String numeroContratoChequera;
	
	/**
	 * Código de producto chequera
	 */
	private String codigoProductoChequera;
	
	/**
	 * Código de subproducto chequera
	 */
	private String codigoSubProductoChequera;
	
	/**
	 * Número de intervinientes
	 */
	private String numeroIntervinientes;
	
	/***
	 * Lista de intervinientes
	 */
	private List<BeanDetalleContratoInversionInterviniente> lstIntervinientes;
	
	/**
	 * Número de contactos
	 */
	private String numeroContactos;
	
	/**
	 * Lista de contactos
	 */
	private List<BeanDetalleContratoInversionContacto> listContactos;
	
	/**
	 * Número de usos domicilios
	 */
	private String numeroUsosDomiclio;
	
	/**
	 * Lista de usos domicilios
	 */
	private List<BeanDetalleContratoInversionUsosDomicilio> lstUsosDomiclio;

	/**
	 * @return the codEmpresa
	 */
	public String getCodEmpresa() {
		return codEmpresa;
	}

	/**
	 * @param codEmpresa the codEmpresa to set
	 */
	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	/**
	 * @return the centroContrato
	 */
	public String getCentroContrato() {
		return centroContrato;
	}

	/**
	 * @param centroContrato the centroContrato to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}

	/**
	 * @return the numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * @param numeroContrato the numeroContrato to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * @return the codProductoContrato
	 */
	public String getCodProductoContrato() {
		return codProductoContrato;
	}

	/**
	 * @param codProductoContrato the codProductoContrato to set
	 */
	public void setCodProductoContrato(String codProductoContrato) {
		this.codProductoContrato = codProductoContrato;
	}

	/**
	 * @return the codSubProductoCto
	 */
	public String getCodSubProductoCto() {
		return codSubProductoCto;
	}

	/**
	 * @param codSubProductoCto the codSubProductoCto to set
	 */
	public void setCodSubProductoCto(String codSubProductoCto) {
		this.codSubProductoCto = codSubProductoCto;
	}

	/**
	 * @return the codMonedaCto
	 */
	public String getCodMonedaCto() {
		return codMonedaCto;
	}

	/**
	 * @param codMonedaCto the codMonedaCto to set
	 */
	public void setCodMonedaCto(String codMonedaCto) {
		this.codMonedaCto = codMonedaCto;
	}

	/**
	 * @return the folioProvieneCto
	 */
	public int getFolioProvieneCto() {
		return folioProvieneCto;
	}

	/**
	 * @param folioProvieneCto the folioProvieneCto to set
	 */
	public void setFolioProvieneCto(int folioProvieneCto) {
		this.folioProvieneCto = folioProvieneCto;
	}

	/**
	 * @return the codEmpContratoInversion
	 */
	public String getCodEmpContratoInversion() {
		return codEmpContratoInversion;
	}

	/**
	 * @param codEmpContratoInversion the codEmpContratoInversion to set
	 */
	public void setCodEmpContratoInversion(String codEmpContratoInversion) {
		this.codEmpContratoInversion = codEmpContratoInversion;
	}

	/**
	 * @return the codBancaContrato
	 */
	public String getCodBancaContrato() {
		return codBancaContrato;
	}

	/**
	 * @param codBancaContrato the codBancaContrato to set
	 */
	public void setCodBancaContrato(String codBancaContrato) {
		this.codBancaContrato = codBancaContrato;
	}

	/**
	 * @return the fechaAperturaContrato
	 */
	public String getFechaAperturaContrato() {
		return fechaAperturaContrato;
	}

	/**
	 * @param fechaAperturaContrato the fechaAperturaContrato to set
	 */
	public void setFechaAperturaContrato(String fechaAperturaContrato) {
		this.fechaAperturaContrato = fechaAperturaContrato;
	}

	/**
	 * @return the aliasContrato
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}

	/**
	 * @param aliasContrato the aliasContrato to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}

	/**
	 * @return the fechaVencimientoCto
	 */
	public String getFechaVencimientoCto() {
		return fechaVencimientoCto;
	}

	/**
	 * @param fechaVencimientoCto the fechaVencimientoCto to set
	 */
	public void setFechaVencimientoCto(String fechaVencimientoCto) {
		this.fechaVencimientoCto = fechaVencimientoCto;
	}

	/**
	 * @return the codEjecutivoCto
	 */
	public String getCodEjecutivoCto() {
		return codEjecutivoCto;
	}

	/**
	 * @param codEjecutivoCto the codEjecutivoCto to set
	 */
	public void setCodEjecutivoCto(String codEjecutivoCto) {
		this.codEjecutivoCto = codEjecutivoCto;
	}

	/**
	 * @return the centroCostosCto
	 */
	public String getCentroCostosCto() {
		return centroCostosCto;
	}

	/**
	 * @param centroCostosCto the centroCostosCto to set
	 */
	public void setCentroCostosCto(String centroCostosCto) {
		this.centroCostosCto = centroCostosCto;
	}

	/**
	 * @return the indTipoCuenta
	 */
	public String getIndTipoCuenta() {
		return indTipoCuenta;
	}

	/**
	 * @param indTipoCuenta the indTipoCuenta to set
	 */
	public void setIndTipoCuenta(String indTipoCuenta) {
		this.indTipoCuenta = indTipoCuenta;
	}

	/**
	 * @return the intTipoContrato
	 */
	public String getIntTipoContrato() {
		return intTipoContrato;
	}

	/**
	 * @param intTipoContrato the intTipoContrato to set
	 */
	public void setIntTipoContrato(String intTipoContrato) {
		this.intTipoContrato = intTipoContrato;
	}

	/**
	 * @return the indAutPosicionCortoCapitales
	 */
	public String getIndAutPosicionCortoCapitales() {
		return indAutPosicionCortoCapitales;
	}

	/**
	 * @param indAutPosicionCortoCapitales the indAutPosicionCortoCapitales to set
	 */
	public void setIndAutPosicionCortoCapitales(String indAutPosicionCortoCapitales) {
		this.indAutPosicionCortoCapitales = indAutPosicionCortoCapitales;
	}

	/**
	 * @return the indAutPosicionCortoFondos
	 */
	public String getIndAutPosicionCortoFondos() {
		return indAutPosicionCortoFondos;
	}

	/**
	 * @param indAutPosicionCortoFondos the indAutPosicionCortoFondos to set
	 */
	public void setIndAutPosicionCortoFondos(String indAutPosicionCortoFondos) {
		this.indAutPosicionCortoFondos = indAutPosicionCortoFondos;
	}

	/**
	 * @return the indAutPosicionFondosInvers
	 */
	public String getIndAutPosicionFondosInvers() {
		return indAutPosicionFondosInvers;
	}

	/**
	 * @param indAutPosicionFondosInvers the indAutPosicionFondosInvers to set
	 */
	public void setIndAutPosicionFondosInvers(String indAutPosicionFondosInvers) {
		this.indAutPosicionFondosInvers = indAutPosicionFondosInvers;
	}

	/**
	 * @return the indDiscrecionalidad
	 */
	public String getIndDiscrecionalidad() {
		return indDiscrecionalidad;
	}

	/**
	 * @param indDiscrecionalidad the indDiscrecionalidad to set
	 */
	public void setIndDiscrecionalidad(String indDiscrecionalidad) {
		this.indDiscrecionalidad = indDiscrecionalidad;
	}

	/**
	 * @return the indTipoCustodiaDirecto
	 */
	public String getIndTipoCustodiaDirecto() {
		return indTipoCustodiaDirecto;
	}

	/**
	 * @param indTipoCustodiaDirecto the indTipoCustodiaDirecto to set
	 */
	public void setIndTipoCustodiaDirecto(String indTipoCustodiaDirecto) {
		this.indTipoCustodiaDirecto = indTipoCustodiaDirecto;
	}

	/**
	 * @return the indTipoCustodiaRepos
	 */
	public String getIndTipoCustodiaRepos() {
		return indTipoCustodiaRepos;
	}

	/**
	 * @param indTipoCustodiaRepos the indTipoCustodiaRepos to set
	 */
	public void setIndTipoCustodiaRepos(String indTipoCustodiaRepos) {
		this.indTipoCustodiaRepos = indTipoCustodiaRepos;
	}

	/**
	 * @return the indEstadoCuenta
	 */
	public String getIndEstadoCuenta() {
		return indEstadoCuenta;
	}

	/**
	 * @param indEstadoCuenta the indEstadoCuenta to set
	 */
	public void setIndEstadoCuenta(String indEstadoCuenta) {
		this.indEstadoCuenta = indEstadoCuenta;
	}

	/**
	 * @return the montoMaxOperContrato
	 */
	public String getMontoMaxOperContrato() {
		return montoMaxOperContrato;
	}

	/**
	 * @param montoMaxOperContrato the montoMaxOperContrato to set
	 */
	public void setMontoMaxOperContrato(String montoMaxOperContrato) {
		this.montoMaxOperContrato = montoMaxOperContrato;
	}

	/**
	 * @return the divisaMontoMaxContrato
	 */
	public String getDivisaMontoMaxContrato() {
		return divisaMontoMaxContrato;
	}

	/**
	 * @param divisaMontoMaxContrato the divisaMontoMaxContrato to set
	 */
	public void setDivisaMontoMaxContrato(String divisaMontoMaxContrato) {
		this.divisaMontoMaxContrato = divisaMontoMaxContrato;
	}

	/**
	 * @return the indEnvioCorreo
	 */
	public String getIndEnvioCorreo() {
		return indEnvioCorreo;
	}

	/**
	 * @param indEnvioCorreo the indEnvioCorreo to set
	 */
	public void setIndEnvioCorreo(String indEnvioCorreo) {
		this.indEnvioCorreo = indEnvioCorreo;
	}

	/**
	 * @return the estatusContrato
	 */
	public String getEstatusContrato() {
		return estatusContrato;
	}

	/**
	 * @param estatusContrato the estatusContrato to set
	 */
	public void setEstatusContrato(String estatusContrato) {
		this.estatusContrato = estatusContrato;
	}

	/**
	 * @return the detalleEstatus
	 */
	public String getDetalleEstatus() {
		return detalleEstatus;
	}

	/**
	 * @param detalleEstatus the detalleEstatus to set
	 */
	public void setDetalleEstatus(String detalleEstatus) {
		this.detalleEstatus = detalleEstatus;
	}

	/**
	 * @return the fechaEstadoContrato
	 */
	public String getFechaEstadoContrato() {
		return fechaEstadoContrato;
	}

	/**
	 * @param fechaEstadoContrato the fechaEstadoContrato to set
	 */
	public void setFechaEstadoContrato(String fechaEstadoContrato) {
		this.fechaEstadoContrato = fechaEstadoContrato;
	}

	/**
	 * @return the indContratoAgrupado
	 */
	public String getIndContratoAgrupado() {
		return indContratoAgrupado;
	}

	/**
	 * @param indContratoAgrupado the indContratoAgrupado to set
	 */
	public void setIndContratoAgrupado(String indContratoAgrupado) {
		this.indContratoAgrupado = indContratoAgrupado;
	}

	/**
	 * @return the indTipoPosicion
	 */
	public String getIndTipoPosicion() {
		return indTipoPosicion;
	}

	/**
	 * @param indTipoPosicion the indTipoPosicion to set
	 */
	public void setIndTipoPosicion(String indTipoPosicion) {
		this.indTipoPosicion = indTipoPosicion;
	}

	/**
	 * @return the indContratoEspejo
	 */
	public String getIndContratoEspejo() {
		return indContratoEspejo;
	}

	/**
	 * @param indContratoEspejo the indContratoEspejo to set
	 */
	public void setIndContratoEspejo(String indContratoEspejo) {
		this.indContratoEspejo = indContratoEspejo;
	}

	/**
	 * @return the indCtoAmparadoChequera
	 */
	public String getIndCtoAmparadoChequera() {
		return indCtoAmparadoChequera;
	}

	/**
	 * @param indCtoAmparadoChequera the indCtoAmparadoChequera to set
	 */
	public void setIndCtoAmparadoChequera(String indCtoAmparadoChequera) {
		this.indCtoAmparadoChequera = indCtoAmparadoChequera;
	}

	/**
	 * @return the codigoTipoServicio
	 */
	public String getCodigoTipoServicio() {
		return codigoTipoServicio;
	}

	/**
	 * @param codigoTipoServicio the codigoTipoServicio to set
	 */
	public void setCodigoTipoServicio(String codigoTipoServicio) {
		this.codigoTipoServicio = codigoTipoServicio;
	}

	/**
	 * @return the codigoAfi
	 */
	public String getCodigoAfi() {
		return codigoAfi;
	}

	/**
	 * @param codigoAfi the codigoAfi to set
	 */
	public void setCodigoAfi(String codigoAfi) {
		this.codigoAfi = codigoAfi;
	}

	/**
	 * @return the indBeneficiaros
	 */
	public String getIndBeneficiaros() {
		return indBeneficiaros;
	}

	/**
	 * @param indBeneficiaros the indBeneficiaros to set
	 */
	public void setIndBeneficiaros(String indBeneficiaros) {
		this.indBeneficiaros = indBeneficiaros;
	}

	/**
	 * @return the indProveedoresRecursos
	 */
	public String getIndProveedoresRecursos() {
		return indProveedoresRecursos;
	}

	/**
	 * @param indProveedoresRecursos the indProveedoresRecursos to set
	 */
	public void setIndProveedoresRecursos(String indProveedoresRecursos) {
		this.indProveedoresRecursos = indProveedoresRecursos;
	}

	/**
	 * @return the indAccionistas
	 */
	public String getIndAccionistas() {
		return indAccionistas;
	}

	/**
	 * @param indAccionistas the indAccionistas to set
	 */
	public void setIndAccionistas(String indAccionistas) {
		this.indAccionistas = indAccionistas;
	}

	/**
	 * @return the fechaUltimaOperacion
	 */
	public String getFechaUltimaOperacion() {
		return fechaUltimaOperacion;
	}

	/**
	 * @param fechaUltimaOperacion the fechaUltimaOperacion to set
	 */
	public void setFechaUltimaOperacion(String fechaUltimaOperacion) {
		this.fechaUltimaOperacion = fechaUltimaOperacion;
	}

	/**
	 * @return the fechaUltimaModificacion
	 */
	public String getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	/**
	 * @param fechaUltimaModificacion the fechaUltimaModificacion to set
	 */
	public void setFechaUltimaModificacion(String fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	/**
	 * @return the descripcionContrato
	 */
	public String getDescripcionContrato() {
		return descripcionContrato;
	}

	/**
	 * @param descripcionContrato the descripcionContrato to set
	 */
	public void setDescripcionContrato(String descripcionContrato) {
		this.descripcionContrato = descripcionContrato;
	}

	/**
	 * @return the descripcionFirma
	 */
	public String getDescripcionFirma() {
		return descripcionFirma;
	}

	/**
	 * @param descripcionFirma the descripcionFirma to set
	 */
	public void setDescripcionFirma(String descripcionFirma) {
		this.descripcionFirma = descripcionFirma;
	}

	/**
	 * @return the descripcionAreaReferencia
	 */
	public String getDescripcionAreaReferencia() {
		return descripcionAreaReferencia;
	}

	/**
	 * @param descripcionAreaReferencia the descripcionAreaReferencia to set
	 */
	public void setDescripcionAreaReferencia(String descripcionAreaReferencia) {
		this.descripcionAreaReferencia = descripcionAreaReferencia;
	}

	/**
	 * @return the descripcionRefExterna
	 */
	public String getDescripcionRefExterna() {
		return descripcionRefExterna;
	}

	/**
	 * @param descripcionRefExterna the descripcionRefExterna to set
	 */
	public void setDescripcionRefExterna(String descripcionRefExterna) {
		this.descripcionRefExterna = descripcionRefExterna;
	}

	/**
	 * @return the descripcionSubestatus
	 */
	public String getDescripcionSubestatus() {
		return descripcionSubestatus;
	}

	/**
	 * @param descripcionSubestatus the descripcionSubestatus to set
	 */
	public void setDescripcionSubestatus(String descripcionSubestatus) {
		this.descripcionSubestatus = descripcionSubestatus;
	}

	/**
	 * @return the empresaContratoAgrupador
	 */
	public String getEmpresaContratoAgrupador() {
		return empresaContratoAgrupador;
	}

	/**
	 * @param empresaContratoAgrupador the empresaContratoAgrupador to set
	 */
	public void setEmpresaContratoAgrupador(String empresaContratoAgrupador) {
		this.empresaContratoAgrupador = empresaContratoAgrupador;
	}

	/**
	 * @return the centroContratoAgrupador
	 */
	public String getCentroContratoAgrupador() {
		return centroContratoAgrupador;
	}

	/**
	 * @param centroContratoAgrupador the centroContratoAgrupador to set
	 */
	public void setCentroContratoAgrupador(String centroContratoAgrupador) {
		this.centroContratoAgrupador = centroContratoAgrupador;
	}

	/**
	 * @return the numeroConAgrupador
	 */
	public String getNumeroConAgrupador() {
		return numeroConAgrupador;
	}

	/**
	 * @param numeroConAgrupador the numeroConAgrupador to set
	 */
	public void setNumeroConAgrupador(String numeroConAgrupador) {
		this.numeroConAgrupador = numeroConAgrupador;
	}

	/**
	 * @return the codProdAgrupador
	 */
	public String getCodProdAgrupador() {
		return codProdAgrupador;
	}

	/**
	 * @param codProdAgrupador the codProdAgrupador to set
	 */
	public void setCodProdAgrupador(String codProdAgrupador) {
		this.codProdAgrupador = codProdAgrupador;
	}

	/**
	 * @return the codSubprdAgrupador
	 */
	public String getCodSubprdAgrupador() {
		return codSubprdAgrupador;
	}

	/**
	 * @param codSubprdAgrupador the codSubprdAgrupador to set
	 */
	public void setCodSubprdAgrupador(String codSubprdAgrupador) {
		this.codSubprdAgrupador = codSubprdAgrupador;
	}

	/**
	 * @return the empresaContratoAgr
	 */
	public String getEmpresaContratoAgr() {
		return empresaContratoAgr;
	}

	/**
	 * @param empresaContratoAgr the empresaContratoAgr to set
	 */
	public void setEmpresaContratoAgr(String empresaContratoAgr) {
		this.empresaContratoAgr = empresaContratoAgr;
	}

	/**
	 * @return the aliasContratoAgrup
	 */
	public String getAliasContratoAgrup() {
		return aliasContratoAgrup;
	}

	/**
	 * @param aliasContratoAgrup the aliasContratoAgrup to set
	 */
	public void setAliasContratoAgrup(String aliasContratoAgrup) {
		this.aliasContratoAgrup = aliasContratoAgrup;
	}

	/**
	 * @return the empresaContratoEspejo
	 */
	public String getEmpresaContratoEspejo() {
		return empresaContratoEspejo;
	}

	/**
	 * @param empresaContratoEspejo the empresaContratoEspejo to set
	 */
	public void setEmpresaContratoEspejo(String empresaContratoEspejo) {
		this.empresaContratoEspejo = empresaContratoEspejo;
	}

	/**
	 * @return the centroContratoEspejo
	 */
	public String getCentroContratoEspejo() {
		return centroContratoEspejo;
	}

	/**
	 * @param centroContratoEspejo the centroContratoEspejo to set
	 */
	public void setCentroContratoEspejo(String centroContratoEspejo) {
		this.centroContratoEspejo = centroContratoEspejo;
	}

	/**
	 * @return the numeroContratoEspejo
	 */
	public String getNumeroContratoEspejo() {
		return numeroContratoEspejo;
	}

	/**
	 * @param numeroContratoEspejo the numeroContratoEspejo to set
	 */
	public void setNumeroContratoEspejo(String numeroContratoEspejo) {
		this.numeroContratoEspejo = numeroContratoEspejo;
	}

	/**
	 * @return the codigoProdEspejo
	 */
	public String getCodigoProdEspejo() {
		return codigoProdEspejo;
	}

	/**
	 * @param codigoProdEspejo the codigoProdEspejo to set
	 */
	public void setCodigoProdEspejo(String codigoProdEspejo) {
		this.codigoProdEspejo = codigoProdEspejo;
	}

	/**
	 * @return the codigoSubProdEspejo
	 */
	public String getCodigoSubProdEspejo() {
		return codigoSubProdEspejo;
	}

	/**
	 * @param codigoSubProdEspejo the codigoSubProdEspejo to set
	 */
	public void setCodigoSubProdEspejo(String codigoSubProdEspejo) {
		this.codigoSubProdEspejo = codigoSubProdEspejo;
	}

	/**
	 * @return the empresaContratoEsp
	 */
	public String getEmpresaContratoEsp() {
		return empresaContratoEsp;
	}

	/**
	 * @param empresaContratoEsp the empresaContratoEsp to set
	 */
	public void setEmpresaContratoEsp(String empresaContratoEsp) {
		this.empresaContratoEsp = empresaContratoEsp;
	}

	/**
	 * @return the aliasContratoEspejo
	 */
	public String getAliasContratoEspejo() {
		return aliasContratoEspejo;
	}

	/**
	 * @param aliasContratoEspejo the aliasContratoEspejo to set
	 */
	public void setAliasContratoEspejo(String aliasContratoEspejo) {
		this.aliasContratoEspejo = aliasContratoEspejo;
	}

	/**
	 * @return the empresaContratoChequera
	 */
	public String getEmpresaContratoChequera() {
		return empresaContratoChequera;
	}

	/**
	 * @param empresaContratoChequera the empresaContratoChequera to set
	 */
	public void setEmpresaContratoChequera(String empresaContratoChequera) {
		this.empresaContratoChequera = empresaContratoChequera;
	}

	/**
	 * @return the centroContratoChequera
	 */
	public String getCentroContratoChequera() {
		return centroContratoChequera;
	}

	/**
	 * @param centroContratoChequera the centroContratoChequera to set
	 */
	public void setCentroContratoChequera(String centroContratoChequera) {
		this.centroContratoChequera = centroContratoChequera;
	}

	/**
	 * @return the numeroContratoChequera
	 */
	public String getNumeroContratoChequera() {
		return numeroContratoChequera;
	}

	/**
	 * @param numeroContratoChequera the numeroContratoChequera to set
	 */
	public void setNumeroContratoChequera(String numeroContratoChequera) {
		this.numeroContratoChequera = numeroContratoChequera;
	}

	/**
	 * @return the codigoProductoChequera
	 */
	public String getCodigoProductoChequera() {
		return codigoProductoChequera;
	}

	/**
	 * @param codigoProductoChequera the codigoProductoChequera to set
	 */
	public void setCodigoProductoChequera(String codigoProductoChequera) {
		this.codigoProductoChequera = codigoProductoChequera;
	}

	/**
	 * @return the codigoSubProductoChequera
	 */
	public String getCodigoSubProductoChequera() {
		return codigoSubProductoChequera;
	}

	/**
	 * @param codigoSubProductoChequera the codigoSubProductoChequera to set
	 */
	public void setCodigoSubProductoChequera(String codigoSubProductoChequera) {
		this.codigoSubProductoChequera = codigoSubProductoChequera;
	}

	/**
	 * @return the numeroIntervinientes
	 */
	public String getNumeroIntervinientes() {
		return numeroIntervinientes;
	}

	/**
	 * @param numeroIntervinientes the numeroIntervinientes to set
	 */
	public void setNumeroIntervinientes(String numeroIntervinientes) {
		this.numeroIntervinientes = numeroIntervinientes;
	}

	/**
	 * @return the lstIntervinientes
	 */
	public List<BeanDetalleContratoInversionInterviniente> getLstIntervinientes() {
		return new ArrayList<>(lstIntervinientes);
	}

	/**
	 * @param lstIntervinientes the lstIntervinientes to set
	 */
	public void setLstIntervinientes(
			List<BeanDetalleContratoInversionInterviniente> lstIntervinientes) {
		this.lstIntervinientes = new ArrayList<>(lstIntervinientes);
	}

	/**
	 * @return the numeroContactos
	 */
	public String getNumeroContactos() {
		return numeroContactos;
	}

	/**
	 * @param numeroContactos the numeroContactos to set
	 */
	public void setNumeroContactos(String numeroContactos) {
		this.numeroContactos = numeroContactos;
	}

	/**
	 * @return the listContactos
	 */
	public List<BeanDetalleContratoInversionContacto> getListContactos() {
		return new ArrayList<>(listContactos);
	}

	/**
	 * @param listContactos the listContactos to set
	 */
	public void setListContactos(
			List<BeanDetalleContratoInversionContacto> listContactos) {
		this.listContactos = new ArrayList<>(listContactos);
	}

	/**
	 * @return the numeroUsosDomiclio
	 */
	public String getNumeroUsosDomiclio() {
		return numeroUsosDomiclio;
	}

	/**
	 * @param numeroUsosDomiclio the numeroUsosDomiclio to set
	 */
	public void setNumeroUsosDomiclio(String numeroUsosDomiclio) {
		this.numeroUsosDomiclio = numeroUsosDomiclio;
	}

	/**
	 * @return the lstUsosDomiclio
	 */
	public List<BeanDetalleContratoInversionUsosDomicilio> getLstUsosDomiclio() {
		return new ArrayList<>(lstUsosDomiclio);
	}

	/**
	 * @param lstUsosDomiclio the lstUsosDomiclio to set
	 */
	public void setLstUsosDomiclio(
			List<BeanDetalleContratoInversionUsosDomicilio> lstUsosDomiclio) {
		this.lstUsosDomiclio = new ArrayList<>(lstUsosDomiclio);
	}

}