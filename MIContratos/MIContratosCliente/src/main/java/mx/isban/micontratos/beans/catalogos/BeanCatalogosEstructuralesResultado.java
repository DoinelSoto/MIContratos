package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de salida e implementacion BeanResultBO para la transacción MI09 : Catálogos estructurales
 *
 */
public class BeanCatalogosEstructuralesResultado extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 8871406606834991070L;
	
	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;

	/**
	 * Lista de beans BeanArbolTiposServicioResponse
	 */
	private List<BeanCatalogosEstructuralesResponse> listaCatalogosEstructurales;
	/**
	 * Class BeanCatalogosEstructuralesResultado
	 */
	public BeanCatalogosEstructuralesResultado(){
		rellamada = new BeanPaginacion();
	}
	/**
	 * @return the consulta
	 */
	public List<BeanCatalogosEstructuralesResponse> getListaCatalogosEstructurales() {
		return new ArrayList<> (listaCatalogosEstructurales);
	}

	/**
	 * @param listaArbolTiposServicio the consulta to set
	 */
	public void setListaCatalogosEstructurales(List<BeanCatalogosEstructuralesResponse> listaCatalogosEstructurales) {
		this.listaCatalogosEstructurales = new ArrayList<> (listaCatalogosEstructurales);
	}

	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	
}
