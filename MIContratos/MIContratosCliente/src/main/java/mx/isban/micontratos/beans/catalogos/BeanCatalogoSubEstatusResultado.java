package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * @author Daniel Hernández Soto
 *
 */

/**  
 *	Bean con los datos de salida e implementación de la interfaz BeanResultBO para la transacción : MI05	Catálogo de sub-Estatus o detalle de Estatus
 **/
public class BeanCatalogoSubEstatusResultado extends ResponseGenerico implements Serializable{

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = -1735606032294734658L;

	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanCatalogoSubEstatusResponse> listaCatalogoSubEstatus;

	
	/**
	 * Class BeanCatalogoSubEstatusResultado
	 */
	public BeanCatalogoSubEstatusResultado(){
		rellamada = new BeanPaginacion();
	}
	
	/**
	 * @return the consulta
	 */
	public List<BeanCatalogoSubEstatusResponse> getListaCatalogoSubEstatus() {
		return new ArrayList<>(listaCatalogoSubEstatus);
	}

	/**
	 * @param listaCatalogoSubEstatus the consulta to set
	 */
	public void setListaCatalogoSubEstatus(List<BeanCatalogoSubEstatusResponse> listaCatalogoSubEstatus) {
		this.listaCatalogoSubEstatus = new ArrayList<>(listaCatalogoSubEstatus);
	}
	
	/**
	 * @return the rellamada
	 */
	public BeanPaginacion getRellamada() {
		return rellamada;
	}

	/**
	 * @param rellamada the rellamada to set
	 */
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	
}
