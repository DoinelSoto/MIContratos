package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * 
 * Clase principal para el conversor de cuentas
 *
 */
public class BeanConversorCuentasRequest implements Serializable{

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 2015435443697831440L;
	
	/**
	 * Bean para la paginación
	 */
	private MulticanalidadGenerica multicanalidad;
	
	/**
	 * ListaContratos
	 */
	private List<BeanConversorCuentasEntrada> contratos;

	
	/**
	 * Constructor para inicializar multicanalidad y array
	 */
	public BeanConversorCuentasRequest() {
		multicanalidad= new MulticanalidadGenerica();
		contratos= new ArrayList<>();
	}

	/**
	 * @return the contratos
	 */
	public List<BeanConversorCuentasEntrada> getContratos() {
		return new ArrayList<>(contratos);
	}

	/**
	 * @param contratos the contratos to set
	 */
	public void setContratos(List<BeanConversorCuentasEntrada> contratos) {
		this.contratos = new ArrayList<>(contratos);
	}

	/**
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}

	/**
	 * @param multicanalidad the multicanalidad to set
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
}