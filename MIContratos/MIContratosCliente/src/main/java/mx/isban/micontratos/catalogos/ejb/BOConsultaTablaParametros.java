package mx.isban.micontratos.catalogos.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanArbolTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoEstatusOperativoResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogoSubEstatusResultado;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesRequest;
import mx.isban.micontratos.beans.catalogos.BeanCatalogosEstructuralesResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultaEstadoDocumentosResultado;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchRequest;
import mx.isban.micontratos.beans.catalogos.BeanConsultarBranchResult;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioRequest;
import mx.isban.micontratos.beans.catalogos.BeanListaTiposServicioResultado;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIRequest;
import mx.isban.micontratos.beans.catalogos.BeanTipoNecesidadAFIResultado;
import mx.isban.micontratos.validator.MIContratosException;


/**
 * @author everis
 *
 */

public interface BOConsultaTablaParametros {
	

	/**
	 * Método que ejecutará el consumo de la transaccion MI04 : Consulta de catálogos de estatus operativo
	 * @param beanAltaCatalogosEstatusRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	BeanCatalogoEstatusOperativoResultado catalogoEstatusOperativos(BeanCatalogoEstatusOperativoRequest beanAltaCatalogosEstatusRequest,ArchitechSessionBean sesion) throws BusinessException;

	
	/**
	 * Método que ejecutará el consumo de la transaccion MI05 : Consulta de catálogos de Subestaus operativo
	 * @param beanCatalogosSubEstatusRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	BeanCatalogoSubEstatusResultado catalogoSubEstatus(BeanCatalogoSubEstatusRequest beanCatalogosSubEstatusRequest,ArchitechSessionBean sesion) throws BusinessException;

	
	/**
	 * Método que ejecutará el consumo de la transaccion MI06 : Consulta de la lista de tipo de servicios
	 * @param beanListaTiposServicioRequest
	 * @param sesion
	 * @return ResponseGenerico
	 * @throws MIContratosException 
	 */
	BeanListaTiposServicioResultado tipoServicio(BeanListaTiposServicioRequest beanListaTiposServicioRequest,ArchitechSessionBean sesion) throws BusinessException;

		
	/**
	 * Método que ejecutará el consumo de la transaccion MI07 : Consulta de arbol de tipo de servicios
	 * @param beanArbolTiposServicioRequest
	 * @param sesion
	 * @return ResponseGenerico
	 */
	BeanArbolTiposServicioResultado arbolTipoServicio(BeanArbolTiposServicioRequest beanArbolTiposServicioRequest,ArchitechSessionBean sesion) throws BusinessException;
	
	/**
	 * Método que permitirá exponer como webService a la transacción : Consulta Branch
	 * @param beanConsultarBranchRequest
	 * @param session
	 * @return ResponseGenerico 
	 */
	BeanConsultarBranchResult consultaBranch(BeanConsultarBranchRequest beanConsultarBranchRequest,ArchitechSessionBean session) throws BusinessException;
	
	/**
	 * Método que permitirá exponer como webService a las transacciónes (OD29, PEL1 y PEL2) y MI07 : Tipo Necesidad AFI
	 * @param beanTipoNecesidadAFIRequest
	 * @param session
	 * @return ResponseGenerico 
	 */
	BeanTipoNecesidadAFIResultado tipoNecesidadAFI(BeanTipoNecesidadAFIRequest beanTipoNecesidadAFIRequest,ArchitechSessionBean session) throws BusinessException;

	/**
	 * Método que ejecutará el consumo de la transaccion MI08 : Consulta de estados de documentos
	 * @param beanConsultaEstadoDocumentosRequest
	 * @param sesion
	 * @return 
	 */
	BeanConsultaEstadoDocumentosResultado ejecutaConsultaEstadoDocumentos(BeanConsultaEstadoDocumentosRequest beanConsultaEstadoDocumentosRequest,ArchitechSessionBean sesion) throws BusinessException;

	/**
	 * Método que ejecutará el consumo de la transaccion MI09 : Catálogos estructurales
	 * @param beanCatalogosEstructuralesRequest
	 * @param sesion
	 * @return 
	 */
	BeanCatalogosEstructuralesResultado ejecutaConsultaCatalogosEstructurales(BeanCatalogosEstructuralesRequest beanCatalogosEstructuralesRequest,ArchitechSessionBean sesion) throws BusinessException;

}
