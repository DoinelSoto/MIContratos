package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * 
 * Clase principal para el conversor de cuentas
 *
 */
public class BeanConversorCuentasEntrada implements Serializable{

	/**
	 * Serialización DTO
	 */
	private static final long serialVersionUID = 2015435443697831440L;
	
	/**
	 * Empresa de contrato
	 */
	@Size(max=4)
	private String empresaContrato;
	
	/**
	 * Centro de contrato
	 */
	@Size(max=4)
	private String centroContrato;
	
	/**
	 * Número de contrato
	 */
	@Size(max=12)
	private String numeroContrato;
	
	/**
	 * Código de producto
	 */
	@Size(max=2)
	private String codigoProducto;
	
	/**
	 * Código de subproducto
	 */
	@Size(max=4)
	private String codigoSubProducto;
	
	/**
	 * Código empresa contrato de inversión
	 */
	@Size(max=4)
	private String codigoEmpresaContratoInversion;
	
	/**
	 * Alias de contrato
	 */
	@Size(max=15)
	private String aliasContrato;
	
	/**
	 * @return the empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	
	/**
	 * @param empresaContrato the empresaContrato to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	
	/**
	 * @return the centroContrato
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	
	/**
	 * @param centroContrato the centroContrato to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	
	/**
	 * @return the numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	
	/**
	 * @param numeroContrato the numeroContrato to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	
	/**
	 * @return the codigoProducto
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	
	/**
	 * @param codigoProducto the codigoProducto to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	
	/**
	 * @return the codigoSubProducto
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	
	/**
	 * @param codigoSubProducto the codigoSubProducto to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	
	/**
	 * @return the codigoEmpresaContratoInversion
	 */
	public String getCodigoEmpresaContratoInversion() {
		return codigoEmpresaContratoInversion;
	}
	
	/**
	 * @param codigoEmpresaContratoInversion the codigoEmpresaContratoInversion to set
	 */
	public void setCodigoEmpresaContratoInversion(
			String codigoEmpresaContratoInversion) {
		this.codigoEmpresaContratoInversion = codigoEmpresaContratoInversion;
	}
	
	/**
	 * @return the aliasContrato
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	
	/**
	 * @param aliasContrato the aliasContrato to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}
	
}