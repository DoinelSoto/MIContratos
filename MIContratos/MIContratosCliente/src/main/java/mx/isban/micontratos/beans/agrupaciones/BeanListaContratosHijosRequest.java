package mx.isban.micontratos.beans.agrupaciones;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;
import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transacción MI34 : Lista Contratos Hijos
 *
 */
public class BeanListaContratosHijosRequest  implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -4680302408974667118L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	

	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;

	
	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias	
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato	
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato	
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del Contrato de Inversión	
	 */
	@Size(max=4)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Tipo de agrupación	
	 */
	@Size(max=3)
	private String tipoAgrupacion;


	/**
	 * Constructor para la multi-canalidad
	 */
	public BeanListaContratosHijosRequest(){
		
		multicanalidad=new MulticanalidadGenerica();	
		rellamada = new BeanPaginacion();
		

	}
	
	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipoConsulta the tipConsu to set
	 */
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idStiPro to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the idEmprCo
	 */


	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @return the empresaContratoInversion
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}

	/**
	 * @param empresaContratoInversion the empresaContratoInversion to set
	 */
	public void setEmpresaContratoInversion(String empresaContratoInversion) {
		this.empresaContratoInversion = empresaContratoInversion;
	}

	/**
	 * @param aliasContrato the desAlias to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}
	/**
	 * @return the tipAgrup
	 */
	public String getTipoAgrupacion() {
		return tipoAgrupacion;
	}
	/**
	 * @param tipoAgrupacion the tipAgrup to set
	 */
	public void setTipoAgrupacion(String tipoAgrupacion) {
		this.tipoAgrupacion = tipoAgrupacion;
	}

	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
}
