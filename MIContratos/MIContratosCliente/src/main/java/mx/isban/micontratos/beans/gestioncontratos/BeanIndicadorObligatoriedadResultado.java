package mx.isban.micontratos.beans.gestioncontratos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.gestordocumental.BeanIndicadorObligatoriedadResponse;

/**
 * 
 * @author everis
 * Bean con los datos de salida e implementación BeanResultBO para la transacción MI38 : Indicador obligatoriedad
 *
 */
public class BeanIndicadorObligatoriedadResultado  extends ResponseGenerico implements Serializable{
	
	/**
	 * Serialización Dto
	 */
	private static final long serialVersionUID = -2064733026113813634L;
	
	
	/**
	 * Lista para almacenar las consultas
	 */
	private List<BeanIndicadorObligatoriedadResponse> listaIndicadorObligatoriedad;

	

	/**
	 * @return the listaIndicadorObligatoriedad list
	 */
	public List<BeanIndicadorObligatoriedadResponse> getListaIndicadorObligatoriedad() {
		return new ArrayList<>(listaIndicadorObligatoriedad);
	}

	/**
	 * @param the listaIndicadorObligatoriedad 
	 */
	public void setListaIndicadorObligatoriedad(List<BeanIndicadorObligatoriedadResponse> listaIndicadorObligatoriedad) {
		this.listaIndicadorObligatoriedad = new ArrayList<>(listaIndicadorObligatoriedad);
	}
	
}
