package mx.isban.micontratos.beans.relacionclientecontrato;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada de la transaccion MI30 : Cambio titular del contrato
 *
 */
public class BeanLocalizadorContratosRequest implements Serializable{
	
	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 2207609675268331213L;
	
	/**
	 * Bean con los campos de multicanalidad
	 */
	@Valid
	private MulticanalidadGenerica multicanalidad;
	
	/**
	 * Entidad persona
	 */
	@Size(max=4)
	private String entidadPersona;
	
	/**
	 * Número de persona
	 */
	@Size(max=8)
	private String numeroPersona;
	
	/**
	 * Calidad de participación
	 */
	@Size(max=2)
	private String calidadPaticipacion;
	
	/**
	 * Código ejecutivo
	 */
	@Size(max=8)
	private String codigoEjecutivo;

	/**
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}

	/**
	 * @param multicanalidad the multicanalidad to set
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}

	/**
	 * @return the entidadPersona
	 */
	public String getEntidadPersona() {
		return entidadPersona;
	}

	/**
	 * @param entidadPersona the entidadPersona to set
	 */
	public void setEntidadPersona(String entidadPersona) {
		this.entidadPersona = entidadPersona;
	}

	/**
	 * @return the numeroPersona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}

	/**
	 * @param numeroPersona the numeroPersona to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}

	/**
	 * @return the calidadPaticipacion
	 */
	public String getCalidadPaticipacion() {
		return calidadPaticipacion;
	}

	/**
	 * @param calidadPaticipacion the calidadPaticipacion to set
	 */
	public void setCalidadPaticipacion(String calidadPaticipacion) {
		this.calidadPaticipacion = calidadPaticipacion;
	}

	/**
	 * @return the codigoEjecutivo
	 */
	public String getCodigoEjecutivo() {
		return codigoEjecutivo;
	}

	/**
	 * @param codigoEjecutivo the codigoEjecutivo to set
	 */
	public void setCodigoEjecutivo(String codigoEjecutivo) {
		this.codigoEjecutivo = codigoEjecutivo;
	}

}