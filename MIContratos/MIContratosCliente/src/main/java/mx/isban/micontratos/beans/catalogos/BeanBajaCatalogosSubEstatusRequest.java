package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada para la transaccion MI29 : Baja de SubEstatus de la tabla de catálogos
 *
 */
public class BeanBajaCatalogosSubEstatusRequest  implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -7898793320816915187L;
	
	/**
	 * Código de estatus	
	 */
	@Size(max=2)
	private String codigoEstatus;
	/**
	 * Código de subestatus	
	 */
	@Size(max=2)
	private String codigoSubEstatus;
	/**
	 * @return the codEstat
	 */
	public String getCodigoEstatus() {
		return codigoEstatus;
	}
	/**
	 * @param codigoEstatus the codEstat to set
	 */
	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}
	/**
	 * @return the codSubes
	 */
	public String getCodigoSubEstatus() {
		return codigoSubEstatus;
	}
	/**
	 * @param codigoSubEstatus the codSubes to set
	 */
	public void setCodigoSubEstatus(String codigoSubEstatus) {
		this.codigoSubEstatus = codigoSubEstatus;
	}
	
	

	
}
