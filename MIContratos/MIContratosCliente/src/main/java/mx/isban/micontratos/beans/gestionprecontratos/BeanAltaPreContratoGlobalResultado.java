package mx.isban.micontratos.beans.gestionprecontratos;

import java.io.Serializable;

import mx.isban.micontratos.beans.generales.ResponseGenerico;

/**
 * 
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los parametros de salida e implementación de la interfaz BeanResultBO para la transacción MI11
 *
 */
public class BeanAltaPreContratoGlobalResultado extends ResponseGenerico implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = 556420923714418604L;
	
	
	/**
	 * Número de folio
	 */
	private int numeroFolio;

	/**
	 * @return the numeroFolio
	 */
	public int getNumeroFolio() {
		return numeroFolio;
	}

	/**
	 * @param numeroFolio the numeroFolio to set
	 */
	public void setNumeroFolio(int numeroFolio) {
		this.numeroFolio = numeroFolio;
	}

}
