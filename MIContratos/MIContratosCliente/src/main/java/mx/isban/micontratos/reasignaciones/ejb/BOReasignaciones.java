package mx.isban.micontratos.reasignaciones.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.micontratos.beans.generales.ResponseGenerico;
import mx.isban.micontratos.beans.reasignaciones.BeanAltaSubEstatusRequest;
/**
 * @author everis
 *
 */

public interface BOReasignaciones {
	    
	/**
	 *  Metódo que hará el llamado a el consumo de la transacción MI22 : alta de subestatus
	 * @param beanAltaSubEstatusResultado
	 * @param session
	 * @return ResponseGenerico
	 */
	ResponseGenerico traspasoEjecutivo(BeanAltaSubEstatusRequest beanAltaSubEstatusResultado,ArchitechSessionBean session) throws BusinessException;

	
}
