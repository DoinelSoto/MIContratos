package mx.isban.micontratos.beans.catalogos;

import java.io.Serializable;

import javax.validation.constraints.Size;

import mx.isban.micontratos.beans.generales.BeanPaginacion;

/**
 * @author Daniel Hernández Soto
 *
 */

/** Bean con los datos de entradas para las transacciones :
 * 
 *  
 *  MI04	Catálogo de Estatus Operativos

 * */
public class BeanCatalogoEstatusOperativoRequest implements Serializable{

	/**
	 * Serialización
	 */
	private static final long serialVersionUID = -224472983201602304L;

	/**
	 * Código de estatus
	 */
	@Size(max=2)
	private String codigoEstatus;

	/**
	 * Instancia a bean de rellamada
	 */
	private BeanPaginacion rellamada;
	
	/**
	 * Constructor para la instancia
	 */
	public BeanCatalogoEstatusOperativoRequest(){
		

		rellamada = new BeanPaginacion();

	}

	/**
	 * @return the codEstat
	 */
	
	public String getCodigoEstatus() {
		return codigoEstatus;
	}
	/**
	 * @param codigoEstatus the codEstat to set
	 */
	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}	

 	public BeanPaginacion getRellamada() {
		return rellamada;
	}
	public void setRellamada(BeanPaginacion rellamada) {
		this.rellamada = rellamada;
	}
	
}
