package mx.isban.micontratos.beans.relacionclientecontrato;

import java.io.Serializable;
import javax.validation.constraints.Size;
import javax.validation.Valid;

import mx.isban.micontratos.beans.generales.MulticanalidadGenerica;

/**
 * @author Daniel Hernández Soto
 *
 */

/**
 * 
 * Bean con los datos de entrada de la transaccion MI30 : Cambio titular del contrato
 *
 */
public class BeanCambioTitularRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2207609675268331213L;
	
	@Valid
	private  MulticanalidadGenerica multicanalidad;
	
	
	/**
	 * Tipo consulta, “01” por contrato, “02” por código empresa contrato de Inversión+ alias	
	 */
	@Size(max=2)
	private String tipoConsulta;
	/**
	 * Empresa del Contrato
	 */
	@Size(max=4)
	private String empresaContrato;
	/**
	 * Centro del Contrato	
	 */
	@Size(max=4)
	private String centroContrato;
	/**
	 * Número de contrato	
	 */
	@Size(max=12)
	private String numeroContrato;
	/**
	 * Código de producto del contrato
	 */
	@Size(max=2)
	private String codigoProducto;
	/**
	 * Código de subproducto del contrato	
	 */
	@Size(max=4)
	private String codigoSubProducto;
	/**
	 * Código de empresa del Contrato de Inversión	
	 */
	@Size(max=4)
	private String empresaContratoInversion;
	/**
	 * Alias del contrato
	 */
	@Size(max=15)
	private String aliasContrato;
	/**
	 * Número de persona nuevo titular
	 */
	@Size(max=8)
	private String numeroPersona;
	/**
	 * Secuencia de domicilio principal	
	 */
	@Size(max=3)
	private String secuenciaDomicilioPrincipal;
	/**
	 * Secuencia de domicilio envío	
	 */
	@Size(max=3)
	private String secuenciaDomicilioEnvio;
	/**
	 * Secuencia de domicilio fiscal	
	 */
	@Size(max=3)
	private String secuenciaDomicilioFiscal;
	/**
	 * Constructor para la rellamada generica
	 */
	public BeanCambioTitularRequest(){
		
		multicanalidad=new MulticanalidadGenerica();	
		
	}
	
	/**
	 * @return the tipConsu
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipoConsulta the tipConsu to set
	 */
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	/**
	 * @return the idEmpr
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}
	/**
	 * @param empresaContrato the idEmpr to set
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}
	/**
	 * @return the idCent
	 */
	public String getCentroContrato() {
		return centroContrato;
	}
	/**
	 * @param centroContrato the idCent to set
	 */
	public void setCentroContrato(String centroContrato) {
		this.centroContrato = centroContrato;
	}
	/**
	 * @return the idContr
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato the idContr to set
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return the idProd
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the idProd to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the idStiPro
	 */
	public String getCodigoSubProducto() {
		return codigoSubProducto;
	}
	/**
	 * @param codigoSubProducto the idStiPro to set
	 */
	public void setCodigoSubProducto(String codigoSubProducto) {
		this.codigoSubProducto = codigoSubProducto;
	}
	/**
	 * @return the idEmprCo
	 */
	public String getEmpresaContratoInversion() {
		return empresaContratoInversion;
	}
	/**
	 * @param empresaContratoInversion the idEmprCo to set
	 */
	public void setEmpresaContratoInversion(String empresaContratoInversion) {
		this.empresaContratoInversion = empresaContratoInversion;
	}
	/**
	 * @return the desAlias
	 */
	public String getAliasContrato() {
		return aliasContrato;
	}
	/**
	 * @param aliasContrato the desAlias to set
	 */
	public void setAliasContrato(String aliasContrato) {
		this.aliasContrato = aliasContrato;
	}
	/**
	 * @return the peNumPer
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	/**
	 * @param numeroPersona the peNumPer to set
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	/**
	 * @return the peSecDo1
	 */
	public String getSecuenciaDomicilioPrincipal() {
		return secuenciaDomicilioPrincipal;
	}
	/**
	 * @param secuenciaDomicilioPrincipal the peSecDo1 to set
	 */
	public void setSecuenciaDomicilioPrincipal(String secuenciaDomicilioPrincipal) {
		this.secuenciaDomicilioPrincipal = secuenciaDomicilioPrincipal;
	}
	/**
	 * @return the peSecDo2
	 */
	public String getSecuenciaDomicilioEnvio() {
		return secuenciaDomicilioEnvio;
	}
	/**
	 * @param secuenciaDomicilioEnvio the peSecDo2 to set
	 */
	public void setSecuenciaDomicilioEnvio(String secuenciaDomicilioEnvio) {
		this.secuenciaDomicilioEnvio = secuenciaDomicilioEnvio;
	}
	/**
	 * @return the peSecDo3
	 */
	public String getSecuenciaDomicilioFiscal() {
		return secuenciaDomicilioFiscal;
	}
	/**
	 * @param secuenciaDomicilioFiscal the peSecDo3 to set
	 */
	public void setSecuenciaDomicilioFiscal(String secuenciaDomicilioFiscal) {
		this.secuenciaDomicilioFiscal = secuenciaDomicilioFiscal;
	}
	
	/**
	 * 
	 * @return the multicanalidad
	 */
	public MulticanalidadGenerica getMulticanalidad() {
		return multicanalidad;
	}
	/**
	 * 
	 * @return the multicanalidad
	 */
	public void setMulticanalidad(MulticanalidadGenerica multicanalidad) {
		this.multicanalidad = multicanalidad;
	}
	
}
